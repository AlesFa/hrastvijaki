// Line.cpp : implementation file
//
#include "stdafx.h"
#include "CLine.h"




// CLine

//IMPLEMENT_DYNAMIC(CLine, CPointFloat)

CLine::CLine()
{
	//Clear();
}

CLine::CLine(float x1, float y1, float x2, float y2)
{
	SetLine(x1, y1, x2, y2);
}

CLine::CLine(CPointFloat point1, CPointFloat point2)
{
	CLine(point1.x, point1.y, point2.x, point2.y);

}
CLine::CLine(CPointFloat point, double alfaRadians)
{
	//	izracun premice, ki gre skozi tocko t in je pod kotom alfa
	SetLine(point, alfaRadians);

}

CLine::CLine(CPointFloat point, int alfaDegrees)
{
	//	izracun premice, ki gre skozi tocko t in je pod kotom alfa
	SetLine(point, alfaDegrees);

}

CLine::CLine(std::vector<CPointFloat>& points)
{
	SetLine(points);
}

CLine::CLine(float k, float n)
{
	SetLine(k, n);
}

CLine::CLine(const CLine& line)
{
	k = line.k;
	n = line.n;
	type = line.type;
	count = line.count;
	p1 = line.p1;
	p2 = line.p2;
	data = line.data;

} // Copy constructor


CLine::~CLine()
{
}
//BEGIN_MESSAGE_MAP(CLine, CPointFloat)
//END_MESSAGE_MAP()


CLine CLine::operator=(CLine line)
{
	k = line.k;
	n = line.n;
	type = line.type;
	count = line.count;
	p1 = line.p1;
	p2 = line.p2;
	data = line.data;

	return *this;
}
bool CLine::operator ==(CLine line)
{
	if (k == line.k && n == line.n)
		return true;

	return false;
}

bool CLine::operator !=(CLine line)
{
	if (k != line.k || n != line.n)
		return true;

	return false;
}

void CLine::SetLine()
{
	//if points p1 and p2 are already set
	SetLine(p1.x, p1.y, p2.x, p2.y);
}

void CLine::SetLine(CPointFloat point1, CPointFloat point2)
{
	SetLine(point1.x, point1.y, point2.x, point2.y);
}

void CLine::SetLine(float x1, float y1, float x2, float y2)
{
	double koex, koey;

	if ((x1 >= -HUGE) && (y1 >= -HUGE) && (x1 < HUGE) && (y1 < HUGE)
		&& (x2 >= -HUGE) && (y2 >= -HUGE) && (x2 < HUGE) && (y2 < HUGE))
	{
		koex = (x1 - x2);
		koey = (y1 - y2);

		p1.SetPointFloat(x1, y1);
		p2.SetPointFloat(x2, y2);

		if (abs(x1 - x2) < MIN_VAL)
		{
			k = HUGE_VAL;
		}
		else
		{
			k = (koey / koex);
		}
		n = -k * x1 + y1;
	}
	else
	{
		k = 0.0;
		n = 0.0;
		p1.SetPointFloat(0, 0);
		p2.SetPointFloat(0, 0);
	}

}
void CLine::SetLine(float k, float n)
{
	this->k = k;
	this->n = n;

	if (abs(k) > HUGE_VAL) //navpicna
	{
		p1.SetPointFloat((0 - n) / k, 0);
		p2.SetPointFloat((500 - n) / k, 500);
	}
	else if (abs(k) < MIN_VAL)  //vodoravna
	{
		p1.SetPointFloat(0, n);
		p2.SetPointFloat(500, n);
	}
	else
	{
		p1.SetPointFloat(0, n);
		p2.SetPointFloat(500, k * 500 + n);
	}
}


void CLine::SetLine(CPointFloat point, double alfaRadians)
{
	//	izracun premice, ki gre skozi tocko t in je pod kotom alfa
	double huge = HUGE_VAL;
	if ((point.x >= 0) && (point.y >= 0) && (point.x < HUGE) && (point.y < HUGE))
	{
		k = tan(alfaRadians);
		if (abs(k) >= HUGE_VAL)
		{
			if (k > 0)
			{
				k = HUGE_VAL;
			}
			else
			{
				k = -HUGE_VAL;
			}
		}
		else if (abs(k) < MIN_VAL)
			k = 0.0;

		p1.SetPointFloat(point.x, point.y);
		p2.SetPointFloat(point.x, point.y);


		n = -k * point.x + point.y;
	}
	else
	{
		k = 0.0;
		n = 0.0;
		p1.SetPointFloat(0, 0);
		p2.SetPointFloat(0, 0);
	}

	//return *this;
}

void CLine::SetLine(CPointFloat point, int alfaDegrees)
{
	double alfa;

	alfa = DegreesToRadians(alfaDegrees);

	SetLine(point, alfa);
}
//sestavi premico iz vektorja tock
void CLine::SetLine(std::vector<CPointFloat>& points)
{
	int i;
	double sumX, sumY, sumSquareX, sumSquareY, sumProduct;
	double xMean, yMean;
	double refK, refN;

	sumX = 0;
	sumY = 0;
	sumSquareX = 0;
	sumSquareY = 0;
	sumProduct = 0;

	for (i = 0; i < points.size(); i++)
	{
		sumX += points[i].x;
		sumSquareX += (points[i].x * points[i].x);

		sumY += points[i].y;
		sumSquareY += (points[i].y * points[i].y);
		sumProduct += (points[i].y * points[i].x);

	}

	if (points.size() > 2)
	{
		xMean = sumX / points.size();
		yMean = sumY / points.size();

		if (abs(sumSquareX - sumX * xMean) < points.size())
		{
			k = HUGE_VAL;
		}
		else
		{
			k = (sumProduct - sumX * yMean) / (sumSquareX - sumX * xMean);
		}
		p1 = points[0];
		p2 = points.back();
		n = yMean - k * xMean;

	}
}

void CLine::SetLine(float * x, float * y, int nPoints)
{
	int i;
	float sumX, sumY, sumSquareX, sumSquareY, sumProduct;
	float xMean, yMean;
	float refK, refN;

	sumX = 0;
	sumY = 0;
	sumSquareX = 0;
	sumSquareY = 0;
	sumProduct = 0;

	for (i = 0; i < nPoints; i++)
	{
		sumX += x[i];
		sumSquareX += (x[i] * x[i]);

		sumY += y[i];
		sumSquareY += y[i] * y[i];
		sumProduct += x[i] * y[i];

	}

	if (nPoints > 2)
	{
		xMean = sumX / nPoints;
		yMean = sumY / nPoints;

		if (abs(sumSquareX - sumX * xMean) < nPoints)
		{
			k = HUGE_VAL;
		}
		else
		{
			k = (sumProduct - sumX * yMean) / (sumSquareX - sumX * xMean);
		}

		p1 = CPointFloat(x[0], y[0]);

		p2 = CPointFloat(x[nPoints - 1], y[nPoints - 1]);
		n = yMean - k * xMean;

	}
}



void CLine::SetAbscissa(double y)
{
	k = 0.0;
	n = y;

	p1.SetPointFloat(10.0f, (float)(y));
	p2.SetPointFloat(500.0f, (float)(y));
}

void CLine::SetOrdinate(double x)
{
	k = HUGE_VAL;
	n = -k * x;

	p1.SetPointFloat((float)x, 0.0f);
	p2.SetPointFloat((float)x, 500.0f);

}

CLine CLine::GetAbscissa(double y)
{
	CLine line;

	line.k = 0.0;
	line.n = y;

	line.p1.SetPointFloat(0.0f, (float)(y));
	line.p2.SetPointFloat(500.0f, (float)(y));

	return line;
}

CLine CLine::GetOrdinate(double x)
{
	CLine line;

	line.k = HUGE_VAL;
	line.n = -line.k*x;

	line.p1.SetPointFloat((float)x, 0.0f);
	line.p2.SetPointFloat((float)x, 500.0f);

	return line;
}

CPointFloat CLine::GetIntersectionPoint(CLine line)
{
	//	funkcija izracuna presecisce med dvema premicama p1 in p2
	//	vrne tocko presecisca

	CPointFloat p;

	if ((k < HUGE_VAL) && (line.k < HUGE_VAL))
	{
		if (k != line.k)
		{
			p.x = (float)((line.n - n) / (k - line.k));
			p.y = (float)(k * p.x + n);
		}
		else //premici sta vzporedni
		{
			p.x = 0.0;
			p.y = 0.0;
		}
	}
	else
	{
		if (k == HUGE_VAL) //ce je p1 navpicna
		{
			p.x = p1.x;
			p.y = (float)(line.k * p.x + line.n);
		}
		else //ce je p2 navpicna
		{
			p.x = line.p1.x;
			p.y = (float)(k * p.x + n);
		}
	}

	return p;
}

CLine CLine::GetIntersectionLine(CPointFloat point, double alfaRadians)
{
	//vrne premico, ki seka to premico v tocki point pod kotom angle v stopinjah
	CLine lineTmp;

	lineTmp.k = -(tan(alfaRadians) - k) / (1 + k * tan(alfaRadians));


	return lineTmp.GetParallel(point);
}

CLine CLine::GetIntersectionLine(CPointFloat point, int alfaDegrees)
{
	double alfa;
	alfa = DegreesToRadians(alfaDegrees);
	return GetIntersectionLine(point, alfa);
}

double CLine::GetDistance(CPointFloat point)  //namesto RazdTT
{
	CLine line;
	CPointFloat tempPoint;
	//razdalja od tocke na premici do point
	if (abs(k) < HUGE_VAL)
	{
		line = GetPerpendicular(point);
		tempPoint = GetIntersectionPoint(line);
	}
	else // ce je navpicna premica
	{
		tempPoint.x = p1.x;
		tempPoint.y = point.y;
	}

	return tempPoint.GetDistance(point);
}

double CLine::GetDistance(CLine line, CPointFloat point)
{

	//	izracuna razdaljo med premicama v tocki t1
	//	point lezi na prvi premici
	//	pravokotnica na premico
	//	vrne razdaljo med t1 in preseciscem pravokotnice s premico p2
	//razdalja od tocke na premici do point
	CPointFloat tempPoint;
	CLine linePerpendicular;

	linePerpendicular = GetPerpendicular(point);
	tempPoint = line.GetIntersectionPoint(linePerpendicular);

	return tempPoint.GetDistance(point);
}

double CLine::GetLineAngleInRadians(CLine line)
{
	// izracuna kot med premicama
	// line je vedno baza
	// this premica je vedno premica 2

	double alfa;

	if (line.k == k)
	{
		alfa = 0.0;
	}
	else
	{
		if (k != 0) //da ni deljenje z 0 v naslednjem pogoju
		{
			if (line.k == -1 / k)	// pravokotnost
			{
				if (k > line.k)
					alfa = (M_PI / 2);		// v radianih
				else
					alfa = -(M_PI / 2);
			}
			if (line.k == HUGE_VAL) //2. premica je navpicna
			{
				alfa = (M_PI / 2.0) + atan(k);

			}
			if (k == HUGE_VAL) //1. premica je navpicna
			{
				alfa = (M_PI / 2.0) + atan(line.k);

			}
			else
				alfa = atanf((k - line.k) / (1.0 + line.k * k));
				//alfa=atan(abs((line.k-k)/(1.0 + k * line.k ))); //vedno racunamo ostri kot, zato uprabljamo absolutno vrednost izraza

		}
		else
		{
			if (line.k >= HUGE_VAL)
				alfa = -(M_PI / 2.0);
			else if (line.k <= -HUGE_VAL)
				alfa = (M_PI / 2.0);
			else
				alfa = atanf((k - line.k) / (1.0 + line.k * k));
			//alfa=atan(abs((line.k - k)/(1.0 + k * line.k ))); //vedno racunamo ostri kot, zato uprabljamo absolutno vrednost izraza
		}
	}

	return alfa;
}
//kot med abciso in premico
double CLine::GetLineAngleInRadians(void)
{
	//	Izracun kota med premico p1 in abciso

	CLine line;

	line.SetAbscissa(0);

	return GetLineAngleInRadians(line);

}

double CLine::GetLineAngleInDegrees(CLine line)
{
	double angle;

	angle = GetLineAngleInRadians(line);

	return RadiansToDegree(angle);

}

double CLine::GetLineAngleInDegrees(void)
{
	double angle;

	angle = GetLineAngleInRadians();

	return RadiansToDegree(angle);

}



CPointFloat CLine::GetPointAtDistance(CPointFloat point, double alfaRadians, double distance)
{
	//Izracuna tocko, ki lezi na premici, ki je pod kotom alfa na abciso
	//in je oddaljena od tocke point za razdaljo

	CPointFloat p2;

	if (abs(alfaRadians) == M_PI / 2) //Navpicna
	{
		p2.x = (float)(point.x);
		p2.y = (float)(point.y + distance);
	}
	else if ((alfaRadians == 0) || (alfaRadians == M_PI)) //0 ali 180 stopinj - vodoravna
	{
		p2.x = (float)(distance + point.x);
		p2.y = (float)(point.y);
	}
	else
	{
		p2.x = (float)(point.x + (cos(alfaRadians)) * distance);
		p2.y = (float)(point.y + (sin(alfaRadians)) * distance);
	}

	if ((p2.x < 0) || (p2.y < 0))
	{
		p2.x = 0;
		p2.y = 0;
	}

	return p2;
}

CPointFloat CLine::GetPointAtDistance(CPointFloat point, int alfaDegrees, double distance)
{
	double alfa;

	alfa = DegreesToRadians(alfaDegrees);

	return GetPointAtDistance(point, alfa, distance);
}

CPointFloat CLine::GetPointAtDistance(CLine line, double distance)
{

	//	Izracuna tocko, ki lezi na premici line, 
	//	in je oddaljena od tocke presecisca med premicama za razdaljo.

	//	line - premica na kateri lezi tocka
	//	distance - razdalja od presecisca med premicama


	CPointFloat point;
	double alfa;

	alfa = line.GetLineAngleInRadians();
	//alfa = GetLineAngleInRadians(line);
	point = GetIntersectionPoint(line);

	return line.GetPointAtDistance(point, alfa, distance);
}

CPointFloat CLine::GetPointAtDistance(CPointFloat point, double distance)
{

	//point ... znana tocka, ki lezi na premici
	//distance .. zeljena razralja od tocke point do nove
	double alfa;
	alfa = GetLineAngleInRadians();

	return GetPointAtDistance(point, alfa, distance);

}

CPointFloat CLine::GetPointAtDistanceP1(double distance)
{
	//point ... znana tocka, ki lezi na premici
	//distance .. zeljena razralja od tocke point do nove
	double alfa;

	alfa = GetLineAngleInRadians();

	return GetPointAtDistance(p1, alfa, distance);
}

CPointFloat CLine::GetPointAtDistanceP2(double distance)
{
	//point ... znana tocka, ki lezi na premici
	//distance .. zeljena razralja od tocke point do nove
	double alfa;

	alfa = GetLineAngleInRadians();

	return GetPointAtDistance(p2, alfa, distance);
}

CPointFloat CLine::GetMiddlePoint()
{
	double alfa;


	CPointFloat t;

	t.x = (p1.x + p2.x) / 2;
	t.y = (p1.y + p2.y) / 2;
	//alfa = GetLineAngleInRadians();
	//float distance = p1.GetDistance(p2)/2;

	//if(distance > 0)
	return t;
	//else
	//	 return GetPointAtDistance(p2, alfa, distance);

	
}



CLine CLine::GetLine()
{
	CLine line;
	line.SetLine();

	return line;

}
CLine CLine::GetLine(CPointFloat point1, CPointFloat point2)
{
	CLine line;
	line.SetLine(point1, point2);

	return line;

}
CLine CLine::GetLine(float x1, float y1, float x2, float y2)
{
	CLine line;
	line.SetLine(x1, y1, x2, y2);

	return line;

}
CLine CLine::GetLine(float k, float n) {
	CLine line;
	line.SetLine(k, n);

	return line;

}
CLine CLine::GetLine(CPointFloat point, double alfaRadians)
{
	CLine line;
	line.SetLine(point, alfaRadians);

	return line;

}
CLine CLine::GetLine(CPointFloat point, int alfaDegrees)
{
	CLine line;
	line.SetLine(point, alfaDegrees);

	return line;

}
/*CLine CLine::GetLine(CMemoryBuffer memoryBuffer, std::vector<CRectRotated> polygons)
{
	CLine line;
	line.SetLine(memoryBuffer, polygons);

	return line;

}*/

//pravokotnica
CLine CLine::GetPerpendicular(CPointFloat p)
{
	CLine line;

	if ((p.x >= -HUGE_VAL) && (p.y >= -HUGE_VAL) && (p.x < HUGE_VAL) && (p.y < HUGE_VAL))
	{
		if (abs(k) < HUGE_VAL) //ni navpicna
		{
			if (abs(k) > MIN_VAL)
			{
				line.k = -1 / k;

				if (abs(line.k) < MIN_VAL)
					line.k = 0.0;
			}
			else
			{
				line.k = HUGE_VAL;
			}

			line.n = -line.k * p.x + p.y;
		}
		else
		{
			line.k = 0.0;
			//line.n = 0.0;
			line.n = p.y;
		}

		line.p1 = p;
		line.p2 = GetIntersectionPoint(line);  //Opomba Martin: tu se pojavi problem, ce je ze p na preseciscu premic
		//line.p2 = p;  //Opomba Martin: tu se pojavi problem, ce je ze p na preseciscu premic


	}
	else
	{
		line.k = 0.0;
		line.n = 0.0;

		line.p1.SetPointFloat(0, 0);
		line.p2.SetPointFloat(0, 0);

	}

	return line;
}



CLine CLine::GetParallel(CPointFloat p)
{
	CLine line;
	CPointFloat p2;

	if ((p.x >= 0) && (p.y >= 0) && (p.x < HUGE_VAL) && (p.y < HUGE_VAL))
	{

		line.k = k;
		line.n = p.y - line.k * p.x;
		line.p1 = p;

		p2.x = p.x + 100;
		p2.y = (float)(line.k * p2.x + line.n); //Tu je bila prej n (nedefinirano), mora pa biti line.n!
		line.p2 = p2;

	}
	else
	{
		line.k = 0.0;
		line.n = 0.0;
		line.p1 = p;
		line.p2 = p;
	}

	return line;
}

CLine CLine::GetParallel(double distance)
{
	CLine perpendicular;
	CPointFloat point;

	perpendicular = GetPerpendicular(p1);
	point = GetPointAtDistance(perpendicular, distance);

	return GetParallel(point);
}

void CLine::Clear()
{
	count = 0;
	type = 0;
	k = 0;
	n = 0;
	p1.SetPointFloat(0, 0);
	p2.SetPointFloat(0, 0);

	data = "";
}

QGraphicsLineItem * CLine::DrawLine(QRect area, QPen pen)
{
	QPainterPath painter;
	float ka = k;
	float na = n;
	float koryy, linex, liney;
	CPointFloat p1Draw, p2Draw;
	
	QGraphicsLineItem* line = new QGraphicsLineItem;
	QLine line2;

	if (abs(k) < HUGE_VAL)  
	{
		if (abs(k) < 1)
		{

			//pri vodoravni premici ni potrebno upostevati xoffseta, samo y
			koryy = (area.left() * k + n);
			p1Draw.x = (area.left());
			p1Draw.y = (koryy);


			koryy = (area.right() * k + n);
			p2Draw.x = (area.right());
			p2Draw.y = (koryy);

			line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);

		}
		else
		{
			//pri navpicni premici ni potrebno upostevati y offseta, samo x

			koryy = ((area.top() - n) / k);
			p1Draw.x = (koryy);
			p1Draw.y = (area.top());


			koryy = ((area.bottom()  - n) /k);
			p2Draw.y = (area.bottom());
			p2Draw.x = (koryy); /* zoomFactor *///);
			line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);

		}
	}
	else
	{
		//pri navpicni premici ni potrebno upostevati y offseta, samo x
			p1Draw.x = (int)((p1.x));
			p1Draw.y = (int)(area.top());


			p2Draw.x = (int)((p1.x));
			p2Draw.y = (int)(area.bottom());
	
			line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);
	}






	line->setLine(line2);
	line->setPen(pen);
	line->setZValue(2);

	return line;
}

QGraphicsLineItem * CLine::DrawSegment(QPen pen,int width)
{


	QGraphicsLineItem* line = new QGraphicsLineItem;
	
	
	pen.setWidth(width);

	line->setPen(pen);
	line->setLine(p1.x, p1.y, p2.x, p2.y);


	return line;
}
QGraphicsLineItem * CLine::DrawArrow(QPen pen, int direction,int arrowSize, int  arrowdegrees)
{
	QGraphicsLineItem* line = new QGraphicsLineItem;
	line->setPen(pen);
	line->setLine(p1.x, p1.y, p2.x, p2.y);
	int arrow_degrees_ = arrowdegrees;

	double angleForward = atan2(p2.y - p1.y, p2.x - p1.x) * 180 / M_PI;
	double angleReverse = atan2(p1.y - p2.y, p1.x - p2.x) * 180 / M_PI;

	//fiDeg = atan2(dy[i], dx[i]) * 180.0 / M_PI;	//atan2 vrne kot med -Pi in Pi

	if (angleForward < 0)
		angleForward += 360;  //Spravimo kot na interval 0 do 360 stopinj


	if (angleReverse < 0)
		angleReverse += 360;  //Spravimo kot na interval 0 do 360 stopinj

	int x11, y11, x12, y12, x21, y21, x22, y22;

	QGraphicsLineItem* lineA11 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA12 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA21 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA22 = new QGraphicsLineItem();



	lineA11->setPen(pen);
	lineA12->setPen(pen);
	lineA21->setPen(pen);
	lineA22->setPen(pen);

	if (direction == 0) //na tocki p1 je premica
	{
		x11 = p1.x + arrowSize * cos((angleForward - arrow_degrees_)* M_PI / 180.0);
		y11 = p1.y + arrowSize * sin((angleForward - arrow_degrees_)* M_PI / 180.0);
		x12 = p1.x + arrowSize * cos((angleForward + arrow_degrees_)* M_PI / 180.0);
		y12 = p1.y + arrowSize * sin((angleForward + arrow_degrees_)* M_PI / 180.0);

		lineA11->setLine(p1.x, p1.y, x11, y11);
		lineA12->setLine(p1.x, p1.y, x12, y12);
		lineA11->setParentItem(line);
		lineA12->setParentItem(line);
	}
	else if (direction == 1)
	{
		x21 = p2.x + arrowSize * cos((angleReverse - arrow_degrees_)* M_PI / 180.0);
		y21 = p2.y + arrowSize * sin((angleReverse - arrow_degrees_)* M_PI / 180.0);
		x22 = p2.x + arrowSize * cos((angleReverse + arrow_degrees_)* M_PI / 180.0);
		y22 = p2.y + arrowSize * sin((angleReverse + arrow_degrees_)* M_PI / 180.0);
		lineA21->setLine(p2.x, p2.y, x21, y21);
		lineA22->setLine(p2.x, p2.y, x22, y22);
		lineA21->setParentItem(line);
		lineA22->setParentItem(line);
	}
	else
	{
		//float kot = cos((angle - arrow_degrees_)* M_PI / 180.0);
	//	float koty = sin((angle - arrow_degrees_)* M_PI / 180.0);
		x11 = p1.x + arrowSize * cos((angleForward - arrow_degrees_)* M_PI / 180.0);
		y11 = p1.y + arrowSize * sin((angleForward - arrow_degrees_)* M_PI / 180.0);
		x12 = p1.x + arrowSize * cos((angleForward + arrow_degrees_)* M_PI / 180.0);
		y12 = p1.y + arrowSize * sin((angleForward + arrow_degrees_)* M_PI / 180.0);

		x21 = p2.x + arrowSize * cos((angleReverse - arrow_degrees_)* M_PI / 180.0);
		y21 = p2.y + arrowSize * sin((angleReverse - arrow_degrees_)* M_PI / 180.0);
		x22 = p2.x + arrowSize * cos((angleReverse + arrow_degrees_)* M_PI / 180.0);
		y22 = p2.y + arrowSize * sin((angleReverse + arrow_degrees_)* M_PI / 180.0);

		lineA11->setLine(p1.x, p1.y, x11, y11);
		lineA12->setLine(p1.x, p1.y, x12, y12);
		lineA11->setParentItem(line);
		lineA12->setParentItem(line);

		lineA21->setLine(p2.x, p2.y, x21, y21);
		lineA22->setLine(p2.x, p2.y, x22, y22);
		lineA21->setParentItem(line);
		lineA22->setParentItem(line);

	}


	line->setActive(true);
	line->setPen(pen);



	line->setZValue(1);




	return line;
}
/*
CPointFloat CLine::GetEdgePointOnSegment(CMemoryBuffer memoryBuffer, int directionForward, int threshold)
{
	CRect rect;
	int i, start, end;
	CPointFloat pointOnSegment;
	CPointFloat edgePoint;

	//sestavimo kvadrat okoli tock daljice
	if (p1.x > p2.x)
	{
		rect.left = p2.x;
		rect.right = p1.x;
	}
	else
	{
		rect.left = p1.x;
		rect.right = p2.x;
	}

	if (p1.y > p2.y)
	{
		rect.top = p2.y;
		rect.bottom = p1.y;
	}
	else
	{
		rect.top = p1.y;
		rect.bottom = p2.y;
	}

	//uporabno za redefinicijo funkcije, lahko klicemo iskanja tobov, ce podamo negativen threshold
	if(threshold > 0)
	{
		memoryBuffer.FindEdgesX(rect, threshold);
		memoryBuffer.FindEdgesY(rect, threshold);
	}

	if(abs(k) < 1) //premica bolj vodoravna - hodimo po x ih
	{
		start = rect.left;
		end = rect.right;
	}
	else
	{
		start = rect.top;
		end = rect.bottom;
	}

	if(directionForward == 	1)
	{
		for(i = start; i < end; i++)
		{
			edgePoint.SetPointFloat(0,0);
			edgePoint.SetActive(false);

			if(abs(k) < 1) //premica bolj vodoravna - hodimo po x ih
			{
				pointOnSegment.SetPointFloat(i, (k * i + n));
			}
			else //premica bolj navpicna
			{
				pointOnSegment.SetPointFloat((i - n)/k, i);
			}

			//pogledamo po x robovih
			if(memoryBuffer.Xedges[(int)pointOnSegment.y][i] != 0)
			{
				edgePoint.x = memoryBuffer.Xedges[(int)pointOnSegment.y][i];
				edgePoint.SetActive(true);
			}
			else
				edgePoint.x = i;

			if(memoryBuffer.Yedges[i][(int)pointOnSegment.y] != 0)
			{
				edgePoint.y = memoryBuffer.Yedges[i][(int)pointOnSegment.y];
				edgePoint.SetActive(true);
			}
			else
				edgePoint.y = pointOnSegment.y;


			//ce je bil rob najden
			if(edgePoint.IsActive())
			{
				return edgePoint;
			}
		}
	}
	else //nazaj po premici
	{
		for(i = end; i > start; i--)
		{
			edgePoint.SetPointFloat(0,0);
			edgePoint.SetActive(false);

			if(abs(k) < 1) //premica bolj vodoravna - hodimo po x ih
			{
				pointOnSegment.SetPointFloat(i, (k * i + n));
			}
			else //premica bolj navpicna
			{
				pointOnSegment.SetPointFloat((i - n)/k, i);
			}

			//pogledamo po x robovih
			if(memoryBuffer.Xedges[(int)pointOnSegment.y][i] != 0)
			{
				edgePoint.x = memoryBuffer.Xedges[(int)pointOnSegment.y][i];
				edgePoint.SetActive(true);
			}
			else
				edgePoint.x = i;

			if(memoryBuffer.Yedges[i][(int)pointOnSegment.y] != 0)
			{
				edgePoint.y = memoryBuffer.Yedges[i][(int)pointOnSegment.y];
				edgePoint.SetActive(true);
			}
			else
				edgePoint.y = pointOnSegment.y;


			//ce je bil rob najden
			if(edgePoint.IsActive())
			{
				return edgePoint;

			}
		}
	}


	return CPointFloat(0,0);
}
//poisce robno tocko na daljici, robovi morajo biti ze predhodno najdeni
CPointFloat CLine::GetEdgePointOnSegment(CMemoryBuffer memoryBuffer, int directionForward)
{
	return GetEdgePointOnSegment(memoryBuffer, directionForward, -1);
}*/

//11.6.2014- popravek GetLineAngleInRadians(CLine line) line.k -> k pri preverjanju ce je premica navpicna