#pragma once

#include "stdafx.h"
#include <qpoint.h>


class CPointFloat 
{
public:
	float	x;
	float	y;
	float	dx;
	float	dy;

	float	basex; //bazna x tocka iz kjer se racuna tocka z odmikom in kotom	 
	float	basey; //bazna y tocka iz kjer se racuna tocka z odmikom in kotom	 
	double	alphaRadians;  //kot tocke
	double	distance;		//razdalja od bazne tocke

private:
	float	offsetX;
	float	offsetY;
	bool	active;


public:
	
	CPointFloat();
	CPointFloat(float _x, float _y)throw();
	CPointFloat(float baseX, float baseY, double distance, double alphaRadians)throw();
	CPointFloat(const CPointFloat& point);// Copy constructor


	virtual ~CPointFloat();

	//	CPointFloat(const CPointFloat& point) { x = point.x;  y = point.y;  dx = point.dx;  dy = point.dy;  active = point.active;  offsetX = point.offsetX; offsetY = point.offsetY;} // Copy constructor

protected:
	//DECLARE_MESSAGE_MAP()

public:
	void SetPointFloat(float x, float y);
	void SetPointFloat(QPoint point);
	void SetPointFloat(float baseX, float baseY, double distance, double alphaRadians);
	void SetPointFloat(float baseX, float baseY, double distance, int alphaDegrees);

	float GetDistance(const CPointFloat & p) const;
	float GetDistance(float pX, float pY);
	float GetDistance();
	CPointFloat PolarCoordinatesRadian(float theta, float dis);
	CPointFloat PolarCoordinatesRadian();
	CPointFloat PolarCoordinatesDegree(float theta, float dis);

	CPointFloat operator=(CPointFloat point);
	bool operator ==(CPointFloat point);
	bool operator !=(CPointFloat point);
	void operator +=(CPointFloat point);
	void operator -=(CPointFloat point);
	CPointFloat operator +(CPointFloat point);
	CPointFloat operator -(CPointFloat point);
	CPointFloat operator *(float k);
	CPointFloat operator /(float k);
	void operator *=(float k);
	void operator /=(float k);

	float GetPointAngleInDegrees();
	float GetPointAngleInRadians();
	CPointFloat Normalize();
	CPointFloat Normalize(float size);
	void SetActive(bool value);
	bool IsActive();
	void GetDelta(CPointFloat point);
	float GetDeltaX(CPointFloat point);
	float GetDeltaY(CPointFloat point);
//	bool IsInside(CRect r);
///	bool IsXInside(CRect r);
//	bool IsYInside(CRect r);

	//funkcije ki delajo z robovi setterji/getterji
	void RotatePoint(CPointFloat basePoint, double alphaRadians);
	void RotatePoint(CPointFloat basePoint, int alphaDegrees);
	CPointFloat GetRotatedPoint(CPointFloat basePoint, double alphaRadians);
	CPointFloat GetRotatedPoint(CPointFloat basePoint, int alphaDegrees);
	CPointFloat GetPointAtDistanceAndAlpha(double distance, double alphaRadians);
	CPointFloat GetPointAtDistanceAndAlpha(double distance, int alphaDegrees);

	void SetPointAtDistanceAndAlpha(double distance, double alphaRadians);
	void SetPointAtDistanceAndAlpha(double distance, int alphaDegrees);
	//void SetMiddlePoint(CPointFloat point);

	CPointFloat GetMiddlePoint(CPointFloat point);

	double DegreesToRadians(double alfaDegree);
	double RadiansToDegree(double alphaRadians);
	void Clear();

	QGraphicsEllipseItem* DrawDot(QPen pen, QBrush brush, int size);
};

