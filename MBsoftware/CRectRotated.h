#pragma once 
#include "stdafx.h"
#include "qrect.h"

#include "CPointFloat.h"
class CRectRotated :public QRect
{
public:
	CRectRotated()throw();
	CRectRotated(int left, int top, int right, int bottom) throw();
	CRectRotated(QRect rect)throw();
	CRectRotated(const CRectRotated& rotatedRect); // Copy constructor

	virtual ~CRectRotated();

public:
	//maksimalni kvadrat okrog polygona - kvadrata bounding box
	std::vector<CPointFloat> polygonPoints;
	int id = 0;



public:
	CRectRotated operator=(CRectRotated rotatedRect);
	bool operator ==(CRectRotated rotatedRect);
	bool operator !=(CRectRotated rotatedRect);

	//OB PRILIKI POENOSTAVITI KODO: operator- bi lahko zamenjal s klicem operator+ z negativno val, operator+= pa bi lahko izrazil z operator+

	CRectRotated operator+ (int val);
	CRectRotated operator+ (CPointFloat move);  //Dodal Martin:  izra�un pravokotnika, premaknjenega za vektor, ki ga dolo�imo s to�ko
	CRectRotated operator- (int val);
	CRectRotated operator* (int val);
	CRectRotated operator/ (int val) const;      //Opomba Martin: const mora biti tu, da lahko uporabljamo operator / tudi na konstantih objektih
	CRectRotated operator/ (float val) const;	//Dodal Martin: deljenje pravokotnika z necelim �tevilom
	CRectRotated operator+= (int val);
	CRectRotated operator+= (CPointFloat move);	//Dodal Martin:  translacija pravokotnika za vektor, ki ga dolo�imo s to�ko
	CRectRotated operator-= (int val);
	CRectRotated operator*= (int val);
	CRectRotated operator/= (int val);
	void SetRectangle(int left, int top, int right, int bottom);		//nastavi pravokotnik - enako kot CRect
	void SetRectangle(QRect rect);
	void SetRectangle();												//nastavi tocke left, top, roght, bottom - glede na tocke poligona (boundong box)
	void SetID(int id);
	int GetID();
	QRect GetCRect(void);
	int AddPoint(CPointFloat point);									//doda tocko v polygonPoints - vrne velikost polygonPoints
	int AddPoint(float x, float y);										//doda tocko v polygonPoints - vrne velikost polygonPoints
	int AddPoint(int x, int y);											//doda tocko v polygonPoints - vrne velikost polygonPoints
	void RotateRectangle(CPointFloat basePoint, double alfaRadians);	//okoli bazne tocke za alfaRAdians rotira pravokotnik - pravokotnik ni vec definiran z l,t,b,r ampak s tockami v polygonPoints, l,t,b,r postane bounding box okoli rotiranega kvadrata
	void RotatePolygon(CPointFloat basePoint, double alfaRadians);		//okoli bazne tocke za alfaRAdians rotira tocke poligona, l,t,b,r postanejo bounding box poligona
	void RemoveLastPoint();												//odstrani zadnjo  tocko poligona
	void ClearPolygon();												//pocisti vse tocke poligona
	BOOLEAN InPolygon(CPointFloat point);								//preveri ali je point v poligonu		//algoritem �teje, kolikokrat se predznak orientacije spremeni: �e se lihokrat, je to�ka v poligonu
	BOOLEAN InPolygon(float x, float y);

	QGraphicsPolygonItem* DrawPolygonLines(QPen pen, QBrush brush);


};

