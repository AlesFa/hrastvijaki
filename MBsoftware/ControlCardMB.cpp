#include "stdafx.h"
#include "ControlCardMB.h"


#include "wsc.h"


int		ControlCardMB::objectCount = 0;


ControlCardMB::ControlCardMB()
{

}

ControlCardMB::ControlCardMB(QString port, int baudRate)
{

	if (port == "COM1")
		this->port = 0;
	else if (port == "COM2")
		this->port = 1;
	else if (port == "COM3")
		this->port = 2;
	else if (port == "COM4")
		this->port = 3;
	else if (port == "COM5")
		this->port = 4;
	else if (port == "COM6")
		this->port = 5;
	else if (port == "COM7")
		this->port = 6;
	else if (port == "COM8")
		this->port = 7;
	else if (port == "COM9")
		this->port = 8;
	


	ui.setupUi(this);
	parseCounter = 0;
	isOpen = false;
	outputReg0 = 0x00;
	outputReg1 = 0x00;
	intCounter = 0;
	bytesWaiting = 0;
	for (int i = 0; i < 4; i++)
	{
		counterFreq[i] = 0;
		counterEn[i] = 0;
	}
	id = objectCount;
	objectCount++;
	ReadFile();



	CreateDisplay();
	int comPort = 0;
	

	int Version;
	code = SioKeyCode(1117990899);
	SioReset(-1, 1, 1); // Set DTR & RTS at port initialization.
	code = SioReset(this->port, 1024, 1024);
	if (code < 0)
	{
		isOpen = false;
	}
	Version = SioInfo('V');

	// set baud rate
	
	code = SioBaud(this->port, baudRate);
	// clear receive buffer
	code = SioRxClear(this->port);
	code = SioDTR(this->port, 'S');
	code = SioRTS(this->port, 'S');
	code = SioParms(this->port, 0, 1, 8);

	if (code >= 0)
	isOpen = true;


	
	connect(ui.spinBoxCH1, SIGNAL(valueChanged(int)), ui.horizontalSliderCH1, SLOT(setValue(int)));
	connect(ui.horizontalSliderCH1, SIGNAL(valueChanged(int)), ui.spinBoxCH1, SLOT(setValue(int)));

	connect(ui.spinCounter1, SIGNAL(valueChanged(int)), this, SLOT(OnChangedCounter0()));
	
	connect(ui.checkBoxCounter1, SIGNAL(pressed()), this, SLOT(OnEnableCounter(int(0))));


	connect(ui.spinCounter2, SIGNAL(clicked()), this, SLOT(OnChangedCounter1()));
	connect(ui.checkBoxCounter2, SIGNAL(clicked()), this, SLOT(OnChangedCounter1()));

	connect(ui.spinCounter3, SIGNAL(clicked()), this, SLOT(OnChangedCounter2()));
	connect(ui.checkBoxCounter3, SIGNAL(clicked()), this, SLOT(OnChangedCounter2()));

	connect(ui.spinCounter4, SIGNAL(clicked()), this, SLOT(OnChangedCounter3()));
	connect(ui.checkBoxCounter4, SIGNAL(clicked()), this, SLOT(OnChangedCounter3()));

	ui.spinCounter1->setMaximum(40000);
	ui.spinCounter2->setMaximum(40000);
	ui.spinCounter3->setMaximum(40000);
	ui.spinCounter4->setMaximum(40000);

	
	viewTimer = new QTimer(this);
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(ViewSignalsTimer()));
	//return code;
}




void ControlCardMB::closeEvent(QCloseEvent *event)
{
	viewTimer->stop();

}




void ControlCardMB::CreateDisplay()
{
	int x, y, width, height, xOffset, port;
	QVBoxLayout *layoutInput0 = new QVBoxLayout(ui.groupInput0);
	QVBoxLayout *layoutInput1 = new QVBoxLayout(ui.groupInput1);
	QVBoxLayout *layoutOutput0 = new QVBoxLayout(ui.groupOutput0);
	QVBoxLayout *layoutOutput1 = new QVBoxLayout(ui.groupOutput1);
	//QCheckBox *outputCheckBox;
	//QCheckBox *inputCheckBox;


	QToolButton *camButton;
	QIcon camIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	int i = 0;

	///output0
	for (i = 0; i < 16; i++)
	{
		if (output[i].isActive == 0)
		{
			outputCheckBox[i] = new QCheckBox("Disabled!!");
			outputCheckBox[i]->setChecked(true);
			outputCheckBox[i]->setCheckable(false);
		}
		else
		{
			outputCheckBox[i] = new QCheckBox(output[i].name);
		}

		if (i < 8)
		{
			layoutOutput0->addWidget(outputCheckBox[i]);
		}
		else
		{
			layoutOutput1->addWidget(outputCheckBox[i]);
		}
		connect(outputCheckBox[i], SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(outputCheckBox[i], i);


		if (input[i].isActive == 0)
		{
			inputCheckBox[i] = new QCheckBox("Disabled!!");
			outputCheckBox[i]->setChecked(true);
		}
		else
		{
			inputCheckBox[i] = new QCheckBox(input[i].name);
		}


		if (i < 8)
		{
			layoutInput0->addWidget(inputCheckBox[i]);
		}
		else
		{
			layoutInput1->addWidget(inputCheckBox[i]);
		}

	}
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnChangedOutputBox(int)));
}

void ControlCardMB::SetDisplay()
{
	

	//vrne pravilen prikaz counterjev
	ui.spinCounter1->setValue(counterFreq[0]); 
	if(counterEn[0] == 1)
	ui.checkBoxCounter1->setCheckState(Qt::Checked);
	else
	ui.checkBoxCounter1->setCheckState(Qt::Unchecked);

	ui.spinCounter2->setValue(counterFreq[1]);
	if (counterEn[1] == 1)
		ui.checkBoxCounter2->setCheckState(Qt::Checked);
	else
		ui.checkBoxCounter2->setCheckState(Qt::Unchecked);

	ui.spinCounter3->setValue(counterFreq[2]);
	if (counterEn[2] == 1)
		ui.checkBoxCounter3->setCheckState(Qt::Checked);
	else
		ui.checkBoxCounter3->setCheckState(Qt::Unchecked);

	ui.spinCounter4->setValue(counterFreq[2]);
	if (counterEn[3] == 1)
		ui.checkBoxCounter4->setCheckState(Qt::Checked);
	else
		ui.checkBoxCounter4->setCheckState(Qt::Unchecked);

	//zazene timer za prikaz
	viewTimer->start(100);
	
	
}

void ControlCardMB::ShowDialog()
{

	SetDisplay();
	show();
}

ControlCardMB::~ControlCardMB()
{
}

void ControlCardMB::ReadData()
{

	QByteArray tmp;
	char tmpChar[1024];
	char tmpNum[256]; 
	char portNum;
	int inNum = 0;
	int numCounter = 0;
	int test = 0;
	int j = 0;

	char frameNum[20];
	char motorStep[20];
	int split = 0;
	int startFrameNum = 0;
	int startMotorNum = 0;
	
	if (isOpen)
	{

		code = SioGets(port, getBuffer, 1024);


		for (int i = 0; i < code; i++)
		{

			parseBuffer[parseCounter] = getBuffer[i];


			if ((parseBuffer[parseCounter] == '\n'))
			{
				for ( j = 0; j < parseCounter; j++)
				{
					tmp[j] = parseBuffer[j];
					tmpChar[j] = parseBuffer[j];
					if (j > 1)
					{
						
						tmpNum[numCounter] = parseBuffer[j];
						
						numCounter++;
					}
				}
				numCounter = 0;
				

				switch (tmp[0])
				{
					case ('I'):
					{ 
						bool ok;
						inNum = atoi(tmpNum);

						//inNum = int(tmpNum);
						if (tmp[1] == '0')
							inputReg0 = inNum;
						else
							inputReg1 = inNum;

					}
					case ('O'):
					{

					}
					case ('F')://pri posiljanju korakov motorja pri dolocenmu frame
					{
						for (int i = 1; i < parseCounter; i++)
						{
							if ((parseBuffer[i] == '-'))
							{
								split = i;
							}

						}
						startFrameNum = 0;
						startMotorNum = 0;
						for (int k = 0; k < 20; k++)
						{
							motorStep[k] = 0;
							frameNum[k] = 0;
						}
						for (int i = 1; i < parseCounter; i++)
						{
							if (i < split)//stevilka frama
							{
								frameNum[startFrameNum] = tmp[i];
								startFrameNum++;
							}
							else if(i > split)
							{
								motorStep[startMotorNum] = tmp[i];
								startMotorNum++;
							}
						}
						currFrame = atoi(frameNum);
						motorCounterArray[currFrame] = atoi(motorStep);
						
						break;
					}

					


				}

			
				
				parseCounter = 0;
				j = 0;

			}
			else
			{
				parseCounter++;
			}
		}


	}
	//serialControl->write(getBuffer);

}

void ControlCardMB::testWrite(qint64 byte)
{
	//serialControl -> flush();
}

void ControlCardMB::WriteData(QByteArray data)
{
 
	QMutex mutex;
	QByteArray sendData = data;
	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");


	if(carrageReturn.indexIn(sendData, 0) == -1)
		sendData.append("\r\n");
	char param[100]; 

	for (int i = 0; i < 100; i++)
	{
		param[i] = 0;
	}


	for (int i = 0; i < sendData.size(); i++)
	{
		param[i] = sendData[i];

	}
	code = -1;
	
	if (isOpen)
	{
		
		code = SioPuts(port, (char*)param, sendData.size());

	}



}

void ControlCardMB::ChangeData()
{

}

void ControlCardMB::StopSend()
{
	QString write;
	QByteArray data;


	write = QString("SS0");
	data.insert(0, write.toLocal8Bit());

	WriteData(data);

}

void ControlCardMB::SetInputvalue()
{
		for (int i = 0; i < 8; i++)
		{
			if ((inputReg0 & input[i].address) == input[i].address)
			{
				input[i].prevValue = input[i].value;
				input[i].value = 0;
			}
			else
			{
				input[i].prevValue = input[i].value;
				input[i].value = 1;
			}
		}

		for (int i = 8; i < 16; i++)
		{
			if ((inputReg1 <<8 & input[i].address) == input[i].address)
			{
				input[i].prevValue = input[i].value;
				input[i].value = 0;
			}
			else
			{
				input[i].prevValue = input[i].value;
				input[i].value = 1;
			}
		}

}



void ControlCardMB::SetOutputValue(int pinNum, int value)
{
	QString write;
	QByteArray data;
	int k = 0;
	if (pinNum < 16)
	{
		if (output[pinNum].isActive)
		{
			output[pinNum].prevValue = output[pinNum].value;
			output[pinNum].value = value;


			if (pinNum < 8)
			{
				outputReg0 = 0;
				for (int i = 0; i < 8; i++)
				{
					outputReg0 += output[i].address*output[i].value;
				}
				write = QString("O0%1").arg(outputReg0);

				data.insert(0, write.toLocal8Bit());
				WriteData(data);
			}
			else
			{
				k = 8;
				outputReg1 = 0;
				for (int i = 8; i < 16; i++)
				{
					outputReg1 += output[i].address*output[i].value >> 8;
					//k++;
				}
				write = QString("O1%1").arg(outputReg1);

				data.insert(0, write.toLocal8Bit());
				WriteData(data);
			}
		}
	}
}

void ControlCardMB::SetOutputByte(int port, int value)
{
	QString write;
	QByteArray data;


	if ((port >= 0) && (port <= 1))
	{
		if (port == 0)
		{
			write = QString("O0%1").arg(value);
			data.insert(0, write.toLocal8Bit());
		}
		else
		{
			write = QString("O1%1").arg(value);
			data.insert(0, write.toLocal8Bit());
		}
		WriteData(data);
	}

}

void ControlCardMB::SetPortByte(int port)
{
	QString write;
	QByteArray data;
	BYTE outReg = 0;
	int pinNum = 0;
	if (port == 0)
	{
		for (pinNum = 0; pinNum < 8; pinNum++)
		{
			if (output[pinNum].isActive)
			{
				outReg += output[pinNum].address*output[pinNum].value;

			}
		}
		write = QString("O0%1").arg(outReg);
		data.insert(0, write.toLocal8Bit());
		WriteData(data);
	}
	else if(port == 1)
	{
		for (int pinNum = 8; pinNum < 16; pinNum++)
		{
			if (output[pinNum].isActive)
			{
				outReg += output[pinNum].address*output[pinNum].value >> 8;
			}
		}

		write = QString("O1%1").arg(outReg);
		data.insert(0, write.toLocal8Bit());
		WriteData(data);
	}


}

void ControlCardMB::SetTimer(int timerNum, int freq, int enable)
{
	QString write;
	QByteArray data;

	//za pravilen prikaz v dialogz

	counterEn[timerNum] = enable;
	counterFreq[timerNum] = freq;

	write = QString("T%1%2%3").arg(timerNum).arg(enable).arg(freq);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}
void ControlCardMB::SetTimerFreq(int timerNum, int freq)
{
	QString write;
	QByteArray data;

	//za pravilen prikaz v dialogz

	
	counterFreq[timerNum] = freq;

	write = QString("T%1%2").arg(timerNum).arg(freq);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}
void ControlCardMB::StartTimer(int timerNum)
{
	QString write;
	QByteArray data;


	counterEn[timerNum] = 1;
	write = QString("G%111").arg(timerNum);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}
void ControlCardMB::StartTimer(int timerNum,int steps)
{

	QString write;
	QByteArray data;

	write = QString("G%1%2%3").arg(timerNum).arg(1).arg(steps);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}

void ControlCardMB::StopTimer(int timerNum)
{
	QString write;
	QByteArray data;

	//za pravilen prikaz v dialogz
	counterEn[timerNum] = 0;
	write = QString("G%110").arg(timerNum);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}

void ControlCardMB::SetSoftStartStopTimer(int timerNum, int rampStep)
{
	QString write;
	QByteArray data;

	write = QString("H%1%2%3").arg(timerNum).arg(1).arg(rampStep);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);


}



int ControlCardMB::ReadFile(void)
{
	QString filePath;
	QStringList values;
	
	QStringList list;
	
	//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
	filePath = QDir::currentPath() + QString("/%1/signali/ControlCardMB%2.ini").arg(REF_FOLDER_NAME).arg(id);
	QSettings settings(filePath, QSettings::IniFormat);

	
	int address = 1;
	//inputs
	for (int j = 0; j < 16; j++)
	{

		values = settings.value(QString("INPUT%1").arg(j)).toStringList();

		if (values.size() > 1)
		{
			input[j].SetSignal(values[0], address, values[1].toInt());
			values.clear();
		}
		else
		{
			input[j].SetSignal(QString("S%1").arg(j), address, 0);

			list.append(QString("INPUT%1").arg(j));
			list.append(QString("0"));
			settings.setValue(QString("INPUT%1").arg(j), list);
			settings.sync();
			list.clear();
		
		}
		address = address * 2;
	}

	//outputs
	address = 1;
	for (int j = 0; j < 16; j++)
	{

		values = settings.value(QString("OUTPUT%1").arg(j)).toStringList();

		if (values.size() > 0)
		{
			output[j].SetSignal(values[0], address, values[1].toInt());
			values.clear();
		}
		else
		{
			output[j].SetSignal(QString("S%1").arg(j), address, 0);

			list.append(QString("OUTPUT%1").arg(j));
			list.append(QString("0"));
			settings.setValue(QString("OUTPUT%1").arg(j), list);
			settings.sync();
			list.clear();

		}
		address = address * 2;
	}

	return 0;
}

bool ControlCardMB::InitCard()
{


	//potrebna inicializacija kartice pri zagonu programa. Programa morata komunicirati preko serijske stevlikje drugace Onemogocim kartico. 
	//
	return false;
}

void ControlCardMB::StartSend(int freq)
{
	QString write;
	QByteArray data;


		write = QString("SS1%1").arg(freq);
		data.insert(0, write.toLocal8Bit());

		WriteData(data);
	




}

void ControlCardMB::StartSendInput(int port)
{
	QString write;
	QByteArray data;


	write = QString("I%011").arg(port);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);


}

void ControlCardMB::StartSendMotorStep(int timerNum)
{
	QString write;
	QByteArray data;


	write = QString("M%011").arg(timerNum);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);

}

void ControlCardMB::StopSendInput(int port)
{
	QString write;
	QByteArray data;


	write = QString("I%010").arg(port);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}

void ControlCardMB::StopSendMotorStep(int timerNum)
{
	QString write;
	QByteArray data;


	write = QString("M%010").arg(timerNum);
	data.insert(0, write.toLocal8Bit());

	WriteData(data);
}

void ControlCardMB::OnChangedOutputBox(int OutNum)
{
	if (output[OutNum].isActive)
	{

		
			if (outputCheckBox[OutNum]->isChecked() == true)
			{
				output[OutNum].prevValue = output[OutNum].value;
				output[OutNum].value = 1;
			}
			else
			{
				output[OutNum].prevValue = output[OutNum].value;
				output[OutNum].value = 0;

			}
			if (OutNum < 8)
			{
				outputReg0 = 0;
				for (int i = 0; i < 8; i++)
				{
					outputReg0 += output[i].address*output[i].value;
				}
				SetOutputByte(0, outputReg0);
			}
			else
			{
				outputReg1 = 0;
				for (int i = 8; i < 16; i++)
				{
					outputReg1 += output[i].address*output[i].value >> 8;
					//k++;
				}
				SetOutputByte(1, outputReg1);
			}
	}

}

void ControlCardMB::ViewSignalsTimer()
{
	
	for (int i = 0; i < 16; i++)
	{
		if (input[i].value == 1)
			inputCheckBox[i]->setCheckState(Qt::Checked);
		else
			inputCheckBox[i]->setChecked(Qt::Unchecked);
	}
	for (int i = 0; i < 16; i++)
	{
		if(output[i].value == 1)
			outputCheckBox[i]->setCheckState(Qt::Checked);
		else
			outputCheckBox[i]->setCheckState(Qt::Unchecked);

	}
	/*for (int i = 0; i < 4; i++)
	{
		if (counterEn[i] == 1)
			ui.checkBoxCounter1[i]->setCheckState(Qt::Checked);
		else
			outputCheckBox[i]->setCheckState(Qt::Unchecked);

	}*/


}

void ControlCardMB::OnChangedCounter0()
{
	int value = ui.spinCounter1->value();

	SetTimerFreq(0, value);

}

void ControlCardMB::OnChangedCounter1()
{
	int value = ui.spinCounter2->value();

	SetTimerFreq(1, value);
	if (ui.checkBoxCounter2->isChecked())
	{

		StartTimer(1);
	}
	else
		StopTimer(1);

}

void ControlCardMB::OnChangedCounter2()
{
	int value = ui.spinCounter3->value();

	SetTimerFreq(2, value);
	if (ui.checkBoxCounter3->isChecked())
	{

		StartTimer(2);
	}
	else
		StopTimer(2);

}

void ControlCardMB::OnChangedCounter3()
{
	int value = ui.spinCounter4->value();

	SetTimerFreq(3, value);
	if (ui.checkBoxCounter4->isChecked())
	{

		StartTimer(3);
	}
	else
		StopTimer(3);

}

void ControlCardMB::OnEnableCounter0()
{
	if (ui.checkBoxCounter1->isChecked())
	{

		StartTimer(0);
	}
	else
		StopTimer(0);


}

void ControlCardMB::OnEnableCounter1()
{

}

void ControlCardMB::OnEnableCounter2()
{

}

void ControlCardMB::OnEnableCounter3()
{

}





