﻿#include "stdafx.h"
#include "DelayedDraw.h"



CDelayedDraw::CDelayedDraw()
{
}


CDelayedDraw::~CDelayedDraw()
{
}

CDelayedDraw CDelayedDraw::operator=(CDelayedDraw delDraw)
{
	displayImage = delDraw.displayImage;

	points = delDraw.points;
	pointsPen = delDraw.pointsPen;
	pointsBrush = delDraw.pointsBrush;
	pointsSize = delDraw.pointsSize;
	pointsText = delDraw.pointsText;
	pointsTextPosition = delDraw.pointsTextPosition;
	pointsConnectText = delDraw.pointsConnectText;
	pointsFont = delDraw.pointsFont;

	arrows = delDraw.arrows;
	arrowspen = delDraw.arrowspen;
	arrowDirction = delDraw.arrowDirction;
	arrowSize = delDraw.arrowSize;
	arrowsText = delDraw.arrowsText;
	arrowsTextPosition = delDraw.arrowsTextPosition;
	arrowsConnectText = delDraw.arrowsConnectText;
	arrowsFont = delDraw.arrowsFont;

	rectangles = delDraw.rectangles;
	rectColor = delDraw.rectColor;
	rectPenSize = delDraw.rectPenSize;
	rectPenMode = delDraw.rectPenMode;
	rectText = delDraw.rectText;
	rectTextPosition = delDraw.rectTextPosition;
	rectConnectText = delDraw.rectConnectText;

	circlePen = delDraw.circlePen;
	circleBrush = delDraw.circleBrush;
	crossSize = delDraw.crossSize;
	circle = delDraw.circle;
	circleText = delDraw.circleText;


	lines = delDraw.lines;
	linesPen = delDraw.linesPen;
	linesArea = delDraw.linesArea;
	return *this;
}

void CDelayedDraw::Reset()
{
	displayImage.clear();

	points.clear();
	pointsPen.clear();
	pointsBrush.clear();
	pointsSize.clear(); 
	pointsText.clear();
	pointsTextPosition.clear();
	pointsConnectText.clear();
	pointsFont.clear();

	arrows.clear();
	arrowspen.clear();
	arrowSize.clear();
	arrowDirction.clear();
	arrowsText.clear();
	arrowsTextPosition.clear();
	arrowsConnectText.clear();
	arrowsFont.clear();

	rectangles.clear();
	rectColor.clear();
	rectPenSize.clear();
	rectPenMode.clear();
	rectText.clear();
	rectTextPosition.clear();
	rectConnectText.clear();

	circlePen.clear();
	circleBrush.clear();
	crossSize.clear();
	circle.clear();
	circleText.clear();

	lines.clear();
	linesPen.clear();
	linesArea.clear();
}

void CDelayedDraw::AddImage(Mat image)
{
	QImage qimgOriginal((uchar*)image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);
	displayImage.push_back(qimgOriginal);

}

void CDelayedDraw::AddPoint(CPointFloat T, int size, QPen pen, QBrush brush,  QString text, CPointFloat textPosition, bool connectText, QFont font)
{
	points.push_back(T);
	pointsPen.push_back(pen);
	pointsBrush.push_back(brush);
	pointsSize.push_back(size);
	pointsText.push_back(text);
	pointsTextPosition.push_back(textPosition);
	pointsConnectText.push_back(connectText);
	pointsFont.push_back(font);
}
//arrow direction 0->P1 v P2, 1-> P2-P1, 2-> v obe smeri
void CDelayedDraw::AddArrow(CLine line, QPen pen, int arrowLineSize, int arrowDir, QString text, CPointFloat textPosition, bool connectText, QFont font)
{
	arrows.push_back(line);
	arrowspen.push_back(pen);
	arrowDirction.push_back(arrowDir);
	arrowSize.push_back(arrowLineSize);

	arrowsConnectText.push_back(connectText);

	arrowsText.push_back(text);
	arrowsTextPosition.push_back(textPosition);
	arrowsFont.push_back(font);
	

}
QGraphicsEllipseItem* CDelayedDraw::DrawPoint(int index)
{
	if (points.size() > index)
	{
		QRect rect(points[index].x, points[index].y, pointsSize[index], pointsSize[index]);
		rect.moveCenter(QPoint(points[index].x, points[index].y));
		QGraphicsEllipseItem* elipse = new QGraphicsEllipseItem(rect);
		QGraphicsLineItem* line = new QGraphicsLineItem();
		QGraphicsTextItem * text = new QGraphicsTextItem();
		if (pointsConnectText[index] == true)
		{
			text->setPlainText(pointsText[index]);
			text->setPos(pointsTextPosition[index].x, pointsTextPosition[index].y);
			text->setFont(pointsFont[index]);
			text->setDefaultTextColor(pointsPen[index].color());
			
			line->setLine(points[index].x, points[index].y, pointsTextPosition[index].x, pointsTextPosition[index].y);
			line->setPen(QPen(pointsPen[index]));
			

			line->setParentItem(elipse);
			text->setParentItem(elipse);
		}
		elipse->setActive(true);
		elipse->setPen(pointsPen[index]);
		elipse->setBrush(pointsBrush[index]);

		elipse->setZValue(1);

		return elipse;
	}


	return nullptr;
}
QGraphicsPixmapItem * CDelayedDraw::DrawImage(int index)
{
	if (displayImage.size() > index)
	{

		QGraphicsPixmapItem*  QGaphicsPixmap = new QGraphicsPixmapItem();
		QGaphicsPixmap->setPixmap(QPixmap::fromImage(displayImage[index]));

		return QGaphicsPixmap;
	}

	return nullptr;
}

QGraphicsEllipseItem* CDelayedDraw::DrawCircle(int index)
{
	int radius = circle[index].GetRadius();
	CPointFloat center = circle[index].GetCenter();
	QRect rect(center.x, center.y, radius * 2, radius * 2);
	rect.moveCenter(QPoint(center.x, center.y));
	QGraphicsEllipseItem* circleIntem = new QGraphicsEllipseItem(rect);

	//circle->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
	circleIntem->setBrush(circleBrush[index]);
	circleIntem->setPen(circlePen[index]);

	QGraphicsLineItem* line = new QGraphicsLineItem();
	QGraphicsLineItem* line2 = new QGraphicsLineItem();
	line->setLine(center.x - crossSize[index] / 2, center.y, center.x + crossSize[index] / 2, center.y);
	line2->setLine(center.x, center.y - crossSize[index] / 2, center.x, center.y + crossSize[index] / 2);
	line2->setPen(QPen(circlePen[index]));

	line->setPen(QPen(circlePen[index]));
	line->setParentItem(circleIntem);
	line2->setParentItem(circleIntem);
	circleIntem->setZValue(1);
	return circleIntem;
}

QGraphicsLineItem * CDelayedDraw::DrawSegment(int index)
{

	
	QGraphicsLineItem* line = new QGraphicsLineItem;

	line->setPen(linesPen[index]);
	line->setLine(lines[index].p1.x, lines[index].p1.y, lines[index].p2.x, lines[index].p2.y);


	return line;
}
QGraphicsLineItem * CDelayedDraw::DrawLine(int index)

{
	QPainterPath painter;
	float ka = lines[index].k;
	float na = lines[index].n;
	float koryy, linex, liney;
	QRect area = linesArea[index];
	CPointFloat p1Draw, p2Draw;

	QGraphicsLineItem* line = new QGraphicsLineItem;
	QLine line2;

	if (abs(ka) < HUGE_VAL)
	{
		if (abs(ka) < 1)
		{

			//pri vodoravni premici ni potrebno upostevati xoffseta, samo y
			koryy = (area.left() * ka + na);
			p1Draw.x = (area.left());
			p1Draw.y = (koryy);


			koryy = (area.right() * ka + na);
			p2Draw.x = (area.right());
			p2Draw.y = (koryy);

			line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);

		}
		else
		{
			//pri navpicni premici ni potrebno upostevati y offseta, samo x

			koryy = ((area.top() - na) / ka);
			p1Draw.x = (koryy);
			p1Draw.y = (area.top());


			koryy = ((area.bottom() - na) / ka);
			p2Draw.y = (area.bottom());
			p2Draw.x = (koryy); /* zoomFactor *///);
			line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);

		}
	}
	else
	{
		//pri navpicni premici ni potrebno upostevati y offseta, samo x
		p1Draw.x = (int)((lines[index].p1.x));
		p1Draw.y = (int)(area.top());


		p2Draw.x = (int)((lines[index].p1.x));
		p2Draw.y = (int)(area.bottom());

		line2.setLine(p1Draw.x, p1Draw.y, p2Draw.x, p2Draw.y);
	}






	line->setLine(line2);
	line->setPen(linesPen[index]);
	line->setZValue(2);

	return line;
}

QGraphicsLineItem * CDelayedDraw::DrawArrow(int index)
{
	QGraphicsLineItem* line = new QGraphicsLineItem;
	line->setPen(arrowspen[index]);
	line->setLine(arrows[index].p1.x, arrows[index].p1.y, arrows[index].p2.x, arrows[index].p2.y);
	int arrow_degrees_ = 30;

	double angleForward = atan2(arrows[index].p2.y - arrows[index].p1.y, arrows[index].p2.x - arrows[index].p1.x) * 180 / M_PI;
	double angleReverse = atan2(arrows[index].p1.y - arrows[index].p2.y, arrows[index].p1.x - arrows[index].p2.x) * 180 / M_PI;

	//fiDeg = atan2(dy[i], dx[i]) * 180.0 / M_PI;	//atan2 vrne kot med -Pi in Pi

	if (angleForward < 0)
		angleForward += 360;  //Spravimo kot na interval 0 do 360 stopinj


	if (angleReverse < 0)
		angleReverse += 360;  //Spravimo kot na interval 0 do 360 stopinj

	int x11, y11, x12, y12,x21,y21,x22,y22;

	QGraphicsLineItem* lineA11 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA12 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA21 = new QGraphicsLineItem();
	QGraphicsLineItem* lineA22 = new QGraphicsLineItem();


	
	lineA11->setPen(arrowspen[index]);
	lineA12->setPen(arrowspen[index]);
	lineA21->setPen(arrowspen[index]);
	lineA22->setPen(arrowspen[index]);

	if(arrowDirction[index] == 0) //na tocki p1 je premica
	{
		x11 = arrows[index].p1.x + arrowSize[index] * cos((angleForward - arrow_degrees_)* M_PI / 180.0);
		y11 = arrows[index].p1.y + arrowSize[index] * sin((angleForward - arrow_degrees_)* M_PI / 180.0);
		x12 = arrows[index].p1.x + arrowSize[index] * cos((angleForward + arrow_degrees_)* M_PI / 180.0);
		y12 = arrows[index].p1.y + arrowSize[index] * sin((angleForward + arrow_degrees_)* M_PI / 180.0);

		lineA11->setLine(arrows[index].p1.x, arrows[index].p1.y, x11, y11);
		lineA12->setLine(arrows[index].p1.x, arrows[index].p1.y, x12, y12);
		lineA11->setParentItem(line);
		lineA12->setParentItem(line);
	}
	else if (arrowDirction[index] == 1)
	{
		x21 = arrows[index].p2.x + arrowSize[index] * cos((angleReverse - arrow_degrees_)* M_PI / 180.0);
		y21 = arrows[index].p2.y + arrowSize[index] * sin((angleReverse - arrow_degrees_)* M_PI / 180.0);
		x22 = arrows[index].p2.x + arrowSize[index] * cos((angleReverse + arrow_degrees_)* M_PI / 180.0);
		y22 = arrows[index].p2.y + arrowSize[index] * sin((angleReverse + arrow_degrees_)* M_PI / 180.0);
		lineA21->setLine(arrows[index].p2.x, arrows[index].p2.y, x21, y21);
		lineA22->setLine(arrows[index].p2.x, arrows[index].p2.y, x22, y22);
		lineA21->setParentItem(line);
		lineA22->setParentItem(line);
	}
	else if (arrowDirction[index] == 2)
	{
		//float kot = cos((angle - arrow_degrees_)* M_PI / 180.0);
	//	float koty = sin((angle - arrow_degrees_)* M_PI / 180.0);
		x11 = arrows[index].p1.x + arrowSize[index] * cos((angleForward - arrow_degrees_)* M_PI / 180.0);
		y11 = arrows[index].p1.y + arrowSize[index] * sin((angleForward - arrow_degrees_)* M_PI / 180.0);
		x12 = arrows[index].p1.x + arrowSize[index] * cos((angleForward + arrow_degrees_)* M_PI / 180.0);
		y12 = arrows[index].p1.y + arrowSize[index] * sin((angleForward + arrow_degrees_)* M_PI / 180.0);

		x21 = arrows[index].p2.x + arrowSize[index] * cos((angleReverse - arrow_degrees_)* M_PI / 180.0);
		y21 = arrows[index].p2.y + arrowSize[index] * sin((angleReverse - arrow_degrees_)* M_PI / 180.0);
		x22 = arrows[index].p2.x + arrowSize[index] * cos((angleReverse + arrow_degrees_)* M_PI / 180.0);
		y22 = arrows[index].p2.y + arrowSize[index] * sin((angleReverse + arrow_degrees_)* M_PI / 180.0);

		lineA11->setLine(arrows[index].p1.x, arrows[index].p1.y, x11, y11);
		lineA12->setLine(arrows[index].p1.x, arrows[index].p1.y, x12, y12);
		lineA21->setLine(arrows[index].p2.x, arrows[index].p2.y, x21, y21);
		lineA22->setLine(arrows[index].p2.x, arrows[index].p2.y, x22, y22);

		lineA11->setParentItem(line);
		lineA12->setParentItem(line);
		lineA21->setParentItem(line);
		lineA22->setParentItem(line);

	}
	else//direction 3 pomeni crta
	{

	}
	QGraphicsTextItem * text = new QGraphicsTextItem();
	//QGraphicsLineItem* textLine = new QGraphicsLineItem();


	text->setPlainText(arrowsText[index]);
	text->setPos(arrowsTextPosition[index].x, arrowsTextPosition[index].y);
	text->setFont(arrowsFont[index]);
	text->setDefaultTextColor(arrowspen[index].color());
	
	/*if (linesConnectText[index] == true)
	{
		CPointFloat intersect;
		intersect = lines[index].GetMiddlePoint();
		textLine->setLine(intersect.x, intersect.y, linesTextPosition[index].x, linesTextPosition[index].y);
		textLine->setPen(QPen(linesPen[index]));
		

	}


	textLine->setParentItem(line);*/
	text->setParentItem(line);

	line->setActive(true);
	line->setPen(arrowspen[index]);



	line->setZValue(1);




	return line;
}


void CDelayedDraw::AddCircle(CCircle circleIn, QPen pen, QBrush brush, int size, QString text)
{
	circle.push_back(circleIn);
	circlePen.push_back(pen);
	circleBrush.push_back(brush);
	crossSize.push_back(size);
	circleText.push_back(text);

}

void CDelayedDraw::AddLine(QRect areaREct, CLine line, QPen pen)
{
	lines.push_back(line);
	linesPen.push_back(pen);
	linesArea.push_back(areaREct);
}

void CDelayedDraw::DrawAll(QGraphicsScene * scene)
{
	for (int i = 0; i < points.size(); i++)
		scene->addItem(DrawPoint(i));

	for (int i = 0; i < circle.size(); i++)
		scene->addItem(DrawCircle(i));

	for (int i = 0; i < arrows.size(); i++)
		scene->addItem(DrawArrow(i));

	for (int i = 0; i < lines.size(); i++)
		scene->addItem(DrawLine(i));
}
/*
void CDelayedDraw::AddRectangle(CRectRotated rect, int penMode, int penSize, COLORREF color, CString text, CPointFloat textPosition, bool connectText)
{
	rectangles.push_back(rect);
	rectColor.push_back(color);
	rectPenSize.push_back(penSize);
	rectPenMode.push_back(penMode);
	rectText.push_back(text);
	rectTextPosition.push_back(textPosition);
	rectConnectText.push_back(connectText);
}

void CDelayedDraw::DrawPoints(CDC* pDC, float zoomFactor, int xOffset, int yOffset)
{
	float x, y;

	//izris točke
	for (int i = 0; i < points.size(); i++)
	{
		points[i].DrawDot(pDC, pointsColor[i], pointsSize[i], zoomFactor, xOffset, yOffset);
	}

	//izpis teksta točke
	pDC->SelectObject(CMainFrame::font18BHelvx);
	pDC->SelectObject(CMainFrame::penBlack);
	pDC->SelectObject(CMainFrame::brushGrey);
	pDC->SetBkColor(CMainFrame::colorGrey);

	for (int i = 0; i < points.size(); i++)
	{
		x = (points[i].x + pointsTextPosition[i].x) * zoomFactor + xOffset;
		y = (points[i].y + pointsTextPosition[i].y) * zoomFactor + yOffset;

		pDC->TextOutW(x, y, pointsText[i]);

	}

	//povezava teksta in točke


	CPen pen(0, 1, RGB(0, 0, 255));
	pDC->SelectObject(pen);

	for (int i = 0; i < points.size(); i++)
	{
		if (pointsConnectText[i])
		{
			//tu narišem črto od transformirane points do transformirane pointsTextPosition
			//pointsTextPosition merim glede na points
			x = points[i].x * zoomFactor + xOffset;
			y = points[i].y * zoomFactor + yOffset;

			pDC->MoveTo(x,y);

			x = (points[i].x + pointsTextPosition[i].x) * zoomFactor + xOffset;
			y = (points[i].y + pointsTextPosition[i].y) * zoomFactor + yOffset;

			pDC->LineTo(x,y);
		}
	}
}

void CDelayedDraw::DrawLines(CDC* pDC, CRect destinationRect, float zoomFactor, int xOffset, int yOffset)
{
	float x, y;

	for (int i = 0; i < lines.size(); i++)
	{
		lines[i].DrawLimited(pDC, linesPenMode[i], linesPenSize[i], linesColor[i], zoomFactor, xOffset, yOffset, destinationRect);
	}

	//izpis teksta premice
	pDC->SelectObject(CMainFrame::font18BHelvx);
	pDC->SelectObject(CMainFrame::penBlack);
	pDC->SelectObject(CMainFrame::brushGrey);
	pDC->SelectObject(CMainFrame::brushGrey);
	pDC->SetBkColor(CMainFrame::colorGrey);
	pDC->SetBkColor(CMainFrame::colorGrey);

	for (int i = 0; i < lines.size(); i++)
	{
		x = linesTextPosition[i].x * zoomFactor + xOffset;
		y = linesTextPosition[i].y * zoomFactor + yOffset;

		pDC->TextOutW(x, y, linesText[i]);
	}

	//povezava teksta z najbližjo točko na premici


	CPen pen(0, 1, RGB(0, 0, 255));
	pDC->SelectObject(pen);

	CLine pravokotnica;
	CPointFloat Tprojekcija;

	for (int i = 0; i < lines.size(); i++)
	{
		if (linesConnectText[i])
		{
			pravokotnica = lines[i].GetPerpendicular(linesTextPosition[i]);
			Tprojekcija = lines[i].GetIntersectionPoint(pravokotnica);

			//tu narišem črto od transformirane linesTextPosition do transformirane najbljižje točke na premici
			//pointsTextPosition merim glede na points
			x = Tprojekcija.x * zoomFactor + xOffset;
			y = Tprojekcija.y * zoomFactor + yOffset;

			pDC->MoveTo(x, y);

			x = (linesTextPosition[i].x) * zoomFactor + xOffset;
			y = (linesTextPosition[i].y) * zoomFactor + yOffset;

			pDC->LineTo(x, y);
		}
	}
}

void CDelayedDraw::DrawRectangles(CDC * pDC, float zoomFactor, int xOffset, int yOffset)
{
	float x, y;

	for (int i = 0; i < rectangles.size(); i++)
	{
		rectangles[i].DrawPolygonLines(pDC, rectColor[i], rectPenSize[i], rectPenMode[i], zoomFactor, xOffset, yOffset);
	}

	//izpis teksta pravokotnika
	pDC->SelectObject(CMainFrame::font18BHelvx);
	pDC->SelectObject(CMainFrame::penBlack);
	pDC->SelectObject(CMainFrame::brushGrey);
	pDC->SetBkColor(CMainFrame::colorGrey);

	for (int i = 0; i < rectangles.size(); i++)
	{
		x = (rectangles[i].left + rectTextPosition[i].x) *  zoomFactor + xOffset;
		y= (rectangles[i].top + rectTextPosition[i].y) * zoomFactor + yOffset;
	
		pDC->TextOutW(x, y, rectText[i]);

	}

	//povezava teksta in pravokotnika

	CPen pen(0, 1, RGB(0, 0, 255));
	pDC->SelectObject(pen);

	for (int i = 0; i < rectangles.size(); i++)
	{
		if (rectConnectText[i])
		{
			//tu narišem črto od transformiranega zgornjega levega oglišča pravokotnika do ustreznega teksta
			//koordinate teksta merim glede na levo zgornje oglišče
			x = (rectangles[i].left + rectTextPosition[i].x) *  zoomFactor + xOffset;
			y = (rectangles[i].top + rectTextPosition[i].y) * zoomFactor + yOffset;

			pDC->MoveTo(x, y);

			x = rectangles[i].left *  zoomFactor + xOffset;
			y = rectangles[i].top * zoomFactor + yOffset;

			pDC->LineTo(x, y);
		}
	}
}


*/

