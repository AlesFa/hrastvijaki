﻿#pragma once



class CDelayedDraw
{
public:



	vector<QImage>  displayImage;


	vector <CPointFloat> points;
	vector<QPen> pointsPen;
	vector<QBrush> pointsBrush;
	vector<int> pointsSize;
	vector <QString> pointsText;
	vector <CPointFloat> pointsTextPosition;  //pointsTextPosition merim glede na points
	vector <QFont> pointsFont;  //pointsTextPosition merim glede na points	
	vector <bool> pointsConnectText;		//če je vrednost true, narišemo črto med točko in pripadajočim tekstom
	
	vector <CLine> arrows;
	vector <QPen> arrowspen;
	vector <int > arrowDirction;
	vector <int> arrowSize;
	vector <QString> arrowsText;
	vector <CPointFloat> arrowsTextPosition;  //pointsTextPosition merim v absolutnih koordinatah originalne slike (pred zoom, offset)
	vector <bool> arrowsConnectText;		//če je vrednost true, narišemo črto med premico in pripadajočim tekstom
	vector <QFont> arrowsFont;

	vector <CRectRotated> rectangles;
	vector<COLORREF> rectColor;
	vector <int> rectPenSize;
	vector <int> rectPenMode;
	vector <QString> rectText;
	vector <CPointFloat> rectTextPosition;		//rectTextPosition merim glede na zgornjo levo točko pravokotnika
	vector <bool> rectConnectText;		//če je vrednost true, narišemo črto med zgornjo levo točko pravokotnika in pripadajočim tekstom

	vector <QPen> circlePen;
	vector <QBrush> circleBrush;
	vector <int > crossSize;
	vector <CCircle> circle;
	vector <QString> circleText;

	vector <CLine> lines;
	vector <QPen>  linesPen;
	vector <QRect> linesArea;


public:
	CDelayedDraw();
	~CDelayedDraw();

	CDelayedDraw operator=(CDelayedDraw delDraw);

	void Reset();


	void AddImage(Mat image);
	//IDEJA: lahko bi dodal function overloads brez parametrov mode, color, ki nastavijo ta dva parametra na memberVar mode, color, ki ju nastavimo le enkrat

	void AddPoint(CPointFloat T, int size, QPen pen, QBrush brush, QString text, CPointFloat textPosition, bool connectText, QFont font);
	void AddCircle(CCircle circle, QPen pen, QBrush brush, int crossSize, QString text);
	void AddLine(QRect areaREct,CLine line, QPen pen);
	//arrowDirection 
	void AddArrow(CLine line, QPen pen, int arrowSize, int arrowDirection, QString text, CPointFloat textPosition, bool connectText, QFont font);




	QGraphicsEllipseItem* DrawPoint(int index);
	QGraphicsPixmapItem* DrawImage(int index);
	QGraphicsEllipseItem * DrawCircle(int index);
	QGraphicsLineItem* DrawSegment(int index);
	QGraphicsLineItem * DrawLine(int index);
	QGraphicsLineItem * DrawArrow(int index);





	void DrawAll(QGraphicsScene*scene);
	//void AddRectangle(CRectRotated rect, int penMode, int penSize, COLORREF color, CString text, CPointFloat textPosition, bool connectText);
	//void AddLine(CLine line, int penMode, int penSize, COLORREF color, QString text, CPointFloat textPosition, bool connectText);

	
	//void AddRectangle(CRectRotated rect, int penMode, int penSize, COLORREF color, QString text, CPointFloat textPosition, bool connectText);

	//void DrawPoints(CDC* pDC, float zoomFactor, int xOffset, int yOffset);

	//void DrawLines(CDC* pDC, CRect destinationRect, float zoomFactor, int xOffset, int yOffset);

	//void DrawRectangles(CDC* pDC, float zoomFactor, int xOffset, int yOffset);

	//void DrawAll(CDC* pDC, CRect destinationRect, float zoomFactor, int xOffset, int yOffset);

	
};

