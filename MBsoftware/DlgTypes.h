#pragma once
#include "stdafx.h"
#include <QDialog>
#include "ui_DlgTypes.h"

class DlgTypes : public QDialog
{
	Q_OBJECT

public:
	DlgTypes(QWidget *parent = Q_NULLPTR);
	DlgTypes(QWidget *parent, int nTypes);
	~DlgTypes();

	std::vector<QString>	types;

private:
	Ui::DlgTypes ui;

	bool slovensko;

public:
	int ShowDialogSelectType(int currType);



};
