#include "stdafx.h"
#include "GeneralSettings.h"

/*GeneralSettings::GeneralSettings(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}*/

GeneralSettings::GeneralSettings()
{
	ui.setupUi(this);


	connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(OnConfirm()));
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(OnRejected()));


	LoadSettings();
	SetState();



	slovensko = SLOVENSKO;

	if (slovensko)
	{
		this->setWindowTitle("Splosne nastavitve");
	}
}

GeneralSettings::~GeneralSettings()
{

}

void GeneralSettings::closeEvent(QCloseEvent * event)
{
	close();
}

void GeneralSettings::OnShowDialog()
{
	setWindowModality(Qt::ApplicationModal);
	SetState();
	show();
}

void GeneralSettings::SaveSettings()
{

		QStringList list;
		//Pot do konfugiracijskih datotek

		QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);
		QDir dir;

		dir.setPath(dirPath);
		if (!dir.exists())
			dir.mkdir(dirPath);

		int index = 0;
		QString filePath = dirPath + QString("generalSettings.ini");

		QSettings settings(filePath, QSettings::IniFormat);

	

		
		list.clear();
		list.append(QString("%1").arg(dimOn));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();
		list.append(QString("%1").arg(workingMode));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(maxSlabi));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();

		list.append(QString("%1").arg(maxSlabiStolp));
		settings.setValue(QString("settings%1").arg(index), list);
		index++;
		list.clear();




}

int GeneralSettings::LoadSettings()
{
	QStringList values;

	QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);
	QDir dir;

	dir.setPath(dirPath);
	if (!dir.exists())
	{
		dir.mkdir(dirPath);
		return 0;
	}

	int index = 0;
	QString filePath = dirPath + QString("generalSettings.ini");

	QSettings settings(filePath, QSettings::IniFormat);
	int n = 0;


		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		dimOn = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		workingMode = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		maxSlabi = values[0].toInt();
		n++;

		values.clear();
		values = settings.value(QString("settings%1").arg(n)).toStringList();
		if (values.size() == 0)
			return 0;
		maxSlabiStolp = values[0].toInt();
		n++;


	

		return 1;
}

void GeneralSettings::SetState()
{


	//lamele

	ui.checkDim->setChecked(dimOn);

	

	if (workingMode == 0)
		ui.radioButtonS1->setChecked(true);
	else if (workingMode == 1)
		ui.radioButtonS2->setChecked(true);
	else if(workingMode == 2)
		ui.radioButtonS12->setChecked(true);
	
	ui.editMaxSlabi->setText(QString("%1").arg(maxSlabi));
	ui.editMaxSlabiNavor->setText(QString("%1").arg(maxSlabiStolp));

}

void GeneralSettings::OnConfirm()
{

	dimOn = ui.checkDim->isChecked();
	if (ui.radioButtonS1->isChecked())
		workingMode = 0;
	if (ui.radioButtonS2->isChecked())
		workingMode = 1;
	if (ui.radioButtonS12->isChecked())
		workingMode = 2;


	maxSlabi = ui.editMaxSlabi->text().toInt();
	maxSlabiStolp = ui.editMaxSlabiNavor->text().toInt();

	SaveSettings();
	closeEvent(NULL);
}

void GeneralSettings::OnRejected()
{
	closeEvent(NULL);
}





