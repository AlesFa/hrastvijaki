#pragma once

#include "stdafx.h"
#include <QWidget>
#include "ui_GeneralSettings.h"

class GeneralSettings : public QWidget
{
	Q_OBJECT

public:
	//GeneralSettings(QWidget *parent = Q_NULLPTR);
	GeneralSettings();
	~GeneralSettings();
	void closeEvent(QCloseEvent * event);


	int workingMode;
	int dimOn;
	int maxSlabi;
	int maxSlabiStolp;

	void OnShowDialog();
	void SaveSettings();
	int LoadSettings();
	void SetState();

private:
	Ui::GeneralSettings ui;

	bool slovensko;

public slots:
	void	OnConfirm();
	void	OnRejected();




};
