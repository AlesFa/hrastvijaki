/****************************************************************************
** Meta object code from reading C++ file 'MBsoftware.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../MBsoftware.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MBsoftware.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MBsoftware_t {
    QByteArrayData data[48];
    char stringdata0[753];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MBsoftware_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MBsoftware_t qt_meta_stringdata_MBsoftware = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MBsoftware"
QT_MOC_LITERAL(1, 11, 9), // "ViewTimer"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 12), // "OnFrameReady"
QT_MOC_LITERAL(4, 35, 14), // "OnTcpDataReady"
QT_MOC_LITERAL(5, 50, 4), // "conn"
QT_MOC_LITERAL(6, 55, 5), // "parse"
QT_MOC_LITERAL(7, 61, 11), // "HistoryCall"
QT_MOC_LITERAL(8, 73, 7), // "station"
QT_MOC_LITERAL(9, 81, 12), // "currentPiece"
QT_MOC_LITERAL(10, 94, 22), // "OnClickedShowLptButton"
QT_MOC_LITERAL(11, 117, 23), // "OnClickedShowUZedButton"
QT_MOC_LITERAL(12, 141, 22), // "OnClickedShowCamButton"
QT_MOC_LITERAL(13, 164, 28), // "OnClickedShowSmartCardButton"
QT_MOC_LITERAL(14, 193, 28), // "OnClickedShowImageProcessing"
QT_MOC_LITERAL(15, 222, 19), // "OnClickedSelectType"
QT_MOC_LITERAL(16, 242, 18), // "OnClickedEditTypes"
QT_MOC_LITERAL(17, 261, 20), // "OnClickedRemoveTypes"
QT_MOC_LITERAL(18, 282, 17), // "OnClickedAddTypes"
QT_MOC_LITERAL(19, 300, 21), // "OnClickedTypeSettings"
QT_MOC_LITERAL(20, 322, 22), // "OnClickedResetCounters"
QT_MOC_LITERAL(21, 345, 14), // "OnClickedLogin"
QT_MOC_LITERAL(22, 360, 18), // "OnClickedStatusBox"
QT_MOC_LITERAL(23, 379, 18), // "OnClickedSmcButton"
QT_MOC_LITERAL(24, 398, 20), // "OnClickedShowHistory"
QT_MOC_LITERAL(25, 419, 24), // "OnClickedGeneralSettings"
QT_MOC_LITERAL(26, 444, 16), // "OnClickedAboutUs"
QT_MOC_LITERAL(27, 461, 17), // "onClientReadyRead"
QT_MOC_LITERAL(28, 479, 21), // "OnClickedTCPconnction"
QT_MOC_LITERAL(29, 501, 13), // "SaveVariables"
QT_MOC_LITERAL(30, 515, 13), // "LoadVariables"
QT_MOC_LITERAL(31, 529, 6), // "AddLog"
QT_MOC_LITERAL(32, 536, 4), // "text"
QT_MOC_LITERAL(33, 541, 4), // "type"
QT_MOC_LITERAL(34, 546, 12), // "EraseLastLOG"
QT_MOC_LITERAL(35, 559, 17), // "OnClickedResetBox"
QT_MOC_LITERAL(36, 577, 3), // "box"
QT_MOC_LITERAL(37, 581, 22), // "OnClickedKoracnoDialog"
QT_MOC_LITERAL(38, 604, 20), // "OnClickedTriggOrient"
QT_MOC_LITERAL(39, 625, 21), // "OnClickedTriggMeasure"
QT_MOC_LITERAL(40, 647, 20), // "OnClickedTriggStolp1"
QT_MOC_LITERAL(41, 668, 20), // "OnClickedTriggStolp2"
QT_MOC_LITERAL(42, 689, 12), // "MeasurePiece"
QT_MOC_LITERAL(43, 702, 5), // "index"
QT_MOC_LITERAL(44, 708, 13), // "UpdateHistory"
QT_MOC_LITERAL(45, 722, 10), // "SetHistory"
QT_MOC_LITERAL(46, 733, 14), // "WriteLogToFile"
QT_MOC_LITERAL(47, 748, 4) // "path"

    },
    "MBsoftware\0ViewTimer\0\0OnFrameReady\0"
    "OnTcpDataReady\0conn\0parse\0HistoryCall\0"
    "station\0currentPiece\0OnClickedShowLptButton\0"
    "OnClickedShowUZedButton\0OnClickedShowCamButton\0"
    "OnClickedShowSmartCardButton\0"
    "OnClickedShowImageProcessing\0"
    "OnClickedSelectType\0OnClickedEditTypes\0"
    "OnClickedRemoveTypes\0OnClickedAddTypes\0"
    "OnClickedTypeSettings\0OnClickedResetCounters\0"
    "OnClickedLogin\0OnClickedStatusBox\0"
    "OnClickedSmcButton\0OnClickedShowHistory\0"
    "OnClickedGeneralSettings\0OnClickedAboutUs\0"
    "onClientReadyRead\0OnClickedTCPconnction\0"
    "SaveVariables\0LoadVariables\0AddLog\0"
    "text\0type\0EraseLastLOG\0OnClickedResetBox\0"
    "box\0OnClickedKoracnoDialog\0"
    "OnClickedTriggOrient\0OnClickedTriggMeasure\0"
    "OnClickedTriggStolp1\0OnClickedTriggStolp2\0"
    "MeasurePiece\0index\0UpdateHistory\0"
    "SetHistory\0WriteLogToFile\0path"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MBsoftware[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      38,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  204,    2, 0x08 /* Private */,
       3,    2,  205,    2, 0x08 /* Private */,
       4,    2,  210,    2, 0x08 /* Private */,
       7,    2,  215,    2, 0x08 /* Private */,
      10,    1,  220,    2, 0x08 /* Private */,
      11,    1,  223,    2, 0x08 /* Private */,
      12,    1,  226,    2, 0x08 /* Private */,
      13,    1,  229,    2, 0x08 /* Private */,
      14,    0,  232,    2, 0x08 /* Private */,
      15,    0,  233,    2, 0x08 /* Private */,
      16,    0,  234,    2, 0x08 /* Private */,
      17,    0,  235,    2, 0x08 /* Private */,
      18,    0,  236,    2, 0x08 /* Private */,
      19,    0,  237,    2, 0x08 /* Private */,
      20,    0,  238,    2, 0x08 /* Private */,
      21,    0,  239,    2, 0x08 /* Private */,
      22,    0,  240,    2, 0x08 /* Private */,
      23,    0,  241,    2, 0x08 /* Private */,
      24,    0,  242,    2, 0x08 /* Private */,
      25,    0,  243,    2, 0x08 /* Private */,
      26,    0,  244,    2, 0x08 /* Private */,
      27,    0,  245,    2, 0x08 /* Private */,
      28,    1,  246,    2, 0x08 /* Private */,
      29,    0,  249,    2, 0x08 /* Private */,
      30,    0,  250,    2, 0x08 /* Private */,
      31,    2,  251,    2, 0x08 /* Private */,
      34,    0,  256,    2, 0x08 /* Private */,
      35,    1,  257,    2, 0x08 /* Private */,
      37,    0,  260,    2, 0x08 /* Private */,
      38,    0,  261,    2, 0x08 /* Private */,
      39,    0,  262,    2, 0x08 /* Private */,
      40,    0,  263,    2, 0x08 /* Private */,
      41,    0,  264,    2, 0x08 /* Private */,
      42,    1,  265,    2, 0x08 /* Private */,
      42,    2,  268,    2, 0x08 /* Private */,
      44,    2,  273,    2, 0x08 /* Private */,
      45,    0,  278,    2, 0x08 /* Private */,
      46,    3,  279,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    5,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    8,    9,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   32,   33,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   36,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   43,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    8,   43,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    8,   43,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,   47,   32,   33,

       0        // eod
};

void MBsoftware::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MBsoftware *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ViewTimer(); break;
        case 1: _t->OnFrameReady((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnTcpDataReady((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->HistoryCall((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->OnClickedShowLptButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->OnClickedShowUZedButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->OnClickedShowCamButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->OnClickedShowSmartCardButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->OnClickedShowImageProcessing(); break;
        case 9: _t->OnClickedSelectType(); break;
        case 10: _t->OnClickedEditTypes(); break;
        case 11: _t->OnClickedRemoveTypes(); break;
        case 12: _t->OnClickedAddTypes(); break;
        case 13: _t->OnClickedTypeSettings(); break;
        case 14: _t->OnClickedResetCounters(); break;
        case 15: _t->OnClickedLogin(); break;
        case 16: _t->OnClickedStatusBox(); break;
        case 17: _t->OnClickedSmcButton(); break;
        case 18: _t->OnClickedShowHistory(); break;
        case 19: _t->OnClickedGeneralSettings(); break;
        case 20: _t->OnClickedAboutUs(); break;
        case 21: _t->onClientReadyRead(); break;
        case 22: _t->OnClickedTCPconnction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->SaveVariables(); break;
        case 24: _t->LoadVariables(); break;
        case 25: _t->AddLog((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 26: _t->EraseLastLOG(); break;
        case 27: _t->OnClickedResetBox((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->OnClickedKoracnoDialog(); break;
        case 29: _t->OnClickedTriggOrient(); break;
        case 30: _t->OnClickedTriggMeasure(); break;
        case 31: _t->OnClickedTriggStolp1(); break;
        case 32: _t->OnClickedTriggStolp2(); break;
        case 33: _t->MeasurePiece((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->MeasurePiece((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 35: _t->UpdateHistory((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 36: _t->SetHistory(); break;
        case 37: _t->WriteLogToFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MBsoftware::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MBsoftware.data,
    qt_meta_data_MBsoftware,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MBsoftware::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MBsoftware::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MBsoftware.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MBsoftware::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 38)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 38;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 38)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 38;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
