/****************************************************************************
** Meta object code from reading C++ file 'SMCdrive.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../SMCdrive.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SMCdrive.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SMCdrive_t {
    QByteArrayData data[13];
    char stringdata0[169];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SMCdrive_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SMCdrive_t qt_meta_stringdata_SMCdrive = {
    {
QT_MOC_LITERAL(0, 0, 8), // "SMCdrive"
QT_MOC_LITERAL(1, 9, 17), // "ViewTimerFunction"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 15), // "MotorOnFunction"
QT_MOC_LITERAL(4, 44, 18), // "GoToOriginFunction"
QT_MOC_LITERAL(5, 63, 13), // "ResetFunction"
QT_MOC_LITERAL(6, 77, 9), // "IdChanged"
QT_MOC_LITERAL(7, 87, 4), // "text"
QT_MOC_LITERAL(8, 92, 14), // "JogPlusPressed"
QT_MOC_LITERAL(9, 107, 15), // "JogMinusPressed"
QT_MOC_LITERAL(10, 123, 15), // "JogPlusReleased"
QT_MOC_LITERAL(11, 139, 16), // "JogMinusReleased"
QT_MOC_LITERAL(12, 156, 12) // "MoveFunction"

    },
    "SMCdrive\0ViewTimerFunction\0\0MotorOnFunction\0"
    "GoToOriginFunction\0ResetFunction\0"
    "IdChanged\0text\0JogPlusPressed\0"
    "JogMinusPressed\0JogPlusReleased\0"
    "JogMinusReleased\0MoveFunction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SMCdrive[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    0,   65,    2, 0x0a /* Public */,
       4,    0,   66,    2, 0x0a /* Public */,
       5,    0,   67,    2, 0x0a /* Public */,
       6,    1,   68,    2, 0x0a /* Public */,
       8,    0,   71,    2, 0x0a /* Public */,
       9,    0,   72,    2, 0x0a /* Public */,
      10,    0,   73,    2, 0x0a /* Public */,
      11,    0,   74,    2, 0x0a /* Public */,
      12,    0,   75,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SMCdrive::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SMCdrive *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ViewTimerFunction(); break;
        case 1: _t->MotorOnFunction(); break;
        case 2: _t->GoToOriginFunction(); break;
        case 3: _t->ResetFunction(); break;
        case 4: _t->IdChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->JogPlusPressed(); break;
        case 6: _t->JogMinusPressed(); break;
        case 7: _t->JogPlusReleased(); break;
        case 8: _t->JogMinusReleased(); break;
        case 9: _t->MoveFunction(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SMCdrive::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SMCdrive.data,
    qt_meta_data_SMCdrive,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SMCdrive::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SMCdrive::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SMCdrive.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SMCdrive::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
