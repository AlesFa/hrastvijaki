/****************************************************************************
** Meta object code from reading C++ file 'Camera.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../Camera.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Camera.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CCamera_t {
    QByteArrayData data[23];
    char stringdata0[258];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CCamera_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CCamera_t qt_meta_stringdata_CCamera = {
    {
QT_MOC_LITERAL(0, 0, 7), // "CCamera"
QT_MOC_LITERAL(1, 8, 16), // "frameReadySignal"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 2), // "id"
QT_MOC_LITERAL(4, 29, 10), // "imageIndex"
QT_MOC_LITERAL(5, 40, 15), // "showImageSignal"
QT_MOC_LITERAL(6, 56, 4), // "Next"
QT_MOC_LITERAL(7, 61, 8), // "Previous"
QT_MOC_LITERAL(8, 70, 6), // "ZoomIn"
QT_MOC_LITERAL(9, 77, 7), // "ZoomOut"
QT_MOC_LITERAL(10, 85, 9), // "ResetZoom"
QT_MOC_LITERAL(11, 95, 8), // "ShowLive"
QT_MOC_LITERAL(12, 104, 4), // "Live"
QT_MOC_LITERAL(13, 109, 15), // "ExposureChanged"
QT_MOC_LITERAL(14, 125, 5), // "value"
QT_MOC_LITERAL(15, 131, 11), // "GainChanged"
QT_MOC_LITERAL(16, 143, 14), // "XoffsetChanged"
QT_MOC_LITERAL(17, 158, 14), // "YoffsetChanged"
QT_MOC_LITERAL(18, 173, 12), // "SaveSettings"
QT_MOC_LITERAL(19, 186, 11), // "OnSaveImage"
QT_MOC_LITERAL(20, 198, 17), // "SliderExpoChanged"
QT_MOC_LITERAL(21, 216, 15), // "SpinExpoChanged"
QT_MOC_LITERAL(22, 232, 25) // "ImageIndexComboBoxChanged"

    },
    "CCamera\0frameReadySignal\0\0id\0imageIndex\0"
    "showImageSignal\0Next\0Previous\0ZoomIn\0"
    "ZoomOut\0ResetZoom\0ShowLive\0Live\0"
    "ExposureChanged\0value\0GainChanged\0"
    "XoffsetChanged\0YoffsetChanged\0"
    "SaveSettings\0OnSaveImage\0SliderExpoChanged\0"
    "SpinExpoChanged\0ImageIndexComboBoxChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CCamera[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  109,    2, 0x06 /* Public */,
       5,    1,  114,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  117,    2, 0x0a /* Public */,
       7,    0,  118,    2, 0x0a /* Public */,
       8,    0,  119,    2, 0x0a /* Public */,
       9,    0,  120,    2, 0x0a /* Public */,
      10,    0,  121,    2, 0x0a /* Public */,
      11,    0,  122,    2, 0x0a /* Public */,
      12,    0,  123,    2, 0x0a /* Public */,
      12,    1,  124,    2, 0x0a /* Public */,
      13,    1,  127,    2, 0x0a /* Public */,
      15,    1,  130,    2, 0x0a /* Public */,
      16,    1,  133,    2, 0x0a /* Public */,
      17,    1,  136,    2, 0x0a /* Public */,
      18,    0,  139,    2, 0x0a /* Public */,
      19,    0,  140,    2, 0x0a /* Public */,
      20,    1,  141,    2, 0x0a /* Public */,
      21,    1,  144,    2, 0x0a /* Public */,
      22,    1,  147,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::Int,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Double,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void, QMetaType::Double,   14,
    QMetaType::Void, QMetaType::Int,   14,

       0        // eod
};

void CCamera::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CCamera *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frameReadySignal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->showImageSignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->Next(); break;
        case 3: _t->Previous(); break;
        case 4: _t->ZoomIn(); break;
        case 5: _t->ZoomOut(); break;
        case 6: _t->ResetZoom(); break;
        case 7: _t->ShowLive(); break;
        case 8: _t->Live(); break;
        case 9: _t->Live((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->ExposureChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->GainChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->XoffsetChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->YoffsetChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->SaveSettings(); break;
        case 15: _t->OnSaveImage(); break;
        case 16: _t->SliderExpoChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->SpinExpoChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->ImageIndexComboBoxChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CCamera::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CCamera::frameReadySignal)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CCamera::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CCamera::showImageSignal)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CCamera::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_CCamera.data,
    qt_meta_data_CCamera,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CCamera::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CCamera::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CCamera.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int CCamera::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void CCamera::frameReadySignal(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CCamera::showImageSignal(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
