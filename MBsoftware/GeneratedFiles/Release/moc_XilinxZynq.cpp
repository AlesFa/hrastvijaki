/****************************************************************************
** Meta object code from reading C++ file 'XilinxZynq.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../XilinxZynq.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'XilinxZynq.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_XilinxZynq_t {
    QByteArrayData data[11];
    char stringdata0[133];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_XilinxZynq_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_XilinxZynq_t qt_meta_stringdata_XilinxZynq = {
    {
QT_MOC_LITERAL(0, 0, 10), // "XilinxZynq"
QT_MOC_LITERAL(1, 11, 15), // "OnNewConnection"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 19), // "OnConnectionTimeout"
QT_MOC_LITERAL(4, 48, 11), // "OnViewTimer"
QT_MOC_LITERAL(5, 60, 6), // "OnSend"
QT_MOC_LITERAL(6, 67, 15), // "OnClickedOutput"
QT_MOC_LITERAL(7, 83, 5), // "index"
QT_MOC_LITERAL(8, 89, 3), // "bit"
QT_MOC_LITERAL(9, 93, 20), // "OnClickedButtonLight"
QT_MOC_LITERAL(10, 114, 18) // "OnClickedButtonCam"

    },
    "XilinxZynq\0OnNewConnection\0\0"
    "OnConnectionTimeout\0OnViewTimer\0OnSend\0"
    "OnClickedOutput\0index\0bit\0"
    "OnClickedButtonLight\0OnClickedButtonCam"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_XilinxZynq[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    2,   53,    2, 0x08 /* Private */,
       9,    1,   58,    2, 0x08 /* Private */,
      10,    1,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void XilinxZynq::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<XilinxZynq *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnNewConnection(); break;
        case 1: _t->OnConnectionTimeout(); break;
        case 2: _t->OnViewTimer(); break;
        case 3: _t->OnSend(); break;
        case 4: _t->OnClickedOutput((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->OnClickedButtonLight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->OnClickedButtonCam((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject XilinxZynq::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_XilinxZynq.data,
    qt_meta_data_XilinxZynq,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *XilinxZynq::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *XilinxZynq::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_XilinxZynq.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int XilinxZynq::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
