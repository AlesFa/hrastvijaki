/****************************************************************************
** Meta object code from reading C++ file 'imageProcessing.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../imageProcessing.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'imageProcessing.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_imageProcessing_t {
    QByteArrayData data[97];
    char stringdata0[1429];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_imageProcessing_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_imageProcessing_t qt_meta_stringdata_imageProcessing = {
    {
QT_MOC_LITERAL(0, 0, 15), // "imageProcessing"
QT_MOC_LITERAL(1, 16, 11), // "imagesReady"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 18), // "measurePieceSignal"
QT_MOC_LITERAL(4, 48, 5), // "nrCam"
QT_MOC_LITERAL(5, 54, 10), // "imageIndex"
QT_MOC_LITERAL(6, 65, 16), // "OnPressedImageUp"
QT_MOC_LITERAL(7, 82, 18), // "OnPressedImageDown"
QT_MOC_LITERAL(8, 101, 14), // "OnPressedCamUp"
QT_MOC_LITERAL(9, 116, 16), // "OnPressedCamDown"
QT_MOC_LITERAL(10, 133, 22), // "ConvertImageForDisplay"
QT_MOC_LITERAL(11, 156, 11), // "imageNumber"
QT_MOC_LITERAL(12, 168, 10), // "ShowImages"
QT_MOC_LITERAL(13, 179, 17), // "ResizeDisplayRect"
QT_MOC_LITERAL(14, 197, 13), // "OnProcessCam0"
QT_MOC_LITERAL(15, 211, 13), // "OnProcessCam1"
QT_MOC_LITERAL(16, 225, 13), // "OnProcessCam2"
QT_MOC_LITERAL(17, 239, 13), // "OnProcessCam3"
QT_MOC_LITERAL(18, 253, 13), // "OnProcessCam4"
QT_MOC_LITERAL(19, 267, 33), // "OnDoneEditingLineInsertImageI..."
QT_MOC_LITERAL(20, 301, 21), // "onPressedAddParameter"
QT_MOC_LITERAL(21, 323, 25), // "onPressedRemoveParameters"
QT_MOC_LITERAL(22, 349, 25), // "onPressedUpdateParameters"
QT_MOC_LITERAL(23, 375, 26), // "onPressedSurfaceInspection"
QT_MOC_LITERAL(24, 402, 21), // "OnPressedCreateRubber"
QT_MOC_LITERAL(25, 424, 21), // "OnPressedResizeRubber"
QT_MOC_LITERAL(26, 446, 21), // "OnPressedSelectRubber"
QT_MOC_LITERAL(27, 468, 30), // "OnPressedTestIntensityFunction"
QT_MOC_LITERAL(28, 499, 11), // "OnViewTimer"
QT_MOC_LITERAL(29, 511, 23), // "AddnewHeightMeasurement"
QT_MOC_LITERAL(30, 535, 24), // "LineDeviationMeasurement"
QT_MOC_LITERAL(31, 560, 39), // "SaveSelectedDynamicFunctionAs..."
QT_MOC_LITERAL(32, 600, 36), // "EditSelectedDynamicFunctionPa..."
QT_MOC_LITERAL(33, 637, 30), // "DynamicFunctionMeasureDistance"
QT_MOC_LITERAL(34, 668, 3), // "cam"
QT_MOC_LITERAL(35, 672, 10), // "imageindex"
QT_MOC_LITERAL(36, 683, 4), // "draw"
QT_MOC_LITERAL(37, 688, 5), // "rect1"
QT_MOC_LITERAL(38, 694, 5), // "rect2"
QT_MOC_LITERAL(39, 700, 8), // "horOrVer"
QT_MOC_LITERAL(40, 709, 7), // "rec1Dir"
QT_MOC_LITERAL(41, 717, 17), // "rect1BlackToWhite"
QT_MOC_LITERAL(42, 735, 7), // "rec2Dir"
QT_MOC_LITERAL(43, 743, 17), // "rect2BlackToWhite"
QT_MOC_LITERAL(44, 761, 4), // "type"
QT_MOC_LITERAL(45, 766, 6), // "filter"
QT_MOC_LITERAL(46, 773, 9), // "threshold"
QT_MOC_LITERAL(47, 783, 8), // "drawType"
QT_MOC_LITERAL(48, 792, 12), // "drawLocation"
QT_MOC_LITERAL(49, 805, 5), // "color"
QT_MOC_LITERAL(50, 811, 8), // "xOffset1"
QT_MOC_LITERAL(51, 820, 8), // "yOffset1"
QT_MOC_LITERAL(52, 829, 8), // "xoffset2"
QT_MOC_LITERAL(53, 838, 8), // "yoffset2"
QT_MOC_LITERAL(54, 847, 8), // "testMode"
QT_MOC_LITERAL(55, 856, 9), // "currParam"
QT_MOC_LITERAL(56, 866, 28), // "DynamicFunctionLineDeviation"
QT_MOC_LITERAL(57, 895, 4), // "rect"
QT_MOC_LITERAL(58, 900, 12), // "recDirection"
QT_MOC_LITERAL(59, 913, 16), // "rectBlackToWhite"
QT_MOC_LITERAL(60, 930, 7), // "xOffset"
QT_MOC_LITERAL(61, 938, 7), // "yOffset"
QT_MOC_LITERAL(62, 946, 17), // "ProcessingCamera0"
QT_MOC_LITERAL(63, 964, 2), // "id"
QT_MOC_LITERAL(64, 967, 17), // "ProcessingCamera1"
QT_MOC_LITERAL(65, 985, 17), // "ProcessingCamera2"
QT_MOC_LITERAL(66, 1003, 17), // "ProcessingCamera3"
QT_MOC_LITERAL(67, 1021, 17), // "ProcessingCamera4"
QT_MOC_LITERAL(68, 1039, 12), // "CheckDefects"
QT_MOC_LITERAL(69, 1052, 10), // "indexImage"
QT_MOC_LITERAL(70, 1063, 7), // "nrPiece"
QT_MOC_LITERAL(71, 1071, 14), // "InspectSurface"
QT_MOC_LITERAL(72, 1086, 24), // "FindThreadAndOrientation"
QT_MOC_LITERAL(73, 1111, 22), // "FindInnerAndOuterRadij"
QT_MOC_LITERAL(74, 1134, 14), // "IsPiecePresent"
QT_MOC_LITERAL(75, 1149, 4), // "area"
QT_MOC_LITERAL(76, 1154, 18), // "thresHoldIntensity"
QT_MOC_LITERAL(77, 1173, 8), // "nrPoints"
QT_MOC_LITERAL(78, 1182, 16), // "FindTopAndBottom"
QT_MOC_LITERAL(79, 1199, 16), // "FindLeftAndRight"
QT_MOC_LITERAL(80, 1216, 20), // "FindTopSurfaceAndDim"
QT_MOC_LITERAL(81, 1237, 10), // "findZareze"
QT_MOC_LITERAL(82, 1248, 9), // "OmitAngle"
QT_MOC_LITERAL(83, 1258, 8), // "alphaRad"
QT_MOC_LITERAL(84, 1267, 10), // "OmitAngle2"
QT_MOC_LITERAL(85, 1278, 15), // "OmitAngleZareza"
QT_MOC_LITERAL(86, 1294, 9), // "ClearDraw"
QT_MOC_LITERAL(87, 1304, 17), // "ClearFunctionList"
QT_MOC_LITERAL(88, 1322, 16), // "SetCurrentBuffer"
QT_MOC_LITERAL(89, 1339, 10), // "dispWindow"
QT_MOC_LITERAL(90, 1350, 9), // "dispImage"
QT_MOC_LITERAL(91, 1360, 7), // "ZoomOut"
QT_MOC_LITERAL(92, 1368, 6), // "ZoomIn"
QT_MOC_LITERAL(93, 1375, 9), // "ZoomReset"
QT_MOC_LITERAL(94, 1385, 11), // "OnLoadImage"
QT_MOC_LITERAL(95, 1397, 19), // "OnLoadMultipleImage"
QT_MOC_LITERAL(96, 1417, 11) // "OnSaveImage"

    },
    "imageProcessing\0imagesReady\0\0"
    "measurePieceSignal\0nrCam\0imageIndex\0"
    "OnPressedImageUp\0OnPressedImageDown\0"
    "OnPressedCamUp\0OnPressedCamDown\0"
    "ConvertImageForDisplay\0imageNumber\0"
    "ShowImages\0ResizeDisplayRect\0OnProcessCam0\0"
    "OnProcessCam1\0OnProcessCam2\0OnProcessCam3\0"
    "OnProcessCam4\0OnDoneEditingLineInsertImageIndex\0"
    "onPressedAddParameter\0onPressedRemoveParameters\0"
    "onPressedUpdateParameters\0"
    "onPressedSurfaceInspection\0"
    "OnPressedCreateRubber\0OnPressedResizeRubber\0"
    "OnPressedSelectRubber\0"
    "OnPressedTestIntensityFunction\0"
    "OnViewTimer\0AddnewHeightMeasurement\0"
    "LineDeviationMeasurement\0"
    "SaveSelectedDynamicFunctionAsMeasurment\0"
    "EditSelectedDynamicFunctionParameter\0"
    "DynamicFunctionMeasureDistance\0cam\0"
    "imageindex\0draw\0rect1\0rect2\0horOrVer\0"
    "rec1Dir\0rect1BlackToWhite\0rec2Dir\0"
    "rect2BlackToWhite\0type\0filter\0threshold\0"
    "drawType\0drawLocation\0color\0xOffset1\0"
    "yOffset1\0xoffset2\0yoffset2\0testMode\0"
    "currParam\0DynamicFunctionLineDeviation\0"
    "rect\0recDirection\0rectBlackToWhite\0"
    "xOffset\0yOffset\0ProcessingCamera0\0id\0"
    "ProcessingCamera1\0ProcessingCamera2\0"
    "ProcessingCamera3\0ProcessingCamera4\0"
    "CheckDefects\0indexImage\0nrPiece\0"
    "InspectSurface\0FindThreadAndOrientation\0"
    "FindInnerAndOuterRadij\0IsPiecePresent\0"
    "area\0thresHoldIntensity\0nrPoints\0"
    "FindTopAndBottom\0FindLeftAndRight\0"
    "FindTopSurfaceAndDim\0findZareze\0"
    "OmitAngle\0alphaRad\0OmitAngle2\0"
    "OmitAngleZareza\0ClearDraw\0ClearFunctionList\0"
    "SetCurrentBuffer\0dispWindow\0dispImage\0"
    "ZoomOut\0ZoomIn\0ZoomReset\0OnLoadImage\0"
    "OnLoadMultipleImage\0OnSaveImage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_imageProcessing[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      56,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  294,    2, 0x06 /* Public */,
       3,    2,  295,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  300,    2, 0x0a /* Public */,
       7,    0,  301,    2, 0x0a /* Public */,
       8,    0,  302,    2, 0x0a /* Public */,
       9,    0,  303,    2, 0x0a /* Public */,
      10,    1,  304,    2, 0x0a /* Public */,
      12,    0,  307,    2, 0x0a /* Public */,
      13,    0,  308,    2, 0x0a /* Public */,
      14,    0,  309,    2, 0x0a /* Public */,
      15,    0,  310,    2, 0x0a /* Public */,
      16,    0,  311,    2, 0x0a /* Public */,
      17,    0,  312,    2, 0x0a /* Public */,
      18,    0,  313,    2, 0x0a /* Public */,
      19,    0,  314,    2, 0x0a /* Public */,
      20,    0,  315,    2, 0x0a /* Public */,
      21,    0,  316,    2, 0x0a /* Public */,
      22,    0,  317,    2, 0x0a /* Public */,
      23,    0,  318,    2, 0x0a /* Public */,
      24,    0,  319,    2, 0x0a /* Public */,
      25,    0,  320,    2, 0x0a /* Public */,
      26,    0,  321,    2, 0x0a /* Public */,
      27,    0,  322,    2, 0x0a /* Public */,
      28,    0,  323,    2, 0x0a /* Public */,
      29,    0,  324,    2, 0x0a /* Public */,
      30,    0,  325,    2, 0x0a /* Public */,
      31,    0,  326,    2, 0x0a /* Public */,
      32,    0,  327,    2, 0x0a /* Public */,
      33,   22,  328,    2, 0x0a /* Public */,
      56,   14,  373,    2, 0x0a /* Public */,
      62,    3,  402,    2, 0x0a /* Public */,
      64,    3,  409,    2, 0x0a /* Public */,
      65,    3,  416,    2, 0x0a /* Public */,
      66,    3,  423,    2, 0x0a /* Public */,
      67,    3,  430,    2, 0x0a /* Public */,
      68,    3,  437,    2, 0x0a /* Public */,
      71,    3,  444,    2, 0x0a /* Public */,
      72,    3,  451,    2, 0x0a /* Public */,
      73,    3,  458,    2, 0x0a /* Public */,
      74,    6,  465,    2, 0x0a /* Public */,
      78,    3,  478,    2, 0x0a /* Public */,
      79,    3,  485,    2, 0x0a /* Public */,
      80,    3,  492,    2, 0x0a /* Public */,
      81,    3,  499,    2, 0x0a /* Public */,
      82,    1,  506,    2, 0x0a /* Public */,
      84,    1,  509,    2, 0x0a /* Public */,
      85,    1,  512,    2, 0x0a /* Public */,
      86,    0,  515,    2, 0x0a /* Public */,
      87,    0,  516,    2, 0x0a /* Public */,
      88,    2,  517,    2, 0x0a /* Public */,
      91,    0,  522,    2, 0x0a /* Public */,
      92,    0,  523,    2, 0x0a /* Public */,
      93,    0,  524,    2, 0x0a /* Public */,
      94,    0,  525,    2, 0x0a /* Public */,
      95,    0,  526,    2, 0x0a /* Public */,
      96,    0,  527,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    4,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Float, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::QRect, QMetaType::QRect, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   34,   35,   36,   37,   38,   39,   40,   41,   42,   43,   44,   45,   46,   47,   48,   49,   50,   51,   52,   53,   54,   55,
    QMetaType::Float, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::QRect, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   34,    5,   36,   57,   58,   59,   46,   44,   45,   49,   60,   61,   54,   55,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   69,   70,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,    5,   70,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Bool, QMetaType::Int, QMetaType::Int, QMetaType::QRect, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   75,   76,   77,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   63,    5,   36,
    QMetaType::Bool, QMetaType::Float,   83,
    QMetaType::Bool, QMetaType::Float,   83,
    QMetaType::Bool, QMetaType::Float,   83,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   89,   90,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void imageProcessing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<imageProcessing *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->imagesReady(); break;
        case 1: _t->measurePieceSignal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnPressedImageUp(); break;
        case 3: _t->OnPressedImageDown(); break;
        case 4: _t->OnPressedCamUp(); break;
        case 5: _t->OnPressedCamDown(); break;
        case 6: { int _r = _t->ConvertImageForDisplay((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->ShowImages(); break;
        case 8: _t->ResizeDisplayRect(); break;
        case 9: _t->OnProcessCam0(); break;
        case 10: _t->OnProcessCam1(); break;
        case 11: _t->OnProcessCam2(); break;
        case 12: _t->OnProcessCam3(); break;
        case 13: _t->OnProcessCam4(); break;
        case 14: _t->OnDoneEditingLineInsertImageIndex(); break;
        case 15: _t->onPressedAddParameter(); break;
        case 16: _t->onPressedRemoveParameters(); break;
        case 17: _t->onPressedUpdateParameters(); break;
        case 18: _t->onPressedSurfaceInspection(); break;
        case 19: _t->OnPressedCreateRubber(); break;
        case 20: _t->OnPressedResizeRubber(); break;
        case 21: _t->OnPressedSelectRubber(); break;
        case 22: _t->OnPressedTestIntensityFunction(); break;
        case 23: _t->OnViewTimer(); break;
        case 24: _t->AddnewHeightMeasurement(); break;
        case 25: _t->LineDeviationMeasurement(); break;
        case 26: { int _r = _t->SaveSelectedDynamicFunctionAsMeasurment();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 27: { int _r = _t->EditSelectedDynamicFunctionParameter();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 28: { float _r = _t->DynamicFunctionMeasureDistance((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< QRect(*)>(_a[4])),(*reinterpret_cast< QRect(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7])),(*reinterpret_cast< int(*)>(_a[8])),(*reinterpret_cast< int(*)>(_a[9])),(*reinterpret_cast< int(*)>(_a[10])),(*reinterpret_cast< int(*)>(_a[11])),(*reinterpret_cast< int(*)>(_a[12])),(*reinterpret_cast< int(*)>(_a[13])),(*reinterpret_cast< int(*)>(_a[14])),(*reinterpret_cast< int(*)>(_a[15])),(*reinterpret_cast< int(*)>(_a[16])),(*reinterpret_cast< int(*)>(_a[17])),(*reinterpret_cast< int(*)>(_a[18])),(*reinterpret_cast< int(*)>(_a[19])),(*reinterpret_cast< int(*)>(_a[20])),(*reinterpret_cast< int(*)>(_a[21])),(*reinterpret_cast< int(*)>(_a[22])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 29: { float _r = _t->DynamicFunctionLineDeviation((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< QRect(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< int(*)>(_a[7])),(*reinterpret_cast< int(*)>(_a[8])),(*reinterpret_cast< int(*)>(_a[9])),(*reinterpret_cast< int(*)>(_a[10])),(*reinterpret_cast< int(*)>(_a[11])),(*reinterpret_cast< int(*)>(_a[12])),(*reinterpret_cast< int(*)>(_a[13])),(*reinterpret_cast< int(*)>(_a[14])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 30: { int _r = _t->ProcessingCamera0((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 31: { int _r = _t->ProcessingCamera1((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 32: { int _r = _t->ProcessingCamera2((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 33: { int _r = _t->ProcessingCamera3((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 34: { int _r = _t->ProcessingCamera4((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 35: { int _r = _t->CheckDefects((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 36: { int _r = _t->InspectSurface((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 37: { int _r = _t->FindThreadAndOrientation((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 38: { int _r = _t->FindInnerAndOuterRadij((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 39: { bool _r = _t->IsPiecePresent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< QRect(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 40: { int _r = _t->FindTopAndBottom((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 41: { int _r = _t->FindLeftAndRight((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 42: { int _r = _t->FindTopSurfaceAndDim((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 43: { int _r = _t->findZareze((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 44: { bool _r = _t->OmitAngle((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 45: { bool _r = _t->OmitAngle2((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 46: { bool _r = _t->OmitAngleZareza((*reinterpret_cast< float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 47: _t->ClearDraw(); break;
        case 48: _t->ClearFunctionList(); break;
        case 49: _t->SetCurrentBuffer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 50: _t->ZoomOut(); break;
        case 51: _t->ZoomIn(); break;
        case 52: _t->ZoomReset(); break;
        case 53: _t->OnLoadImage(); break;
        case 54: _t->OnLoadMultipleImage(); break;
        case 55: _t->OnSaveImage(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (imageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&imageProcessing::imagesReady)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (imageProcessing::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&imageProcessing::measurePieceSignal)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject imageProcessing::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_imageProcessing.data,
    qt_meta_data_imageProcessing,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *imageProcessing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *imageProcessing::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_imageProcessing.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int imageProcessing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 56)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 56;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 56)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 56;
    }
    return _id;
}

// SIGNAL 0
void imageProcessing::imagesReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void imageProcessing::measurePieceSignal(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
