/********************************************************************************
** Form generated from reading UI file 'Camera.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERA_H
#define UI_CAMERA_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Camera
{
public:
    QGridLayout *gridLayout_2;
    QGraphicsView *imageView;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPushButton *buttonSaveSettings;
    QPushButton *buttonLive;
    QSpacerItem *horizontalSpacer_4;
    QSlider *sliderExpo;
    QGroupBox *groupBoxIntensity;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelin;
    QLabel *labelB;
    QLabel *labelG;
    QLabel *labelR;
    QLabel *labelGain;
    QGroupBox *groupBoxCoordinates;
    QHBoxLayout *horizontalLayout;
    QLabel *labelX;
    QLabel *labelY;
    QPushButton *buttonReset;
    QPushButton *buttonSaveImage;
    QPushButton *buttonZoomOut;
    QSlider *sliderOffsetY;
    QLabel *labelXoffset;
    QLabel *labelZoom;
    QSlider *sliderOffsetX;
    QLabel *labelExpo;
    QLabel *labelYoffset;
    QSlider *sliderGain;
    QPushButton *buttonZoomIn;
    QSpinBox *SpinOffsetX;
    QSpinBox *spinOffsetY;
    QDoubleSpinBox *spinExpo;
    QSpinBox *spinGain;
    QComboBox *comboBoxImageIndex;
    QLabel *labelManufacturer;

    void setupUi(QWidget *Camera)
    {
        if (Camera->objectName().isEmpty())
            Camera->setObjectName(QString::fromUtf8("Camera"));
        Camera->resize(1506, 1002);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Camera->sizePolicy().hasHeightForWidth());
        Camera->setSizePolicy(sizePolicy);
        Camera->setMinimumSize(QSize(1024, 0));
        Camera->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QString::fromUtf8("MBicon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Camera->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(Camera);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        imageView = new QGraphicsView(Camera);
        imageView->setObjectName(QString::fromUtf8("imageView"));
        imageView->setEnabled(true);
        imageView->setMinimumSize(QSize(0, 500));
        imageView->setMaximumSize(QSize(16777215, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Comic Sans MS"));
        font.setPointSize(20);
        imageView->setFont(font);
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        imageView->setTabletTracking(true);

        gridLayout_2->addWidget(imageView, 4, 0, 1, 1);

        groupBox = new QGroupBox(Camera);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(640, 250));
        groupBox->setMaximumSize(QSize(1920, 1080));
        groupBox->setCursor(QCursor(Qt::ArrowCursor));
        groupBox->setMouseTracking(true);
        groupBox->setTabletTracking(true);
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        buttonSaveSettings = new QPushButton(groupBox);
        buttonSaveSettings->setObjectName(QString::fromUtf8("buttonSaveSettings"));
        buttonSaveSettings->setMinimumSize(QSize(0, 30));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        buttonSaveSettings->setFont(font1);
        buttonSaveSettings->setAutoFillBackground(false);

        gridLayout->addWidget(buttonSaveSettings, 0, 13, 1, 1);

        buttonLive = new QPushButton(groupBox);
        buttonLive->setObjectName(QString::fromUtf8("buttonLive"));
        buttonLive->setMinimumSize(QSize(200, 40));
        buttonLive->setMaximumSize(QSize(100, 40));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        buttonLive->setFont(font2);
        buttonLive->setAutoFillBackground(true);
        buttonLive->setStyleSheet(QString::fromUtf8(""));
        buttonLive->setCheckable(false);
        buttonLive->setAutoDefault(false);
        buttonLive->setFlat(false);

        gridLayout->addWidget(buttonLive, 6, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(37, 37, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 1, 3, 1, 1);

        sliderExpo = new QSlider(groupBox);
        sliderExpo->setObjectName(QString::fromUtf8("sliderExpo"));
        sizePolicy1.setHeightForWidth(sliderExpo->sizePolicy().hasHeightForWidth());
        sliderExpo->setSizePolicy(sizePolicy1);
        sliderExpo->setMinimumSize(QSize(400, 25));
        sliderExpo->setMaximumSize(QSize(250, 30));
        sliderExpo->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderExpo, 2, 4, 1, 1);

        groupBoxIntensity = new QGroupBox(groupBox);
        groupBoxIntensity->setObjectName(QString::fromUtf8("groupBoxIntensity"));
        groupBoxIntensity->setMinimumSize(QSize(0, 60));
        horizontalLayout_2 = new QHBoxLayout(groupBoxIntensity);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelin = new QLabel(groupBoxIntensity);
        labelin->setObjectName(QString::fromUtf8("labelin"));
        labelin->setMinimumSize(QSize(100, 0));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        labelin->setFont(font3);
        labelin->setFrameShape(QFrame::Box);
        labelin->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelin);

        labelB = new QLabel(groupBoxIntensity);
        labelB->setObjectName(QString::fromUtf8("labelB"));
        labelB->setMinimumSize(QSize(100, 0));
        labelB->setFont(font3);
        labelB->setFrameShape(QFrame::Box);
        labelB->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelB);

        labelG = new QLabel(groupBoxIntensity);
        labelG->setObjectName(QString::fromUtf8("labelG"));
        labelG->setMinimumSize(QSize(100, 0));
        labelG->setMaximumSize(QSize(200, 16777215));
        labelG->setFont(font3);
        labelG->setFrameShape(QFrame::Box);
        labelG->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelG);

        labelR = new QLabel(groupBoxIntensity);
        labelR->setObjectName(QString::fromUtf8("labelR"));
        labelR->setMinimumSize(QSize(100, 0));
        labelR->setFont(font3);
        labelR->setFrameShape(QFrame::Box);
        labelR->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelR);


        gridLayout->addWidget(groupBoxIntensity, 5, 5, 3, 7);

        labelGain = new QLabel(groupBox);
        labelGain->setObjectName(QString::fromUtf8("labelGain"));
        labelGain->setFont(font1);

        gridLayout->addWidget(labelGain, 1, 2, 1, 1);

        groupBoxCoordinates = new QGroupBox(groupBox);
        groupBoxCoordinates->setObjectName(QString::fromUtf8("groupBoxCoordinates"));
        groupBoxCoordinates->setMinimumSize(QSize(0, 60));
        groupBoxCoordinates->setMaximumSize(QSize(16777215, 60));
        horizontalLayout = new QHBoxLayout(groupBoxCoordinates);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelX = new QLabel(groupBoxCoordinates);
        labelX->setObjectName(QString::fromUtf8("labelX"));
        labelX->setMinimumSize(QSize(100, 0));
        labelX->setMaximumSize(QSize(200, 16777215));
        labelX->setFont(font3);
        labelX->setFrameShape(QFrame::Box);
        labelX->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelX);

        labelY = new QLabel(groupBoxCoordinates);
        labelY->setObjectName(QString::fromUtf8("labelY"));
        labelY->setMinimumSize(QSize(100, 0));
        labelY->setMaximumSize(QSize(200, 16777215));
        labelY->setFont(font3);
        labelY->setFrameShape(QFrame::Box);
        labelY->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelY);


        gridLayout->addWidget(groupBoxCoordinates, 5, 12, 3, 2);

        buttonReset = new QPushButton(groupBox);
        buttonReset->setObjectName(QString::fromUtf8("buttonReset"));
        buttonReset->setFont(font1);

        gridLayout->addWidget(buttonReset, 4, 12, 1, 1);

        buttonSaveImage = new QPushButton(groupBox);
        buttonSaveImage->setObjectName(QString::fromUtf8("buttonSaveImage"));
        buttonSaveImage->setMinimumSize(QSize(0, 30));
        buttonSaveImage->setFont(font1);
        buttonSaveImage->setCheckable(false);
        buttonSaveImage->setChecked(false);
        buttonSaveImage->setAutoRepeat(false);
        buttonSaveImage->setAutoExclusive(false);
        buttonSaveImage->setAutoDefault(false);
        buttonSaveImage->setFlat(false);

        gridLayout->addWidget(buttonSaveImage, 1, 13, 1, 1);

        buttonZoomOut = new QPushButton(groupBox);
        buttonZoomOut->setObjectName(QString::fromUtf8("buttonZoomOut"));
        buttonZoomOut->setFont(font1);

        gridLayout->addWidget(buttonZoomOut, 4, 11, 1, 1);

        sliderOffsetY = new QSlider(groupBox);
        sliderOffsetY->setObjectName(QString::fromUtf8("sliderOffsetY"));
        sliderOffsetY->setMinimumSize(QSize(400, 25));
        sliderOffsetY->setSingleStep(14);
        sliderOffsetY->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderOffsetY, 4, 4, 1, 1);

        labelXoffset = new QLabel(groupBox);
        labelXoffset->setObjectName(QString::fromUtf8("labelXoffset"));
        labelXoffset->setFont(font1);

        gridLayout->addWidget(labelXoffset, 3, 2, 1, 1);

        labelZoom = new QLabel(groupBox);
        labelZoom->setObjectName(QString::fromUtf8("labelZoom"));
        labelZoom->setMinimumSize(QSize(100, 0));
        labelZoom->setFont(font3);
        labelZoom->setFrameShape(QFrame::Box);
        labelZoom->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(labelZoom, 4, 13, 1, 1);

        sliderOffsetX = new QSlider(groupBox);
        sliderOffsetX->setObjectName(QString::fromUtf8("sliderOffsetX"));
        sliderOffsetX->setMinimumSize(QSize(400, 25));
        sliderOffsetX->setMaximumSize(QSize(250, 16777215));
        sliderOffsetX->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderOffsetX, 3, 4, 1, 3);

        labelExpo = new QLabel(groupBox);
        labelExpo->setObjectName(QString::fromUtf8("labelExpo"));
        labelExpo->setFont(font1);

        gridLayout->addWidget(labelExpo, 2, 2, 1, 1);

        labelYoffset = new QLabel(groupBox);
        labelYoffset->setObjectName(QString::fromUtf8("labelYoffset"));
        labelYoffset->setFont(font1);

        gridLayout->addWidget(labelYoffset, 4, 2, 1, 1);

        sliderGain = new QSlider(groupBox);
        sliderGain->setObjectName(QString::fromUtf8("sliderGain"));
        sizePolicy1.setHeightForWidth(sliderGain->sizePolicy().hasHeightForWidth());
        sliderGain->setSizePolicy(sizePolicy1);
        sliderGain->setMinimumSize(QSize(400, 25));
        sliderGain->setMaximumSize(QSize(250, 30));
        sliderGain->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderGain, 1, 4, 1, 1);

        buttonZoomIn = new QPushButton(groupBox);
        buttonZoomIn->setObjectName(QString::fromUtf8("buttonZoomIn"));
        buttonZoomIn->setFont(font1);

        gridLayout->addWidget(buttonZoomIn, 4, 10, 1, 1);

        SpinOffsetX = new QSpinBox(groupBox);
        SpinOffsetX->setObjectName(QString::fromUtf8("SpinOffsetX"));

        gridLayout->addWidget(SpinOffsetX, 3, 7, 1, 1);

        spinOffsetY = new QSpinBox(groupBox);
        spinOffsetY->setObjectName(QString::fromUtf8("spinOffsetY"));

        gridLayout->addWidget(spinOffsetY, 4, 7, 1, 1);

        spinExpo = new QDoubleSpinBox(groupBox);
        spinExpo->setObjectName(QString::fromUtf8("spinExpo"));

        gridLayout->addWidget(spinExpo, 2, 7, 1, 1);

        spinGain = new QSpinBox(groupBox);
        spinGain->setObjectName(QString::fromUtf8("spinGain"));

        gridLayout->addWidget(spinGain, 1, 7, 1, 1);

        comboBoxImageIndex = new QComboBox(groupBox);
        comboBoxImageIndex->setObjectName(QString::fromUtf8("comboBoxImageIndex"));

        gridLayout->addWidget(comboBoxImageIndex, 0, 12, 1, 1);

        labelManufacturer = new QLabel(groupBox);
        labelManufacturer->setObjectName(QString::fromUtf8("labelManufacturer"));
        labelManufacturer->setMinimumSize(QSize(0, 30));
        labelManufacturer->setMaximumSize(QSize(16777215, 20));
        labelManufacturer->setFont(font1);
        labelManufacturer->setFrameShape(QFrame::Box);
        labelManufacturer->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(labelManufacturer, 0, 0, 1, 4);


        gridLayout_2->addWidget(groupBox, 1, 0, 1, 1);


        retranslateUi(Camera);

        buttonLive->setDefault(false);
        buttonSaveImage->setDefault(false);


        QMetaObject::connectSlotsByName(Camera);
    } // setupUi

    void retranslateUi(QWidget *Camera)
    {
        Camera->setWindowTitle(QCoreApplication::translate("Camera", "Form", nullptr));
        groupBox->setTitle(QString());
        buttonSaveSettings->setText(QCoreApplication::translate("Camera", "SaveSettings", nullptr));
        buttonLive->setText(QCoreApplication::translate("Camera", "EnableLive", nullptr));
        groupBoxIntensity->setTitle(QCoreApplication::translate("Camera", "Intensity:", nullptr));
        labelin->setText(QString());
        labelB->setText(QString());
        labelG->setText(QString());
        labelR->setText(QString());
        labelGain->setText(QCoreApplication::translate("Camera", "Gain:", nullptr));
        groupBoxCoordinates->setTitle(QCoreApplication::translate("Camera", "Coordinates:", nullptr));
        labelX->setText(QString());
        labelY->setText(QString());
        buttonReset->setText(QCoreApplication::translate("Camera", "Reset Zoom", nullptr));
        buttonSaveImage->setText(QCoreApplication::translate("Camera", "SaveImage", nullptr));
        buttonZoomOut->setText(QCoreApplication::translate("Camera", "ZoomOut", nullptr));
        labelXoffset->setText(QCoreApplication::translate("Camera", "X offest:", nullptr));
        labelZoom->setText(QString());
        labelExpo->setText(QCoreApplication::translate("Camera", "Expositure:", nullptr));
        labelYoffset->setText(QCoreApplication::translate("Camera", "Y offset:", nullptr));
        buttonZoomIn->setText(QCoreApplication::translate("Camera", "ZoomIn", nullptr));
        labelManufacturer->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Camera: public Ui_Camera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERA_H
