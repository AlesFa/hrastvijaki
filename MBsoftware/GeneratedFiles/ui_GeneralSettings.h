/********************************************************************************
** Form generated from reading UI file 'GeneralSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENERALSETTINGS_H
#define UI_GENERALSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GeneralSettings
{
public:
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox_5;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_16;
    QCheckBox *checkDim;
    QHBoxLayout *horizontalLayout_18;
    QRadioButton *radioButtonS1;
    QLabel *label_18;
    QHBoxLayout *horizontalLayout_20;
    QRadioButton *radioButtonS2;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_21;
    QRadioButton *radioButtonS12;
    QLabel *label_21;
    QGroupBox *groupBox;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_19;
    QLineEdit *editMaxSlabi;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_22;
    QLineEdit *editMaxSlabiNavor;

    void setupUi(QWidget *GeneralSettings)
    {
        if (GeneralSettings->objectName().isEmpty())
            GeneralSettings->setObjectName(QString::fromUtf8("GeneralSettings"));
        GeneralSettings->resize(338, 324);
        buttonBox = new QDialogButtonBox(GeneralSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 290, 301, 31));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
        buttonBox->setSizePolicy(sizePolicy);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox_5 = new QGroupBox(GeneralSettings);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 10, 321, 181));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        groupBox_5->setFont(font);
        verticalLayoutWidget = new QWidget(groupBox_5);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 30, 301, 141));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_16 = new QLabel(verticalLayoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(120, 0));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        label_16->setFont(font1);

        horizontalLayout_16->addWidget(label_16);

        checkDim = new QCheckBox(verticalLayoutWidget);
        checkDim->setObjectName(QString::fromUtf8("checkDim"));
        checkDim->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_16->addWidget(checkDim);


        verticalLayout->addLayout(horizontalLayout_16);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        radioButtonS1 = new QRadioButton(verticalLayoutWidget);
        radioButtonS1->setObjectName(QString::fromUtf8("radioButtonS1"));
        radioButtonS1->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_18->addWidget(radioButtonS1);

        label_18 = new QLabel(verticalLayoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(120, 0));
        label_18->setFont(font1);

        horizontalLayout_18->addWidget(label_18);


        verticalLayout->addLayout(horizontalLayout_18);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        radioButtonS2 = new QRadioButton(verticalLayoutWidget);
        radioButtonS2->setObjectName(QString::fromUtf8("radioButtonS2"));
        radioButtonS2->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_20->addWidget(radioButtonS2);

        label_20 = new QLabel(verticalLayoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(120, 0));
        label_20->setFont(font1);

        horizontalLayout_20->addWidget(label_20);


        verticalLayout->addLayout(horizontalLayout_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        radioButtonS12 = new QRadioButton(verticalLayoutWidget);
        radioButtonS12->setObjectName(QString::fromUtf8("radioButtonS12"));
        radioButtonS12->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_21->addWidget(radioButtonS12);

        label_21 = new QLabel(verticalLayoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(120, 0));
        label_21->setFont(font1);

        horizontalLayout_21->addWidget(label_21);


        verticalLayout->addLayout(horizontalLayout_21);

        groupBox = new QGroupBox(GeneralSettings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 190, 321, 91));
        verticalLayoutWidget_2 = new QWidget(groupBox);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(0, 20, 311, 61));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_19 = new QLabel(verticalLayoutWidget_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(220, 0));
        label_19->setFont(font1);

        horizontalLayout->addWidget(label_19);

        editMaxSlabi = new QLineEdit(verticalLayoutWidget_2);
        editMaxSlabi->setObjectName(QString::fromUtf8("editMaxSlabi"));

        horizontalLayout->addWidget(editMaxSlabi);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_22 = new QLabel(verticalLayoutWidget_2);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(220, 0));
        label_22->setFont(font1);

        horizontalLayout_2->addWidget(label_22);

        editMaxSlabiNavor = new QLineEdit(verticalLayoutWidget_2);
        editMaxSlabiNavor->setObjectName(QString::fromUtf8("editMaxSlabiNavor"));

        horizontalLayout_2->addWidget(editMaxSlabiNavor);


        verticalLayout_2->addLayout(horizontalLayout_2);


        retranslateUi(GeneralSettings);

        QMetaObject::connectSlotsByName(GeneralSettings);
    } // setupUi

    void retranslateUi(QWidget *GeneralSettings)
    {
        GeneralSettings->setWindowTitle(QCoreApplication::translate("GeneralSettings", "GeneralSettings", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("GeneralSettings", "NA\304\214IN DELOVANJA", nullptr));
        label_16->setText(QCoreApplication::translate("GeneralSettings", "OMOGO\304\214I DIMENZIJSKO KONTROLO", nullptr));
        checkDim->setText(QString());
        radioButtonS1->setText(QString());
        label_18->setText(QCoreApplication::translate("GeneralSettings", "SAMO NAVOJNA POSTAJA 1", nullptr));
        radioButtonS2->setText(QString());
        label_20->setText(QCoreApplication::translate("GeneralSettings", "SAMO NAVOJNA POSTAJA 2", nullptr));
        radioButtonS12->setText(QString());
        label_21->setText(QCoreApplication::translate("GeneralSettings", "DELOVANJE OBEH NAVOJNIH POSTAJ", nullptr));
        groupBox->setTitle(QCoreApplication::translate("GeneralSettings", "Nastavitve", nullptr));
        label_19->setText(QCoreApplication::translate("GeneralSettings", "MAX ST. SLABIH KOSOV NA DIMENZIJI", nullptr));
        label_22->setText(QCoreApplication::translate("GeneralSettings", "MAX ST. SLABIH KOSOV  NA  NAVORU", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GeneralSettings: public Ui_GeneralSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENERALSETTINGS_H
