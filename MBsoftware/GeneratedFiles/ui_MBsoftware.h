/********************************************************************************
** Form generated from reading UI file 'MBsoftware.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSOFTWARE_H
#define UI_MBSOFTWARE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MBsoftwareClass
{
public:
    QWidget *centralWidget;
    QFrame *frameStatus;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QFrame *frameDeviceStatus;
    QLabel *labelDeviceStatus;
    QListWidget *listWidgetStatus;
    QLabel *labelLogin;
    QLabel *labelTip;
    QLabel *labelStatus;
    QLabel *labelEpson;
    QLabel *labelPC2;
    QLabel *labelVibrator;
    QLabel *labelVrata;
    QLabel *labelTakt;
    QLabel *labelTakt_2;
    QSpacerItem *verticalSpacer;
    QTabWidget *MainMenuBar;
    QWidget *tabLogin;
    QHBoxLayout *horizontalLayout_4;
    QToolButton *buttonLogin;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tabSettings;
    QHBoxLayout *horizontalLayout_6;
    QToolButton *buttonGeneralSettings;
    QToolButton *buttonKoracno;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tabTypes;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *buttonSetTolerance;
    QToolButton *buttonTypeSelect;
    QToolButton *buttonAddType;
    QToolButton *buttonRemoveType;
    QToolButton *buttonTypeSettings;
    QToolButton *buttonResetCounters;
    QSpacerItem *horizontalSpacer_3;
    QWidget *tabCameras;
    QWidget *tabSignals;
    QWidget *tabImageProcessing;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *buttonImageProcessing;
    QSpacerItem *horizontalSpacer;
    QWidget *tabStatistcs;
    QHBoxLayout *horizontalLayout_5;
    QToolButton *buttonHistory;
    QToolButton *buttonStatusBox;
    QSpacerItem *horizontalSpacer_4;
    QToolButton *buttonAboutUs;
    QLabel *labelLogo;
    QGraphicsView *imageViewMainWindow_0;
    QFrame *frameMeasurements;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurements;
    QLabel *labelStation2_3;
    QLabel *labelStatusP0;
    QFrame *statisticsFrame_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QLabel *labelStation0bad;
    QLabel *labelGood_2;
    QLabel *labelBAD_2;
    QLabel *labelGoodProcent_2;
    QLabel *labelBadProcent_2;
    QLabel *labelStation0good;
    QLabel *labelStation0counters;
    QLabel *labelStation0counters_1;
    QFrame *frameMeasurements_2;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *layoutMeasurements_2;
    QLabel *labelStationP1;
    QLabel *labelStatusP1;
    QFrame *frameMeasurements_3;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *layoutMeasurements_3;
    QLabel *labelStatusP2;
    QLabel *labelStation2_2;
    QGraphicsView *imageViewMainWindow_2;
    QGraphicsView *imageViewMainWindow_1;
    QFrame *statisticsFrame_1;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_1;
    QLabel *labelStation0bad_1;
    QLabel *labelBAD_1;
    QLabel *labelGoodProcent_1;
    QLabel *labelGood_1;
    QLabel *labelStation0good_1;
    QLabel *labelBadProcent_1;
    QFrame *frameChart;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *chartLayout;
    QFrame *frameNavoj;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_3;
    QLabel *labelStation0good_2;
    QLabel *labelGoodN0;
    QLabel *labelBadProcentN0;
    QLabel *labelBADN0;
    QLabel *labelGoodProcentN0;
    QLabel *labelStation0bad_2;
    QLabel *labelStation2_4;
    QLabel *labelStatusN0;
    QFrame *frameNavoj_2;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_4;
    QLabel *labelGoodN0_2;
    QLabel *labelBADN0_2;
    QLabel *labelStation0bad_4;
    QLabel *labelGoodProcentN0_2;
    QLabel *labelBadProcentN0_2;
    QLabel *labelStation0good_3;
    QLabel *labelStation2_5;
    QLabel *labelStatusN1;
    QFrame *frameNavoj_3;
    QWidget *gridLayoutWidget_6;
    QGridLayout *gridLayout_5;
    QLabel *labelBadBox3;
    QLabel *labelStation0bad_7;
    QLabel *labelStation0bad_8;
    QLabel *labelGoodBox1;
    QLabel *labelBadBox2;
    QLabel *labelGoodBox111;
    QToolButton *buttonReset1;
    QToolButton *buttonReset4;
    QToolButton *buttonReset5;
    QLabel *labelStation0bad_9;
    QLabel *labelBadBox1;
    QLabel *labelStation0bad_10;
    QLabel *labelGoodBox2;
    QToolButton *buttonReset2;
    QToolButton *buttonReset3;
    QLabel *labelStation2_6;
    QFrame *frameNavoj_4;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_6;
    QLabel *labelSlabiKam;
    QLabel *labelSlabiNavojProc;
    QLabel *labelSlabiKamProc;
    QLabel *labelStation0bad_13;
    QLabel *labelStation0bad_14;
    QLabel *labelSlabiNavoj;
    QLabel *labelGoodBox111_2;
    QLabel *labelDobri;
    QLabel *labelStation0bad_12;
    QLabel *labelVsi;
    QLabel *labelDobriProc;
    QLabel *labelStation2_7;

    void setupUi(QMainWindow *MBsoftwareClass)
    {
        if (MBsoftwareClass->objectName().isEmpty())
            MBsoftwareClass->setObjectName(QString::fromUtf8("MBsoftwareClass"));
        MBsoftwareClass->setWindowModality(Qt::NonModal);
        MBsoftwareClass->resize(1920, 1080);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MBsoftwareClass->sizePolicy().hasHeightForWidth());
        MBsoftwareClass->setSizePolicy(sizePolicy);
        MBsoftwareClass->setMinimumSize(QSize(1920, 1080));
        MBsoftwareClass->setMaximumSize(QSize(16777215, 16777215));
        MBsoftwareClass->setCursor(QCursor(Qt::ArrowCursor));
        MBsoftwareClass->setAutoFillBackground(false);
        MBsoftwareClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MBsoftwareClass->setTabShape(QTabWidget::Triangular);
        MBsoftwareClass->setDockNestingEnabled(false);
        centralWidget = new QWidget(MBsoftwareClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setMinimumSize(QSize(1280, 860));
        centralWidget->setMaximumSize(QSize(1920, 1300));
        frameStatus = new QFrame(centralWidget);
        frameStatus->setObjectName(QString::fromUtf8("frameStatus"));
        frameStatus->setGeometry(QRect(1611, 110, 300, 661));
        frameStatus->setMinimumSize(QSize(300, 0));
        frameStatus->setMaximumSize(QSize(300, 16777215));
        frameStatus->setFrameShape(QFrame::Box);
        frameStatus->setFrameShadow(QFrame::Raised);
        frameStatus->setLineWidth(4);
        verticalLayoutWidget_2 = new QWidget(frameStatus);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 281, 651));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frameDeviceStatus = new QFrame(verticalLayoutWidget_2);
        frameDeviceStatus->setObjectName(QString::fromUtf8("frameDeviceStatus"));
        frameDeviceStatus->setMinimumSize(QSize(0, 200));
        frameDeviceStatus->setFrameShape(QFrame::StyledPanel);
        frameDeviceStatus->setFrameShadow(QFrame::Plain);
        frameDeviceStatus->setLineWidth(1);
        labelDeviceStatus = new QLabel(frameDeviceStatus);
        labelDeviceStatus->setObjectName(QString::fromUtf8("labelDeviceStatus"));
        labelDeviceStatus->setGeometry(QRect(10, 10, 141, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        labelDeviceStatus->setFont(font);
        listWidgetStatus = new QListWidget(frameDeviceStatus);
        listWidgetStatus->setObjectName(QString::fromUtf8("listWidgetStatus"));
        listWidgetStatus->setGeometry(QRect(10, 30, 261, 161));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setBold(true);
        font1.setWeight(75);
        listWidgetStatus->setFont(font1);

        verticalLayout->addWidget(frameDeviceStatus);

        labelLogin = new QLabel(verticalLayoutWidget_2);
        labelLogin->setObjectName(QString::fromUtf8("labelLogin"));
        labelLogin->setMinimumSize(QSize(200, 43));
        labelLogin->setMaximumSize(QSize(16777215, 43));
        labelLogin->setFont(font);
        labelLogin->setStyleSheet(QString::fromUtf8(""));
        labelLogin->setFrameShape(QFrame::Panel);
        labelLogin->setFrameShadow(QFrame::Sunken);
        labelLogin->setLineWidth(3);

        verticalLayout->addWidget(labelLogin);

        labelTip = new QLabel(verticalLayoutWidget_2);
        labelTip->setObjectName(QString::fromUtf8("labelTip"));
        labelTip->setMinimumSize(QSize(200, 43));
        labelTip->setMaximumSize(QSize(16777215, 43));
        labelTip->setFont(font);
        labelTip->setStyleSheet(QString::fromUtf8(""));
        labelTip->setFrameShape(QFrame::Panel);
        labelTip->setFrameShadow(QFrame::Sunken);
        labelTip->setLineWidth(3);

        verticalLayout->addWidget(labelTip);

        labelStatus = new QLabel(verticalLayoutWidget_2);
        labelStatus->setObjectName(QString::fromUtf8("labelStatus"));
        labelStatus->setMinimumSize(QSize(200, 43));
        labelStatus->setMaximumSize(QSize(16777215, 43));
        labelStatus->setFont(font);
        labelStatus->setStyleSheet(QString::fromUtf8(""));
        labelStatus->setFrameShape(QFrame::Panel);
        labelStatus->setFrameShadow(QFrame::Sunken);
        labelStatus->setLineWidth(3);

        verticalLayout->addWidget(labelStatus);

        labelEpson = new QLabel(verticalLayoutWidget_2);
        labelEpson->setObjectName(QString::fromUtf8("labelEpson"));
        labelEpson->setMinimumSize(QSize(200, 43));
        labelEpson->setMaximumSize(QSize(16777215, 43));
        labelEpson->setFont(font);
        labelEpson->setStyleSheet(QString::fromUtf8(""));
        labelEpson->setFrameShape(QFrame::Panel);
        labelEpson->setFrameShadow(QFrame::Sunken);
        labelEpson->setLineWidth(3);

        verticalLayout->addWidget(labelEpson);

        labelPC2 = new QLabel(verticalLayoutWidget_2);
        labelPC2->setObjectName(QString::fromUtf8("labelPC2"));
        labelPC2->setMinimumSize(QSize(200, 43));
        labelPC2->setMaximumSize(QSize(16777215, 43));
        labelPC2->setFont(font);
        labelPC2->setStyleSheet(QString::fromUtf8(""));
        labelPC2->setFrameShape(QFrame::Panel);
        labelPC2->setFrameShadow(QFrame::Sunken);
        labelPC2->setLineWidth(3);

        verticalLayout->addWidget(labelPC2);

        labelVibrator = new QLabel(verticalLayoutWidget_2);
        labelVibrator->setObjectName(QString::fromUtf8("labelVibrator"));
        labelVibrator->setMinimumSize(QSize(200, 43));
        labelVibrator->setMaximumSize(QSize(16777215, 43));
        labelVibrator->setFont(font);
        labelVibrator->setStyleSheet(QString::fromUtf8(""));
        labelVibrator->setFrameShape(QFrame::Panel);
        labelVibrator->setFrameShadow(QFrame::Sunken);
        labelVibrator->setLineWidth(3);

        verticalLayout->addWidget(labelVibrator);

        labelVrata = new QLabel(verticalLayoutWidget_2);
        labelVrata->setObjectName(QString::fromUtf8("labelVrata"));
        labelVrata->setMinimumSize(QSize(200, 43));
        labelVrata->setMaximumSize(QSize(16777215, 43));
        labelVrata->setFont(font);
        labelVrata->setStyleSheet(QString::fromUtf8(""));
        labelVrata->setFrameShape(QFrame::Panel);
        labelVrata->setFrameShadow(QFrame::Sunken);
        labelVrata->setLineWidth(3);

        verticalLayout->addWidget(labelVrata);

        labelTakt = new QLabel(verticalLayoutWidget_2);
        labelTakt->setObjectName(QString::fromUtf8("labelTakt"));
        labelTakt->setMinimumSize(QSize(200, 43));
        labelTakt->setMaximumSize(QSize(16777215, 43));
        labelTakt->setFont(font);
        labelTakt->setStyleSheet(QString::fromUtf8(""));
        labelTakt->setFrameShape(QFrame::Panel);
        labelTakt->setFrameShadow(QFrame::Sunken);
        labelTakt->setLineWidth(3);

        verticalLayout->addWidget(labelTakt);

        labelTakt_2 = new QLabel(verticalLayoutWidget_2);
        labelTakt_2->setObjectName(QString::fromUtf8("labelTakt_2"));
        labelTakt_2->setMinimumSize(QSize(200, 43));
        labelTakt_2->setMaximumSize(QSize(16777215, 43));
        labelTakt_2->setFont(font);
        labelTakt_2->setStyleSheet(QString::fromUtf8(""));
        labelTakt_2->setFrameShape(QFrame::Panel);
        labelTakt_2->setFrameShadow(QFrame::Sunken);
        labelTakt_2->setLineWidth(3);

        verticalLayout->addWidget(labelTakt_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        MainMenuBar = new QTabWidget(centralWidget);
        MainMenuBar->setObjectName(QString::fromUtf8("MainMenuBar"));
        MainMenuBar->setGeometry(QRect(9, 9, 640, 150));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(MainMenuBar->sizePolicy().hasHeightForWidth());
        MainMenuBar->setSizePolicy(sizePolicy2);
        MainMenuBar->setMinimumSize(QSize(640, 120));
        MainMenuBar->setMaximumSize(QSize(16777215, 150));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Comic Sans MS"));
        font2.setPointSize(10);
        MainMenuBar->setFont(font2);
        MainMenuBar->setTabPosition(QTabWidget::North);
        MainMenuBar->setTabShape(QTabWidget::Triangular);
        MainMenuBar->setMovable(true);
        MainMenuBar->setTabBarAutoHide(true);
        tabLogin = new QWidget();
        tabLogin->setObjectName(QString::fromUtf8("tabLogin"));
        horizontalLayout_4 = new QHBoxLayout(tabLogin);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonLogin = new QToolButton(tabLogin);
        buttonLogin->setObjectName(QString::fromUtf8("buttonLogin"));
        buttonLogin->setMinimumSize(QSize(100, 100));
        buttonLogin->setFocusPolicy(Qt::ClickFocus);
        QIcon icon;
        icon.addFile(QString::fromUtf8("res/LoginBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonLogin->setIcon(icon);
        buttonLogin->setIconSize(QSize(80, 80));
        buttonLogin->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonLogin->setAutoRaise(true);

        horizontalLayout_4->addWidget(buttonLogin);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        MainMenuBar->addTab(tabLogin, QString());
        tabSettings = new QWidget();
        tabSettings->setObjectName(QString::fromUtf8("tabSettings"));
        horizontalLayout_6 = new QHBoxLayout(tabSettings);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        buttonGeneralSettings = new QToolButton(tabSettings);
        buttonGeneralSettings->setObjectName(QString::fromUtf8("buttonGeneralSettings"));
        buttonGeneralSettings->setMinimumSize(QSize(100, 100));
        buttonGeneralSettings->setFont(font2);
        buttonGeneralSettings->setFocusPolicy(Qt::ClickFocus);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("res/settings.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonGeneralSettings->setIcon(icon1);
        buttonGeneralSettings->setIconSize(QSize(70, 90));
        buttonGeneralSettings->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonGeneralSettings->setAutoRaise(true);

        horizontalLayout_6->addWidget(buttonGeneralSettings);

        buttonKoracno = new QToolButton(tabSettings);
        buttonKoracno->setObjectName(QString::fromUtf8("buttonKoracno"));
        buttonKoracno->setMinimumSize(QSize(100, 100));
        buttonKoracno->setFont(font2);
        buttonKoracno->setFocusPolicy(Qt::ClickFocus);
        buttonKoracno->setAcceptDrops(false);
        buttonKoracno->setAutoFillBackground(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("res/StepMode.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonKoracno->setIcon(icon2);
        buttonKoracno->setIconSize(QSize(60, 60));
        buttonKoracno->setCheckable(false);
        buttonKoracno->setPopupMode(QToolButton::DelayedPopup);
        buttonKoracno->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonKoracno->setAutoRaise(true);

        horizontalLayout_6->addWidget(buttonKoracno);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        MainMenuBar->addTab(tabSettings, QString());
        tabTypes = new QWidget();
        tabTypes->setObjectName(QString::fromUtf8("tabTypes"));
        horizontalLayout_3 = new QHBoxLayout(tabTypes);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonSetTolerance = new QToolButton(tabTypes);
        buttonSetTolerance->setObjectName(QString::fromUtf8("buttonSetTolerance"));
        buttonSetTolerance->setMinimumSize(QSize(100, 100));
        buttonSetTolerance->setFont(font2);
        buttonSetTolerance->setFocusPolicy(Qt::ClickFocus);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("res/ParametersEditBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonSetTolerance->setIcon(icon3);
        buttonSetTolerance->setIconSize(QSize(60, 60));
        buttonSetTolerance->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonSetTolerance->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonSetTolerance);

        buttonTypeSelect = new QToolButton(tabTypes);
        buttonTypeSelect->setObjectName(QString::fromUtf8("buttonTypeSelect"));
        buttonTypeSelect->setMinimumSize(QSize(100, 100));
        buttonTypeSelect->setFont(font2);
        buttonTypeSelect->setFocusPolicy(Qt::ClickFocus);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8("res/TypesLarge.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonTypeSelect->setIcon(icon4);
        buttonTypeSelect->setIconSize(QSize(70, 70));
        buttonTypeSelect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonTypeSelect->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonTypeSelect);

        buttonAddType = new QToolButton(tabTypes);
        buttonAddType->setObjectName(QString::fromUtf8("buttonAddType"));
        buttonAddType->setMinimumSize(QSize(100, 100));
        buttonAddType->setFont(font2);
        buttonAddType->setFocusPolicy(Qt::ClickFocus);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8("res/TypeNewIco.ico"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAddType->setIcon(icon5);
        buttonAddType->setIconSize(QSize(60, 60));
        buttonAddType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAddType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonAddType);

        buttonRemoveType = new QToolButton(tabTypes);
        buttonRemoveType->setObjectName(QString::fromUtf8("buttonRemoveType"));
        buttonRemoveType->setMinimumSize(QSize(100, 100));
        buttonRemoveType->setFont(font2);
        buttonRemoveType->setFocusPolicy(Qt::ClickFocus);
        buttonRemoveType->setAcceptDrops(false);
        buttonRemoveType->setAutoFillBackground(false);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8("res/X.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonRemoveType->setIcon(icon6);
        buttonRemoveType->setIconSize(QSize(60, 60));
        buttonRemoveType->setCheckable(false);
        buttonRemoveType->setPopupMode(QToolButton::DelayedPopup);
        buttonRemoveType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonRemoveType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonRemoveType);

        buttonTypeSettings = new QToolButton(tabTypes);
        buttonTypeSettings->setObjectName(QString::fromUtf8("buttonTypeSettings"));
        buttonTypeSettings->setMinimumSize(QSize(100, 100));
        buttonTypeSettings->setFont(font2);
        buttonTypeSettings->setFocusPolicy(Qt::ClickFocus);
        QIcon icon7;
        icon7.addFile(QString::fromUtf8("res/ParametersBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonTypeSettings->setIcon(icon7);
        buttonTypeSettings->setIconSize(QSize(60, 60));
        buttonTypeSettings->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonTypeSettings->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonTypeSettings);

        buttonResetCounters = new QToolButton(tabTypes);
        buttonResetCounters->setObjectName(QString::fromUtf8("buttonResetCounters"));
        buttonResetCounters->setMinimumSize(QSize(100, 100));
        buttonResetCounters->setFont(font2);
        buttonResetCounters->setFocusPolicy(Qt::ClickFocus);
        buttonResetCounters->setAcceptDrops(false);
        buttonResetCounters->setAutoFillBackground(false);
        QIcon icon8;
        icon8.addFile(QString::fromUtf8("res/resetButton.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonResetCounters->setIcon(icon8);
        buttonResetCounters->setIconSize(QSize(60, 60));
        buttonResetCounters->setCheckable(false);
        buttonResetCounters->setPopupMode(QToolButton::DelayedPopup);
        buttonResetCounters->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonResetCounters->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonResetCounters);

        horizontalSpacer_3 = new QSpacerItem(830, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        MainMenuBar->addTab(tabTypes, QString());
        tabCameras = new QWidget();
        tabCameras->setObjectName(QString::fromUtf8("tabCameras"));
        MainMenuBar->addTab(tabCameras, QString());
        tabSignals = new QWidget();
        tabSignals->setObjectName(QString::fromUtf8("tabSignals"));
        MainMenuBar->addTab(tabSignals, QString());
        tabImageProcessing = new QWidget();
        tabImageProcessing->setObjectName(QString::fromUtf8("tabImageProcessing"));
        tabImageProcessing->setAcceptDrops(false);
        horizontalLayout = new QHBoxLayout(tabImageProcessing);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(tabImageProcessing);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font2);
        groupBox->setAutoFillBackground(false);
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonImageProcessing = new QToolButton(groupBox);
        buttonImageProcessing->setObjectName(QString::fromUtf8("buttonImageProcessing"));
        buttonImageProcessing->setMinimumSize(QSize(100, 100));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8("res/imageProcessing.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImageProcessing->setIcon(icon9);
        buttonImageProcessing->setIconSize(QSize(60, 45));
        buttonImageProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonImageProcessing->setAutoRaise(true);

        horizontalLayout_2->addWidget(buttonImageProcessing);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        horizontalLayout->addWidget(groupBox);

        MainMenuBar->addTab(tabImageProcessing, QString());
        tabStatistcs = new QWidget();
        tabStatistcs->setObjectName(QString::fromUtf8("tabStatistcs"));
        horizontalLayout_5 = new QHBoxLayout(tabStatistcs);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        buttonHistory = new QToolButton(tabStatistcs);
        buttonHistory->setObjectName(QString::fromUtf8("buttonHistory"));
        buttonHistory->setMinimumSize(QSize(100, 100));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8("res/history.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonHistory->setIcon(icon10);
        buttonHistory->setIconSize(QSize(70, 70));
        buttonHistory->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonHistory->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonHistory);

        buttonStatusBox = new QToolButton(tabStatistcs);
        buttonStatusBox->setObjectName(QString::fromUtf8("buttonStatusBox"));
        buttonStatusBox->setMinimumSize(QSize(100, 100));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8("res/status.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonStatusBox->setIcon(icon11);
        buttonStatusBox->setIconSize(QSize(70, 80));
        buttonStatusBox->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonStatusBox->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonStatusBox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);

        buttonAboutUs = new QToolButton(tabStatistcs);
        buttonAboutUs->setObjectName(QString::fromUtf8("buttonAboutUs"));
        buttonAboutUs->setMinimumSize(QSize(100, 100));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8("res/about.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAboutUs->setIcon(icon12);
        buttonAboutUs->setIconSize(QSize(70, 80));
        buttonAboutUs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAboutUs->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonAboutUs);

        MainMenuBar->addTab(tabStatistcs, QString());
        labelLogo = new QLabel(centralWidget);
        labelLogo->setObjectName(QString::fromUtf8("labelLogo"));
        labelLogo->setGeometry(QRect(1610, 10, 301, 101));
        labelLogo->setPixmap(QPixmap(QString::fromUtf8("res/MB.bmp")));
        labelLogo->setScaledContents(true);
        imageViewMainWindow_0 = new QGraphicsView(centralWidget);
        imageViewMainWindow_0->setObjectName(QString::fromUtf8("imageViewMainWindow_0"));
        imageViewMainWindow_0->setGeometry(QRect(1190, 10, 411, 151));
        imageViewMainWindow_0->setMinimumSize(QSize(0, 0));
        imageViewMainWindow_0->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_0->setFrameShadow(QFrame::Sunken);
        frameMeasurements = new QFrame(centralWidget);
        frameMeasurements->setObjectName(QString::fromUtf8("frameMeasurements"));
        frameMeasurements->setGeometry(QRect(680, 10, 500, 151));
        frameMeasurements->setMinimumSize(QSize(400, 50));
        frameMeasurements->setMaximumSize(QSize(500, 16777215));
        frameMeasurements->setFrameShape(QFrame::Box);
        frameMeasurements->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget = new QWidget(frameMeasurements);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 50, 481, 91));
        layoutMeasurements = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurements->setSpacing(6);
        layoutMeasurements->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements->setObjectName(QString::fromUtf8("layoutMeasurements"));
        layoutMeasurements->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements->setContentsMargins(0, 0, 0, 0);
        labelStation2_3 = new QLabel(frameMeasurements);
        labelStation2_3->setObjectName(QString::fromUtf8("labelStation2_3"));
        labelStation2_3->setGeometry(QRect(10, 10, 151, 36));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        labelStation2_3->setFont(font3);
        labelStation2_3->setFrameShape(QFrame::NoFrame);
        labelStation2_3->setFrameShadow(QFrame::Raised);
        labelStatusP0 = new QLabel(frameMeasurements);
        labelStatusP0->setObjectName(QString::fromUtf8("labelStatusP0"));
        labelStatusP0->setGeometry(QRect(200, 10, 291, 36));
        labelStatusP0->setFont(font3);
        labelStatusP0->setFrameShape(QFrame::Box);
        labelStatusP0->setFrameShadow(QFrame::Raised);
        labelStatusP0->setTextFormat(Qt::PlainText);
        labelStatusP0->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        statisticsFrame_2 = new QFrame(centralWidget);
        statisticsFrame_2->setObjectName(QString::fromUtf8("statisticsFrame_2"));
        statisticsFrame_2->setGeometry(QRect(10, 920, 501, 121));
        statisticsFrame_2->setFrameShape(QFrame::Box);
        statisticsFrame_2->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_2 = new QWidget(statisticsFrame_2);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 30, 481, 81));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        labelStation0bad = new QLabel(gridLayoutWidget_2);
        labelStation0bad->setObjectName(QString::fromUtf8("labelStation0bad"));
        labelStation0bad->setMinimumSize(QSize(150, 36));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Calibri"));
        font4.setPointSize(12);
        font4.setBold(true);
        font4.setItalic(false);
        font4.setWeight(75);
        labelStation0bad->setFont(font4);
        labelStation0bad->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelStation0bad->setFrameShape(QFrame::Panel);
        labelStation0bad->setFrameShadow(QFrame::Sunken);
        labelStation0bad->setLineWidth(3);

        gridLayout_2->addWidget(labelStation0bad, 1, 0, 1, 1);

        labelGood_2 = new QLabel(gridLayoutWidget_2);
        labelGood_2->setObjectName(QString::fromUtf8("labelGood_2"));
        labelGood_2->setFont(font4);
        labelGood_2->setStyleSheet(QString::fromUtf8(""));
        labelGood_2->setFrameShape(QFrame::Panel);
        labelGood_2->setFrameShadow(QFrame::Sunken);
        labelGood_2->setLineWidth(3);
        labelGood_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelGood_2, 0, 1, 1, 1);

        labelBAD_2 = new QLabel(gridLayoutWidget_2);
        labelBAD_2->setObjectName(QString::fromUtf8("labelBAD_2"));
        labelBAD_2->setMinimumSize(QSize(0, 36));
        labelBAD_2->setFont(font4);
        labelBAD_2->setFrameShape(QFrame::Panel);
        labelBAD_2->setFrameShadow(QFrame::Sunken);
        labelBAD_2->setLineWidth(3);
        labelBAD_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelBAD_2, 1, 1, 1, 1);

        labelGoodProcent_2 = new QLabel(gridLayoutWidget_2);
        labelGoodProcent_2->setObjectName(QString::fromUtf8("labelGoodProcent_2"));
        labelGoodProcent_2->setMinimumSize(QSize(0, 36));
        labelGoodProcent_2->setFont(font4);
        labelGoodProcent_2->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent_2->setFrameShape(QFrame::Panel);
        labelGoodProcent_2->setFrameShadow(QFrame::Sunken);
        labelGoodProcent_2->setLineWidth(3);
        labelGoodProcent_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelGoodProcent_2, 0, 2, 1, 1);

        labelBadProcent_2 = new QLabel(gridLayoutWidget_2);
        labelBadProcent_2->setObjectName(QString::fromUtf8("labelBadProcent_2"));
        labelBadProcent_2->setMinimumSize(QSize(0, 36));
        labelBadProcent_2->setFont(font4);
        labelBadProcent_2->setFrameShape(QFrame::Panel);
        labelBadProcent_2->setFrameShadow(QFrame::Sunken);
        labelBadProcent_2->setLineWidth(3);
        labelBadProcent_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelBadProcent_2, 1, 2, 1, 1);

        labelStation0good = new QLabel(gridLayoutWidget_2);
        labelStation0good->setObjectName(QString::fromUtf8("labelStation0good"));
        labelStation0good->setMinimumSize(QSize(150, 36));
        labelStation0good->setFont(font4);
        labelStation0good->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        labelStation0good->setFrameShape(QFrame::Panel);
        labelStation0good->setFrameShadow(QFrame::Sunken);
        labelStation0good->setLineWidth(3);

        gridLayout_2->addWidget(labelStation0good, 0, 0, 1, 1);

        labelStation0counters = new QLabel(statisticsFrame_2);
        labelStation0counters->setObjectName(QString::fromUtf8("labelStation0counters"));
        labelStation0counters->setGeometry(QRect(10, 0, 221, 31));
        labelStation0counters->setFont(font4);
        labelStation0counters->setFrameShape(QFrame::NoFrame);
        labelStation0counters->setFrameShadow(QFrame::Sunken);
        labelStation0counters_1 = new QLabel(statisticsFrame_2);
        labelStation0counters_1->setObjectName(QString::fromUtf8("labelStation0counters_1"));
        labelStation0counters_1->setGeometry(QRect(300, 0, 221, 31));
        labelStation0counters_1->setFont(font4);
        labelStation0counters_1->setFrameShape(QFrame::NoFrame);
        labelStation0counters_1->setFrameShadow(QFrame::Sunken);
        frameMeasurements_2 = new QFrame(centralWidget);
        frameMeasurements_2->setObjectName(QString::fromUtf8("frameMeasurements_2"));
        frameMeasurements_2->setGeometry(QRect(10, 170, 500, 341));
        frameMeasurements_2->setMinimumSize(QSize(400, 50));
        frameMeasurements_2->setMaximumSize(QSize(500, 16777215));
        frameMeasurements_2->setFrameShape(QFrame::Box);
        frameMeasurements_2->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_3 = new QWidget(frameMeasurements_2);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 50, 481, 281));
        layoutMeasurements_2 = new QVBoxLayout(verticalLayoutWidget_3);
        layoutMeasurements_2->setSpacing(6);
        layoutMeasurements_2->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements_2->setObjectName(QString::fromUtf8("layoutMeasurements_2"));
        layoutMeasurements_2->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements_2->setContentsMargins(0, 0, 0, 0);
        labelStationP1 = new QLabel(frameMeasurements_2);
        labelStationP1->setObjectName(QString::fromUtf8("labelStationP1"));
        labelStationP1->setGeometry(QRect(10, 10, 161, 36));
        labelStationP1->setFont(font3);
        labelStationP1->setFrameShape(QFrame::NoFrame);
        labelStationP1->setFrameShadow(QFrame::Raised);
        labelStatusP1 = new QLabel(frameMeasurements_2);
        labelStatusP1->setObjectName(QString::fromUtf8("labelStatusP1"));
        labelStatusP1->setGeometry(QRect(200, 10, 291, 36));
        labelStatusP1->setFont(font3);
        labelStatusP1->setFrameShape(QFrame::Box);
        labelStatusP1->setFrameShadow(QFrame::Raised);
        labelStatusP1->setTextFormat(Qt::PlainText);
        labelStatusP1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        frameMeasurements_3 = new QFrame(centralWidget);
        frameMeasurements_3->setObjectName(QString::fromUtf8("frameMeasurements_3"));
        frameMeasurements_3->setGeometry(QRect(10, 630, 501, 281));
        frameMeasurements_3->setMinimumSize(QSize(400, 50));
        frameMeasurements_3->setMaximumSize(QSize(600, 16777215));
        frameMeasurements_3->setFrameShape(QFrame::Box);
        frameMeasurements_3->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_4 = new QWidget(frameMeasurements_3);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 50, 481, 231));
        layoutMeasurements_3 = new QVBoxLayout(verticalLayoutWidget_4);
        layoutMeasurements_3->setSpacing(6);
        layoutMeasurements_3->setContentsMargins(11, 11, 11, 11);
        layoutMeasurements_3->setObjectName(QString::fromUtf8("layoutMeasurements_3"));
        layoutMeasurements_3->setSizeConstraint(QLayout::SetMinimumSize);
        layoutMeasurements_3->setContentsMargins(0, 0, 0, 0);
        labelStatusP2 = new QLabel(frameMeasurements_3);
        labelStatusP2->setObjectName(QString::fromUtf8("labelStatusP2"));
        labelStatusP2->setGeometry(QRect(200, 10, 291, 36));
        labelStatusP2->setFont(font3);
        labelStatusP2->setFrameShape(QFrame::Box);
        labelStatusP2->setFrameShadow(QFrame::Raised);
        labelStatusP2->setTextFormat(Qt::PlainText);
        labelStatusP2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        labelStation2_2 = new QLabel(frameMeasurements_3);
        labelStation2_2->setObjectName(QString::fromUtf8("labelStation2_2"));
        labelStation2_2->setGeometry(QRect(10, 10, 161, 36));
        labelStation2_2->setFont(font3);
        labelStation2_2->setFrameShape(QFrame::NoFrame);
        labelStation2_2->setFrameShadow(QFrame::Raised);
        imageViewMainWindow_2 = new QGraphicsView(centralWidget);
        imageViewMainWindow_2->setObjectName(QString::fromUtf8("imageViewMainWindow_2"));
        imageViewMainWindow_2->setGeometry(QRect(520, 630, 490, 409));
        imageViewMainWindow_2->setMinimumSize(QSize(490, 409));
        imageViewMainWindow_2->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_2->setFrameShadow(QFrame::Sunken);
        imageViewMainWindow_1 = new QGraphicsView(centralWidget);
        imageViewMainWindow_1->setObjectName(QString::fromUtf8("imageViewMainWindow_1"));
        imageViewMainWindow_1->setGeometry(QRect(520, 170, 490, 409));
        imageViewMainWindow_1->setMinimumSize(QSize(306, 256));
        imageViewMainWindow_1->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_1->setFrameShadow(QFrame::Sunken);
        statisticsFrame_1 = new QFrame(centralWidget);
        statisticsFrame_1->setObjectName(QString::fromUtf8("statisticsFrame_1"));
        statisticsFrame_1->setGeometry(QRect(10, 520, 501, 101));
        statisticsFrame_1->setFrameShape(QFrame::Box);
        statisticsFrame_1->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_3 = new QWidget(statisticsFrame_1);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(10, 10, 481, 81));
        gridLayout_1 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_1->setSpacing(6);
        gridLayout_1->setContentsMargins(11, 11, 11, 11);
        gridLayout_1->setObjectName(QString::fromUtf8("gridLayout_1"));
        gridLayout_1->setContentsMargins(0, 0, 0, 0);
        labelStation0bad_1 = new QLabel(gridLayoutWidget_3);
        labelStation0bad_1->setObjectName(QString::fromUtf8("labelStation0bad_1"));
        labelStation0bad_1->setMinimumSize(QSize(150, 36));
        labelStation0bad_1->setFont(font4);
        labelStation0bad_1->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelStation0bad_1->setFrameShape(QFrame::Panel);
        labelStation0bad_1->setFrameShadow(QFrame::Sunken);
        labelStation0bad_1->setLineWidth(3);

        gridLayout_1->addWidget(labelStation0bad_1, 1, 0, 1, 1);

        labelBAD_1 = new QLabel(gridLayoutWidget_3);
        labelBAD_1->setObjectName(QString::fromUtf8("labelBAD_1"));
        labelBAD_1->setMinimumSize(QSize(0, 36));
        labelBAD_1->setFont(font4);
        labelBAD_1->setFrameShape(QFrame::Panel);
        labelBAD_1->setFrameShadow(QFrame::Sunken);
        labelBAD_1->setLineWidth(3);
        labelBAD_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_1->addWidget(labelBAD_1, 1, 1, 1, 1);

        labelGoodProcent_1 = new QLabel(gridLayoutWidget_3);
        labelGoodProcent_1->setObjectName(QString::fromUtf8("labelGoodProcent_1"));
        labelGoodProcent_1->setMinimumSize(QSize(0, 36));
        labelGoodProcent_1->setFont(font4);
        labelGoodProcent_1->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent_1->setFrameShape(QFrame::Panel);
        labelGoodProcent_1->setFrameShadow(QFrame::Sunken);
        labelGoodProcent_1->setLineWidth(3);
        labelGoodProcent_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_1->addWidget(labelGoodProcent_1, 0, 2, 1, 1);

        labelGood_1 = new QLabel(gridLayoutWidget_3);
        labelGood_1->setObjectName(QString::fromUtf8("labelGood_1"));
        labelGood_1->setFont(font4);
        labelGood_1->setStyleSheet(QString::fromUtf8(""));
        labelGood_1->setFrameShape(QFrame::Panel);
        labelGood_1->setFrameShadow(QFrame::Sunken);
        labelGood_1->setLineWidth(3);
        labelGood_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_1->addWidget(labelGood_1, 0, 1, 1, 1);

        labelStation0good_1 = new QLabel(gridLayoutWidget_3);
        labelStation0good_1->setObjectName(QString::fromUtf8("labelStation0good_1"));
        labelStation0good_1->setMinimumSize(QSize(150, 36));
        labelStation0good_1->setFont(font4);
        labelStation0good_1->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        labelStation0good_1->setFrameShape(QFrame::Panel);
        labelStation0good_1->setFrameShadow(QFrame::Sunken);
        labelStation0good_1->setLineWidth(3);

        gridLayout_1->addWidget(labelStation0good_1, 0, 0, 1, 1);

        labelBadProcent_1 = new QLabel(gridLayoutWidget_3);
        labelBadProcent_1->setObjectName(QString::fromUtf8("labelBadProcent_1"));
        labelBadProcent_1->setMinimumSize(QSize(0, 36));
        labelBadProcent_1->setFont(font4);
        labelBadProcent_1->setFrameShape(QFrame::Panel);
        labelBadProcent_1->setFrameShadow(QFrame::Sunken);
        labelBadProcent_1->setLineWidth(3);
        labelBadProcent_1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_1->addWidget(labelBadProcent_1, 1, 2, 1, 1);

        frameChart = new QFrame(centralWidget);
        frameChart->setObjectName(QString::fromUtf8("frameChart"));
        frameChart->setGeometry(QRect(1020, 170, 581, 331));
        frameChart->setFrameShape(QFrame::StyledPanel);
        frameChart->setFrameShadow(QFrame::Sunken);
        verticalLayoutWidget_5 = new QWidget(frameChart);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 10, 561, 311));
        chartLayout = new QVBoxLayout(verticalLayoutWidget_5);
        chartLayout->setSpacing(6);
        chartLayout->setContentsMargins(11, 11, 11, 11);
        chartLayout->setObjectName(QString::fromUtf8("chartLayout"));
        chartLayout->setContentsMargins(0, 0, 0, 0);
        frameNavoj = new QFrame(centralWidget);
        frameNavoj->setObjectName(QString::fromUtf8("frameNavoj"));
        frameNavoj->setGeometry(QRect(1420, 780, 491, 131));
        frameNavoj->setFrameShape(QFrame::StyledPanel);
        frameNavoj->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_4 = new QWidget(frameNavoj);
        gridLayoutWidget_4->setObjectName(QString::fromUtf8("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(10, 50, 471, 80));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        labelStation0good_2 = new QLabel(gridLayoutWidget_4);
        labelStation0good_2->setObjectName(QString::fromUtf8("labelStation0good_2"));
        labelStation0good_2->setMinimumSize(QSize(150, 36));
        labelStation0good_2->setFont(font4);
        labelStation0good_2->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        labelStation0good_2->setFrameShape(QFrame::Panel);
        labelStation0good_2->setFrameShadow(QFrame::Sunken);
        labelStation0good_2->setLineWidth(3);

        gridLayout_3->addWidget(labelStation0good_2, 0, 0, 1, 1);

        labelGoodN0 = new QLabel(gridLayoutWidget_4);
        labelGoodN0->setObjectName(QString::fromUtf8("labelGoodN0"));
        labelGoodN0->setFont(font4);
        labelGoodN0->setStyleSheet(QString::fromUtf8(""));
        labelGoodN0->setFrameShape(QFrame::Panel);
        labelGoodN0->setFrameShadow(QFrame::Sunken);
        labelGoodN0->setLineWidth(3);
        labelGoodN0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelGoodN0, 0, 1, 1, 1);

        labelBadProcentN0 = new QLabel(gridLayoutWidget_4);
        labelBadProcentN0->setObjectName(QString::fromUtf8("labelBadProcentN0"));
        labelBadProcentN0->setMinimumSize(QSize(0, 36));
        labelBadProcentN0->setFont(font4);
        labelBadProcentN0->setFrameShape(QFrame::Panel);
        labelBadProcentN0->setFrameShadow(QFrame::Sunken);
        labelBadProcentN0->setLineWidth(3);
        labelBadProcentN0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelBadProcentN0, 1, 2, 1, 1);

        labelBADN0 = new QLabel(gridLayoutWidget_4);
        labelBADN0->setObjectName(QString::fromUtf8("labelBADN0"));
        labelBADN0->setMinimumSize(QSize(0, 36));
        labelBADN0->setFont(font4);
        labelBADN0->setFrameShape(QFrame::Panel);
        labelBADN0->setFrameShadow(QFrame::Sunken);
        labelBADN0->setLineWidth(3);
        labelBADN0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelBADN0, 1, 1, 1, 1);

        labelGoodProcentN0 = new QLabel(gridLayoutWidget_4);
        labelGoodProcentN0->setObjectName(QString::fromUtf8("labelGoodProcentN0"));
        labelGoodProcentN0->setMinimumSize(QSize(0, 36));
        labelGoodProcentN0->setFont(font4);
        labelGoodProcentN0->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcentN0->setFrameShape(QFrame::Panel);
        labelGoodProcentN0->setFrameShadow(QFrame::Sunken);
        labelGoodProcentN0->setLineWidth(3);
        labelGoodProcentN0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelGoodProcentN0, 0, 2, 1, 1);

        labelStation0bad_2 = new QLabel(gridLayoutWidget_4);
        labelStation0bad_2->setObjectName(QString::fromUtf8("labelStation0bad_2"));
        labelStation0bad_2->setMinimumSize(QSize(150, 36));
        labelStation0bad_2->setFont(font4);
        labelStation0bad_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelStation0bad_2->setFrameShape(QFrame::Panel);
        labelStation0bad_2->setFrameShadow(QFrame::Sunken);
        labelStation0bad_2->setLineWidth(3);

        gridLayout_3->addWidget(labelStation0bad_2, 1, 0, 1, 1);

        labelStation2_4 = new QLabel(frameNavoj);
        labelStation2_4->setObjectName(QString::fromUtf8("labelStation2_4"));
        labelStation2_4->setGeometry(QRect(10, 10, 251, 36));
        labelStation2_4->setFont(font3);
        labelStation2_4->setFrameShape(QFrame::NoFrame);
        labelStation2_4->setFrameShadow(QFrame::Raised);
        labelStatusN0 = new QLabel(frameNavoj);
        labelStatusN0->setObjectName(QString::fromUtf8("labelStatusN0"));
        labelStatusN0->setGeometry(QRect(250, 10, 231, 36));
        labelStatusN0->setFont(font3);
        labelStatusN0->setFrameShape(QFrame::Box);
        labelStatusN0->setFrameShadow(QFrame::Raised);
        labelStatusN0->setTextFormat(Qt::PlainText);
        labelStatusN0->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        frameNavoj_2 = new QFrame(centralWidget);
        frameNavoj_2->setObjectName(QString::fromUtf8("frameNavoj_2"));
        frameNavoj_2->setGeometry(QRect(1420, 910, 491, 131));
        frameNavoj_2->setFrameShape(QFrame::StyledPanel);
        frameNavoj_2->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_5 = new QWidget(frameNavoj_2);
        gridLayoutWidget_5->setObjectName(QString::fromUtf8("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(10, 50, 471, 80));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        labelGoodN0_2 = new QLabel(gridLayoutWidget_5);
        labelGoodN0_2->setObjectName(QString::fromUtf8("labelGoodN0_2"));
        labelGoodN0_2->setFont(font4);
        labelGoodN0_2->setStyleSheet(QString::fromUtf8(""));
        labelGoodN0_2->setFrameShape(QFrame::Panel);
        labelGoodN0_2->setFrameShadow(QFrame::Sunken);
        labelGoodN0_2->setLineWidth(3);
        labelGoodN0_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelGoodN0_2, 0, 1, 1, 1);

        labelBADN0_2 = new QLabel(gridLayoutWidget_5);
        labelBADN0_2->setObjectName(QString::fromUtf8("labelBADN0_2"));
        labelBADN0_2->setMinimumSize(QSize(0, 36));
        labelBADN0_2->setFont(font4);
        labelBADN0_2->setFrameShape(QFrame::Panel);
        labelBADN0_2->setFrameShadow(QFrame::Sunken);
        labelBADN0_2->setLineWidth(3);
        labelBADN0_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelBADN0_2, 1, 1, 1, 1);

        labelStation0bad_4 = new QLabel(gridLayoutWidget_5);
        labelStation0bad_4->setObjectName(QString::fromUtf8("labelStation0bad_4"));
        labelStation0bad_4->setMinimumSize(QSize(150, 36));
        labelStation0bad_4->setFont(font4);
        labelStation0bad_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelStation0bad_4->setFrameShape(QFrame::Panel);
        labelStation0bad_4->setFrameShadow(QFrame::Sunken);
        labelStation0bad_4->setLineWidth(3);

        gridLayout_4->addWidget(labelStation0bad_4, 1, 0, 1, 1);

        labelGoodProcentN0_2 = new QLabel(gridLayoutWidget_5);
        labelGoodProcentN0_2->setObjectName(QString::fromUtf8("labelGoodProcentN0_2"));
        labelGoodProcentN0_2->setMinimumSize(QSize(0, 36));
        labelGoodProcentN0_2->setFont(font4);
        labelGoodProcentN0_2->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcentN0_2->setFrameShape(QFrame::Panel);
        labelGoodProcentN0_2->setFrameShadow(QFrame::Sunken);
        labelGoodProcentN0_2->setLineWidth(3);
        labelGoodProcentN0_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelGoodProcentN0_2, 0, 2, 1, 1);

        labelBadProcentN0_2 = new QLabel(gridLayoutWidget_5);
        labelBadProcentN0_2->setObjectName(QString::fromUtf8("labelBadProcentN0_2"));
        labelBadProcentN0_2->setMinimumSize(QSize(0, 36));
        labelBadProcentN0_2->setFont(font4);
        labelBadProcentN0_2->setFrameShape(QFrame::Panel);
        labelBadProcentN0_2->setFrameShadow(QFrame::Sunken);
        labelBadProcentN0_2->setLineWidth(3);
        labelBadProcentN0_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelBadProcentN0_2, 1, 2, 1, 1);

        labelStation0good_3 = new QLabel(gridLayoutWidget_5);
        labelStation0good_3->setObjectName(QString::fromUtf8("labelStation0good_3"));
        labelStation0good_3->setMinimumSize(QSize(150, 36));
        labelStation0good_3->setFont(font4);
        labelStation0good_3->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        labelStation0good_3->setFrameShape(QFrame::Panel);
        labelStation0good_3->setFrameShadow(QFrame::Sunken);
        labelStation0good_3->setLineWidth(3);

        gridLayout_4->addWidget(labelStation0good_3, 0, 0, 1, 1);

        labelStation2_5 = new QLabel(frameNavoj_2);
        labelStation2_5->setObjectName(QString::fromUtf8("labelStation2_5"));
        labelStation2_5->setGeometry(QRect(10, 10, 251, 36));
        labelStation2_5->setFont(font3);
        labelStation2_5->setFrameShape(QFrame::NoFrame);
        labelStation2_5->setFrameShadow(QFrame::Raised);
        labelStatusN1 = new QLabel(frameNavoj_2);
        labelStatusN1->setObjectName(QString::fromUtf8("labelStatusN1"));
        labelStatusN1->setGeometry(QRect(250, 10, 231, 36));
        labelStatusN1->setFont(font3);
        labelStatusN1->setFrameShape(QFrame::Box);
        labelStatusN1->setFrameShadow(QFrame::Raised);
        labelStatusN1->setTextFormat(Qt::PlainText);
        labelStatusN1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        frameNavoj_3 = new QFrame(centralWidget);
        frameNavoj_3->setObjectName(QString::fromUtf8("frameNavoj_3"));
        frameNavoj_3->setGeometry(QRect(1020, 790, 391, 251));
        frameNavoj_3->setFrameShape(QFrame::StyledPanel);
        frameNavoj_3->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_6 = new QWidget(frameNavoj_3);
        gridLayoutWidget_6->setObjectName(QString::fromUtf8("gridLayoutWidget_6"));
        gridLayoutWidget_6->setGeometry(QRect(10, 40, 371, 206));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_6);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        labelBadBox3 = new QLabel(gridLayoutWidget_6);
        labelBadBox3->setObjectName(QString::fromUtf8("labelBadBox3"));
        labelBadBox3->setMinimumSize(QSize(0, 36));
        labelBadBox3->setFont(font4);
        labelBadBox3->setFrameShape(QFrame::Panel);
        labelBadBox3->setFrameShadow(QFrame::Sunken);
        labelBadBox3->setLineWidth(3);
        labelBadBox3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelBadBox3, 4, 1, 1, 1);

        labelStation0bad_7 = new QLabel(gridLayoutWidget_6);
        labelStation0bad_7->setObjectName(QString::fromUtf8("labelStation0bad_7"));
        labelStation0bad_7->setMinimumSize(QSize(80, 36));
        labelStation0bad_7->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_7->setFont(font4);
        labelStation0bad_7->setStyleSheet(QString::fromUtf8(""));
        labelStation0bad_7->setFrameShape(QFrame::Panel);
        labelStation0bad_7->setFrameShadow(QFrame::Sunken);
        labelStation0bad_7->setLineWidth(3);

        gridLayout_5->addWidget(labelStation0bad_7, 4, 0, 1, 1);

        labelStation0bad_8 = new QLabel(gridLayoutWidget_6);
        labelStation0bad_8->setObjectName(QString::fromUtf8("labelStation0bad_8"));
        labelStation0bad_8->setMinimumSize(QSize(80, 36));
        labelStation0bad_8->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_8->setFont(font4);
        labelStation0bad_8->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        labelStation0bad_8->setFrameShape(QFrame::Panel);
        labelStation0bad_8->setFrameShadow(QFrame::Sunken);
        labelStation0bad_8->setLineWidth(3);

        gridLayout_5->addWidget(labelStation0bad_8, 3, 0, 1, 1);

        labelGoodBox1 = new QLabel(gridLayoutWidget_6);
        labelGoodBox1->setObjectName(QString::fromUtf8("labelGoodBox1"));
        labelGoodBox1->setFont(font4);
        labelGoodBox1->setStyleSheet(QString::fromUtf8(""));
        labelGoodBox1->setFrameShape(QFrame::Panel);
        labelGoodBox1->setFrameShadow(QFrame::Sunken);
        labelGoodBox1->setLineWidth(3);
        labelGoodBox1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelGoodBox1, 0, 1, 1, 1);

        labelBadBox2 = new QLabel(gridLayoutWidget_6);
        labelBadBox2->setObjectName(QString::fromUtf8("labelBadBox2"));
        labelBadBox2->setMinimumSize(QSize(0, 36));
        labelBadBox2->setFont(font4);
        labelBadBox2->setFrameShape(QFrame::Panel);
        labelBadBox2->setFrameShadow(QFrame::Sunken);
        labelBadBox2->setLineWidth(3);
        labelBadBox2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelBadBox2, 3, 1, 1, 1);

        labelGoodBox111 = new QLabel(gridLayoutWidget_6);
        labelGoodBox111->setObjectName(QString::fromUtf8("labelGoodBox111"));
        labelGoodBox111->setMinimumSize(QSize(80, 36));
        labelGoodBox111->setMaximumSize(QSize(230, 16777215));
        labelGoodBox111->setFont(font4);
        labelGoodBox111->setStyleSheet(QString::fromUtf8(""));
        labelGoodBox111->setFrameShape(QFrame::Panel);
        labelGoodBox111->setFrameShadow(QFrame::Sunken);
        labelGoodBox111->setLineWidth(3);

        gridLayout_5->addWidget(labelGoodBox111, 0, 0, 1, 1);

        buttonReset1 = new QToolButton(gridLayoutWidget_6);
        buttonReset1->setObjectName(QString::fromUtf8("buttonReset1"));
        buttonReset1->setMinimumSize(QSize(60, 36));

        gridLayout_5->addWidget(buttonReset1, 0, 2, 1, 1);

        buttonReset4 = new QToolButton(gridLayoutWidget_6);
        buttonReset4->setObjectName(QString::fromUtf8("buttonReset4"));
        buttonReset4->setMinimumSize(QSize(60, 36));

        gridLayout_5->addWidget(buttonReset4, 3, 2, 1, 1);

        buttonReset5 = new QToolButton(gridLayoutWidget_6);
        buttonReset5->setObjectName(QString::fromUtf8("buttonReset5"));
        buttonReset5->setMinimumSize(QSize(60, 36));

        gridLayout_5->addWidget(buttonReset5, 4, 2, 1, 1);

        labelStation0bad_9 = new QLabel(gridLayoutWidget_6);
        labelStation0bad_9->setObjectName(QString::fromUtf8("labelStation0bad_9"));
        labelStation0bad_9->setMinimumSize(QSize(80, 36));
        labelStation0bad_9->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_9->setFont(font4);
        labelStation0bad_9->setStyleSheet(QString::fromUtf8(""));
        labelStation0bad_9->setFrameShape(QFrame::Panel);
        labelStation0bad_9->setFrameShadow(QFrame::Sunken);
        labelStation0bad_9->setLineWidth(3);

        gridLayout_5->addWidget(labelStation0bad_9, 1, 0, 1, 1);

        labelBadBox1 = new QLabel(gridLayoutWidget_6);
        labelBadBox1->setObjectName(QString::fromUtf8("labelBadBox1"));
        labelBadBox1->setMinimumSize(QSize(0, 36));
        labelBadBox1->setFont(font4);
        labelBadBox1->setFrameShape(QFrame::Panel);
        labelBadBox1->setFrameShadow(QFrame::Sunken);
        labelBadBox1->setLineWidth(3);
        labelBadBox1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelBadBox1, 1, 1, 1, 1);

        labelStation0bad_10 = new QLabel(gridLayoutWidget_6);
        labelStation0bad_10->setObjectName(QString::fromUtf8("labelStation0bad_10"));
        labelStation0bad_10->setMinimumSize(QSize(80, 36));
        labelStation0bad_10->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_10->setFont(font4);
        labelStation0bad_10->setStyleSheet(QString::fromUtf8("background-color: rgb(200, 200, 200);"));
        labelStation0bad_10->setFrameShape(QFrame::Panel);
        labelStation0bad_10->setFrameShadow(QFrame::Sunken);
        labelStation0bad_10->setLineWidth(3);

        gridLayout_5->addWidget(labelStation0bad_10, 2, 0, 1, 1);

        labelGoodBox2 = new QLabel(gridLayoutWidget_6);
        labelGoodBox2->setObjectName(QString::fromUtf8("labelGoodBox2"));
        labelGoodBox2->setMinimumSize(QSize(0, 36));
        labelGoodBox2->setFont(font4);
        labelGoodBox2->setFrameShape(QFrame::Panel);
        labelGoodBox2->setFrameShadow(QFrame::Sunken);
        labelGoodBox2->setLineWidth(3);
        labelGoodBox2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelGoodBox2, 2, 1, 1, 1);

        buttonReset2 = new QToolButton(gridLayoutWidget_6);
        buttonReset2->setObjectName(QString::fromUtf8("buttonReset2"));
        buttonReset2->setMinimumSize(QSize(60, 36));

        gridLayout_5->addWidget(buttonReset2, 2, 2, 1, 1);

        buttonReset3 = new QToolButton(gridLayoutWidget_6);
        buttonReset3->setObjectName(QString::fromUtf8("buttonReset3"));
        buttonReset3->setMinimumSize(QSize(60, 36));

        gridLayout_5->addWidget(buttonReset3, 1, 2, 1, 1);

        labelStation2_6 = new QLabel(frameNavoj_3);
        labelStation2_6->setObjectName(QString::fromUtf8("labelStation2_6"));
        labelStation2_6->setGeometry(QRect(0, 0, 251, 36));
        labelStation2_6->setFont(font3);
        labelStation2_6->setFrameShape(QFrame::NoFrame);
        labelStation2_6->setFrameShadow(QFrame::Raised);
        frameNavoj_4 = new QFrame(centralWidget);
        frameNavoj_4->setObjectName(QString::fromUtf8("frameNavoj_4"));
        frameNavoj_4->setGeometry(QRect(1020, 510, 581, 261));
        frameNavoj_4->setFrameShape(QFrame::StyledPanel);
        frameNavoj_4->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget_7 = new QWidget(frameNavoj_4);
        gridLayoutWidget_7->setObjectName(QString::fromUtf8("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(10, 40, 561, 211));
        gridLayout_6 = new QGridLayout(gridLayoutWidget_7);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        labelSlabiKam = new QLabel(gridLayoutWidget_7);
        labelSlabiKam->setObjectName(QString::fromUtf8("labelSlabiKam"));
        labelSlabiKam->setFont(font4);
        labelSlabiKam->setStyleSheet(QString::fromUtf8(""));
        labelSlabiKam->setFrameShape(QFrame::Panel);
        labelSlabiKam->setFrameShadow(QFrame::Sunken);
        labelSlabiKam->setLineWidth(3);
        labelSlabiKam->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelSlabiKam, 0, 1, 1, 1);

        labelSlabiNavojProc = new QLabel(gridLayoutWidget_7);
        labelSlabiNavojProc->setObjectName(QString::fromUtf8("labelSlabiNavojProc"));
        labelSlabiNavojProc->setMinimumSize(QSize(0, 36));
        labelSlabiNavojProc->setMaximumSize(QSize(80, 16777215));
        labelSlabiNavojProc->setFont(font4);
        labelSlabiNavojProc->setFrameShape(QFrame::Panel);
        labelSlabiNavojProc->setFrameShadow(QFrame::Sunken);
        labelSlabiNavojProc->setLineWidth(3);
        labelSlabiNavojProc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelSlabiNavojProc, 1, 2, 1, 1);

        labelSlabiKamProc = new QLabel(gridLayoutWidget_7);
        labelSlabiKamProc->setObjectName(QString::fromUtf8("labelSlabiKamProc"));
        labelSlabiKamProc->setMaximumSize(QSize(80, 16777215));
        labelSlabiKamProc->setFont(font4);
        labelSlabiKamProc->setStyleSheet(QString::fromUtf8(""));
        labelSlabiKamProc->setFrameShape(QFrame::Panel);
        labelSlabiKamProc->setFrameShadow(QFrame::Sunken);
        labelSlabiKamProc->setLineWidth(3);
        labelSlabiKamProc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelSlabiKamProc, 0, 2, 1, 1);

        labelStation0bad_13 = new QLabel(gridLayoutWidget_7);
        labelStation0bad_13->setObjectName(QString::fromUtf8("labelStation0bad_13"));
        labelStation0bad_13->setMinimumSize(QSize(80, 36));
        labelStation0bad_13->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_13->setFont(font4);
        labelStation0bad_13->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelStation0bad_13->setFrameShape(QFrame::Panel);
        labelStation0bad_13->setFrameShadow(QFrame::Sunken);
        labelStation0bad_13->setLineWidth(3);

        gridLayout_6->addWidget(labelStation0bad_13, 1, 0, 1, 1);

        labelStation0bad_14 = new QLabel(gridLayoutWidget_7);
        labelStation0bad_14->setObjectName(QString::fromUtf8("labelStation0bad_14"));
        labelStation0bad_14->setMinimumSize(QSize(180, 36));
        labelStation0bad_14->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_14->setFont(font4);
        labelStation0bad_14->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        labelStation0bad_14->setFrameShape(QFrame::Panel);
        labelStation0bad_14->setFrameShadow(QFrame::Sunken);
        labelStation0bad_14->setLineWidth(3);

        gridLayout_6->addWidget(labelStation0bad_14, 2, 0, 1, 1);

        labelSlabiNavoj = new QLabel(gridLayoutWidget_7);
        labelSlabiNavoj->setObjectName(QString::fromUtf8("labelSlabiNavoj"));
        labelSlabiNavoj->setMinimumSize(QSize(0, 36));
        labelSlabiNavoj->setFont(font4);
        labelSlabiNavoj->setFrameShape(QFrame::Panel);
        labelSlabiNavoj->setFrameShadow(QFrame::Sunken);
        labelSlabiNavoj->setLineWidth(3);
        labelSlabiNavoj->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelSlabiNavoj, 1, 1, 1, 1);

        labelGoodBox111_2 = new QLabel(gridLayoutWidget_7);
        labelGoodBox111_2->setObjectName(QString::fromUtf8("labelGoodBox111_2"));
        labelGoodBox111_2->setMinimumSize(QSize(80, 36));
        labelGoodBox111_2->setMaximumSize(QSize(230, 16777215));
        labelGoodBox111_2->setFont(font4);
        labelGoodBox111_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        labelGoodBox111_2->setFrameShape(QFrame::Panel);
        labelGoodBox111_2->setFrameShadow(QFrame::Sunken);
        labelGoodBox111_2->setLineWidth(3);

        gridLayout_6->addWidget(labelGoodBox111_2, 0, 0, 1, 1);

        labelDobri = new QLabel(gridLayoutWidget_7);
        labelDobri->setObjectName(QString::fromUtf8("labelDobri"));
        labelDobri->setMinimumSize(QSize(250, 36));
        labelDobri->setFont(font4);
        labelDobri->setFrameShape(QFrame::Panel);
        labelDobri->setFrameShadow(QFrame::Sunken);
        labelDobri->setLineWidth(3);
        labelDobri->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelDobri, 2, 1, 1, 1);

        labelStation0bad_12 = new QLabel(gridLayoutWidget_7);
        labelStation0bad_12->setObjectName(QString::fromUtf8("labelStation0bad_12"));
        labelStation0bad_12->setMinimumSize(QSize(80, 36));
        labelStation0bad_12->setMaximumSize(QSize(230, 16777215));
        labelStation0bad_12->setFont(font4);
        labelStation0bad_12->setStyleSheet(QString::fromUtf8(""));
        labelStation0bad_12->setFrameShape(QFrame::Panel);
        labelStation0bad_12->setFrameShadow(QFrame::Sunken);
        labelStation0bad_12->setLineWidth(3);

        gridLayout_6->addWidget(labelStation0bad_12, 3, 0, 1, 1);

        labelVsi = new QLabel(gridLayoutWidget_7);
        labelVsi->setObjectName(QString::fromUtf8("labelVsi"));
        labelVsi->setMinimumSize(QSize(0, 36));
        labelVsi->setFont(font4);
        labelVsi->setFrameShape(QFrame::Panel);
        labelVsi->setFrameShadow(QFrame::Sunken);
        labelVsi->setLineWidth(3);
        labelVsi->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelVsi, 3, 1, 1, 1);

        labelDobriProc = new QLabel(gridLayoutWidget_7);
        labelDobriProc->setObjectName(QString::fromUtf8("labelDobriProc"));
        labelDobriProc->setMinimumSize(QSize(0, 36));
        labelDobriProc->setMaximumSize(QSize(80, 16777215));
        labelDobriProc->setFont(font4);
        labelDobriProc->setFrameShape(QFrame::Panel);
        labelDobriProc->setFrameShadow(QFrame::Sunken);
        labelDobriProc->setLineWidth(3);
        labelDobriProc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelDobriProc, 2, 2, 1, 1);

        labelStation2_7 = new QLabel(frameNavoj_4);
        labelStation2_7->setObjectName(QString::fromUtf8("labelStation2_7"));
        labelStation2_7->setGeometry(QRect(10, 0, 251, 36));
        labelStation2_7->setFont(font3);
        labelStation2_7->setFrameShape(QFrame::NoFrame);
        labelStation2_7->setFrameShadow(QFrame::Raised);
        MBsoftwareClass->setCentralWidget(centralWidget);

        retranslateUi(MBsoftwareClass);

        MainMenuBar->setCurrentIndex(6);


        QMetaObject::connectSlotsByName(MBsoftwareClass);
    } // setupUi

    void retranslateUi(QMainWindow *MBsoftwareClass)
    {
        MBsoftwareClass->setWindowTitle(QCoreApplication::translate("MBsoftwareClass", "MBsoftware", nullptr));
        labelDeviceStatus->setText(QCoreApplication::translate("MBsoftwareClass", "DEVICE STATUS:", nullptr));
        labelLogin->setText(QString());
        labelTip->setText(QString());
        labelStatus->setText(QString());
        labelEpson->setText(QString());
        labelPC2->setText(QString());
        labelVibrator->setText(QString());
        labelVrata->setText(QString());
        labelTakt->setText(QString());
        labelTakt_2->setText(QString());
        buttonLogin->setText(QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabLogin), QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        buttonGeneralSettings->setText(QCoreApplication::translate("MBsoftwareClass", "System settings", nullptr));
        buttonKoracno->setText(QCoreApplication::translate("MBsoftwareClass", "Kora\304\215no DIALOG", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSettings), QCoreApplication::translate("MBsoftwareClass", "Settings", nullptr));
        buttonSetTolerance->setText(QCoreApplication::translate("MBsoftwareClass", "Edit tolerance", nullptr));
        buttonTypeSelect->setText(QCoreApplication::translate("MBsoftwareClass", "Type select", nullptr));
        buttonAddType->setText(QCoreApplication::translate("MBsoftwareClass", "Add type", nullptr));
        buttonRemoveType->setText(QCoreApplication::translate("MBsoftwareClass", "Remove type", nullptr));
        buttonTypeSettings->setText(QCoreApplication::translate("MBsoftwareClass", "Type Settings", nullptr));
        buttonResetCounters->setText(QCoreApplication::translate("MBsoftwareClass", "Reset Counters", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabTypes), QCoreApplication::translate("MBsoftwareClass", "Types", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabCameras), QCoreApplication::translate("MBsoftwareClass", "Cameras", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSignals), QCoreApplication::translate("MBsoftwareClass", "I/O signals", nullptr));
        buttonImageProcessing->setText(QCoreApplication::translate("MBsoftwareClass", "ImageProcessing Dialog", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabImageProcessing), QCoreApplication::translate("MBsoftwareClass", "ImageProcessing", nullptr));
        buttonHistory->setText(QCoreApplication::translate("MBsoftwareClass", "History", nullptr));
        buttonStatusBox->setText(QCoreApplication::translate("MBsoftwareClass", "Status window\n"
"", nullptr));
        buttonAboutUs->setText(QCoreApplication::translate("MBsoftwareClass", "About Us", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabStatistcs), QCoreApplication::translate("MBsoftwareClass", "Statistics && Status", nullptr));
        labelLogo->setText(QString());
        labelStation2_3->setText(QCoreApplication::translate("MBsoftwareClass", "POSTAJA 0:", nullptr));
        labelStatusP0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        labelGood_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBAD_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodProcent_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadProcent_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0good->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        labelStation0counters->setText(QCoreApplication::translate("MBsoftwareClass", "\305\240tevci postaje 2:", nullptr));
        labelStation0counters_1->setText(QCoreApplication::translate("MBsoftwareClass", "STATION 0 COUNTERS:", nullptr));
        labelStationP1->setText(QCoreApplication::translate("MBsoftwareClass", "POSTAJA 1:", nullptr));
        labelStatusP1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStatusP2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation2_2->setText(QCoreApplication::translate("MBsoftwareClass", "POSTAJA 2:", nullptr));
        labelStation0bad_1->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        labelBAD_1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodProcent_1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGood_1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0good_1->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        labelBadProcent_1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0good_2->setText(QCoreApplication::translate("MBsoftwareClass", "DOBRI:", nullptr));
        labelGoodN0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadProcentN0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBADN0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodProcentN0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_2->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI:", nullptr));
        labelStation2_4->setText(QCoreApplication::translate("MBsoftwareClass", "MERITEV NAVOJA POSTAJA 1:", nullptr));
        labelStatusN0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodN0_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBADN0_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_4->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI:", nullptr));
        labelGoodProcentN0_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadProcentN0_2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0good_3->setText(QCoreApplication::translate("MBsoftwareClass", "DOBRI:", nullptr));
        labelStation2_5->setText(QCoreApplication::translate("MBsoftwareClass", "MERITEV NAVOJA POSTAJA 2:", nullptr));
        labelStatusN1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadBox3->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_7->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI KOSI \305\240KATLA DIMENZIJE:", nullptr));
        labelStation0bad_8->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI KOSI \305\240KATLA 2:", nullptr));
        labelGoodBox1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelBadBox2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodBox111->setText(QCoreApplication::translate("MBsoftwareClass", "DOBRI KOSI \305\240KATLA 1:", nullptr));
        buttonReset1->setText(QCoreApplication::translate("MBsoftwareClass", "RESET", nullptr));
        buttonReset4->setText(QCoreApplication::translate("MBsoftwareClass", "RESET", nullptr));
        buttonReset5->setText(QCoreApplication::translate("MBsoftwareClass", "RESET", nullptr));
        labelStation0bad_9->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI KOSI \305\240KATLA 1:", nullptr));
        labelBadBox1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_10->setText(QCoreApplication::translate("MBsoftwareClass", "DOBRI KOSI \305\240KATLA 2:", nullptr));
        labelGoodBox2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        buttonReset2->setText(QCoreApplication::translate("MBsoftwareClass", "RESET", nullptr));
        buttonReset3->setText(QCoreApplication::translate("MBsoftwareClass", "RESET", nullptr));
        labelStation2_6->setText(QCoreApplication::translate("MBsoftwareClass", "\305\240TEVCI KOSOV V \305\240KATLAH:", nullptr));
        labelSlabiKam->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelSlabiNavojProc->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelSlabiKamProc->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_13->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI NAVOJ:", nullptr));
        labelStation0bad_14->setText(QCoreApplication::translate("MBsoftwareClass", "DOBRI SKUPAJ:", nullptr));
        labelSlabiNavoj->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelGoodBox111_2->setText(QCoreApplication::translate("MBsoftwareClass", "SLABI KAMERE:", nullptr));
        labelDobri->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation0bad_12->setText(QCoreApplication::translate("MBsoftwareClass", "VSI KOSI:", nullptr));
        labelVsi->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelDobriProc->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelStation2_7->setText(QCoreApplication::translate("MBsoftwareClass", "\305\240TEVCI VSEH KOSOV:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MBsoftwareClass: public Ui_MBsoftwareClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSOFTWARE_H
