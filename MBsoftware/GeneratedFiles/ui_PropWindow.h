/********************************************************************************
** Form generated from reading UI file 'PropWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROPWINDOW_H
#define UI_PROPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PropWindow
{
public:
    QTreeView *treeProperty;

    void setupUi(QWidget *PropWindow)
    {
        if (PropWindow->objectName().isEmpty())
            PropWindow->setObjectName(QString::fromUtf8("PropWindow"));
        PropWindow->resize(400, 300);
        treeProperty = new QTreeView(PropWindow);
        treeProperty->setObjectName(QString::fromUtf8("treeProperty"));
        treeProperty->setGeometry(QRect(15, 11, 341, 261));

        retranslateUi(PropWindow);

        QMetaObject::connectSlotsByName(PropWindow);
    } // setupUi

    void retranslateUi(QWidget *PropWindow)
    {
        PropWindow->setWindowTitle(QCoreApplication::translate("PropWindow", "PropWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PropWindow: public Ui_PropWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROPWINDOW_H
