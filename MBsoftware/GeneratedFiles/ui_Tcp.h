/********************************************************************************
** Form generated from reading UI file 'Tcp.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCP_H
#define UI_TCP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TcpForm
{
public:
    QPushButton *buttonSend;
    QPlainTextEdit *TextEdit;
    QLineEdit *lineSend;

    void setupUi(QWidget *TcpForm)
    {
        if (TcpForm->objectName().isEmpty())
            TcpForm->setObjectName(QString::fromUtf8("TcpForm"));
        TcpForm->resize(512, 439);
        buttonSend = new QPushButton(TcpForm);
        buttonSend->setObjectName(QString::fromUtf8("buttonSend"));
        buttonSend->setGeometry(QRect(370, 400, 131, 31));
        TextEdit = new QPlainTextEdit(TcpForm);
        TextEdit->setObjectName(QString::fromUtf8("TextEdit"));
        TextEdit->setGeometry(QRect(10, 10, 491, 381));
        lineSend = new QLineEdit(TcpForm);
        lineSend->setObjectName(QString::fromUtf8("lineSend"));
        lineSend->setGeometry(QRect(12, 399, 341, 31));

        retranslateUi(TcpForm);

        QMetaObject::connectSlotsByName(TcpForm);
    } // setupUi

    void retranslateUi(QWidget *TcpForm)
    {
        TcpForm->setWindowTitle(QCoreApplication::translate("TcpForm", "Form", nullptr));
        buttonSend->setText(QCoreApplication::translate("TcpForm", "send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TcpForm: public Ui_TcpForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCP_H
