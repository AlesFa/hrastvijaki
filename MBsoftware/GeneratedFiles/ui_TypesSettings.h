/********************************************************************************
** Form generated from reading UI file 'TypesSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TYPESSETTINGS_H
#define UI_TYPESSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TypeSettings
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *labelTypeName;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox0;
    QGroupBox *groupBox1;
    QGroupBox *groupBox2;
    QGroupBox *groupBox3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonGroupName;
    QPushButton *buttonAddSetting;
    QPushButton *buttonRemoveSetting;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonOK;
    QPushButton *buttonCancel;

    void setupUi(QWidget *TypeSettings)
    {
        if (TypeSettings->objectName().isEmpty())
            TypeSettings->setObjectName(QString::fromUtf8("TypeSettings"));
        TypeSettings->resize(1024, 608);
        verticalLayout = new QVBoxLayout(TypeSettings);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelTypeName = new QLabel(TypeSettings);
        labelTypeName->setObjectName(QString::fromUtf8("labelTypeName"));
        labelTypeName->setMaximumSize(QSize(16777215, 30));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelTypeName->setFont(font);
        labelTypeName->setFrameShape(QFrame::StyledPanel);

        verticalLayout->addWidget(labelTypeName);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        groupBox0 = new QGroupBox(TypeSettings);
        groupBox0->setObjectName(QString::fromUtf8("groupBox0"));

        horizontalLayout->addWidget(groupBox0);

        groupBox1 = new QGroupBox(TypeSettings);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));

        horizontalLayout->addWidget(groupBox1);

        groupBox2 = new QGroupBox(TypeSettings);
        groupBox2->setObjectName(QString::fromUtf8("groupBox2"));

        horizontalLayout->addWidget(groupBox2);

        groupBox3 = new QGroupBox(TypeSettings);
        groupBox3->setObjectName(QString::fromUtf8("groupBox3"));

        horizontalLayout->addWidget(groupBox3);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        buttonGroupName = new QPushButton(TypeSettings);
        buttonGroupName->setObjectName(QString::fromUtf8("buttonGroupName"));
        buttonGroupName->setMinimumSize(QSize(0, 40));

        horizontalLayout_2->addWidget(buttonGroupName);

        buttonAddSetting = new QPushButton(TypeSettings);
        buttonAddSetting->setObjectName(QString::fromUtf8("buttonAddSetting"));
        buttonAddSetting->setMaximumSize(QSize(16777215, 40));

        horizontalLayout_2->addWidget(buttonAddSetting);

        buttonRemoveSetting = new QPushButton(TypeSettings);
        buttonRemoveSetting->setObjectName(QString::fromUtf8("buttonRemoveSetting"));
        buttonRemoveSetting->setMaximumSize(QSize(16777215, 40));

        horizontalLayout_2->addWidget(buttonRemoveSetting);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        buttonOK = new QPushButton(TypeSettings);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));
        buttonOK->setMinimumSize(QSize(100, 40));

        horizontalLayout_3->addWidget(buttonOK);

        buttonCancel = new QPushButton(TypeSettings);
        buttonCancel->setObjectName(QString::fromUtf8("buttonCancel"));
        buttonCancel->setMinimumSize(QSize(100, 40));

        horizontalLayout_3->addWidget(buttonCancel);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(TypeSettings);

        QMetaObject::connectSlotsByName(TypeSettings);
    } // setupUi

    void retranslateUi(QWidget *TypeSettings)
    {
        TypeSettings->setWindowTitle(QCoreApplication::translate("TypeSettings", "Type Settings", nullptr));
        labelTypeName->setText(QCoreApplication::translate("TypeSettings", "Type Name:", nullptr));
        groupBox0->setTitle(QCoreApplication::translate("TypeSettings", "GroupBox", nullptr));
        groupBox1->setTitle(QCoreApplication::translate("TypeSettings", "GroupBox", nullptr));
        groupBox2->setTitle(QCoreApplication::translate("TypeSettings", "GroupBox", nullptr));
        groupBox3->setTitle(QCoreApplication::translate("TypeSettings", "GroupBox", nullptr));
        buttonGroupName->setText(QCoreApplication::translate("TypeSettings", "Change group name", nullptr));
        buttonAddSetting->setText(QCoreApplication::translate("TypeSettings", "Add Setting", nullptr));
        buttonRemoveSetting->setText(QCoreApplication::translate("TypeSettings", "Remove Setting", nullptr));
        buttonOK->setText(QCoreApplication::translate("TypeSettings", "OK", nullptr));
        buttonCancel->setText(QCoreApplication::translate("TypeSettings", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TypeSettings: public Ui_TypeSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TYPESSETTINGS_H
