/********************************************************************************
** Form generated from reading UI file 'imageProcessing.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEPROCESSING_H
#define UI_IMAGEPROCESSING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_imageProcessing
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_3;
    QPushButton *buttonImageUp;
    QPushButton *buttonCamDown;
    QPushButton *buttonImageDown;
    QPushButton *buttonCamUp;
    QLabel *labelInsertImageIndex;
    QLineEdit *lineInsertImageIndex;
    QLabel *labelLocation;
    QPushButton *buttonZoomIn;
    QPushButton *buttonReset;
    QLabel *labelZoom;
    QPushButton *buttonZoomOut;
    QLabel *labelX;
    QLabel *labelY;
    QLabel *labelG;
    QLabel *labelB;
    QLabel *labelR;
    QLabel *labelCurrStation;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_4;
    QPushButton *buttonProcessCam0;
    QPushButton *buttonProcessCam3;
    QPushButton *buttonProcessCam1;
    QPushButton *buttonSurfaceInspection;
    QPushButton *buttonProcessCam4;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *buttonProcessCam2;
    QWidget *tab_calibration;
    QPushButton *buttonRubberResize;
    QPushButton *buttonCreateRubber;
    QWidget *tab_2;
    QToolButton *buttonLoadImage;
    QToolButton *buttonSaveImage;
    QToolButton *buttonLoadMultipleImages;
    QWidget *tabTestFunctions;
    QToolButton *buttonFunctionFunctionAddAsMeasuredParameter;
    QToolButton *buttonParameterEdit;
    QGroupBox *groupBox;
    QToolButton *buttonDrawIntensity;
    QGroupBox *groupBox_2;
    QToolButton *buttonFunctionHeight;
    QToolButton *buttonFunctionLineDeviation;
    QFrame *frame;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QPushButton *addParameter;
    QPushButton *removeParameter;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *update;
    QSpacerItem *horizontalSpacer;
    QPushButton *Cancel;
    QFrame *frame_2;
    QGridLayout *gridLayout_3;
    QGraphicsView *imageView;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QWidget *gridLayoutWidget;
    QGridLayout *ParameterLayout;
    QListWidget *listFunctionsReturn;

    void setupUi(QWidget *imageProcessing)
    {
        if (imageProcessing->objectName().isEmpty())
            imageProcessing->setObjectName(QString::fromUtf8("imageProcessing"));
        imageProcessing->resize(1280, 1024);
        imageProcessing->setMinimumSize(QSize(1280, 1024));
        imageProcessing->setLayoutDirection(Qt::LeftToRight);
        gridLayout = new QGridLayout(imageProcessing);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_3 = new QFrame(imageProcessing);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(0, 220));
        frame_3->setMaximumSize(QSize(16777215, 250));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        buttonImageUp = new QPushButton(frame_3);
        buttonImageUp->setObjectName(QString::fromUtf8("buttonImageUp"));
        buttonImageUp->setGeometry(QRect(60, 70, 50, 50));
        buttonImageUp->setMinimumSize(QSize(50, 50));
        buttonImageUp->setMaximumSize(QSize(50, 50));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        buttonImageUp->setFont(font);
        buttonCamDown = new QPushButton(frame_3);
        buttonCamDown->setObjectName(QString::fromUtf8("buttonCamDown"));
        buttonCamDown->setGeometry(QRect(10, 120, 50, 50));
        buttonCamDown->setMinimumSize(QSize(50, 50));
        buttonCamDown->setMaximumSize(QSize(50, 50));
        buttonCamDown->setFont(font);
        buttonImageDown = new QPushButton(frame_3);
        buttonImageDown->setObjectName(QString::fromUtf8("buttonImageDown"));
        buttonImageDown->setGeometry(QRect(60, 120, 50, 50));
        buttonImageDown->setMinimumSize(QSize(50, 50));
        buttonImageDown->setMaximumSize(QSize(50, 50));
        buttonImageDown->setFont(font);
        buttonCamUp = new QPushButton(frame_3);
        buttonCamUp->setObjectName(QString::fromUtf8("buttonCamUp"));
        buttonCamUp->setGeometry(QRect(110, 120, 50, 50));
        buttonCamUp->setMinimumSize(QSize(50, 50));
        buttonCamUp->setMaximumSize(QSize(50, 50));
        buttonCamUp->setFont(font);
        labelInsertImageIndex = new QLabel(frame_3);
        labelInsertImageIndex->setObjectName(QString::fromUtf8("labelInsertImageIndex"));
        labelInsertImageIndex->setGeometry(QRect(290, 30, 191, 30));
        labelInsertImageIndex->setFont(font);
        lineInsertImageIndex = new QLineEdit(frame_3);
        lineInsertImageIndex->setObjectName(QString::fromUtf8("lineInsertImageIndex"));
        lineInsertImageIndex->setGeometry(QRect(410, 30, 195, 20));
        labelLocation = new QLabel(frame_3);
        labelLocation->setObjectName(QString::fromUtf8("labelLocation"));
        labelLocation->setGeometry(QRect(170, 130, 224, 30));
        labelLocation->setMinimumSize(QSize(100, 0));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        labelLocation->setFont(font1);
        labelLocation->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        buttonZoomIn = new QPushButton(frame_3);
        buttonZoomIn->setObjectName(QString::fromUtf8("buttonZoomIn"));
        buttonZoomIn->setGeometry(QRect(480, 140, 80, 30));
        buttonZoomIn->setMinimumSize(QSize(80, 30));
        buttonZoomIn->setMaximumSize(QSize(80, 30));
        buttonZoomIn->setFont(font);
        buttonReset = new QPushButton(frame_3);
        buttonReset->setObjectName(QString::fromUtf8("buttonReset"));
        buttonReset->setGeometry(QRect(560, 100, 80, 30));
        buttonReset->setMinimumSize(QSize(80, 30));
        buttonReset->setMaximumSize(QSize(60, 30));
        buttonReset->setFont(font);
        labelZoom = new QLabel(frame_3);
        labelZoom->setObjectName(QString::fromUtf8("labelZoom"));
        labelZoom->setGeometry(QRect(650, 110, 71, 21));
        labelZoom->setFont(font);
        buttonZoomOut = new QPushButton(frame_3);
        buttonZoomOut->setObjectName(QString::fromUtf8("buttonZoomOut"));
        buttonZoomOut->setGeometry(QRect(560, 140, 80, 30));
        buttonZoomOut->setMinimumSize(QSize(80, 30));
        buttonZoomOut->setMaximumSize(QSize(60, 30));
        buttonZoomOut->setFont(font);
        labelX = new QLabel(frame_3);
        labelX->setObjectName(QString::fromUtf8("labelX"));
        labelX->setGeometry(QRect(690, 40, 71, 20));
        labelX->setFont(font);
        labelX->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelY = new QLabel(frame_3);
        labelY->setObjectName(QString::fromUtf8("labelY"));
        labelY->setGeometry(QRect(760, 40, 71, 20));
        labelY->setFont(font);
        labelY->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelG = new QLabel(frame_3);
        labelG->setObjectName(QString::fromUtf8("labelG"));
        labelG->setGeometry(QRect(570, 170, 71, 21));
        labelG->setMinimumSize(QSize(50, 0));
        labelG->setFont(font);
        labelG->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelB = new QLabel(frame_3);
        labelB->setObjectName(QString::fromUtf8("labelB"));
        labelB->setGeometry(QRect(660, 170, 50, 21));
        labelB->setMinimumSize(QSize(50, 0));
        labelB->setFont(font);
        labelB->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelR = new QLabel(frame_3);
        labelR->setObjectName(QString::fromUtf8("labelR"));
        labelR->setGeometry(QRect(740, 170, 61, 20));
        labelR->setMinimumSize(QSize(50, 0));
        labelR->setFont(font);
        labelR->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelCurrStation = new QLabel(frame_3);
        labelCurrStation->setObjectName(QString::fromUtf8("labelCurrStation"));
        labelCurrStation->setGeometry(QRect(40, 20, 101, 16));
        labelCurrStation->setFont(font);

        gridLayout->addWidget(frame_3, 1, 1, 1, 2);

        tabWidget = new QTabWidget(imageProcessing);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setMaximumSize(QSize(16777215, 120));
        tabWidget->setFont(font);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_4 = new QGridLayout(tab);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        buttonProcessCam0 = new QPushButton(tab);
        buttonProcessCam0->setObjectName(QString::fromUtf8("buttonProcessCam0"));
        buttonProcessCam0->setMinimumSize(QSize(180, 25));
        buttonProcessCam0->setMaximumSize(QSize(100, 16777215));

        gridLayout_4->addWidget(buttonProcessCam0, 0, 0, 1, 1);

        buttonProcessCam3 = new QPushButton(tab);
        buttonProcessCam3->setObjectName(QString::fromUtf8("buttonProcessCam3"));
        buttonProcessCam3->setMinimumSize(QSize(120, 25));

        gridLayout_4->addWidget(buttonProcessCam3, 0, 1, 1, 1);

        buttonProcessCam1 = new QPushButton(tab);
        buttonProcessCam1->setObjectName(QString::fromUtf8("buttonProcessCam1"));
        buttonProcessCam1->setMinimumSize(QSize(180, 25));
        buttonProcessCam1->setMaximumSize(QSize(100, 16777215));

        gridLayout_4->addWidget(buttonProcessCam1, 2, 0, 1, 1);

        buttonSurfaceInspection = new QPushButton(tab);
        buttonSurfaceInspection->setObjectName(QString::fromUtf8("buttonSurfaceInspection"));
        buttonSurfaceInspection->setMinimumSize(QSize(120, 25));

        gridLayout_4->addWidget(buttonSurfaceInspection, 2, 2, 1, 1);

        buttonProcessCam4 = new QPushButton(tab);
        buttonProcessCam4->setObjectName(QString::fromUtf8("buttonProcessCam4"));
        buttonProcessCam4->setMinimumSize(QSize(120, 25));

        gridLayout_4->addWidget(buttonProcessCam4, 2, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_2, 2, 3, 1, 1);

        buttonProcessCam2 = new QPushButton(tab);
        buttonProcessCam2->setObjectName(QString::fromUtf8("buttonProcessCam2"));
        buttonProcessCam2->setMinimumSize(QSize(180, 25));
        buttonProcessCam2->setMaximumSize(QSize(100, 16777215));

        gridLayout_4->addWidget(buttonProcessCam2, 3, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_calibration = new QWidget();
        tab_calibration->setObjectName(QString::fromUtf8("tab_calibration"));
        buttonRubberResize = new QPushButton(tab_calibration);
        buttonRubberResize->setObjectName(QString::fromUtf8("buttonRubberResize"));
        buttonRubberResize->setGeometry(QRect(50, 20, 181, 31));
        buttonCreateRubber = new QPushButton(tab_calibration);
        buttonCreateRubber->setObjectName(QString::fromUtf8("buttonCreateRubber"));
        buttonCreateRubber->setGeometry(QRect(20, 60, 75, 23));
        tabWidget->addTab(tab_calibration, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        buttonLoadImage = new QToolButton(tab_2);
        buttonLoadImage->setObjectName(QString::fromUtf8("buttonLoadImage"));
        buttonLoadImage->setGeometry(QRect(90, 20, 66, 30));
        buttonLoadImage->setMinimumSize(QSize(50, 30));
        buttonSaveImage = new QToolButton(tab_2);
        buttonSaveImage->setObjectName(QString::fromUtf8("buttonSaveImage"));
        buttonSaveImage->setGeometry(QRect(90, 50, 67, 30));
        buttonSaveImage->setMinimumSize(QSize(50, 30));
        buttonLoadMultipleImages = new QToolButton(tab_2);
        buttonLoadMultipleImages->setObjectName(QString::fromUtf8("buttonLoadMultipleImages"));
        buttonLoadMultipleImages->setGeometry(QRect(210, 60, 111, 30));
        buttonLoadMultipleImages->setMinimumSize(QSize(50, 30));
        tabWidget->addTab(tab_2, QString());
        tabTestFunctions = new QWidget();
        tabTestFunctions->setObjectName(QString::fromUtf8("tabTestFunctions"));
        buttonFunctionFunctionAddAsMeasuredParameter = new QToolButton(tabTestFunctions);
        buttonFunctionFunctionAddAsMeasuredParameter->setObjectName(QString::fromUtf8("buttonFunctionFunctionAddAsMeasuredParameter"));
        buttonFunctionFunctionAddAsMeasuredParameter->setGeometry(QRect(570, 10, 191, 31));
        buttonParameterEdit = new QToolButton(tabTestFunctions);
        buttonParameterEdit->setObjectName(QString::fromUtf8("buttonParameterEdit"));
        buttonParameterEdit->setGeometry(QRect(570, 50, 191, 31));
        groupBox = new QGroupBox(tabTestFunctions);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 241, 91));
        buttonDrawIntensity = new QToolButton(groupBox);
        buttonDrawIntensity->setObjectName(QString::fromUtf8("buttonDrawIntensity"));
        buttonDrawIntensity->setGeometry(QRect(0, 20, 121, 31));
        groupBox_2 = new QGroupBox(tabTestFunctions);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(240, 0, 301, 91));
        buttonFunctionHeight = new QToolButton(groupBox_2);
        buttonFunctionHeight->setObjectName(QString::fromUtf8("buttonFunctionHeight"));
        buttonFunctionHeight->setGeometry(QRect(0, 20, 161, 31));
        buttonFunctionLineDeviation = new QToolButton(groupBox_2);
        buttonFunctionLineDeviation->setObjectName(QString::fromUtf8("buttonFunctionLineDeviation"));
        buttonFunctionLineDeviation->setGeometry(QRect(0, 50, 161, 31));
        tabWidget->addTab(tabTestFunctions, QString());

        gridLayout->addWidget(tabWidget, 0, 1, 1, 2);

        frame = new QFrame(imageProcessing);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        layoutWidget = new QWidget(frame);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 401, 120));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        addParameter = new QPushButton(layoutWidget);
        addParameter->setObjectName(QString::fromUtf8("addParameter"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(addParameter->sizePolicy().hasHeightForWidth());
        addParameter->setSizePolicy(sizePolicy);
        addParameter->setMinimumSize(QSize(100, 0));

        gridLayout_2->addWidget(addParameter, 0, 0, 1, 1);

        removeParameter = new QPushButton(layoutWidget);
        removeParameter->setObjectName(QString::fromUtf8("removeParameter"));
        sizePolicy.setHeightForWidth(removeParameter->sizePolicy().hasHeightForWidth());
        removeParameter->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(removeParameter, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        update = new QPushButton(layoutWidget);
        update->setObjectName(QString::fromUtf8("update"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(update->sizePolicy().hasHeightForWidth());
        update->setSizePolicy(sizePolicy1);
        update->setFlat(false);

        horizontalLayout_3->addWidget(update);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        Cancel = new QPushButton(layoutWidget);
        Cancel->setObjectName(QString::fromUtf8("Cancel"));
        sizePolicy1.setHeightForWidth(Cancel->sizePolicy().hasHeightForWidth());
        Cancel->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(Cancel);


        gridLayout_2->addLayout(horizontalLayout_3, 2, 0, 1, 1);


        gridLayout->addWidget(frame, 0, 0, 1, 1);

        frame_2 = new QFrame(imageProcessing);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        imageView = new QGraphicsView(frame_2);
        imageView->setObjectName(QString::fromUtf8("imageView"));
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        imageView->setFrameShape(QFrame::WinPanel);
        imageView->setDragMode(QGraphicsView::RubberBandDrag);
        imageView->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        imageView->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
        imageView->setRubberBandSelectionMode(Qt::ContainsItemBoundingRect);

        gridLayout_3->addWidget(imageView, 0, 0, 1, 1);


        gridLayout->addWidget(frame_2, 2, 1, 1, 2);

        scrollArea = new QScrollArea(imageProcessing);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setMinimumSize(QSize(250, 0));
        scrollArea->setMaximumSize(QSize(400, 16777215));
        scrollArea->setLayoutDirection(Qt::LeftToRight);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 398, 878));
        gridLayoutWidget = new QWidget(scrollAreaWidgetContents_3);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 381, 561));
        ParameterLayout = new QGridLayout(gridLayoutWidget);
        ParameterLayout->setSpacing(2);
        ParameterLayout->setObjectName(QString::fromUtf8("ParameterLayout"));
        ParameterLayout->setSizeConstraint(QLayout::SetNoConstraint);
        ParameterLayout->setContentsMargins(1, 1, 1, 1);
        listFunctionsReturn = new QListWidget(scrollAreaWidgetContents_3);
        listFunctionsReturn->setObjectName(QString::fromUtf8("listFunctionsReturn"));
        listFunctionsReturn->setGeometry(QRect(10, 580, 381, 271));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        gridLayout->addWidget(scrollArea, 1, 0, 2, 1);


        retranslateUi(imageProcessing);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(imageProcessing);
    } // setupUi

    void retranslateUi(QWidget *imageProcessing)
    {
        imageProcessing->setWindowTitle(QCoreApplication::translate("imageProcessing", "ImageProcessing", nullptr));
        buttonImageUp->setText(QCoreApplication::translate("imageProcessing", "IMAGE+", nullptr));
        buttonCamDown->setText(QCoreApplication::translate("imageProcessing", "CAM-", nullptr));
        buttonImageDown->setText(QCoreApplication::translate("imageProcessing", "IMAGE-", nullptr));
        buttonCamUp->setText(QCoreApplication::translate("imageProcessing", "CAM+", nullptr));
        labelInsertImageIndex->setText(QCoreApplication::translate("imageProcessing", "Insert image Index:", nullptr));
        labelLocation->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        buttonZoomIn->setText(QCoreApplication::translate("imageProcessing", "ZOOM IN", nullptr));
        buttonReset->setText(QCoreApplication::translate("imageProcessing", "ZOOM RESET", nullptr));
        labelZoom->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        buttonZoomOut->setText(QCoreApplication::translate("imageProcessing", "ZOOM OUT", nullptr));
        labelX->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelY->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelG->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelB->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelR->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelCurrStation->setText(QCoreApplication::translate("imageProcessing", "TextLabel", nullptr));
        buttonProcessCam0->setText(QCoreApplication::translate("imageProcessing", "Obdelava Orientacija", nullptr));
        buttonProcessCam3->setText(QCoreApplication::translate("imageProcessing", "Obdelava Povrsine ZGORAJ", nullptr));
        buttonProcessCam1->setText(QCoreApplication::translate("imageProcessing", "Obdelava Dimenzije ZGORAJ", nullptr));
        buttonSurfaceInspection->setText(QCoreApplication::translate("imageProcessing", "Surface inspection", nullptr));
        buttonProcessCam4->setText(QCoreApplication::translate("imageProcessing", "ProcessImage4", nullptr));
        buttonProcessCam2->setText(QCoreApplication::translate("imageProcessing", "Obdelava Dimenzije STRANI", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("imageProcessing", "ProcessImages", nullptr));
        buttonRubberResize->setText(QCoreApplication::translate("imageProcessing", "RubberBandResize", nullptr));
        buttonCreateRubber->setText(QCoreApplication::translate("imageProcessing", "CreateRubberBand", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_calibration), QCoreApplication::translate("imageProcessing", "calibration", nullptr));
        buttonLoadImage->setText(QCoreApplication::translate("imageProcessing", "LoadImage", nullptr));
        buttonSaveImage->setText(QCoreApplication::translate("imageProcessing", "SaveImage", nullptr));
        buttonLoadMultipleImages->setText(QCoreApplication::translate("imageProcessing", "Load Multiple", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("imageProcessing", "Tab 2", nullptr));
        buttonFunctionFunctionAddAsMeasuredParameter->setText(QCoreApplication::translate("imageProcessing", "Add Function as measurement ", nullptr));
        buttonParameterEdit->setText(QCoreApplication::translate("imageProcessing", "Edit selected dynamic measuremt", nullptr));
        groupBox->setTitle(QCoreApplication::translate("imageProcessing", "Test Functions", nullptr));
        buttonDrawIntensity->setText(QCoreApplication::translate("imageProcessing", "Test Intensity", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("imageProcessing", "Dynamic Functions", nullptr));
        buttonFunctionHeight->setText(QCoreApplication::translate("imageProcessing", "Dimensions Measurements", nullptr));
        buttonFunctionLineDeviation->setText(QCoreApplication::translate("imageProcessing", "Line deviation", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabTestFunctions), QCoreApplication::translate("imageProcessing", "Test Functions", nullptr));
        addParameter->setText(QCoreApplication::translate("imageProcessing", "Add parameter", nullptr));
        removeParameter->setText(QCoreApplication::translate("imageProcessing", "Remove parameter", nullptr));
        update->setText(QCoreApplication::translate("imageProcessing", "Update", nullptr));
        Cancel->setText(QCoreApplication::translate("imageProcessing", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class imageProcessing: public Ui_imageProcessing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEPROCESSING_H
