/********************************************************************************
** Form generated from reading UI file 'koracno.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KORACNO_H
#define UI_KORACNO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_korDialog
{
public:
    QPushButton *buttonTriggOrient;
    QPushButton *buttonTriggMeasure;
    QPushButton *buttonTriggStolp1;
    QPushButton *buttonTriggStolp2;
    QPushButton *pushButton_5;

    void setupUi(QWidget *korDialog)
    {
        if (korDialog->objectName().isEmpty())
            korDialog->setObjectName(QString::fromUtf8("korDialog"));
        korDialog->resize(400, 300);
        buttonTriggOrient = new QPushButton(korDialog);
        buttonTriggOrient->setObjectName(QString::fromUtf8("buttonTriggOrient"));
        buttonTriggOrient->setGeometry(QRect(20, 20, 361, 41));
        buttonTriggMeasure = new QPushButton(korDialog);
        buttonTriggMeasure->setObjectName(QString::fromUtf8("buttonTriggMeasure"));
        buttonTriggMeasure->setGeometry(QRect(20, 70, 361, 41));
        buttonTriggStolp1 = new QPushButton(korDialog);
        buttonTriggStolp1->setObjectName(QString::fromUtf8("buttonTriggStolp1"));
        buttonTriggStolp1->setGeometry(QRect(20, 120, 361, 41));
        buttonTriggStolp2 = new QPushButton(korDialog);
        buttonTriggStolp2->setObjectName(QString::fromUtf8("buttonTriggStolp2"));
        buttonTriggStolp2->setGeometry(QRect(20, 170, 361, 41));
        pushButton_5 = new QPushButton(korDialog);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(20, 220, 361, 41));

        retranslateUi(korDialog);

        QMetaObject::connectSlotsByName(korDialog);
    } // setupUi

    void retranslateUi(QWidget *korDialog)
    {
        korDialog->setWindowTitle(QCoreApplication::translate("korDialog", "Form", nullptr));
        buttonTriggOrient->setText(QCoreApplication::translate("korDialog", "PRO\305\275I KAMERO ZA ORIENTACIJO", nullptr));
        buttonTriggMeasure->setText(QCoreApplication::translate("korDialog", "PRO\305\275I MERILNO POSTAJO", nullptr));
        buttonTriggStolp1->setText(QCoreApplication::translate("korDialog", "PRO\305\275I NAVOJNO POSTAJO 1", nullptr));
        buttonTriggStolp2->setText(QCoreApplication::translate("korDialog", "PRO\305\275I NAVOJNO POSTAJO 2", nullptr));
        pushButton_5->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class korDialog: public Ui_korDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KORACNO_H
