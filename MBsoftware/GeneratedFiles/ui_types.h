/********************************************************************************
** Form generated from reading UI file 'types.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TYPES_H
#define UI_TYPES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_typesUi
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *labelNum;
    QLabel *labelName;
    QLabel *labelMinTol;
    QLabel *labelMaxTol;
    QLabel *labelNominal;
    QLabel *labelOffset;
    QLabel *labelCorrection;
    QLabel *labelMinTolCond;
    QLabel *labelMaxTolCond;
    QLabel *labelIsActive;
    QLabel *labelConditional;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonOK;
    QPushButton *butttonCancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonOpenPlans;
    QPushButton *buttonAddParameter;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_14;
    QLabel *labelTypeName;
    QFrame *line;

    void setupUi(QWidget *typesUi)
    {
        if (typesUi->objectName().isEmpty())
            typesUi->setObjectName(QString::fromUtf8("typesUi"));
        typesUi->resize(1442, 674);
        typesUi->setMinimumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(typesUi);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelNum = new QLabel(typesUi);
        labelNum->setObjectName(QString::fromUtf8("labelNum"));
        labelNum->setMinimumSize(QSize(40, 0));
        labelNum->setMaximumSize(QSize(40, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelNum->setFont(font);

        horizontalLayout->addWidget(labelNum);

        labelName = new QLabel(typesUi);
        labelName->setObjectName(QString::fromUtf8("labelName"));
        labelName->setMinimumSize(QSize(200, 0));
        labelName->setMaximumSize(QSize(200, 16777215));
        labelName->setFont(font);
        labelName->setLayoutDirection(Qt::LeftToRight);
        labelName->setFrameShape(QFrame::Box);
        labelName->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(labelName);

        labelMinTol = new QLabel(typesUi);
        labelMinTol->setObjectName(QString::fromUtf8("labelMinTol"));
        labelMinTol->setMinimumSize(QSize(120, 0));
        labelMinTol->setMaximumSize(QSize(120, 16777215));
        labelMinTol->setFont(font);
        labelMinTol->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelMinTol);

        labelMaxTol = new QLabel(typesUi);
        labelMaxTol->setObjectName(QString::fromUtf8("labelMaxTol"));
        labelMaxTol->setMinimumSize(QSize(120, 0));
        labelMaxTol->setMaximumSize(QSize(120, 16777215));
        labelMaxTol->setFont(font);
        labelMaxTol->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelMaxTol);

        labelNominal = new QLabel(typesUi);
        labelNominal->setObjectName(QString::fromUtf8("labelNominal"));
        labelNominal->setMinimumSize(QSize(120, 0));
        labelNominal->setMaximumSize(QSize(120, 16777215));
        labelNominal->setFont(font);
        labelNominal->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelNominal);

        labelOffset = new QLabel(typesUi);
        labelOffset->setObjectName(QString::fromUtf8("labelOffset"));
        labelOffset->setMinimumSize(QSize(120, 0));
        labelOffset->setMaximumSize(QSize(120, 16777215));
        labelOffset->setFont(font);
        labelOffset->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelOffset);

        labelCorrection = new QLabel(typesUi);
        labelCorrection->setObjectName(QString::fromUtf8("labelCorrection"));
        labelCorrection->setMinimumSize(QSize(120, 0));
        labelCorrection->setMaximumSize(QSize(120, 16777215));
        labelCorrection->setFont(font);
        labelCorrection->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelCorrection);

        labelMinTolCond = new QLabel(typesUi);
        labelMinTolCond->setObjectName(QString::fromUtf8("labelMinTolCond"));
        labelMinTolCond->setMinimumSize(QSize(120, 0));
        labelMinTolCond->setMaximumSize(QSize(120, 16777215));
        labelMinTolCond->setFont(font);
        labelMinTolCond->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelMinTolCond);

        labelMaxTolCond = new QLabel(typesUi);
        labelMaxTolCond->setObjectName(QString::fromUtf8("labelMaxTolCond"));
        labelMaxTolCond->setMinimumSize(QSize(120, 0));
        labelMaxTolCond->setMaximumSize(QSize(120, 16777215));
        labelMaxTolCond->setFont(font);
        labelMaxTolCond->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelMaxTolCond);

        labelIsActive = new QLabel(typesUi);
        labelIsActive->setObjectName(QString::fromUtf8("labelIsActive"));
        labelIsActive->setMinimumSize(QSize(120, 0));
        labelIsActive->setMaximumSize(QSize(120, 16777215));
        labelIsActive->setFont(font);
        labelIsActive->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelIsActive);

        labelConditional = new QLabel(typesUi);
        labelConditional->setObjectName(QString::fromUtf8("labelConditional"));
        labelConditional->setMinimumSize(QSize(120, 0));
        labelConditional->setMaximumSize(QSize(120, 16777215));
        labelConditional->setFont(font);
        labelConditional->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(labelConditional);

        label_10 = new QLabel(typesUi);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(25, 0));
        label_10->setMaximumSize(QSize(25, 16777215));

        horizontalLayout->addWidget(label_10);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonOK = new QPushButton(typesUi);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));
        buttonOK->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(buttonOK);

        butttonCancel = new QPushButton(typesUi);
        butttonCancel->setObjectName(QString::fromUtf8("butttonCancel"));
        butttonCancel->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(butttonCancel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        buttonOpenPlans = new QPushButton(typesUi);
        buttonOpenPlans->setObjectName(QString::fromUtf8("buttonOpenPlans"));
        buttonOpenPlans->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(buttonOpenPlans);

        buttonAddParameter = new QPushButton(typesUi);
        buttonAddParameter->setObjectName(QString::fromUtf8("buttonAddParameter"));
        buttonAddParameter->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(buttonAddParameter);


        gridLayout->addLayout(horizontalLayout_2, 4, 0, 1, 1);

        scrollArea = new QScrollArea(typesUi);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1422, 532));
        verticalLayoutWidget = new QWidget(scrollAreaWidgetContents);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 1421, 521));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        label_14 = new QLabel(typesUi);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(140, 0));
        label_14->setMaximumSize(QSize(140, 16777215));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(16);
        font1.setBold(false);
        font1.setWeight(50);
        label_14->setFont(font1);

        horizontalLayout_3->addWidget(label_14);

        labelTypeName = new QLabel(typesUi);
        labelTypeName->setObjectName(QString::fromUtf8("labelTypeName"));
        labelTypeName->setMinimumSize(QSize(200, 0));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        labelTypeName->setFont(font2);
        labelTypeName->setFrameShape(QFrame::Panel);
        labelTypeName->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(labelTypeName);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        line = new QFrame(typesUi);
        line->setObjectName(QString::fromUtf8("line"));
        line->setAutoFillBackground(false);
        line->setStyleSheet(QString::fromUtf8("gridline-color: rgb(0, 85, 255);\n"
"border-color: rgb(0, 85, 255);\n"
"background-color: rgb(0, 85, 255);"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(10);
        line->setMidLineWidth(10);
        line->setFrameShape(QFrame::HLine);

        gridLayout->addWidget(line, 1, 0, 1, 1);


        retranslateUi(typesUi);

        QMetaObject::connectSlotsByName(typesUi);
    } // setupUi

    void retranslateUi(QWidget *typesUi)
    {
        typesUi->setWindowTitle(QCoreApplication::translate("typesUi", "Form", nullptr));
        labelNum->setText(QCoreApplication::translate("typesUi", "Num:", nullptr));
        labelName->setText(QCoreApplication::translate("typesUi", "Name:", nullptr));
        labelMinTol->setText(QCoreApplication::translate("typesUi", "Min Tolerance:", nullptr));
        labelMaxTol->setText(QCoreApplication::translate("typesUi", "Max Tolerance:", nullptr));
        labelNominal->setText(QCoreApplication::translate("typesUi", "Nominal:", nullptr));
        labelOffset->setText(QCoreApplication::translate("typesUi", "Offset:", nullptr));
        labelCorrection->setText(QCoreApplication::translate("typesUi", "Correction:", nullptr));
        labelMinTolCond->setText(QCoreApplication::translate("typesUi", "Min Tol. Cond.", nullptr));
        labelMaxTolCond->setText(QCoreApplication::translate("typesUi", "Max Tol. Cond.", nullptr));
        labelIsActive->setText(QCoreApplication::translate("typesUi", "Is active:", nullptr));
        labelConditional->setText(QCoreApplication::translate("typesUi", "Conditional:", nullptr));
        label_10->setText(QString());
        buttonOK->setText(QCoreApplication::translate("typesUi", "OK", nullptr));
        butttonCancel->setText(QCoreApplication::translate("typesUi", "Cancel", nullptr));
        buttonOpenPlans->setText(QCoreApplication::translate("typesUi", "Open Plans", nullptr));
        buttonAddParameter->setText(QCoreApplication::translate("typesUi", "Add parameter", nullptr));
        label_14->setText(QCoreApplication::translate("typesUi", "Selected Type:", nullptr));
        labelTypeName->setText(QCoreApplication::translate("typesUi", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class typesUi: public Ui_typesUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TYPES_H
