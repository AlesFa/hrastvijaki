#pragma once

#include <QWidget>
#include "ui_History.h"

class History : public QWidget
{
	Q_OBJECT

public:
	//History(QWidget *parent = Q_NULLPTR);
	History();
	~History();

	vector <QTableWidget*>  parametersTable;
	//QTableWidget  *parametersTable[NR_STATIONS];
	void CreateMeasurementsTable();

	void DrawMeasurements(int currentNr);
	void DrawData(int currentNr);

	int IsGood(int station, int currentNr, int nParam);
	void Init();
	void OnShowDialog(int station);
	void UpdateCounter(int station);
	void DrawImageOnScreen(int statNr);

	std::vector<float>		toleranceHigh[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		nominal[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		toleranceLow[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		toleranceLowCond[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		toleranceHighCond[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		isConditional[NR_STATIONS][HISTORY_SIZE];
	std::vector<float>		measuredValue[NR_STATIONS][HISTORY_SIZE];
	std::vector<int>		isActive[NR_STATIONS][HISTORY_SIZE];
	std::vector<QString>	name[NR_STATIONS][HISTORY_SIZE];
	std::vector<int>		parametersCounter[NR_STATIONS];
	std::vector<cv::Mat>	image[2][NR_STATIONS];

	int lastIn[NR_STATIONS];
	int displayCurrent[NR_STATIONS];

	QGraphicsScene * showScene[4];
	QGraphicsPixmapItem*	pixmapVideo[4];
	bool isSceneAdded;
	bool isScaled[HISTORY_SIZE];
	int currentStation;

	int currentPiece[HISTORY_SIZE];
	int pieceNum[NR_STATIONS][HISTORY_SIZE];
	float std[4][HISTORY_SIZE];
	int avr[4][HISTORY_SIZE];
	int nrBlobs[4][HISTORY_SIZE];
	int bigBlobSize[4][HISTORY_SIZE];
	int isGood[NR_STATIONS][HISTORY_SIZE];
	int localSTD[4][HISTORY_SIZE];
	float speed[NR_STATIONS][HISTORY_SIZE];
	float angle[NR_STATIONS][HISTORY_SIZE];



private:
	Ui::History ui;
	int currentHis;

	bool slovensko;

public slots:

	void OnButtonPlus();
	void OnButtonMinus();
	void OnButtonUpload();
signals:
	void callFromHistory(int station, int currentPiece);//signal za v razred MBVISION OnFrameReady
};
