#include "stdafx.h"
#include "LogWindow.h"

LogWindow::LogWindow(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	if(!QDir("C:/log/").exists())
		QDir().mkdir("C:/log");

	m_logFile = new QFile();
	fileName = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	m_logFile->setFileName(fileName);
	m_logFile->open(QIODevice::Append | QIODevice::Text);
	ui.plainTextEdit->setMaximumBlockCount(100);
	ui.plainTextEdit->setMidLineWidth(600);
	ui.plainTextEdit->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOn);
}

LogWindow::~LogWindow()
{
	if (m_logFile != 0)
		m_logFile->close();

	if(isActiveWindow())
		close();
}

void LogWindow::AppendMessage(const QString& text)
{
	//lockWrite.lockForWrite();
	QString fileNameTmp = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	QString value;// + "";
	value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + text + "\n";
	//ui.plainTextEdit->appendPlainText(value); // Adds the message to the widget
	//ui.plainTextEdit->verticalScrollBar()->setValue(ui.plainTextEdit->verticalScrollBar()->maximum()); // Scrolls to the bottom

	if (fileNameTmp.compare(fileName) != 0)
	{
		//ko se spremeni datum odpre nov fajl
		m_logFile->close();
		m_logFile->setFileName(fileNameTmp);
		fileName = fileNameTmp;
		m_logFile->open(QIODevice::Append | QIODevice::Text);
	}
	if (m_logFile != 0)
	{
		QTextStream out(m_logFile);
		out.setCodec("UTF-8");
		out << value;
	} // Logs to file
	//lockWrite.unlock();
}

void LogWindow::ShowDialog()
{
	setWindowModality(Qt::ApplicationModal);
	show();
}

/*
 errNr: custom
 msgType: 0 = warning; 1 = ereror; 2 = info
 msgContent: message
 Lahko nastavimo samo, ce ni napaka �e od prej 
*/
void LogWindow::SetErrorMessage(int errNr, QString msgContent)
{
	int isSet = 0;
	QString append;
	lockWrite.lockForWrite();
	if (FindErrorFlagValue(errNr) < 0) //ce zapis se ne obstaja
	{
		
		if (errorFlag.size() >= maxLogs - 1)
		{
			errorFlag.erase(errorFlag.begin());
			errorMessage.erase(errorMessage.begin());
			msgType.erase(msgType.begin());
		}
		errorFlag.push_back(errNr);
		errorMessage.push_back(QString("%1:%2:%3 - %4").arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(msgContent));
		msgType.push_back(1);
		append = "ERROR:" + msgContent;
		AppendMessage(append);
	}

	lockWrite.unlock();
	
}
void LogWindow::SetWarningMessage(int errNr, QString msgContent)
{
	
}
void LogWindow::SetInfoMessage(int infoNr, QString msgContent)
{
	
	QString append;
	lockWrite.lockForWrite();

	if (FindErrorFlagValue(infoNr) < 0) //ce zapis se ne obstaja
	{
		if (errorFlag.size() >= maxLogs - 1)
		{
			errorFlag.erase(errorFlag.begin());
			errorMessage.erase(errorMessage.begin());
			msgType.erase(msgType.begin());
		}
		errorFlag.push_back(infoNr);
		errorMessage.push_back(QString("%1:%2:%3 - %4").arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(msgContent));
		msgType.push_back(2);
		append = "INFO:" + msgContent;
		AppendMessage(append);
	}
	lockWrite.unlock();
	
}

void LogWindow::ClearWindowMessage()
{
	errorFlag.clear();
	errorMessage.clear();
	msgType.clear();
}

void LogWindow::ClearWindowMessage(int index)
{
	if (index < errorFlag.size())
	{
		errorFlag.erase(errorFlag.begin() + index);
		errorMessage.erase(errorMessage.begin() + index);
		msgType.erase(msgType.begin() + index);
	}
}

void LogWindow::ClearLastWindowMessage()
{
	errorFlag.pop_back();
	errorMessage.pop_back();
	msgType.pop_back();
}

int LogWindow::FindErrorFlagValue(int value)
{
	for (int i = 0; i < errorFlag.size(); i++)
	{
		if (errorFlag[i] == value)
			return i;
	}
	return -1;
}
