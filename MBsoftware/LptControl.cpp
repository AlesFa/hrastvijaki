#include "stdafx.h"
#include "LPTcontrol.h"



HANDLE	LptControl::HwCtrl = NULL;
bool	LptControl::ActiveHW = FALSE;
DWORD	LptControl::nPort = 0;
int		LptControl::objectCount = 0;


LptControl::LptControl()
{
}

LptControl::LptControl(DWORD portAddress)
{
	ui.setupUi(this);

	//LptControlD 

	id = objectCount;
	ReadData();
	portAddr = portAddress;

	portStatusValue = 0;
	portDataValue = 0;
	portControlValue = 0;

	for (int i = 1; i <= 17; i++)
		SetPin(HwCtrl, i, FALSE);

	CreateDisplay();

	portStatusValue = 0;
	portDataValue = 0;
	portControlValue = 0;
	objectCount++;




}


LptControl::~LptControl()
{
	viewTimer->stop();

	for (int i = 1; i <= 17; i++)
		SetPin(HwCtrl, i, FALSE);
}

bool LptControl::OpenDriver(void)
{
	if (ActiveHW == FALSE)
	{
		HwCtrl = OpenTVicHW();
		if (!HwCtrl || !GetActiveHW(HwCtrl))
		{
			MessageBeep(0);
			//MessageBoxA("Can't open the driver!",
			//	" Warning! ", MB_OK | MB_ICONWARNING);
		}
		else
		{
			ActiveHW = TRUE;
			nPort = GetLPTBasePort(HwCtrl);
		}
	}

	return ActiveHW;
	return 0;
}

void LptControl::Init(int port)
{
	int i;


	if (id > 0)
		AddNewLPT(HwCtrl, port);

	SetLPTNumber(HwCtrl, id + 1);

	for (int i = 1; i <= 17; i++)
		SetPin(HwCtrl, i, FALSE);

	controlValue = 0x0b;

	portAddr = port;

}

unsigned char LptControl::GetPortControlValue(void)

{
	lockReadWrite.lockForWrite();
	portControlValue = GetPortByte(HwCtrl, portAddr + 2); 
	lockReadWrite.unlock();
	return portControlValue;
}


void LptControl::SetDataByte(UCHAR bits)
{
	portDataValue = bits;
	SetPortByte(HwCtrl, portAddr, portDataValue);

	SetOutputSignals();
}

void LptControl::SetDataByte(UCHAR bits, bool value)
{
	portDataValue = GetPortByte(HwCtrl, portAddr);

	if (value)
		portDataValue = portDataValue | bits;
	else
		portDataValue = portDataValue & (~bits);

	SetPortByte(HwCtrl, portAddr, portDataValue);

	SetOutputSignals();
}

void LptControl::SetOutputValue(int outputNumber, bool value)
{
	SetDataByte(output[outputNumber].address, value);
}

unsigned char LptControl::GetStatusValue(void)
{
	portStatusValue = GetPortByte(HwCtrl, portAddr + 1);

	//obrne invertiran bit
	if ((portStatusValue & 0x80) == 0x80)
		portStatusValue = portStatusValue & 0x7F;
	else
		portStatusValue = portStatusValue | 0x80;

	SetInputSignals();

	return portStatusValue;
}

void LptControl::SetControlByte(int contolBit, bool value)
{
	//contolBit ... control but number (1-4)
	lockReadWrite.lockForWrite();
	if (value)
	{
		switch (contolBit)
		{
		case 1:
			portControlValue = portControlValue & 0xfe;
			break;

		case 2:
			portControlValue = portControlValue & 0xfd;
			break;

		case 3:
			portControlValue = portControlValue | 0x04;
			break;

		case 4:
			portControlValue = portControlValue & 0xf7;
			break;

		default:
			break;
		}
	}
	else
	{
		switch (contolBit)
		{
		case 1:
			portControlValue = portControlValue | 0x01;
			break;

		case 2:
			portControlValue = portControlValue | 0x02;
			break;

		case 3:
			portControlValue = portControlValue & 0xfb;
			break;

		case 4:
			portControlValue = portControlValue | 0x08;
			break;

		default:

			break;

		}
	}
	SetPortByte(HwCtrl, portAddr + 2, portControlValue);
	SetControlSignals();
	lockReadWrite.unlock();

}

void LptControl::SetControlByte(char bits)
{
	SetPortByte(HwCtrl, portAddr + 2, bits);
}

void LptControl::SetControlByteAllON(void)
{
        portControlValue = GetPortByte(HwCtrl, portAddr + 2);

        portControlValue = portControlValue & 0xf0;
        portControlValue = portControlValue | 0x04; //samo 3 bit 1, ker ni invertiran

        SetPortByte(HwCtrl, portAddr + 2, portControlValue);
}

void LptControl::SetControlByteAllOFF(void)
{
	portControlValue = GetPortByte(HwCtrl, portAddr + 2);

	portControlValue = portControlValue & 0xf0;
	portControlValue = portControlValue | 0x0b; //samo 3 bit 1, ker ni invertiran	

	SetPortByte(HwCtrl, portAddr + 2, portControlValue);
}

void LptControl::SetDataBits(UCHAR bits, bool value)
{
	lockReadWrite.lockForWrite();
	if (value)
		portDataValue = portDataValue | bits;
	else
		portDataValue = portDataValue & (~bits);

	SetPortByte(HwCtrl, portAddr, portDataValue);
	SetOutputSignals();
	lockReadWrite.unlock();
}

void LptControl::SetDataBits(UCHAR bits)
{
	lockReadWrite.lockForWrite();
	portDataValue = portDataValue | bits;
	SetPortByte(HwCtrl, portAddr, portDataValue);
	SetOutputSignals();
	lockReadWrite.unlock();
}

void LptControl::UnSetDataBits(UCHAR bits)
{
	lockReadWrite.lockForWrite();
	portDataValue = portDataValue & (~bits);
	SetPortByte(HwCtrl, portAddr, portDataValue);
	SetOutputSignals();
	lockReadWrite.unlock();
}


void LptControl::SetOutputSignals()
{
	int i;
	for (i = 0; i < 8; i++)
	{
		if ((portDataValue & output[i].address) == output[i].address)
		{
			output[i].prevValue = output[i].value;
			output[i].value = 1;
		}
		else
		{
			output[i].prevValue = output[i].value;
			output[i].value = 0;
		}
	}
}

void LptControl::SetInputSignals()
{
	int i;
	for (i = 0; i < 5; i++)
	{
		if ((portStatusValue & input[i].address) == input[i].address)
		{
			input[i].prevValue = input[i].value;
			input[i].value = 0;
		}
		else
		{
			input[i].prevValue = input[i].value;
			input[i].value = 1;
		}
	}
}

void LptControl::SetControlSignals()
{
	for (int i = 0; i < 4; i++)
	{
		if ((portControlValue & control[i].address) == control[i].address)
		{
			control[i].prevValue = control[i].value;
			control[i].value = 0;
		}
		else
		{
			control[i].prevValue = control[i].value;
			control[i].value = 1;
		}
	}
}



void LptControl::SlotItemChanged(QStandardItem *poItem)
{
	int i;
	// Get current index from item
	const QModelIndex oCurrentIndex =
		poListItemOutputs[0]->model()->indexFromItem(poItem);

	// Get list selection model
	QItemSelectionModel *poSelModel =
		listViewOutputs->selectionModel();

	i = oCurrentIndex.row();

	lockReadWrite.lockForWrite();
	if ((portDataValue & output[i].address) == output[i].address)
	{
		output[i].prevValue = output[i].value;
		output[i].value = 0;
		//poListItemOutputs[i]->setCheckState(Qt::Unchecked);
	}
	else
	{
		output[i].prevValue = output[i].value;
		output[i].value = 1;
		//poListItemOutputs[i]->setCheckState(Qt::Checked);
	}
	lockReadWrite.unlock();
	SetDataBits(output[i].address, output[i].value);


	listViewOutputs->setModel(poModelOutputs);
}

void LptControl::SlotItemChangedControl(QStandardItem *poItem)
{
	int i;
	// Get current index from item
	const QModelIndex oCurrentIndex =
		poListItemControl[0]->model()->indexFromItem(poItem);

	// Get list selection model
	QItemSelectionModel *poSelModel =
		listViewControl->selectionModel();

	i = oCurrentIndex.row();
	lockReadWrite.lockForWrite();
	
	//if(control[i].value == 1)
	if ((portControlValue & control[i].address) == control[i].address)
	{
		control[i].prevValue = control[i].value;
		control[i].value = 0;
	}
	else
	{
		control[i].prevValue = control[i].value;
		control[i].value = 1;
	}
	lockReadWrite.unlock();
	SetControlByte(i+1, control[i].value);
}


void LptControl::ShowDialog()
{


	viewTimer->start(100);
	setWindowModality(Qt::ApplicationModal);
	show();
}

int LptControl::ReadData(void)
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QApplication::applicationDirPath() + QString("/../%1/Signali/LptControl%2.ini").arg(REF_FOLDER_NAME).arg(id);
	QSettings settings(filePath, QSettings::IniFormat);

	uchar adresses[8] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

	//inputs
	for (int i = 0; i < 5; i++)
	{
		values = settings.value(QString("Input%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			input[i].SetSignal(values[0], adresses[i + 3], values[1].toInt());
			values.clear();
		}
		else
			input[i].SetSignal(QString("S%1").arg(i), adresses[i + 3], 0);
	}

	//outputs
	for (int i = 0; i < 8; i++)
	{
		values = settings.value(QString("Output%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			output[i].SetSignal(values[0], adresses[i], values[1].toInt());
			values.clear();
		}
		else
			output[i].SetSignal(QString("D%1").arg(i), adresses[i], 0);
	}

	//control
	for (int i = 0; i < 4; i++)
	{
		values = settings.value(QString("Control%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			control[i].SetSignal(values[0], adresses[i], values[1].toInt());
			values.clear();
		}
		else
			control[i].SetSignal(QString("C%1").arg(i), adresses[i], 0);
	}

	portAddr = settings.value("BasePort").toInt();

	return 0;
}

int LptControl::WriteData(void)
{

	QString filePath;
	QStringList list;

	/*filePath = QApplication::applicationDirPath() + QString("/../%1/Port/LptPort%2.ini").arg(REF_FOLDER_NAME).arg(id);
	QSettings settings(filePath, QSettings::IniFormat);

	//inputs
	for (int i = 0; i < 5; i++)
	{
		list << input[i].name << QString("%1").arg(input[i].isActive);
		settings.setValue(QString("Input%1").arg(i), list);
		list.clear();
	}*/
	return 0;
}



void LptControl::CreateDisplay()
{
	int x, y, width, height, xOffset, port;
	layout = new QHBoxLayout;
	x = 10;
	y = 10;
	width = 360;
	height = 200;
	xOffset = 400;
	//outputi
	port = 0;
	groupBox[port] = new QGroupBox(this);
	groupBox[port]->setObjectName(QStringLiteral("groupBoxLPT%1").arg(port));
	groupBox[port]->setGeometry(QRect(x, y, width + 20, height + 30));
	listViewOutputs = new QListView(groupBox[port]);
	listViewOutputs->setObjectName(QStringLiteral("listViewOutputsLPT%1").arg(port));
	listViewOutputs->setGeometry(QRect(10, 20, width, height));
	poModelOutputs = new QStandardItemModel(listViewOutputs);

	///inputi
	port = 1;
	x += xOffset;
	groupBox[port] = new QGroupBox(this);
	groupBox[port]->setObjectName(QStringLiteral("groupBoxLPT%1").arg(port));
	groupBox[port]->setGeometry(QRect(x, y, width + 20, height + 30));
	listViewInputs = new QListView(groupBox[port]);
	listViewInputs->setObjectName(QStringLiteral("listViewInputsLPT%1").arg(port));
	listViewInputs->setGeometry(QRect(10, 20, width, height));
	poModelInputs = new QStandardItemModel(listViewInputs);

	//control
	port = 2;
	x += xOffset;
	groupBox[port] = new QGroupBox(this);
	groupBox[port]->setObjectName(QStringLiteral("groupBoxLPT%1").arg(port));
	groupBox[port]->setGeometry(QRect(x, y, width + 20, height + 30));
	listViewControl = new QListView(groupBox[port]);
	listViewControl->setObjectName(QStringLiteral("listViewInputsLPT%1").arg(port));
	listViewControl->setGeometry(QRect(10, 20, width, height));
	poModelControl = new QStandardItemModel(listViewControl);

	connect(poModelOutputs, SIGNAL(itemChanged(QStandardItem*)),
		this, SLOT(SlotItemChanged(QStandardItem*)));

	connect(poModelControl, SIGNAL(itemChanged(QStandardItem*)),
		this, SLOT(SlotItemChangedControl(QStandardItem*)));

	



	

	// Create list view item model
	for (int i = 0; i < 8; i++)
	{
		poListItemOutputs[i] = new QStandardItem;

		poListItemOutputs[i]->setCheckable(true);

	

		// Uncheck the item
		poListItemOutputs[i]->setCheckState(Qt::Unchecked);

		// Save checke state
		poListItemOutputs[i]->setData(Qt::Unchecked, Qt::CheckStateRole);
		poListItemOutputs[i]->setText(output[i].name);
		
		poModelOutputs->setItem(i, poListItemOutputs[i]);

	}

	listViewOutputs->setModel(poModelOutputs);


	for (int i = 0; i < 5; i++)
	{
		poListItemInputs[i] = new QStandardItem;

		// Uncheck the item
		poListItemInputs[i]->setCheckState(Qt::Unchecked);

		// Save checke state
		poListItemInputs[i]->setData(Qt::Unchecked, Qt::CheckStateRole);
		poListItemInputs[i]->setText(input[i].name);

		poModelInputs->setItem(i, poListItemInputs[i]);

	}
	listViewInputs->setModel(poModelInputs);

	for (int i = 0; i < 4; i++)
	{
		poListItemControl[i] = new QStandardItem;

		// Uncheck the item
		poListItemControl[i]->setCheckable(true);

		// Save checke state
		poListItemControl[i]->setData(Qt::Unchecked, Qt::CheckStateRole);
		poListItemControl[i]->setText(control[i].name);
		poModelControl->setItem(i, poListItemControl[i]);
	}
	listViewControl->setModel(poModelControl);

	/*layout->addWidget(groupBox[0]);
	layout->addWidget(groupBox[1]);
	layout->addWidget(groupBox[2]);
	setLayout(layout);*/



	viewTimer = new QTimer(this);     //timer instantiation
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(TimerViewSignals()));
}


void LptControl::TimerViewSignals()
{
	for (int i = 0; i < 5; i++)
	{
		if (input[i].value == 1)
		{
			poListItemInputs[i]->setCheckState(Qt::Checked);
		}
		else
		{
			poListItemInputs[i]->setCheckState(Qt::Unchecked);
		}
	}
	for (int i = 0; i < 8; i++)
	{
		if (output[i].value == 1)
		{
			poListItemOutputs[i]->setCheckState(Qt::Checked);
		}
		else
		{
			poListItemOutputs[i]->setCheckState(Qt::Unchecked);
		}
	}

	for (int i = 0; i < 4; i++)
	{
		if (control[i].value == 1)
		{
			poListItemControl[i]->setCheckState(Qt::Checked);
		}
		else
		{
			poListItemControl[i]->setCheckState(Qt::Unchecked);
		}
	}

}
