﻿#include "stdafx.h"
#include "MBsoftware.h"


//prvi test commit za hrast vijake

//include za media timer ( multimedia timer )
#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")
     
MBsoftware*									MBsoftware::glob;
std::vector<CCamera*>						MBsoftware::cam;
std::vector<CMemoryBuffer*>					MBsoftware::images;
std::vector<Serial*>						MBsoftware::serial;
std::vector<SMCdrive*>						MBsoftware::smc;
std::vector<ControlCardMB*>					MBsoftware::controlCardMB;
std::vector<Pcie*>							MBsoftware::pcieDio;
std::vector<Types*>							MBsoftware::types[NR_STATIONS];
std::vector<ControlLPT*>					MBsoftware::lpt;
std::vector<TCP*>							MBsoftware::tcp;
std::vector<XilinxZynq*>					MBsoftware::uZed;
History*									MBsoftware::history;
std::vector<StatusBox*>						MBsoftware::statusBox;
std::vector<LogWindow*>						MBsoftware::log;
Timer										MBsoftware::MMTimer;
Timer										MBsoftware::viewTimerTimer;
std::vector<Timer*>							MBsoftware::TestFun;
int											MBsoftware::mmCounter;
int											MBsoftware::viewCounter;
imageProcessing*							MBsoftware::imageProcess;
LoginWindow*								MBsoftware::login;
AboutUS*										MBsoftware::about;
int counter = 0;
int											MBsoftware::currentType = 0;
int											MBsoftware::prevType = 0;


Measurand									MBsoftware::measureObject; //object with mesurand parameters
GeneralSettings*							MBsoftware::settings; //generalSettings


QTcpSocket*									MBsoftware::client;
QFuture<int>								MBsoftware::future[4];

vector<int>						MBsoftware::clearLogIndex;
vector<int>						MBsoftware::logIndex;
vector<QString>						MBsoftware::logText;
QReadWriteLock					MBsoftware::lockReadWrite;
//QSerialPort*					serial;
MMRESULT						FTimerID;

QString logPath;
QFile* m_logFile;
int updateImage[NR_STATIONS];
int stationStatus[NR_STATIONS];

int pc2Connected = 0, pc2ConnectedOld = 0;
int pc2Initialized = 0, pc2InitializedOld = 0;
int sendRestPC2 = 0;
int epsonConnected = 0, eposnConnectedOld = 0;
int epsonInitialized = 0, epsonInitializedOld = 0;

int startErrorSet[20];
int wd = 0;
int epsonError = 0;
int processStatus = -3;// nujen pritisk glaven start tipke
int cylnderStatus =0;
int orientationCameraStatus =0;
int oreintationCameraDelay = 0;
int measureStationStatus = 0;
int measureStationDelay = 0;
//
//semafor
int rdeca = 0, zelena = 0, zelenaUtripa = 0, rdecaUtripa = 0;

int praznaNaprava = 0;
int praznaNapravaCounter = 0;

int sensorDelay = 0;
int cylnderDelay = 0;
int cylinderErrorCounter = 0;

bool startKey = false;
bool startMotor = false;
bool stopMotor = false;
bool kosNaMestu = false;
bool zahtevaZaNovKos = false;
bool motorRunning = false;
int errorCounter = 0;
int errorNum = 0, errorNumOld = 0;

int stolp1Kor = 0, stolp1KorOld = 0;
int stolp2Kor = 0, stolp2KorOld = 0;


int orient, navoj;//orientacija
int doberDimVrh = 0;
int doberDimSide = 0;
int doberPovrsina = 0;
int pieceOn[NR_STATIONS];
//robot command 
bool newPieceRequest = false;
bool havePiece = false;
bool orientationCameraTrigger = false;
bool measureStationTrigger = false;
bool startStolp1 = false;
bool startStolp2 = false;


int stolp1Status = 0;
int stolp2Status = 0;
int stolp1Dober = 0;
int stolp2Dober = 0;
int stolp1Delay = 0;
int stolp2Delay = 0;
int stolp1StatusPrikaz = 0;
int stolp2StatusPrikaz = 0;
int stolp1Error = 0, stolp1ErrorOld = 0;
int stolp2Error = 0, stolp2ErrorOld = 0;
int stolp1ErrorCounter = 0, stolp2ErrorCounter = 0;



bool referenciranje = false;


int referenciranjeCounter = 0;
int referenciranjeErrorCounter = 0;
int referenciranjeError = 0, referenciranjeErrorOld = 0;
int referenciranjeErrorMax = 500;
bool referenciranjeRobotKoncano = false;
int statusReferenciranje = 0;

int vibratorOn = 0, vibratorOnOld = 0, vibratorRunning = 0;
int vibratorCounterStart = 0, vibratorCounterStop = 0;

float taktArray[10] = { 0,0,0,0,0,0,0,0,0,0 };
int taktCounter = 0;


int currPiece[NR_STATIONS] = { 0,0,0 };
//stevci skatel:


int badBox1Counter = 0, badBox1CounterOld = 0;
int badBox2Counter = 0, badBox2CounterOld = 0;
int badBoxDimCounter = 0, badBoxDimCounterOld = 0;
int dobriBox1Counter = 0, dobriBox1CounterOld = 0;
int stolp1DobriCounter = 0, stolp1SlabiCounter = 0;
int stolp2DobriCounter= 0, stolp2SlabiCounter = 0;
int skatleError = 0, skatleErrorOld = 0;
int skatlaPolna[5] = { 0,0,0,0,0 };
int skatlaPolnaOld[5] = { 0,0,0,0,0 };
int goodBox2Counter = 0, goodBox2CounterOld = 0;
int errorRobot = 0, errorRobotOld = 0;
int errorKosNaS1 = 0, errorKosNaS1Old = 0;
int errorKosNaS2 = 0, errorKosNaS2Old = 0;

int workingMode = 0, workingModeOld = 0;
int dimOn = 0, dimOnOld = 0;
bool sendWorkingMode = false;
bool sendDimOn = false;
bool sendError = false;
bool sendStatusSkatel = false;
bool sendPause = false;
bool sendStolp1Poln = false;
bool sendStolp2Poln = false;
bool stolp1Poln = false;
bool stolp2Poln = false;
bool sendStolp1OK = false;
bool sendStolp2OK = false;
bool stolp1OK = false;
bool stolp2OK = false;
//chart
//graf QBARCHART
#define CHART_DATA_SIZE 100
float dataArray[NR_STATIONS][12][CHART_DATA_SIZE]; //  11zamenjamo na 12 
int charDataCounter[NR_STATIONS] = { 0,0,0 };
float minValueChart[NR_STATIONS];
float maxValueChart[NR_STATIONS];
int chartStation = 0;
int chartParam = 0;
int chartStationOld = -1;
int chartParamOld = -1;

int vrataBlinking = 0;
int rocnoProzi = 0;

int currChartUpdated = 0;

int slabiOrientacija = 0;
int slabiDimenzija = 0;
int slabiDimenzijaError= 0, slabiDimenzijaErrorPrev = 0;


int slabiS1Zap = 0;
int slabiS2Zap = 0;

int paramCounter[NR_STATIONS] = { 0,0,0 }, paramCounterOld[NR_STATIONS] = { 0,0,0 };
int lucOn = 0,  lucOnOld = 0;

void MBsoftware::closeEvent(QCloseEvent *event)
{
	SaveVariables();
	CloseXilinx();
	disconnect(this, 0, 0, 0);

	//first you have to close all timers
	timeKillEvent(FTimerID);
	timeEndPeriod(1);
	OnTimer->stop();

	//ales novi test
	
	for (int i = 0; i < cam.size(); i++)
	{
		delete (cam[i]);
	}
	
	for (int i = 0; i < images.size(); i++)
	{
		delete (images[i]);
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		delete (TestFun[i]);
	}
	//lpt.clear();
		for (int i = 0; i < lpt.size(); i++)
		{
			lpt[i]->SetControlByteAllOFF();
			lpt[i]->SetDataByte(0x00);
			lpt[i]->close();

			//delete (lpt[i]);
		}
		lpt.clear();

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			controlCardMB[i]->StopTimer(0);
			controlCardMB[i]->StopTimer(2);
			controlCardMB[i]->StopTimer(3);
			controlCardMB[i]->StopTimer(1);
			controlCardMB[i]->SetOutputByte(0, 0x00);
			controlCardMB[i]->SetOutputByte(1, 0x00);

			delete (controlCardMB[i]);
		}
		controlCardMB.clear();

		for (int i = 0; i < serial.size(); i++)
		{
			delete (serial[i]);
		}
		serial.clear();

		for (int i = 0; i < statusBox.size(); i++)
		{
			delete (statusBox[i]);
		}
		for (int i = 0; i < smc.size(); i++)
		{
			delete (smc[i]);
		}
		for (int i = 0; i < pcieDio.size(); i++)
		{
			delete (pcieDio[i]);
		}
		for (int i = 0; i < uZed.size(); i++)
		{
			delete (uZed[i]);
		}
		for (int i = 0; i < tcp.size(); i++)
		{
			delete (tcp[i]);
		}

		for (int i = 0; i < NR_STATIONS; i++) //izbrisemo kar je bilo prikazano na zaslonu 
		{
			qDeleteAll(showScene[i]->items());
			delete showScene[i];
		}
		//showScene.clear();
		for (int i = 0; i < NR_STATIONS; i++)
		{
			delete parametersTable[i];
			for (int j = 0; j < types[i].size(); j++)
			{
				delete types[i][j];
				
			}
		}
		//log[0]->AppendMessage("Application STOP.");

		for (int i = 0; i < log.size(); i++)
		{
			delete log[i];
		}
		log.clear();

		if (slovensko) AddLog("Program ustavljen", 3);
		else AddLog("Application stoped", 3);
		logText.clear();
		logIndex.clear();
		delete history;
		delete imageProcess;
		delete login;
		delete settings;
		delete about;
		
		

}

MBsoftware::MBsoftware(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	glob = this;
	koracnoDialog = new(QWidget);
	ui2.setupUi(koracnoDialog);

	slovensko = SLOVENSKO;
	NastaviJezik();


	QThread::currentThread()->setPriority(QThread::HighPriority);
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);


	ReadTypes();
	LoadVariables();
	ReadBoxCounter();
	
	about = new AboutUS();
	login = new LoginWindow();
	settings = new GeneralSettings();
	statusBox.push_back(new StatusBox);
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());//stevec takta robota
	TestFun.push_back(new Timer());//stevec takta dobtih kosov

#if defined _DEBUG
	login->currentRights = 1;
	login->currentUser = "Administrator";
#else
	login->currentRights = 0;
	login->currentUser = "";
#endif
	if (!QDir("C:/log/").exists())
		QDir().mkdir("C:/log");
	logPath = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	m_logFile = new QFile();
	m_logFile->setFileName(logPath);
	m_logFile->open(QIODevice::Append | QIODevice::Text);



	if (slovensko) AddLog("Program pognan!", 3);
	else AddLog("Application STARTED!", 3);

	isSceneAdded = false;

	//lpt.push_back(new ControlLPT());
	uZed.push_back(new XilinxZynq("172.16.10.54", 7));
	uZed[0]->fps = 100;
	uZed[0]->SendHandShake();
	uZed[0]->SendSetTransmitSpeed(uZed[0]->fps);


uZed[0]->output[0][0].name = "WD";
uZed[0]->output[0][1].name = "CILINDER PIHANJE";
uZed[0]->output[0][2].name = "CILINDER VHODNI TRAK";
uZed[0]->output[0][3].name = "STOLP 1 POMIK";
uZed[0]->output[0][4].name = "STOLP 1 LOPUTA";
uZed[0]->output[0][5].name = "STOLP 2 POMIK";
uZed[0]->output[0][6].name = "STOLP 2 LOPUTA";
uZed[0]->output[0][8].name = "VIBRATOR ON SIGNAL";
uZed[0]->output[0][9].name = "STROPNA LUC ON";
uZed[0]->output[0][10].name = "";
uZed[0]->output[0][11].name = "VIBRATOR NAPAJANJE";
uZed[0]->output[0][12].name = "STOLP 2 OBDELUJEM ROBOT";
uZed[0]->output[0][13].name = "STOLP 1 OBDELUJEM ROBOT";
uZed[0]->output[0][14].name = "STOLP 2 SLIKAJ";
uZed[0]->output[0][15].name = "STOLP 1 SLIKAJ";


uZed[0]->output[1][0].name = "Vhodni Trak onemogoci pogon";
uZed[0]->output[1][1].name = "Vhodni Trak Smer";

uZed[0]->input[0][0].name = "EMG";
uZed[0]->input[0][1].name = "+12V";
uZed[0]->input[0][2].name = "+24V";
uZed[0]->input[0][3].name = "+24V izhodi";
uZed[0]->input[0][4].name = "START TIPKA";
uZed[0]->input[0][5].name = "STOP TIPKA";
uZed[0]->input[0][6].name = "KORACNO";
uZed[0]->input[0][7].name = "VRATA SENZOR";
uZed[0]->input[0][8].name = "KOS PRISOTEN ZA ROBOTA";
uZed[0]->input[0][9].name = "CILINDER TRAK ZAPRT";
uZed[0]->input[0][10].name = "CILINDER TRAK ODPRT";
uZed[0]->input[0][11].name = "TRAK SENZOR ZUNANJI ";
uZed[0]->input[0][12].name = "TRAK SENZOR NOTRANJI";
uZed[0]->input[0][13].name = "KOS PRISOTEN  TRAKU";
uZed[0]->input[0][14].name = "GLAVNI START";
uZed[0]->input[0][15].name = "";

uZed[0]->input[2][0].name = "WD PC2";
uZed[0]->input[2][1].name = "STOLP1 OBDELUJEM";
uZed[0]->input[2][2].name = "STOLP1 DOBER";
uZed[0]->input[2][3].name = "STOLP2 OBDELUJEM";
uZed[0]->input[2][4].name = "STOLP2 DOBER";
uZed[0]->input[2][5].name = "ZRAK PRISOTEN";
uZed[0]->input[2][6].name = "STOLP2 SENZOR";
uZed[0]->input[2][7].name = "STOLP1 SENZOR";
uZed[0]->input[2][8].name = "STOLP1 POMIK ODPRT";
uZed[0]->input[2][9].name = "STOLP1 POMIK ZAPRT";
uZed[0]->input[2][10].name = "STOLP1 LOPUTA SLAB";
uZed[0]->input[2][11].name = "STOLP1 LOPUTA DOBER";
uZed[0]->input[2][12].name = "STOLP2 POMIK ODPRT";
uZed[0]->input[2][13].name = "STOLP2 POMIK ZAPRT";
uZed[0]->input[2][14].name = "STOLP2 LOPUTA SLAB";
uZed[0]->input[2][15].name = "STOLP2 LOPUTA DOBER";

uZed[0]->SetIONames();

//uZed[0]->OnShowDialog();

	tcp.push_back(new TCP("172.16.9.5", 2000, true));
	connect(tcp[0], SIGNAL(dataReady(QString,int)), this, SLOT(OnTcpDataReady(QString,int)));


	

	for (int i = 0; i < 20; i++)
	{
		startErrorSet[i] = 0;
	}
	//kamera orientacija
	int visina = 1080;
	int sirina = 1440;
	int numImages = 1;
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "44020117";
	cam.back()->width = sirina;
	cam.back()->height = visina;

	cam.back()->depth = 1;
	cam.back()->FPS = 15;
	cam.back()->trigger = false;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = false;
	cam.back()->rotatedVertical = false;
	cam.back()->num_images = numImages;
	cam.back()->LoadReferenceSettings();
	QString videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{							   
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}   
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera nif odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);

	//kamera top
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "44020119";
	cam.back()->width = 2448;
	cam.back()->height = 2048;

	cam.back()->depth = 1;
	cam.back()->FPS = 10;						   
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = false;
	cam.back()->rotatedVertical = true;
	cam.back()->num_images = 5;
	cam.back()->LoadReferenceSettings();
	videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera ni odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);

	//kamera side
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "44020118";
	cam.back()->width = 2448;
	cam.back()->height = 2048;

	cam.back()->depth = 1;
	cam.back()->FPS = 10;
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = false;
	cam.back()->rotatedVertical = false;
	cam.back()->num_images = 1;
	cam.back()->LoadReferenceSettings();
	videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera ni odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);

	QString tmp;
	images.push_back(new CMemoryBuffer(1440, 1080, 3));

	cam[2]->image[0].LoadBuffer("C:\\images\\0_2.bmp");
	cam[1]->image[0].LoadBuffer("C:\\images\\pari\\0_0.bmp");
	cam[1]->image[1].LoadBuffer("C:\\images\\pari\\0_1.bmp");

	imageProcess = new imageProcessing();
	imageProcess->ConnectImages(cam, images);
	imageProcess->ConnectMeasurand(&measureObject);

	for (int i = 0; i < NR_STATIONS; i++)
	{
		imageProcess->ConnectTypes(i,types[i]);
	}
	imageProcess->LoadTypesProperty();//nalozimo nastavitve funkcij za vse tipe

	//timer za viewDisplay
	OnTimer = new QTimer(this);

	//dinamično dodajanje ikon v tabih
	CreateCameraTab();
	CreateSignalsTab();
	CreateMeasurementsTable();


	CreateChart();


	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));
	OnTimer->start(100);

	for (int i = 0; i < cam.size(); i++)
	{
		connect(cam[i], SIGNAL(frameReadySignal(int, int)), this, SLOT(OnFrameReady(int, int)));
	}
	connect(imageProcess, SIGNAL(measurePieceSignal(int, int)), this, SLOT(MeasurePiece(int, int)));

	connect(imageProcess, SIGNAL(imagesReady()), this, SLOT(ProcessImages()));
	connect(ui.buttonImageProcessing, SIGNAL(pressed()), this, SLOT(OnClickedShowImageProcessing()));
	connect(ui.buttonTypeSelect, SIGNAL(pressed()), this, SLOT(OnClickedSelectType()));
	connect(ui.buttonSetTolerance, SIGNAL(pressed()), this, SLOT(OnClickedEditTypes()));
	connect(ui.buttonAddType, SIGNAL(pressed()), this, SLOT(OnClickedAddTypes()));
	connect(ui.buttonRemoveType, SIGNAL(pressed()), this, SLOT(OnClickedRemoveTypes()));
	connect(ui.buttonTypeSettings, SIGNAL(pressed()), this, SLOT(OnClickedTypeSettings()));
	connect(ui.buttonResetCounters, SIGNAL(pressed()), this, SLOT(OnClickedResetCounters()));
	connect(ui.buttonLogin, SIGNAL(pressed()), this, SLOT(OnClickedLogin()));
	connect(ui.buttonStatusBox, SIGNAL(pressed()), this, SLOT(OnClickedStatusBox()));
	connect(ui.buttonHistory, SIGNAL(pressed()), this, SLOT(OnClickedShowHistory()));
	connect(ui.buttonGeneralSettings, SIGNAL(pressed()), this, SLOT(OnClickedGeneralSettings()));
	connect(ui.buttonAboutUs, SIGNAL(pressed()), this, SLOT(OnClickedAboutUs()));
	connect(ui.buttonKoracno, SIGNAL(pressed()), this, SLOT(OnClickedKoracnoDialog()));
	connect(ui2.buttonTriggOrient, SIGNAL(pressed()), this, SLOT(OnClickedTriggOrient()));
	connect(ui2.buttonTriggMeasure, SIGNAL(pressed()), this, SLOT(OnClickedTriggMeasure()));
	connect(ui2.buttonTriggStolp1, SIGNAL(pressed()), this, SLOT(OnClickedTriggStolp1()));
	connect(ui2.buttonTriggStolp2, SIGNAL(pressed()), this, SLOT(OnClickedTriggStolp2()));
	
	connect(ui.buttonReset1, &QAbstractButton::clicked, [=]() { OnClickedResetBox(0); });
	connect(ui.buttonReset2, &QAbstractButton::clicked, [=]() { OnClickedResetBox(1); });
	connect(ui.buttonReset3, &QAbstractButton::clicked, [=]() { OnClickedResetBox(2); });
	connect(ui.buttonReset4, &QAbstractButton::clicked, [=]() { OnClickedResetBox(3); });
	connect(ui.buttonReset5, &QAbstractButton::clicked, [=]() { OnClickedResetBox(4); });


	//connect(ui.buttonSMCdialog, SIGNAL(pressed()), this, SLOT(OnClickedSmcButton()));
	////		MULTIMEDIA	TIMER		////
	UINT uDelay = 10;				// 1 = 1000Hz, 10 = 100Hz
	UINT uResolution = 1;
	DWORD dwUser = NULL;
	UINT fuEvent = TIME_PERIODIC;

	// timeBeginPeriod(1);
	FTimerID = timeSetEvent(uDelay, uResolution, OnMultimediaTimer, dwUser, fuEvent);
	

	DrawImageOnScreen();


	history = new History();
	SetHistory();
	connect(history, SIGNAL(callFromHistory(int , int)), this, SLOT(HistoryCall(int ,int)));

	for (int i = 0; i < 12; i++)
	{
		ui.listWidgetStatus->addItem("");
	}
	grabKeyboard();



	cam[1]->image[0].LoadBuffer("C:\\images\\zgorajDim\\0.bmp");
	cam[1]->image[1].LoadBuffer("C:\\images\\zgorajPov\\0.bmp");

}

void MBsoftware::OnFrameReady(int camera, int imageIndex)
{
	/*if (cam[camera]->enableLive == false)
	{
		if (cam[camera]->imageReady[imageIndex] == 1)
		{
			cam[camera]->imageReady[imageIndex] = 2;
			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera0, camera, imageIndex, 0);
		}
	}*/
}

void MBsoftware::OnTcpDataReady(QString conn, int parse)
{
	int value;
	char request;
	if (conn == "EPSON")
	{
		for (int i = 0; i < parse; i++)
		{
			request = tcp[0]->returnCharArray[i];
			value = tcp[0]->returnIntArray[i];
			if (request == 'A')
			{
				if (value == 100)
					zahtevaZaNovKos = true;

				if (value == 101)
					havePiece = true;

				if (value == 102)
					orientationCameraTrigger = true;
				if (value == 200)
					measureStationTrigger = true;

				if (value == 300)
					startStolp1 = true;
				if (value == 400)
					startStolp2 = true;

				if (value == 500)
					referenciranjeRobotKoncano = true;
				if ((value > 800) && (value < 899))
					errorRobot = value % 800;
				if (value == 901) 
					errorKosNaS1 =1;
				if (value == 902)
					errorKosNaS2 = 1;

		
				if (value == 750)
				{
					badBoxDimCounter++;
					types[1][currentType]->totalGlobalMeasurementsCounter++;
				}

			}
		}
	}
	if (conn == "PC2")
	{
		for (int i = 0; i < parse; i++)
		{
			request = tcp[0]->returnCharArray[i];
			value = tcp[0]->returnIntArray[i];
			if (request == 'Z')
			{
				if (value == 1)
					stolp1Kor = 1;
				else
					stolp1Kor = 0;
			}
			if (request == 'H')
			{
				if (value == 1)
					stolp2Kor = 1;
				else
					stolp2Kor = 0;
			}
		}

	}

}

void MBsoftware::HistoryCall(int station, int currentPiece)
{

	if(currentPiece >= 0 )
		if (station == 1)
		{
			*cam[station]->image[0].buffer = history->image[0][station][currentPiece].clone();
			*cam[station]->image[1].buffer = history->image[1][station][currentPiece].clone();
		}
		else
		{
			*cam[station]->image[0].buffer = history->image[0][station][currentPiece].clone();
		}

}

void MBsoftware::MeasurePiece(int index)
{


	
}

void MBsoftware::MeasurePiece(int nrCam, int index)
{
	if (nrCam == 0)
	{
		types[0][currentType]->SetMeasuredValue(measureObject.orientation, 0);
		types[0][currentType]->SetMeasuredValue(measureObject.stNavojev, 1);


		if (types[0][currentType]->IsConditional())
		{
			orient = measureObject.orientation;
				navoj = 1;
				slabiOrientacija = 0;
		}
		else
		{
			orient = measureObject.orientation;
			navoj = 0;
			slabiOrientacija++;
		}
		/*for (int i = 0; i < types[0][currentType]->parameterCounter; i++)
		{
			if (types[0][currentType]->isGood[i] == 0)
			{
				types[0][currentType]->badCounterByParameter[i]++;T
			}
		}*/
		///types[0][currentType]->totalMeasurementsCounter++;
		updateImage[nrCam] = 1;
		UpdateHistory(0, 0);
	}

	else if (nrCam == 1)
	{
		if (measureObject.pieceOnStation[1] == 1)
		{
			if (index == 0)
			{
				pieceOn[1] = measureObject.pieceOnStation[1];
				types[1][currentType]->SetMeasuredValue(measureObject.innerCircle.radius*2, 0);
				types[1][currentType]->SetMeasuredValue(measureObject.innerDev, 1);
				types[1][currentType]->SetMeasuredValue(measureObject.outerCircle.radius*2, 2);
				types[1][currentType]->SetMeasuredValue(measureObject.outerDev, 3);


			}
			else
			{
				types[1][currentType]->SetMeasuredValue(measureObject.navojCircle.radius*2, 4);
				types[1][currentType]->SetMeasuredValue(measureObject.navojCircleDev, 5);
				types[1][currentType]->SetMeasuredValue(measureObject.krogPovrsinaNotranjiDev, 6);
				types[1][currentType]->SetMeasuredValue(measureObject.krogPovrsinaZunanjiDev, 7);
				types[1][currentType]->SetMeasuredValue(measureObject.stDefects, 8);
				if (types[1][currentType]->typeName.compare("11.22.424.771.009") == 0)
				{
					types[1][currentType]->SetMeasuredValue(measureObject.razdaljaZarez, 9);
					types[1][currentType]->SetMeasuredValue(measureObject.kotZarez, 10);
				}
				//doberPovrsina = 1;


				if (measureObject.pieceOnStation[1] == 1)
				{
					if (types[1][currentType]->IsConditional())
					{
						doberDimVrh = 1;
						types[1][currentType]->goodMeasurementsCounter++;
					}
					else
					{
						doberDimVrh = 0;
						types[1][currentType]->badMeasurementsCounter++;
					}
					types[1][currentType]->totalMeasurementsCounter++;
					types[1][currentType]->WriteCounters();
				}
				else
					doberDimVrh = 0;

				for (int i = 0; i < types[1][currentType]->parameterCounter; i++)
				{
					if (types[1][currentType]->isGood[i] == 0)
					{
						types[1][currentType]->badCounterByParameter[i]++;
					}
				}
				types[1][currentType]->SaveMeasurements("C:\\data\\POSTAJA1\\");
				UpdateHistory(1, 0);
			}
		}

		else
		{
			for (int i = 0; i < types[1][currentType]->parameterCounter; i++)
			{
				types[1][currentType]->SetMeasuredValue(0, i);
			}
			doberDimVrh = 0;
		}


			updateImage[nrCam] = 1;



			
		

	}
	else if (nrCam == 2)
	{

		pieceOn[2] = measureObject.pieceOnStation[2];
		//types[2][currentType]->SetMeasuredValue(measureObject.visina[0], 0);
		//types[2][currentType]->SetMeasuredValue(measureObject.sirina[0], 1);
		//types[2][currentType]->SetMeasuredValue(measureObject.sirina[1], 2);
		if (pieceOn[2] == 1)
		{
			for (int i = 0; i < types[2][currentType]->parameterCounter; i++)
			{
				if (types[2][currentType]->dynamicParameters[i][0] > 0)
					types[2][currentType]->SetMeasuredValue(measureObject.measuredValue[2][i], i);
			}

			if (types[2][currentType]->IsConditional())
			{
				doberDimSide = 1;
				types[2][currentType]->goodMeasurementsCounter++;
			}
			else
			{
				doberDimSide = 0;
				types[2][currentType]->badMeasurementsCounter++;
			}

			for (int i = 0; i < types[2][currentType]->parameterCounter; i++)
			{
				if (types[2][currentType]->isGood[i] == 0)
				{
					types[2][currentType]->badCounterByParameter[i]++;
				}
			}

			measureObject.measuredValue[2].clear();
			types[2][currentType]->totalMeasurementsCounter++;
			types[2][currentType]->WriteCounters();
		}
		else
		{
			for (int i = 0; i < types[2][currentType]->parameterCounter; i++)
			{
				types[2][currentType]->SetMeasuredValue(0, i);
			}
			doberDimSide = 0;
		}


		updateImage[nrCam] = 1;
		types[2][currentType]->SaveMeasurements("C:\\data\\POSTAJA2\\");
		UpdateHistory(2, 0);
	}

	if (index == 0)
	{
		for (int i = 0; i < types[nrCam][currentType]->parameterCounter; i++)
		{
			dataArray[nrCam][i][charDataCounter[nrCam]] = types[nrCam][currentType]->measuredValue[i];
		}
		charDataCounter[nrCam]++;
		if (charDataCounter[nrCam] >= CHART_DATA_SIZE)
			charDataCounter[nrCam] = 0;

	}
	cam[nrCam]->imageReady[index] = 4;
	types[0][currentType]->WriteCounters();
	
}

void MBsoftware::UpdateHistory(int station, int index)
{
	
		for (int j = 0; j < types[station][currentType]->parameterCounter; j++)
		{
			history->measuredValue[station][history->lastIn[station]][j] = types[station][currentType]->measuredValue[j];
			history->nominal[station][history->lastIn[station]][j] = types[station][currentType]->nominal[j];
			history->toleranceHigh[station][history->lastIn[station]][j] = types[station][currentType]->toleranceHigh[j];
			history->toleranceLow[station][history->lastIn[station]][j] = types[station][currentType]->toleranceLow[j];
			history->isConditional[station][history->lastIn[station]][j] = types[station][currentType]->isConditional[j];
			history->isActive[station][history->lastIn[station]][j] = types[station][currentType]->isActive[j];
			history->name[station][history->lastIn[station]][j] = types[station][currentType]->name[j];
		}
		if (station == 1)
		{
			history->image[0][station][history->lastIn[station]] = cam[station]->image[0].buffer->clone();
			history->image[1][station][history->lastIn[station]] = cam[station]->image[1].buffer->clone();
		}
		else
			history->image[0][station][history->lastIn[station]] = cam[station]->image[0].buffer->clone();


		history->isGood[station][history->lastIn[station]] = types[station][currentType]->AllGood();
	history->currentPiece[history->lastIn[station]] = index;
	history->UpdateCounter(station);

}

void MBsoftware::SetHistory()
{
	//najprej pobrisemo stare vektorje
	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{

			history->measuredValue[i][j].clear();
			history->nominal[i][j].clear();
			history->toleranceHigh[i][j].clear();
			history->toleranceLow[i][j].clear();
			history->toleranceLowCond[i][j].clear();
			history->toleranceHighCond[i][j].clear();
			history->name[i][j].clear();
			history->isActive[i][j].clear();
			history->isConditional[i][j].clear();
			history->image[0][i].clear();
			history->image[1][i].clear();
		}
		history->parametersCounter[i].clear();
	}


	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{
			if (types[i].size() > 0)
			{
				for (int k = 0; k < types[i][currentType]->parameterCounter; k++)
				{
					history->measuredValue[i][j].push_back(types[i][currentType]->measuredValue[k]);

					history->nominal[i][j].push_back(types[i][currentType]->nominal[k]);
					history->toleranceHigh[i][j].push_back(types[i][currentType]->toleranceHigh[k]);
					history->toleranceLow[i][j].push_back(types[i][currentType]->toleranceLow[k]);
					history->toleranceLowCond[i][j].push_back(types[i][currentType]->toleranceLowCond[k]);
					history->toleranceHighCond[i][j].push_back(types[i][currentType]->toleranceHighCond[k]);
					history->isActive[i][j].push_back(types[i][currentType]->isActive[k]);
					history->isConditional[i][j].push_back(types[i][currentType]->isConditional[k]);
					history->name[i][j].push_back(types[i][currentType]->name[k]);


				}
				if (i == 1)
				{
					history->image[0][i].push_back(Mat(cam[i]->image[0].buffer[0].rows, cam[i]->image[0].buffer[0].rows, CV_8UC3));
					history->image[1][i].push_back(Mat(cam[i]->image[0].buffer[0].rows, cam[i]->image[0].buffer[0].rows, CV_8UC3));
				}
				else
					history->image[0][i].push_back(Mat(cam[i]->image[0].buffer[0].rows, cam[i]->image[0].buffer[0].rows, CV_8UC3));
				//history->cam1Image[i].push_back(Mat(images[1]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//	history->cam2Image[i].push_back(Mat(images[2]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//	history->cam3Image[i].push_back(Mat(images[3]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
				history->parametersCounter[i].push_back(types[i][currentType]->parameterCounter);
			}
		}
	}
	history->CreateMeasurementsTable();


}

void MBsoftware::WriteLogToFile(QString path, QString text, int type)
{
	//lockWrite.lockForWrite();

	QString fileNameTmp = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	QString value;// + "";

	value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + text + type + "\n";
	//ui.plainTextEdit->appendPlainText(value); // Adds the message to the widget
	//ui.plainTextEdit->verticalScrollBar()->setValue(ui.plainTextEdit->verticalScrollBar()->maximum()); // Scrolls to the bottom

	if (fileNameTmp.compare(path) != 0)
	{
		//ko se spremeni datum odpre nov fajl
		m_logFile->close();
		m_logFile->setFileName(fileNameTmp);
		//logPath = fileNameTmp;
		m_logFile->open(QIODevice::Append | QIODevice::Text);
	}
	if (m_logFile != 0)
	{
		QTextStream out(m_logFile);
		out.setCodec("UTF-8");
		out << value;
	} // Logs to file

}

void MBsoftware::InitXilinx()
{
	//set init Belt speed
	uZed[0]->WriteDatagram("R3-3f;"); //hitrost input belt
	uZed[0]->WriteDatagram("R4-4;");	//krmilnik input belt 0
	uZed[0]->SetP3Direction(0, 1);//p3 output
	uZed[0]->SetP8Direction(0);//p8 cel kot input
	uZed[0]->WriteDatagram("R2A-0;");
	uZed[0]->WriteDatagram("R14-0;");	//kkamere trigger
	uZed[0]->WriteDatagram("R1E-0;");	//luci trigger
	uZed[0]->SetOutputValue(0, 11,true);	//vibrator napajanje 
	uZed[0]->SetOutputValue(1,0, 1);// //vhodni trak omogoci motor enable = 0
	uZed[0]->RocnoLuci(0, true);


	uZed[0]->isInitialized = true;
}

void MBsoftware::InitEpson()
{
	QString sendString;
	QByteArray send;

	sendString = QString("R600%1").arg(workingMode);
	send = sendString.toUtf8();
	tcp[0]->WriteServerToClient("EPSON", send);//working mode 

	sendString = QString("R601%1").arg(dimOn);
	send = sendString.toUtf8();
	tcp[0]->WriteServerToClient("EPSON", send);//checkDim

	//poslje error
}

void MBsoftware::SendTypePC2()
{
	
	QString sendString;
	
	QByteArray send;

	sendString = QString("T:%1").arg(types[0][currentType]->typeName);
	send = sendString.toUtf8();
	tcp[0]->WriteServerToClient("PC2", send);
}

void MBsoftware::SendResetPC2()
{
	QString sendString;

	QByteArray send;

	sendString = QString("R0");
	send = sendString.toUtf8();
	tcp[0]->WriteServerToClient("PC2", send);
}

void MBsoftware::CloseXilinx()
{
	uZed[0]->WriteDatagram("R4-0;");	//krmilnik input belt 0
	uZed[0]->WriteDatagram("H0;");	//krmilnik input belt 0
	uZed[0]->SetOutputValue(1,0, 1);	//motor enable = 1
	uZed[0]->WriteDatagram("R14-0;");	//kkamere trigger
	uZed[0]->WriteDatagram("R1E-0;");	//luci trigger
	uZed[0]->SetOutputValue(0, 11, false);	//vibrator napajanje 
	uZed[0]->WriteDatagram("R2A-0;");
	uZed[0]->SetOutputValue(0, 9, true);	//luc pustim prizgano 

}


void MBsoftware::OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	imageProcess->currentType = currentType; //samo zacasno za dodajanje propBrowserja
	QString robotSend;
	QByteArray robotSendArray;
	if (MMTimer.timeCounter >= 500)
	{
		MMTimer.SetStop();
		MMTimer.CalcFrequency(MMTimer.timeCounter);
		MMTimer.timeCounter = 0;
		MMTimer.SetStart();
	}

	MMTimer.timeCounter++;
	mmCounter++;

	uZed[0]->readPendingDatagrams();
	float h = (types[0][currentType]->setting[0][0] - types[0][currentType]->setting[0][1]) / 2;
	h = h * 10;
	int heightZ = h;
	if (heightZ < 0)
		heightZ = 0;
	if (heightZ > 40)
		heightZ = 40;


if (uZed[0]->isConnected == true)
{
	if (uZed[0]->isInitialized == false)
	{
		glob->InitXilinx();
	}
}




workingMode = settings->workingMode;
dimOn = settings->dimOn;
if (workingMode != workingModeOld)
{
	sendWorkingMode = true;
}
workingModeOld = workingMode;
if (dimOn != dimOnOld)
{
	sendDimOn = true;
}
dimOnOld = dimOn;

if (epsonConnected == 1)
{
	QString sendString;
	QByteArray send;

	if (sendWorkingMode == true)
	{
		sendWorkingMode = false;
		sendString = QString("R600%1").arg(workingMode);
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);//working mode 
	}
	if (sendDimOn == true)
	{
		sendDimOn = false;
		sendString = QString("R601%1").arg(dimOn);
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
	if (sendPause == true)
	{
		sendPause = false;
		sendString = QString("R7000");
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
	if (sendStolp1Poln == true)
	{
		sendStolp1Poln = false;
		sendString = QString("R6101");
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
	if (sendStolp2Poln == true)
	{
		sendStolp2Poln = false;
		sendString = QString("R6102");
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
	if (sendStolp1OK == true)
	{
		sendStolp1OK = false;
		sendString = QString("R6100");
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
	if (sendStolp2OK == true)
	{
		sendStolp2OK = false;
		sendString = QString("R6103");
		send = sendString.toUtf8();
		tcp[0]->WriteServerToClient("EPSON", send);
	}
}

//delovanje semaforja
if (processStatus < 0)//sveti rdeca
{
	rdeca = 1;
	zelena = 0;
}
else if (processStatus == 0)//svetita obe
{
	rdeca = 1;
	zelena = 1;
}
else if (processStatus > 0)//tukaj deluje samo zelena in utripajoca zelena , ce je kaksna napaka
{
	rdeca = 0;

	if (workingMode == 2)
	{
			if ((stolp1Error > 0) || (stolp2Error > 0))//dodano ce je napaka na stolpu utripa rdeca luc
			{
				rdecaUtripa = 1;
				zelena = 0;
			}
			else if ((stolp1Poln) || (stolp2Poln))
			{
				zelenaUtripa = 1;
				zelena = 0;
			}
			else
			{
				zelena = 1;
				zelenaUtripa = 0;
				rdecaUtripa = 0;
			}
	}
	else if ((workingMode == 0) || (workingMode == 1))
	{

	}
	if (praznaNapravaCounter > 1000)
	{
		zelenaUtripa = 1;
		zelena = 0;
	}

		
	}

	//luc
	if (uZed[0]->input[0][7].value == 0)
	{
		lucOn = 1;
	}
	else
		lucOn = 0;


	if ((lucOn == 1) && (lucOnOld == 0))
		uZed[0]->SetOutputValue(0, 9, true);
	else if ((lucOn == 0) && (lucOnOld == 1))
		uZed[0]->SetOutputValue(0, 9, false);

	lucOnOld = lucOn;
	if (mmCounter % 50 == 0)
	{
		if (rdeca == 1)
			uZed[0]->RocnoLuci(6, true);
		else if (rdecaUtripa == 0 && rdeca == 0)
			uZed[0]->RocnoLuci(6, false);
		else if (rdecaUtripa == 1)
		{
			if (uZed[0]->luci[6].value == 1)
				uZed[0]->RocnoLuci(6, false);
			else
				uZed[0]->RocnoLuci(6, true);
		}
		if (zelena == 1)
		{
			uZed[0]->RocnoLuci(7, true);
		}
		else if (zelenaUtripa == 1)
		{
			if(uZed[0]->luci[7].value == 1)
			uZed[0]->RocnoLuci(7, false);
			else
				uZed[0]->RocnoLuci(7, true);
		}
	}


	//wD
	if (mmCounter % 2 == 0)
	{
		if (wd == 0)
		{
			wd = 1;
			uZed[0]->SetOutputValue(0, 0, 1);
		}
		else
		{
			wd = 0;
			uZed[0]->SetOutputValue(0, 0, 0);
		}
	}


	if (uZed[0]->input[0][0].value == 0) //pritisnjen je EMG STOP 
	{
		processStatus = -5;
	}
	if ((processStatus > -6))
	{
		if ((skatlaPolna[0] == 1) && (skatlaPolnaOld[0] == 0))
		{
			if (workingMode != 2)
			{
				sendPause = true;
				glob->AddLog("NAPRAVA USTAVLJENA POLNA SKATLA DOBRI 1", 1);
				processStatus = 0;
			}
			else
			{
				sendStolp1Poln = true;
				stolp1Poln = true;
				glob->AddLog("POLNA SKATLA DOBRI 1", 2);
			}
			skatlaPolnaOld[0] = skatlaPolna[0];
		}
		else if ((skatlaPolna[0] == 0) && (skatlaPolnaOld[0] == 1))//dobri kosi skatla2 ce je working mode 2 lahko naprej se deluje ena postaja
		{
			sendStolp1OK = true;
			stolp1OK = true;
			skatlaPolnaOld[0] = skatlaPolna[0];
			glob->AddLog("SKATLA IZPRAZNJENA DOBRI 1", 3);
			stolp1Poln = false;
		}
		if ((skatlaPolna[1] == 1) && (skatlaPolnaOld[1] == 0))
		{
			if (workingMode != 2)
			{
				sendPause = true;
				glob->AddLog("NAPRAVA USTAVLJENA POLNA SKATLA SLABI 1", 1);
				processStatus = 0;
			}
			else
			{
				sendStolp1Poln = true;
				stolp1Poln = true;
				glob->AddLog("POLNA SKATLA SLABI 1", 2);
			}
			skatlaPolnaOld[1] = skatlaPolna[1];
		}
		else if ((skatlaPolna[1] == 0) && (skatlaPolnaOld[1] == 1))
		{
			sendStolp1OK = true;
			stolp1OK = true;
			skatlaPolnaOld[1] = skatlaPolna[1];
			glob->AddLog("SKATLA IZPRAZNJENA SLABI 1", 3);
			stolp1Poln = false;
		}
		if ((skatlaPolna[2] == 1) && (skatlaPolnaOld[2] == 0))
		{
			if (workingMode != 2)
			{
				sendPause = true;
				glob->AddLog("NAPRAVA USTAVLJENA POLNA SKATLA DOBI 2", 1);
				processStatus = 0;
			}
			else
			{
				sendStolp2Poln = true;
				stolp2Poln = true;
				glob->AddLog("POLNA SKATLA DOBRI 2", 2);
			}
			skatlaPolnaOld[2] = skatlaPolna[2];
		}
		else if ((skatlaPolna[2] == 0) && (skatlaPolnaOld[2] == 1))
		{
			sendStolp2OK = true;
			stolp2OK = true;
			skatlaPolnaOld[2] = skatlaPolna[2];
			glob->AddLog("SKATLA IZPRAZNJENA DOBRI 2", 3);
			stolp2Poln = false;
		}
		if ((skatlaPolna[3] == 1) && (skatlaPolnaOld[3] == 0))
		{
			if (workingMode != 2)
			{
				sendPause = true;
				glob->AddLog("NAPRAVA USTAVLJENA POLNA SKATLA SLABI 2", 1);
				processStatus = 0;
			}
			else
			{
				sendStolp2Poln = true;
				stolp2Poln = true;
				glob->AddLog("POLNA SKATLA SLABI 2", 2);
			}
			skatlaPolnaOld[3] = skatlaPolna[3];

		}
		else if ((skatlaPolna[3] == 0) && (skatlaPolnaOld[3] == 1))
		{
			sendStolp2OK = true;
			stolp2OK = true;
			skatlaPolnaOld[3] = skatlaPolna[3];
			glob->AddLog("SKATLA IZPRAZNJENA SLABI 2", 3);
			stolp2Poln = false;
		}
		if ((skatlaPolna[4] == 1) && (skatlaPolnaOld[4] == 0))//slabe dimenzijsko naprava se mora takoj ustaviti. 
		{
			sendPause = true;
			glob->AddLog(" NAPRAVA USTAVLJENA POLNA SKATLA SLABI DIMENZIJE", 1);
			processStatus = 0;
			skatlaPolnaOld[4] = skatlaPolna[4];
		}
		else if ((skatlaPolna[4] == 0) && (skatlaPolnaOld[4] == 1))
		{
			skatlaPolnaOld[4] = skatlaPolna[4];
			glob->AddLog("SKATLA IZPRAZNJENA SLABI DIMENZIJE", 3);

		}

		if ((stolp1Error > 0) && (stolp1Error != stolp1ErrorOld))
		{
			if(stolp1Error == 1)
				glob->AddLog("NAVOJNA POSTAJA 1 NI KONCALA OBDELAVE", 1);
			if(stolp1Error == 2)
				glob->AddLog("PREKORACENO ST. MAX SLABIH KOSOV NA STOLP1", 1);
			stolp1ErrorOld = stolp1Error;
			sendStolp1Poln = true;
			stolp1Poln = true;
			if(workingMode != 2)
			{
				sendPause = true;
				processStatus = 0;
			}

		}
		else if ((stolp1Error == 0) && (stolp1ErrorOld > 0))
		{
			sendStolp1OK = true;
			stolp1OK = true;
			stolp1ErrorOld = stolp1Error;
		}
		if ((stolp2Error > 0) && (stolp2Error != stolp2ErrorOld))
		{
			if (stolp2Error == 1)
			glob->AddLog("NAVOJNA POSTAJA 2 NI KONCALA OBDELAVE", 1);
			if (stolp2Error == 2)
				glob->AddLog("PREKORACENO ST. MAX SLABIH KOSOV NA STOLP2", 1);
			stolp2ErrorOld = stolp2Error;
			sendStolp2Poln = true;
			stolp2Poln = true;
			if (workingMode != 2)
			{
				sendPause = true;
				processStatus = 0;
			}
		}
		else if ((stolp2Error == 0) && (stolp2ErrorOld > 0))
		{
			sendStolp2OK = true;
			stolp2OK = true;
			stolp2ErrorOld = stolp2Error;
		}
		if ((errorNum > 0) && (errorNum != errorNumOld))
		{
			if (errorNum == 1)
			{
				glob->AddLog("NAPAKA CILINDER NA TRAKU", 2);
				processStatus = 0;
				sendPause = true;
				
			}
			if (errorNum == 2)
			{
				glob->AddLog("SLABI NA DIMENZIJI", 2);
				processStatus = 0;
				sendPause = true;

			}
		}
		errorNumOld = errorNum;
		
		if ((stolp1Kor == 0) && (stolp1KorOld == 1))
			glob->AddLog("STOLP 1 V KORACNEM NACINU", 2);
		 if ((stolp1Kor == 1) && (stolp1KorOld == 0))
			 glob->AddLog("STOLP 1 AVTOMATSKEM NACINU", 3);
		 stolp1KorOld = stolp1Kor;
		 if ((stolp2Kor == 0) && (stolp2KorOld == 1))
			 glob->AddLog("STOLP 2 V KORACNEM NACINU", 2);
		 if ((stolp2Kor == 1) && (stolp2KorOld == 0))
			 glob->AddLog("STOLP 2 AVTOMATSKEM NACINU", 3);
		 stolp2KorOld = stolp2Kor;
	}

	if ((processStatus > 0) && (processStatus != 10))
	{
		if ((startKey == 1) || (uZed[0]->isInitialized == false) || ((uZed[0]->input[0][5].value == 0) && (uZed[0]->input[0][5].prevValue == 1))) //stop tipka za stop delovanja
		{
			processStatus = 0;
			startKey = 0;
			stopMotor = true;
			sendPause = true;
		 glob->AddLog("NAPRAVA USTAVLJENA!", 3);
		}
		if (errorRobot > 0)//robot poslje napako 
		{
			if (errorRobot == 1)
			{
				processStatus = 0;
				glob->AddLog(QString("NAPRAVA USATAVLJENA! NAPAKA NA ROBOTU:  %1").arg(errorRobot), 2);
			}
		}

		if ((errorKosNaS1 == 1) && (errorKosNaS1Old == 0))
		{
			glob->AddLog(QString("KOS NI PADEL IZ POSTAJE S1"), 2);
			sendStolp1Poln = true;
			stolp1Poln = true;
		}
		if ((errorKosNaS2 == 1) && (errorKosNaS2Old == 0))
		{
			glob->AddLog(QString("KOS NI PADEL IZ POSTAJE S2"), 2);
			sendStolp2Poln = true;
			stolp2Poln = true;
		}

		errorKosNaS1Old = errorKosNaS1;
		errorKosNaS2Old = errorKosNaS2;

		if (slabiDimenzija >= settings->maxSlabi)//pogoj ce je vec slabih dimenzijskih kosov 
		{
			errorNum = 2;
			processStatus = 0;
		}

		if ((referenciranjeError > 0) && (referenciranjeError != referenciranjeErrorOld))
		{
			referenciranjeErrorOld = referenciranjeError;
			processStatus = 0;
			if (referenciranjeError == 1)
				glob->AddLog("NAPAKA NA REFERENCIRANJU1", 1);
			else if (referenciranjeError == 2)
				glob->AddLog("NAPAKA NA REFERENCIRANJU2", 1);
			else if (referenciranjeError == 3)
				glob->AddLog("NAPAKA NA REFERENCIRANJU3", 1);
			else if (referenciranjeError == 4)
				glob->AddLog("NAPAKA NA REFERENCIRANJU4", 1);
			sendPause = true;
			referenciranjeErrorOld = referenciranjeError;
		}
		if ((uZed[0]->input[0][6].value == 0) && (uZed[0]->input[0][6].prevValue == 1))//iz koracnega v normalno  naprava se mora ustaviti
		{
			processStatus = 0;
			glob->AddLog("NAPRAVA USTAVLJENA! IZ KORACNEGA DELOVANJA V NORMALNO", 2);
			
		}
		if (workingMode == 2)
		{
			if ((stolp2Poln == 1) && (stolp1Poln == 1))
			{
				sendPause = true;
				glob->AddLog("NAPRAVA USTAVLJENA NAVOJNE POSTAJE ZATAKNJENE", 1);
				processStatus = 0;
				sendStolp1Poln = true;
				sendStolp2Poln = true;
			}
		}
			

	

	skatleErrorOld = skatleError;

		
	}
	if (processStatus  >-1)
	{
		if (epsonConnected == 0)//ce epson ni povezan ponastavim vse njegove ukaze 
		{
			zahtevaZaNovKos = false;
			havePiece = false;
			processStatus = 0;

		}
	}
	if ((uZed[0]->input[0][3].value == 0))
	{
		processStatus = -3;

	}
	if ((uZed[0]->input[0][7].value == 0) && (uZed[0]->input[0][7].prevValue == 1))//odprta vrata
	{

		glob->AddLog("VRATA ODPRTA!", 2);
		if (processStatus > 0)
		{
			if ((uZed[0]->input[0][6].value == 0))//ce ni v koracnem nacinu se vse ustavi 
			{
				glob->AddLog("VRATA ODPRTA NAPRAVA USTAVLJENA!", 2);
				sendPause = true;
			}
			processStatus = 0;

		}
	}
	

	//ce je potrebno referenciranje pred novim zagonom 
	if ((uZed[0]->input[0][7].value == 1) && (uZed[0]->input[0][7].prevValue == 0))
	{
		
		glob->AddLog("VRATA ZAPRTA POTREBNO REFERENCIRANJE!", 2);
		referenciranje = true;
	}



	//motor 
	if (startMotor == true)
	{
		startMotor = false;
		uZed[0]->SetOutputValue(1, 0, 0);
		motorRunning = true;
	}
	if (stopMotor == true)
	{
		stopMotor = false;
		motorRunning = false;
		uZed[0]->SetOutputValue(1, 0, 1);
	}

	//vibrator
	if (processStatus == 1)
	{
		if (vibratorOn == 0)
		{
			vibratorCounterStop = 0;
			if ((uZed[0]->input[0][12].value == 0) && (uZed[0]->input[0][11].value == 0))
				vibratorCounterStart++;
			else
				vibratorCounterStart = 0;
			if (vibratorCounterStart == 500)
				vibratorOn = 1;
		}
		else
		{
			vibratorCounterStart = 0;
			if ((uZed[0]->input[0][12].value == 1) && (uZed[0]->input[0][11].value == 1))
				vibratorCounterStop++;
			else
				vibratorCounterStop = 0;
			if (vibratorCounterStop == 200)
				vibratorOn = 0;
		}
	}
	else
		vibratorOn = 0;


	if ((vibratorOn == 0) && (vibratorOnOld == 1))
	{
		uZed[0]->SetOutputValue(0, 8, false);
		vibratorRunning = 0;
	}
	else if ((vibratorOn == 1) && (vibratorOnOld == 0))
	{
		uZed[0]->SetOutputValue(0, 8, true);
		vibratorRunning = 1;
	}
	vibratorOnOld = vibratorOn;

	//luc na stropu. Aktivna je lahko samo kadar naprava stoji, ali je v koracnem nacinu


	if ((uZed[0]->input[0][3].value == 0) && (uZed[0]->input[0][3].prevValue == 1))
	{
		glob->AddLog("GLAVNI STOP!", 1);
		sendPause = true;
	}
	//error status 


	switch (processStatus)
		{
		case(-5)://emergancy stop
		{
			if ((uZed[0]->input[0][0].value == 1) && (uZed[0]->input[0][0].prevValue == 0))
			{
				processStatus = -3;
				glob->AddLog("EMG STOP SPROSCEN", 2);
			}
			break;
		}
		case(-4): //cakanje potrditve EMG
		{

		}
		case(-3): //cakanje na zrak in glavno start tipko
		{
			if ((uZed[0]->input[0][14].value == 1) && (uZed[0]->input[0][14].prevValue == 0))
			{
				processStatus = 0;
				glob->AddLog("PRITISNJEN GLAVNI START!", 3);
			}
			if (motorRunning == 1)
				stopMotor = true;
			vibratorOn = 0;
			break;
		}
		case(0):	//naprava miruje caka na tipko start
		{
			if ((startKey == true)||((uZed[0]->input[0][4].value == 1) && (uZed[0]->input[0][4].prevValue == 0))) //tipka start za zacetek delovanja
			{
				startKey = 0;
				if(referenciranje == true)
					processStatus = 2;
				else
				processStatus = 1;

				cylnderStatus = 0;
				referenciranjeError = 0;
				referenciranjeErrorOld = 0;
				statusReferenciranje = 0;
				vibratorOn = 0;
				praznaNapravaCounter = 0;
				stolp1Error = 0;
				stolp2Error = 0;
				stolp1ErrorCounter = 0;
				stolp2ErrorCounter = 0;
				sendStolp1OK = true;
				sendStolp2OK = true;
				stolp1Poln = false;
				stolp2Poln = false;
				errorNum = 0;
				cylinderErrorCounter = 0;
				slabiDimenzija = 0;
				slabiOrientacija = 0;
				slabiS1Zap = 0;
				slabiS2Zap = 0;
				errorKosNaS1 = 0;
				errorKosNaS2 = 0;
				//stolp1OK = true;
				//stolp2OK = true;
				for (int i = 0; i < 5; i++)
					skatlaPolnaOld[i] = 0;

		
			glob->AddLog("NAPRAVA POGNANA!", 0);
			if(epsonConnected == 0)
			glob->AddLog("ROBOT NI POVEZAN NAPRAVA USTAVLJENA!", 1);

			if (uZed[0]->input[2][5].value == 0)
			{
				glob->AddLog("NI ZRAKA V NAPRAVI! NAPRAVA USTAVLJENA!", 1);
				sendPause = true;
				processStatus = 0;
			}
			}

			if (motorRunning == 1)
				stopMotor = true;


			break;
		}
		case(1):	//normalno delovanje
		{
			switch (cylnderStatus)
			{
			case 0:
			{
				cylnderStatus = 1;
				uZed[0]->SetOutputValue(0, 2, 0);//pomakne cilinder nazaj za vsak slucaj ce se prej ni zadeva dokoncala
				praznaNapravaCounter = 0;
				sensorDelay = 0;
				break;
			}
			case 1://zacni premikat motor ali preveri ce je kos ze prisoten za prijemalo
			{
				if (uZed[0]->input[0][8].value == 1)
				{
					sensorDelay++;
					if (sensorDelay > 100)
					{
						cylnderStatus = 6; // preskoci cilinder kos je z pripravljen 
						startMotor = true;
						sensorDelay = 0;
					}
				}
				else
				{
					startMotor = true;
					cylnderStatus = 2;
					sensorDelay = 0;
				}
				break;
			}
			case 2://caka na kos gleda senzor 
			{
				if (uZed[0]->input[0][13].value == 1)
				{
					sensorDelay++;
				}
				else
				{
					sensorDelay = 0;
				}

				if (sensorDelay == 10)//kos je pripravljen
				{
					stopMotor = true;
					cylnderStatus = 3;
					sensorDelay = 0;
				}

				praznaNapravaCounter++;//ce je naprava prazna po 60 sekundah se ustavi
				if (praznaNapravaCounter > 6000)
				{
					glob->AddLog("PRAZNA NAPRAVA! NAPRAVA USTAVLJENA!", 1);
					processStatus = 0;
					sendPause = true;
				}
				break;
			}

			case 3:
			{

				sensorDelay++;
				if (sensorDelay > 50)
				{
					uZed[0]->SetOutputValue(0, 2, 1); //odpre cilinder
					sensorDelay = 0;
					cylnderStatus = 4;

				}
				break;
			}
			case 4:
			{
				if (uZed[0]->input[0][10].value == 1)//cakam senzor javi da je cilinder odprt
				{
					sensorDelay++;
				}
				else//tukaj se dodati error counter
				{
					sensorDelay = 0;
				}

				if (sensorDelay > 20)
				{
					uZed[0]->SetOutputValue(0, 2, 0); //zapre cilinder
					cylnderStatus = 5;
					sensorDelay = 0;
				}
				errorCounter++;

				break;
			}
			case 5://pocaka da se cilinder vrne na svoje mesto 
			{
				if (uZed[0]->input[0][9].value == 1)//tudi tukaj error counter
				{
					cylnderStatus = 6;
					startMotor = true;
				}

				break;
			}
			case 6://c
			{
				sensorDelay = 0;
				robotSend = QString("R100%1").arg(heightZ);
				robotSendArray = robotSend.toUtf8();

				if (tcp[0]->WriteServerToClient("EPSON", robotSendArray) == 1)//request naj gre po kos
				{
					cylnderStatus = 8;

				}
				else
					cylnderStatus = 7;
				//	}

				break;
			}
			case 7://ce robot ni aktiven oz. povezan ponovi 
			{
				sensorDelay++;
				if (sensorDelay > 100)
				{
					sensorDelay = 0;
					cylnderStatus = 7;
				}
				break;
			}
			case 8://pocaka odgovor robota da je uzel kos. 
			{
				
				if (uZed[0]->input[0][13].value == 1)
					sensorDelay++;
				else
					sensorDelay = 0;
				if (vibratorRunning == 1)
				{
					if(sensorDelay == 6000)//po 1 minuti se trak ustavi 
						stopMotor = true;
	
				}
				else
				{
					if(sensorDelay == 100)//po 2 sekundah se trak ustavi 
					stopMotor = true;
				}

				if (havePiece == true)
				{
					TestFun[3]->SetStop();
					taktArray[taktCounter] = TestFun[3]->ElapsedTime();
					taktCounter++;
					if (taktCounter > 9)
						taktCounter = 0;
					TestFun[3]->SetStart();
					havePiece = false;
					types[0][currentType]->totalGlobalMeasurementsCounter++; //stevec vseh kosov
					cylnderStatus = 0;
				}

				break;
			}
			}
			
			break;
		}
		case 2://referenciranje 
		{
			//najprej sprozim referenciranje robota da se vrne na svoje mesto, nato pa se preverim postaje, ce je kos gor.

			switch (statusReferenciranje)
			{
			case 0:
			{
				if (epsonConnected == 1)
				{
					referenciranjeRobotKoncano = false;
					if (tcp[0]->WriteServerToClient("EPSON", "R500") == 1)//request naj gre po kos
					{
						
						statusReferenciranje = 1;
						referenciranjeErrorCounter = 0;
						referenciranjeError = 0;

					}
				}
				
				break;
			}
			case 1:
			{
				if (referenciranjeRobotKoncano == true)
				{
					referenciranjeRobotKoncano = false;
					statusReferenciranje = 2;
				}

				break;
			}
			case 2://robot se je postavil na svoje mesto. pocisti se postaje
			{referenciranjeErrorCounter++;
				if (uZed[0]->input[2][7].value == 1)//stolp 1 je koncal obdelavo
				{
					uZed[0]->SetOutputValue(0, 3, 1);
					uZed[0]->SetOutputValue(0, 4, 0);//loputa je zihr zaprta
					statusReferenciranje = 3;
					referenciranjeErrorCounter = 0;
				}
				if (referenciranjeErrorCounter > referenciranjeErrorMax)
					referenciranjeError = 1;

				break;
			}
			case 3:
			{referenciranjeErrorCounter++;
				if (uZed[0]->input[2][6].value == 1)//stolp 2 je koncal obdelavo
				{
					uZed[0]->SetOutputValue(0, 5, 1);
					uZed[0]->SetOutputValue(0, 6, 0);//loputa je zihr zaprta
					statusReferenciranje = 4;
					referenciranjeErrorCounter = 0;
				}
				if (referenciranjeErrorCounter > referenciranjeErrorMax)
					referenciranjeError = 2;
				break;
			}
			case 4:
			{
				referenciranjeErrorCounter++;
				if (uZed[0]->input[2][8].value == 1)
				{
					uZed[0]->SetOutputValue(0, 3, 0);
					statusReferenciranje = 5;
					referenciranjeErrorCounter = 0;
				}
				if (referenciranjeErrorCounter > referenciranjeErrorMax)
					referenciranjeError = 3;
			}
			break;
			case 5:
			{
				referenciranjeErrorCounter++;
				if (uZed[0]->input[2][12].value == 1) 
				{
					uZed[0]->SetOutputValue(0, 5, 0);
					statusReferenciranje = 6;
					referenciranjeErrorCounter = 0;

				}
				if (referenciranjeErrorCounter > referenciranjeErrorMax)
					referenciranjeError = 4;
				break;
			}
			case 6:
			{
				statusReferenciranje = 0;
				processStatus = 1;
				glob->AddLog("REFERENCIRANJE KONCANO", 0);
				referenciranje = false;
				break;
			}

			}

		 
			break;
		}


	}//tukaj je konec glavnega switch stavka
	if ((cylnderStatus == 3) || (cylnderStatus == 4) || (cylnderStatus == 5))
	{
		cylinderErrorCounter++;
		if (cylinderErrorCounter ==500)
		{
			errorNum = 1;
		}
	}
	else
	cylinderErrorCounter = 0;




	switch (stolp1Status)//Za stolp
	{
	case 0:
	{
		if (startStolp1 == true)
		{
			stolp1ErrorCounter = 0;
			startStolp1 = false;
			stolp1Status = 1;
			TestFun[1]->SetStart();
			stolp1StatusPrikaz = 1;
			uZed[0]->SetOutputValue(0, 15, 1);//poslje za zacetek obdelave PC2
			uZed[0]->SetOutputValue(0, 13, 1);//poslje bit da je stolp zaseden 
		}
		break;
	}
	case 1://cakam na sprejem obdelave{
	{
		if ((uZed[0]->input[2][1].value == 1) && (uZed[0]->input[2][1].prevValue == 0))
		{
			stolp1Status = 2;
			uZed[0]->SetOutputValue(0, 15, 0);//ugasnem zacetek obdelave
		}
		break;
	}
	case 2: //cakam konec obdelave
	{
		if ((uZed[0]->input[2][1].prevValue == 1) && (uZed[0]->input[2][1].value == 0))//ko ugasne obdelujem preberem stanje dober / slab
		{

			if (uZed[0]->input[2][2].value == 1)
			{
				stolp1Dober = 1;
				slabiS1Zap = 0;
				stolp1Status = 3;
			}
			else
			{
				stolp1Dober = 0;
				stolp1Status = 5;
				slabiS1Zap++;
				uZed[0]->SetOutputValue(0, 4, 0);
			}
		}
		break;
	}
	case 3: //ce je kos dober prestavim loputo drugace gre naprej
	{
		uZed[0]->SetOutputValue(0, 4, 1);//prestavim loputo in pocakam na senzor
		stolp1Status = 4;
		break;
	}
	case 4: //cakam senzor da je loputa res na mestu
	{
		if ((uZed[0]->input[2][11].value == 1) && (uZed[0]->input[2][11].prevValue == 0))//cakam senzor lopute
		{
			stolp1Status = 5;
		}

		break;
	}
	case 5: //sprozim pomik
	{
		if (uZed[0]->input[2][7].value == 1)
		{
			uZed[0]->SetOutputValue(0, 3, 1);
			stolp1Status = 6;
		}

		break;
	}
	case 6: //cakam na konec pomika in vrnem senzor na svoje mesto
	{
		if ((uZed[0]->input[2][8].value == 1) && (uZed[0]->input[2][8].prevValue == 0))//
		{
			uZed[0]->SetOutputValue(0, 3, 0);
			stolp1Status = 7;
			if (stolp1Dober == 0)
			{
				badBox1Counter++;
				stolp1SlabiCounter++;
			}
			else
			{
				dobriBox1Counter++;
				stolp1DobriCounter++;
			}

			TestFun[1]->SetStop();
			TestFun[1]->ElapsedTime();
			stolp1StatusPrikaz = 2;
			stolp1Delay = 0;

		}

		break;
	}
	case 7: //pocakam se delay da vrnem loputo nzaj na default pozicijo za slabe
	{
		stolp1Delay++;
		if(stolp1Delay == 100)
			uZed[0]->SetOutputValue(0, 13, 0);//poslje bit da je stolp prost
		if (stolp1Delay > 300)
		{
			stolp1Status = 0;
			uZed[0]->SetOutputValue(0, 4, 0);
	 


		}
		break;
	}
	}
	

		switch (stolp2Status)//Za stolp
		{
		case 0:
		{
			if (startStolp2 == true)
			{
				stolp2ErrorCounter = 0;
				startStolp2 = false;
				stolp2Status = 1;
				TestFun[2]->SetStart();
				stolp2StatusPrikaz = 1;
				uZed[0]->SetOutputValue(0, 14, 1);//poslje za zacetek obdelave PC2
				uZed[0]->SetOutputValue(0, 12, 1);//poslje bit da je stolp zaseden 
			}
			break;
		}
		case 1://cakam na sprejem obdelave{
		{
			if ((uZed[0]->input[2][3].value == 1) && (uZed[0]->input[2][3].prevValue == 0))
			{
				stolp2Status = 2;
				uZed[0]->SetOutputValue(0, 14, 0);//ugasnem zacetek obdelave
			}
			break;
		}
		case 2: //cakam konec obdelave
		{
			if ((uZed[0]->input[2][3].prevValue == 1) && (uZed[0]->input[2][3].value == 0))//ko ugasne obdelujem preberem stanje dober / slab
			{

				if (uZed[0]->input[2][4].value == 1)
				{
					stolp2Dober = 1;
					stolp2Status = 3;
					slabiS2Zap = 0;

				}
				else
				{
					stolp2Dober = 0;
					stolp2Status = 5;
					slabiS2Zap++;
					uZed[0]->SetOutputValue(0, 6, 0);//loputa 
				}
			}
			break;
		}
		case 3: //ce je kos dober prestavim loputo drugace gre naprej
		{
			uZed[0]->SetOutputValue(0, 6, 1);//prestavim loputo in pocakam na senzor
			stolp2Status = 4;
			break;
		}
		case 4: //cakam senzor da je loputa res na mestu
		{
			if ((uZed[0]->input[2][15].value == 1) && (uZed[0]->input[2][15].prevValue == 0))//cakam senzor lopute
			{
				stolp2Status = 5;
			}

			break;
		}
		case 5: //sprozim pomik
		{
			if (uZed[0]->input[2][6].value == 1)
			{
				uZed[0]->SetOutputValue(0, 5, 1);
				stolp2Status = 6;
			}

			break;
		}
		case 6: //cakam na konec pomika in vrnem senzor na svoje mesto
		{
			if ((uZed[0]->input[2][12].value == 1) && (uZed[0]->input[2][12].prevValue == 0))//
			{
				uZed[0]->SetOutputValue(0, 5, 0);
				stolp2Status = 7;
				if (stolp2Dober == 0)
				{
					badBox2Counter++;
					stolp2SlabiCounter++;
					
				}
				else
				{
					goodBox2Counter++;
					stolp2DobriCounter++;
				}

				TestFun[2]->SetStop();
				TestFun[2]->ElapsedTime();
				stolp2StatusPrikaz = 2;
				stolp2Delay = 0;
				 
			}

			break;
		}
		case 7: //pocakam se delay da vrnem loputo nzaj na default pozicijo za slabe
		{
			stolp2Delay++;
			if (stolp2Delay == 100)
				uZed[0]->SetOutputValue(0, 12, 0);//poslje bit da je stolp prost
			if (stolp2Delay > 300)
			{
				stolp2Status = 0;
				uZed[0]->SetOutputValue(0, 6, 0);



			}
			break;
		}
		}
	
		if (stolp1Status > 0)
		{
			stolp1ErrorCounter++;
			if (stolp1ErrorCounter > 6000)
				stolp1Error = 1;
			if (slabiS1Zap >= settings->maxSlabiStolp)
				stolp1Error = 2;
		}
		if (stolp2Status > 0)
		{
			stolp2ErrorCounter++;
			if (stolp2ErrorCounter > 6000)
				stolp2Error = 1;
			if (slabiS2Zap >= settings->maxSlabiStolp)
				stolp2Error = 2;
		}

		//skatle status in erorji

		if (dobriBox1Counter >= types[0][currentType]->setting[1][0])
			skatlaPolna[0] = 1;
		else
			skatlaPolna[0] = 0;
	
		 if (badBox1Counter >= types[0][currentType]->setting[1][1])
			 skatlaPolna[1] = 1;
		 else
			 skatlaPolna[1] = 0;

		 if (goodBox2Counter >= types[0][currentType]->setting[1][2])
			 skatlaPolna[2] = 1;
		 else
			 skatlaPolna[2] = 0;

		 if (badBox2Counter >= types[0][currentType]->setting[1][3])
			 skatlaPolna[3] = 1;
		 else
			 skatlaPolna[3] = 0;

		 if (badBoxDimCounter >= types[0][currentType]->setting[1][4])
			 skatlaPolna[4] = 1;
		 else
			 skatlaPolna[4] = 0;


		 /*if (tmpE == -1)
			 skatleError = 0;
		 else
			 skatleError = tmpE;*/
			




			


	switch (orientationCameraStatus)
	{
	case 0:
		if (orientationCameraTrigger == true)
		{
			orientationCameraTrigger = false;
			orientationCameraStatus = 1;
		}
		break;
	case 1://poslje trigger
	{
		cam[0]->imageReady[0] = 0;
		uZed[0]->RocnoProzenjeKamere(0);
		cam[0]->GrabImage(0);

		orientationCameraStatus = 2;
		stationStatus[0] = 1;
		break;
	}
	case 2://caka sliko
	{
		stationStatus[0] = 2;
		if (cam[0]->imageReady[0] == 1)
		{
			cam[0]->imageReady[0] = 2;
			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera0, 0, 0, 0);
			orientationCameraStatus = 3;
		}
		else
			oreintationCameraDelay++;


		if (oreintationCameraDelay > 100)
			orientationCameraStatus = 1;

		break;
	}
	case 3:
	{		stationStatus[0] = 3;
			if (cam[0]->imageReady[0] == 4)
			{
				robotSend = QString("R102%1%2").arg(orient).arg(navoj);
				robotSendArray = robotSend.toUtf8();
				orient = -1;
				navoj = -1;
				tcp[0]->WriteServerToClient("EPSON", robotSendArray); 
				orientationCameraStatus = 0;
				stationStatus[0] = 4;

			}


		break;
	}



	}


	int kosDober = 0;
		int kosJe = 0;
	switch (measureStationStatus)
	{
	case 0:
		if (measureStationTrigger == true)
		{
			measureStationTrigger = false;
			measureStationStatus = 1;
			uZed[0]->RocnoLuci(1, false);
			uZed[0]->RocnoLuci(2, false);
			uZed[0]->RocnoLuci(3, false);
			uZed[0]->RocnoLuci(4, false);
			currChartUpdated = 0;
		}
		break;
	case 1: //zacni slikanje iz vrha prizge luc od spodaj
	{
		uZed[0]->RocnoLuci(1, true);
		cam[1]->imageReady[0] = 0;
		cam[1]->imageIndex = 0;
		cam[1]->SetExposureAbsolute(cam[1]->exposure[0]);
		cam[1]->SetGainAbsolute(cam[1]->gain[0]);
		measureStationStatus = 2;
		measureStationDelay = 0;
		break;
	}
	
	case 2://prozi kamero od spodaj
	{
		measureStationDelay++;
		if (measureStationDelay > 4)
		{
			uZed[0]->RocnoProzenjeKamere(1);
			//cam[1]->GrabImage(0);
			measureStationStatus = 3;
			measureStationDelay = 0;
		}
		break;
	}
	case 3://caka prvo sliko 
	{
		if (cam[1]->imageReady[0] == 1)
		{
			cam[1]->imageReady[0] = 2;
			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera1, 1, 0, 0);
			measureStationStatus = 4;
			uZed[0]->RocnoLuci(1, false);
		}
		else
		{
			measureStationDelay++;
		}
		if (measureStationDelay > 40)//ce slike ni nekaj casa ponovimo postopek
		{
			measureStationDelay = 0;
			measureStationStatus = 1;
		}
		break;
		
	}
	case 4://zacni slkianje iz vrha povrsina prizge luc
	{
		uZed[0]->RocnoLuci(2, true);
		cam[1]->imageReady[1] = 0;
		cam[1]->imageIndex = 1;
		cam[1]->SetExposureAbsolute(cam[1]->exposure[1]);
		cam[1]->SetGainAbsolute(cam[1]->gain[1]);
		measureStationStatus = 5;
		measureStationDelay = 0;
		break;
	}
	case 5://zacni slkianje iz vrha povrsina trigne kamero
	{
		measureStationDelay++;
		if (measureStationDelay > 4)
		{
			uZed[0]->RocnoProzenjeKamere(1);
			//cam[1]->GrabImage(1);
			measureStationStatus = 6;
			measureStationDelay = 0;
		}
		break;
	}
	case 6://ko pride slika sprozi obdelavo
	{
		if (cam[1]->imageReady[1] == 1)
		{
			cam[1]->imageReady[1] = 2;
			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera3, 1, 1, 0);
			measureStationStatus = 7;
			uZed[0]->RocnoLuci(2, false);
		}
		else
		{
			measureStationDelay++;
		}
		if (measureStationDelay > 40)
		{
			measureStationDelay = 0;
			measureStationStatus = 5;
		}
		break;
	}
	case 7: //slikanje iz strani sprozi luc
	{
		uZed[0]->RocnoLuci(3, true);
		cam[2]->imageReady[0] = 0;
		cam[2]->imageIndex = 0;
		measureStationStatus = 8;
		measureStationDelay = 0;
		break;

	}
	case 8://slikanje iz strani prizge kamero
	{
		measureStationDelay++;
		if (measureStationDelay > 4)
		{
			uZed[0]->RocnoProzenjeKamere(2);
			//cam[2]->GrabImage(0);
			measureStationStatus = 9;
			measureStationDelay = 0;
		}
		break;
	}
	case 9://ko pride slika sprozi obdelavo
	{
		if (cam[2]->imageReady[0] == 1)
		{
			cam[2]->imageReady[0] = 2;
			
			QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera2, 2, 0, 0);
			measureStationStatus = 10;
			uZed[0]->RocnoLuci(3, false);
		}
		else
		{
			measureStationDelay++;
		}
		if (measureStationDelay > 20)
		{
			measureStationDelay = 0;
			measureStationStatus = 7;
		}
		break;
	}



	case 10: 
	{
		if ((cam[1]->imageReady[0] == 4) && (cam[1]->imageReady[1] == 4) && (cam[2]->imageReady[0] == 4))
		{
			if ((doberDimSide) && (doberDimVrh))
			{
				kosDober = 1;
				slabiDimenzija = 0;
			}
			else
			{
				kosDober = 0;
				if (pieceOn[1] == 1)
				{
					slabiDimenzija++;
					//if(rocnoProzi == 0)
					//types[1][currentType]->totalGlobalMeasurementsCounter++;//za slabe kose dimenzijski postaji 
				}

			}

			if (pieceOn[1] == 0)
				kosJe = 0; 
			else
				kosJe = 1;
			robotSend = QString("R200%1%2").arg(kosJe).arg(kosDober);
			//robotSend = QString("R200%1%2%3%4").arg(doberDimVrh).arg(doberDimSide).arg(doberPovrsina).arg(pieceOn[1]);
			robotSendArray = robotSend.toUtf8();
			doberDimVrh = -1;
			doberDimSide = -1;
			doberPovrsina = -1;
			tcp[0]->WriteServerToClient("EPSON", robotSendArray);
			measureStationStatus = 0;
			rocnoProzi = 0;
		}

		break;
	}

	}

	//epson in PC2 connected statusu
	QString name1 = "PC2";
	QString name2 = "EPSON";
	int stej1 = 0;
	int stej2 = 0;
	for (int i = 0; i < MAX_CONN; i++)
	{
		if (name1.compare(tcp[0]->tcpConnectionsName[i]) == 0)
		{
			stej1++;
		}
		if (name2.compare(tcp[0]->tcpConnectionsName[i]) == 0)
		{
			stej2++;
		}

	}
	if (stej1 > 0)
		pc2Connected = 1;
	else
	{
		pc2Connected = 0;
		pc2Initialized = 0;
	}
	if (stej2 > 0)
			epsonConnected = 1;
	else
	{
		epsonConnected = 0;
		epsonInitialized = 0;
		errorRobot = 0;
	}

	if ((epsonConnected == 1) && (epsonInitialized == 0))
	{
		glob->InitEpson();
		epsonInitialized = 1;
		referenciranje = true;
	}



	if ((pc2Connected == 1) && (pc2Initialized == 0))
	{
		glob->SendTypePC2();
		pc2Initialized = 1;
	}
	if ((pc2Connected == 1) && (sendRestPC2 == 1))
	{
		glob->SendResetPC2();
		sendRestPC2 = 0;
	}


}



void MBsoftware::CreateCameraTab()
{

//QWidget *cameraWidget;
	QGroupBox *camGroup = new QGroupBox(ui.tabCameras);;
	QHBoxLayout *camLayout = new QHBoxLayout;
	QToolButton *camButton;
	QIcon camIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	int k = 0;

	//QString fileP = QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(k);
	QImage image;
	/*if (!image.isNull())
		camIcon.addPixmap(QPixmap::fromImage(image));*/

	for (int i = 0; i < cam.size(); i++)
	{
		image.load(QString(QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(i)));
		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		camButton = new QToolButton();
		camButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		
		if (slovensko)  camButton->setText(QString("KAM%1").arg(i));
		else camButton->setText(QString("CAM%1").arg(i));
		camButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
		camButton->setIcon(camIcon);
		camButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
		camButton->setAutoRaise(true);
		camLayout->addWidget(camButton);
		connect(camButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(camButton, i);
	}


	
	tabLayout->addWidget(camGroup);
	tabLayout->addSpacerItem(spacer);
	ui.tabCameras->setLayout(tabLayout);
	camGroup->setLayout(camLayout);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));

}

void MBsoftware::CreateSignalsTab()
{


	QHBoxLayout *lptLayout = new QHBoxLayout;
	QToolButton *lptButton;
	QHBoxLayout *smartCardLayout = new QHBoxLayout;
	QToolButton *smartCardButton;
	QHBoxLayout *tcpLayout = new QHBoxLayout;
	QToolButton *tcpButton;
	QHBoxLayout *xilLayout = new QHBoxLayout;
	QToolButton *xilButton;

	QIcon camIcon;
	QIcon smartIcon;
	QIcon xilIcon;

	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QSignalMapper* signalSmartCardMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;


	int k = 0;

	
	QImage image;
	ui.tabSignals->setLayout(tabLayout);
	if (lpt.size() > 0)
	{
		QGroupBox *lptGroup = new QGroupBox(ui.tabSignals);
		lptGroup->setTitle("LPT");

		image.load(QString(QDir::currentPath() + QString("\\res\\lptIcon.bmp")));

		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < lpt.size(); i++)
		{

			lptButton = new QToolButton();
			lptButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			lptButton->setText(QString("LPT%1").arg(i));
			lptButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			lptButton->setIcon(camIcon);
			lptButton->setAutoRaise(true);
			lptButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			lptLayout->addWidget(lptButton);
			connect(lptButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
			signalMapper->setMapping(lptButton, i);
		}
		tabLayout->addWidget(lptGroup);

		ui.tabSignals->setLayout(tabLayout);
		lptLayout->setMargin(0);
		lptGroup->setLayout(lptLayout);
		connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowLptButton(int)));
	}
	
	if (controlCardMB.size() > 0)
	{
		QGroupBox *SmartCardGroup = new QGroupBox(ui.tabSignals);
		SmartCardGroup->setTitle("SmartCard");

		image.load(QString(QDir::currentPath() + QString("\\res\\serialPort.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			smartCardButton = new QToolButton();
			smartCardButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			smartCardButton->setText(QString("SmartC%1").arg(i));
			smartCardButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			smartCardButton->setIcon(smartIcon);
			smartCardButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			smartCardButton->setAutoRaise(true);
			smartCardLayout->addWidget(smartCardButton);
			connect(smartCardButton, SIGNAL(clicked()), signalSmartCardMapper, SLOT(map()));
			signalSmartCardMapper->setMapping(smartCardButton, i);
		}

		tabLayout->addWidget(SmartCardGroup);
		smartCardLayout->setMargin(0);
		SmartCardGroup->setLayout(smartCardLayout);
		connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));

	}
	if (uZed.size() > 0)
	{
		QGroupBox *uZedBox = new QGroupBox(ui.tabSignals);
		uZedBox->setTitle("Zynq I/O");

		image.load(QString(QDir::currentPath() + QString("\\res\\xilinxIO.bmp")));

		if (!image.isNull())
			xilIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < uZed.size(); i++)
		{

			xilButton = new QToolButton();
			xilButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			xilButton->setText(QString("uZed%1").arg(i));
			xilButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			xilButton->setIcon(xilIcon);
			xilButton->setAutoRaise(true);
			xilButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			xilLayout->addWidget(xilButton);
			connect(xilButton, &QPushButton::clicked, [=](int index) { OnClickedShowUZedButton(i); });
		}
		//xilLayout->addWidget(uZedBox);

		tabLayout->addWidget(uZedBox);
		xilLayout->setMargin(0);
		uZedBox->setLayout(xilLayout);



	}
	if (tcp.size() > 0)
	{
		QGroupBox *tcpGroup = new QGroupBox(ui.tabSignals);
		tcpGroup->setTitle("TCP");
		image.load(QString(QDir::currentPath() + QString("\\res\\localNetwork.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < tcp.size(); i++)
		{
			tcpButton = new QToolButton();
			tcpButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			tcpButton->setText(QString("TCP%1").arg(i));
			tcpButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			tcpButton->setIcon(smartIcon);
			tcpButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			tcpButton->setAutoRaise(true);
			tcpLayout->addWidget(tcpButton);

			connect(tcpButton, &QPushButton::clicked,
				[=](int index) { OnClickedTCPconnction(i); });

		}
		tabLayout->addWidget(tcpGroup);
		tcpLayout->setMargin(0);
		tcpGroup->setLayout(tcpLayout);
		//connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));
	}



	tabLayout->addSpacerItem(spacer);



}

void MBsoftware::CreateMeasurementsTable()
{
	int height = 0;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		parametersTable[i] = new QTableWidget();



	//measurementsBox
		if (types[i].size() > 0)
		{
			//parametersTable[0] = new QTableWidget();
			parametersTable[i]->setStyleSheet("QHeaderView::section { background-color:rgb(236, 236, 236); }");
			parametersTable[i]->setMinimumWidth(420);


			height = (types[i][0]->parameterCounter + 1) * 25;
			parametersTable[i]->setMinimumHeight(height + 25);
			//parametersTable[i]->setMaximumHeight(height + 25);
			parametersTable[i]->setMinimumWidth(420);
		

			parametersTable[i]->setEditTriggers(QAbstractItemView::NoEditTriggers); //diable edit
			parametersTable[i]->setSelectionMode(QAbstractItemView::NoSelection); //diable edit
			QStringList celice;
			if(i > 0)
			parametersTable[i]->setColumnCount(5);
			if(i == 0)
				parametersTable[i]->setColumnCount(4);
		
			if (slovensko) celice << "Parameter" << "Izmerjeno" << "Min" << "Max" <<"Proc. slab.";
			else celice << "Parameter" << "Measured" << "Min" << "Max" << "Bad proc.";

			if (i == 0) celice << "Parameter" << "Izmerjeno" << "Min" << "Max";
			else
				celice << "Parameter" << "Izmerjeno" << "Min" << "Max" << "Proc. slab.";
			parametersTable[i]->setHorizontalHeaderLabels(celice);
			parametersTable[i]->setColumnWidth(0, 150);
			parametersTable[i]->setColumnWidth(1, 76);
			parametersTable[i]->setColumnWidth(2, 76);
			parametersTable[i]->setColumnWidth(3, 76);
			if(i > 0)
			parametersTable[i]->setColumnWidth(4, 76);

			parametersTable[i]->setRowCount(types[i][0]->parameterCounter);
			parametersTable[i]->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
		}
	if (i == 0)
	{
		ui.frameMeasurements->setMinimumHeight(height + 60); //25 za naslov nad meritvami
		ui.frameMeasurements->setMaximumHeight(height + 60); //25 za naslov nad meritvami
		ui.layoutMeasurements->addWidget(parametersTable[0], Qt::AlignTop);
	
	}
	else if (i == 1)
	{
		ui.frameMeasurements_2->setMinimumHeight(height + 100); //25 za naslov nad meritvami
		ui.frameMeasurements_2->setMaximumHeight(height + 100); //25 za naslov nad meritvami
		ui.layoutMeasurements_2->addWidget(parametersTable[1], Qt::AlignTop);
	}
	else if (i == 2)
	{
		ui.frameMeasurements_3->setMinimumHeight(height + 100); //25 za naslov nad meritvami
		ui.frameMeasurements_3->setMaximumHeight(height + 100); //25 za naslov nad meritvami
		ui.layoutMeasurements_3->addWidget(parametersTable[2], Qt::AlignTop);
	}

	}

}
void MBsoftware::CreateChart()
{
	for (int i = 0; i < 1; i++)//2 postaji
	{
		set0[i] = new QBarSet("");
		setBad[i] = new QBarSet("");
		series0[i] = new QStackedBarSeries();
		series0[i]->append(set0[i]);
		series0[i]->append(setBad[i]);
		
		chart[i] = new QChart();
		chart[i]->addSeries(series0[i]);
		chart[i]->setTitle(QString("Zadnjih %1 meritev : Mere: %2").arg(CHART_DATA_SIZE).arg(types[chartStation][currentType]->name[chartParam]));
		chart[i]->setAnimationOptions(QChart::NoAnimation);

		axisX[i] = new QValueAxis();
		axisX[i]->setRange(0, CHART_DATA_SIZE);
		chart[i]->addAxis(axisX[i], Qt::AlignBottom);
		series0[i]->attachAxis(axisX[i]);

		axisY[i] = new QValueAxis();
		chart[i]->addAxis(axisY[i], Qt::AlignLeft);
		series0[i]->attachAxis(axisY[i]);

		chartView[i] = new QChartView();
		chartView[i]->setChart(chart[i]);
		chartView[i]->setRenderHint(QPainter::Antialiasing);

		if (i == 0)
			ui.chartLayout->addWidget(chartView[i]);
		//else
		//	ui.chartLayout_3->addWidget(chartView[i]);
		//chart[i]->legend()->setVisible(false);

	}

}



void MBsoftware::AddLog(QString text, int type)
{
	logText.push_back(QString("%1:%2:%3 - %4").arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(text));
	logIndex.push_back(type);
	WriteLogToFile(logPath, text, type);
	EraseLastLOG();
}


void MBsoftware::EraseLastLOG()
{
	if (logIndex.size() > 10)
	{
		logIndex.erase(logIndex.begin());
		logText.erase(logText.begin());
	}
}

void MBsoftware::OnClickedResetBox(int box)
{
	QMessageBox msgBox;
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	QString tmp;
	if (box == 0)
		tmp = "Zelite ponastaviti skatlo dobri kosi 1";
	else if(box == 1)
		tmp = "Zelite ponastaviti skatlo dobri kosi 2";
	else if (box == 2)
		tmp = "Zelite ponastaviti skatlo slabi kosi 1";
	else if (box == 3)
		tmp = "Zelite ponastaviti skatlo slabi kosi 2";
	else if (box == 4)
		tmp = "Zelite ponastaviti skatlo slabi kosi dimenzija";

	msgBox.setIcon(QMessageBox::Warning);
	msgBox.setText(tmp);
	msgBox.setWindowTitle("Opozorilo");
	if (msgBox.exec() == QMessageBox::Ok)
	{
		if (box == 0)
			dobriBox1Counter = 0;
		else if (box == 1)
			goodBox2Counter = 0;
		else if (box == 2)
			badBox1Counter = 0;
		else if (box == 3)
			badBox2Counter = 0;
		else if (box == 4)
			badBoxDimCounter = 0;
	}



}

void MBsoftware::OnClickedKoracnoDialog()
{
	int bla = 100;
	QMessageBox::StandardButton reply;
	Qt::WindowFlags flags = windowFlags();
	koracnoDialog->setWindowFlags((flags | Qt::WindowStaysOnTopHint));

	if (uZed[0]->input[0][6].value == 1)  //tipe lahko dodaja administrator ali operater, worker pa ne
	{
		koracnoDialog->show();
		koracnoDialog->activateWindow();
	}
	else
	{
		QString niz;

		niz = QString("Za testiranje modulov morate biti v koracnem NACINU");


		reply = QMessageBox::warning(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Ok);
	}

}

void MBsoftware::OnClickedTriggOrient()
{
	orientationCameraTrigger = true;
}

void MBsoftware::OnClickedTriggMeasure()
{
	measureStationTrigger = true;
}

void MBsoftware::OnClickedTriggStolp1()
{
	startStolp1 = true;
}

void MBsoftware::OnClickedTriggStolp2()
{
	startStolp2 = true;
}


void MBsoftware::ReadTypes()
{
	QString filePath;
	QStringList values;
	int count = 0;
	QString typePath;
	QString typeName;
	QDir dir;
	filePath = QDir::currentPath() + QString("/%1/types").arg(REF_FOLDER_NAME);

	if (!QDir(filePath).exists())
		QDir().mkdir(filePath);

	for (int i = 0; i < NR_STATIONS; i++)
	{
		filePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(filePath, QSettings::IniFormat);

		if (!QDir(filePath).exists())
			QDir().mkdir(filePath);

		dir.setPath(filePath);
		dir.setNameFilters(QStringList() << "*.ini");
		count = dir.count();
		QStringList list = dir.entryList();
		//inputs
		for (int j = 0; j < count; j++)
		{
			typePath = filePath;
			typeName = list.at(j);
			typeName.chop(4);
			types[i].push_back(new Types());

			//	cam.push_back(new ImagingSource());
			types[i].back()->stationNumber = i;
			types[i].back()->Init(typeName, typePath);

		}
	}


}

void MBsoftware::ShowErrorMessage()
{
	for (int i = 0; i < logIndex.size(); i++)
	{
		if (logIndex[i] == 0) //status ok - zelena
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(150, 255, 150));
		}
		else if (logIndex[i] == 1) //napaka na napravi - rdeca
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(255, 150, 150));
		}
		else if (logIndex[i] == 2) //warning
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(Qt::yellow));
		}
		else //info - modra
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(100, 150, 255));
		}
	}
	if (logIndex.size() > 6)
	{
		//ui.listWidgetStatus->scrollToBottom();
	}
}

void MBsoftware::ClearErrorMessage(int index)
{
	ui.listWidgetStatus->item(index)->setBackgroundColor(QColor(236, 236, 236));
	ui.listWidgetStatus->item(index)->setText("");
	//log[0]->ClearWindowMessage(index);
}

void MBsoftware::AppendClearErrorMessageIndex(int index)
{
	int indexExists = 0;
	for (int i = 0; i < clearLogIndex.size(); i++)
	{
		if (clearLogIndex[i] == index)
		{
			indexExists = 1;
			break;
		}
	}
	if (indexExists == 0)
		clearLogIndex.push_back(index);
}


void MBsoftware::DrawMeasurements()
{



	 QColor bcolor;

	 //v prihodnosti dodaj se za vec postaj
	
	 QString tip;

	 if (slovensko)
	 {
		 tip = QString("TIP");
	 }
	 else
	 {
		 tip = QString("TYPE");
	 }


	// ui.labelCurrentTypeP0->setText(QString("%1: %2").arg(tip).arg(types[0][currentType]->typeName));
	// ui.labelCurrentTypeP1->setText(QString("%1: %2").arg(tip).arg(types[1][currentType]->typeName));
	// ui.labelCurrentTypeP2->setText(QString("%1: %2").arg(tip).arg(types[2][currentType]->typeName));

	 float badProc;
		 for (int i = 0; i < NR_STATIONS; i++)
		 {

			 if (types[i].size() > 0)
			 {
				 parametersTable[i]->setRowCount(types[i][currentType]->parameterCounter);
				 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
				 {
					
					 //ce se je dodal nov parameter, dodamo novo vrstico
					 parametersTable[i]->setRowHeight(i, 12);
					 parametersTable[i]->resizeRowsToContents();
					 for (int column = 0; column < parametersTable[i]->columnCount(); column++)
					 {
						 QTableWidgetItem *newItem;
						 switch (column)
						 {
						 case 0:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->name[row]));
							 break;
						 case 1:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->measuredValue[row]));
							 break;
						 case 2:
							 if(types[i][currentType]->nominal[row] == 0)
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceLow[row]));
							 else
							newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceLow[row] + types[i][currentType]->nominal[row]));
							 break;

						 case 3:
							 if (types[i][currentType]->nominal[row] == 0)
								 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceHigh[row]));
							 else
								 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceHigh[row] + types[i][currentType]->nominal[row]));
							 break;

						 case 4:
							 if (types[i][currentType]->totalMeasurementsCounter == 0)
							 {
								 badProc = 0;
							 }
							 else
							 {
								 badProc = (float)types[i][currentType]->badCounterByParameter[row] / types[i][currentType]->totalMeasurementsCounter;
								 badProc *= 1000;
								 badProc = roundf(badProc);
								 badProc /= 10;
							 }

							 if(i > 0)
							 newItem = new QTableWidgetItem(tr("%1 % (%2)").arg(badProc).arg(types[i][currentType]->badCounterByParameter[row]));
							 break;

						 default:
							 break;
						 }
						 
						 parametersTable[i]->setItem(row, column, newItem);
					 }

				 }

				 //obarva vrstice dobre slabe
				 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
				 {
					 types[i][currentType]->IsGood(row);
					 if (types[i][currentType]->isActive[row])
					 {
						 //measurements[i][vars->selectedType]->IsGood(row);

						 if (types[i][currentType]->isGood[row] == 1)
						 {
							 bcolor = QColor(150, 255, 150);
						 }
						 else if (types[i][currentType]->isGood[row] == 0)
							 bcolor = QColor(255, 150, 150);
						 else
							 bcolor = QColor(0, 170, 255);
					 }
					 else
					 {
						 bcolor = QColor(236, 236, 236);
					 }

					 parametersTable[i]->item(row, 0)->setBackground(bcolor);
					 parametersTable[i]->item(row, 1)->setBackground(bcolor);
					 parametersTable[i]->item(row, 2)->setBackground(bcolor);
					 parametersTable[i]->item(row, 3)->setBackground(bcolor);
					 //measurementsResoults->item(row, 1)->setText(QString("%1").arg(types[0][vars->selectedType]->measuredValue[row]));
				 }
			 }
		 }

}




void MBsoftware::PopulateStatusBox()
{
	int index = 0;

	index = 0;
	statusBox[0]->AddItem(index, QString("MM timer:  %1 Hz").arg(MMTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer:  %1 Hz").arg(viewTimerTimer.frequency));
	index++;

	for (int i = 0; i < cam.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("cam %1 FPS:  %2").arg(i).arg(cam[i]->frameReadyFreq.frequency));
		index++;
	}
	for (int i = 0; i < imageProcess->processingTimer.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("Obdelave%1 : %2 [ms]").arg(i).arg(imageProcess->processingTimer[i]->elapsed));
		index++;
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("GlavniStevec%1 : %2 [ms]").arg(i).arg(TestFun[i]->elapsed));
		index++;
	}
	statusBox[0]->AddItem(index, QString("processStatus: %1").arg(processStatus));
	index++;
	statusBox[0]->AddItem(index, QString("cylinderStatus: %1").arg(cylnderStatus));
	index++;
	statusBox[0]->AddItem(index, QString("orientationCameraStatus: %1").arg(orientationCameraStatus));
	index++;
	statusBox[0]->AddItem(index, QString("MeasureStationStatus: %1").arg(measureStationStatus));
	index++;
	statusBox[0]->AddItem(index, QString("Stolp1 status: %1").arg(stolp1Status));
	index++;
	statusBox[0]->AddItem(index, QString("Stolp2 status: %1").arg(stolp2Status));
	index++;
	statusBox[0]->AddItem(index, QString("REFERENCIRANJE STATUS: %1").arg(statusReferenciranje));
	index++;
	statusBox[0]->AddItem(index, QString("slabiOrientacija: %1").arg(slabiOrientacija));
	index++;
	statusBox[0]->AddItem(index, QString("slabiDimenzija: %1").arg(slabiDimenzija));
	index++;
	statusBox[0]->AddItem(index, QString("slabiS1Zap: %1").arg(slabiS1Zap));
	index++;
	statusBox[0]->AddItem(index, QString("slabiS2Zap: %1").arg(slabiS2Zap));
	index++;


	//test.toUInt(), 16statusReferenciranje
}

void MBsoftware::GetError()
{
	QString text;
	int index = 0;
	for (int i = 0; i < cam.size(); i++)
	{
		if ((cam[i]->isLive == 1) && (cam[i]->isLiveOld == false))
		{
			

			if (slovensko) text = QString("Kamera %1 POVEZANA").arg(i);
			else text = QString("Camera%1 ONLINE").arg(i);
			AddLog(text, 0);
			startErrorSet[index] = 0;
		}
		if ((cam[i]->isLive == 0) && (startErrorSet[index] == 0))
		{
			if (slovensko) text = QString("Kamera %1 NI povezana").arg(i);
			else text = QString("Camera %1 OFFLINE").arg(i);
			AddLog(text, 1);
			startErrorSet[index] = 1;
		}

		cam[i]->isLiveOld = cam[i]->isLive;
		index++;
	}

	if ((uZed[0]->isConnected == 1) && (uZed[0]->isConnectedOld == 0))
	{
		if (slovensko) text = QString("Xilinx POVEZANA");
		else text = QString("Xilinx  ONLINE");

		AddLog(text, 0);
		startErrorSet[index] = 0;
	}
	if ((uZed[0]->isConnected == 0) && (startErrorSet[index] == 0))
	{
		if (slovensko) text = QString("Xilinx NI povezana");
		else text = QString("Xilinx OFFLINE");
		AddLog(text, 1);
		startErrorSet[index] = 1;
	}
	uZed[0]->isConnectedOld = uZed[0]->isConnected;
	index++;


	if ((pc2Connected == 1) && (pc2ConnectedOld == 0))
	{
		text = QString("PC2 POVEZAN!");
		

		AddLog(text, 0);
		startErrorSet[index] = 0;
	}
	if ((pc2Connected == 0) && (startErrorSet[index] == 0))
	{
		if (slovensko) text = QString("PC2 NI POVEZAN!");
		
		AddLog(text, 1);
		startErrorSet[index]  = 1;
	}
	pc2ConnectedOld = pc2Connected;
	index++;

	if ((epsonConnected == 1) && (eposnConnectedOld == 0))
	{
		if (slovensko) text = QString("ROBOT POVEZAN!");
		

		AddLog(text, 0);
		startErrorSet[index] = 0;
	}
	if ((epsonConnected == 0) && (startErrorSet[index] == 0))
	{
		if (slovensko) text = QString("ROBOT NI POVEZAN!");
	
		AddLog(text, 1);
		startErrorSet[index] = 1;
	}
	eposnConnectedOld = epsonConnected;
	index++;


}

void MBsoftware::WriteBoxCounter()
{
QString fileName;
QString line;
QString dirPath;
QDir dir;


dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

if (!QDir(dirPath).exists())
QDir().mkdir(dirPath);


dir.setPath(dirPath);

fileName = dirPath + "boxCounter.txt";
QFile file(fileName);
if (file.open(QIODevice::WriteOnly | QIODevice::Text))
{
	QTextStream stream(&file);

	stream << dobriBox1Counter;
	stream << endl;
	stream << goodBox2Counter;
	stream << endl;
	stream << badBox1Counter;
	stream << endl;
	stream << badBox2Counter;
	stream << endl;
	stream << badBoxDimCounter;
	stream << endl;
	stream << stolp1DobriCounter;
	stream << endl;
	stream << stolp1SlabiCounter;
	stream << endl;
	stream << stolp2DobriCounter;
	stream << endl;
	stream << stolp2SlabiCounter;
	stream << endl;



	file.close();
}
}
void MBsoftware::ReadBoxCounter()

{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "boxCounter.txt";
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);

		line = stream.readLine();
		dobriBox1Counter = line.toInt();
		line = stream.readLine();
		goodBox2Counter = line.toInt();
		line = stream.readLine();
		badBox1Counter = line.toInt();
		line = stream.readLine();
		badBox2Counter = line.toInt();
		line = stream.readLine();
		badBoxDimCounter = line.toInt();
		line = stream.readLine();
		stolp1DobriCounter = line.toInt();
		line = stream.readLine();
		stolp1SlabiCounter = line.toInt();
		line = stream.readLine();
		stolp2DobriCounter = line.toInt();
		line = stream.readLine();
		stolp2SlabiCounter = line.toInt();
		file.close();
	}
	else
	{
		chartStation = 0;

		chartParam = 0;

	}

	if (currentType >= types[0].size())
		currentType = 0;
	//prevType = currentType;
}

void MBsoftware::NastaviJezik()
{
	if (slovensko)
	{
		ui.labelDeviceStatus->setText("STANJE NAPRAVE");
		ui.labelStation0counters->setText("STEVCI POSTAJE 2");
		ui.labelStation0counters_1->setText("STEVCI POSTAJE 1");
		ui.labelStation0good_1->setText("DOBRI");
		ui.labelStation0bad_1->setText("SLABI");
		//ui.labelStation0total_1->setText("VSI");



		ui.labelStation0good->setText("DOBRI:");
		ui.labelStation0bad->setText("SLABI:");
		//ui.labelStation0total->setText("VSI:");

		//ui.labelStation0->setText("Postaja 0:");
		//ui.labelStation1->setText("Postaja 1:");
		//ui.labelStation2->setText("Postaja 2:");

		ui.buttonLogin->setText("Prijava");
		ui.buttonGeneralSettings->setText("Nastavitve");
		//ui.buttonResetGoodBox->setText("Ponastavi stevec kosov v skatli");
		ui.buttonSetTolerance->setText("Tolerance");
		ui.buttonTypeSelect->setText("Izberi tip");
		ui.buttonAddType->setText("Dodaj tip");
		ui.buttonTypeSettings->setText("Nastavitve tipa");
		ui.buttonRemoveType->setText("Odstrani tip");
		ui.buttonResetCounters->setText("Ponastavi stevce");

		ui.buttonImageProcessing->setText("Obdelava slik");

		ui.buttonHistory->setText("Zgodovina");
		ui.buttonStatusBox->setText("Podatki");
		ui.buttonAboutUs->setText("Kontakt");

		ui.MainMenuBar->setTabText(0, "Prijava");
		ui.MainMenuBar->setTabText(1, "Nastavitve");
		ui.MainMenuBar->setTabText(2, "Tipi");
		ui.MainMenuBar->setTabText(3, "Kamere");
		ui.MainMenuBar->setTabText(4, "Signali");
		ui.MainMenuBar->setTabText(5, "Obdelava slik");
		ui.MainMenuBar->setTabText(6, "Podatki in zgodovina");
	}

	//Angleških imen ni treba posebej nastavljati, ker so že nastavljena v ui
}



void MBsoftware::DrawImageOnScreen()
{
	if (!isSceneAdded)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			MBsoftware::showScene[i] = new QGraphicsScene(this);
		}
		ui.imageViewMainWindow_0->setScene(MBsoftware::showScene[0]);
		ui.imageViewMainWindow_1->setScene(MBsoftware::showScene[1]);
		ui.imageViewMainWindow_2->setScene(MBsoftware::showScene[2]);
	

		isSceneAdded = true;
	}
	for (int i = 0; i < NR_STATIONS; i++)
	{
		qDeleteAll(showScene[i]->items());
	}

	ui.imageViewMainWindow_0->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageViewMainWindow_1->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageViewMainWindow_2->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	

}

void MBsoftware::UpdateImageView(int station)
{

		qDeleteAll(showScene[station]->items());
		
			//QImage qimgOriginal((uchar*)imageProcess->displayLamela[station][index].data, imageProcess->displayLamela[station][index].cols, imageProcess->displayLamela[station][index].rows, imageProcess->displayLamela[station][index].step, QImage::Format_Grayscale8);

			//showScene[station]->addPixmap(QPixmap::fromImage(qimgOriginal));
		//QImage qimgOriginal((uchar*)imageProcess->drawImageMain[station].data,imageProcess->drawImageMain[station].cols, imageProcess->drawImageMain[station].rows, imageProcess->drawImageMain[station].step, QImage::Format_RGB888);
	
		imageProcess->mainScreenDrawFinal[station].DrawAll(showScene[station]);
		if (station == 0)
		{
			showScene[station]->addItem(imageProcess->mainScreenDrawFinal[0].DrawImage(0));
			//ui.imageViewMainWindow_0->fitInView(QRectF(0, 0, imageProcess->drawImageMain[station].cols, imageProcess->drawImageMain[station].rows), Qt::KeepAspectRatio);
			ui.imageViewMainWindow_0->resetTransform();
			ui.imageViewMainWindow_0->fitInView(showScene[station]->sceneRect(), Qt::KeepAspectRatio);
			ui.imageViewMainWindow_0->setSceneRect(ui.imageViewMainWindow_0->rect());
		}
		else if (station == 1)
		{
		showScene[station]->addItem(imageProcess->mainScreenDrawFinal[1].DrawImage(0));
		ui.imageViewMainWindow_1->fitInView(QRectF(0, 0, imageProcess->drawImageMain[station].cols, imageProcess->drawImageMain[station].rows), Qt::KeepAspectRatio);
		ui.imageViewMainWindow_1->setSceneRect(ui.imageViewMainWindow_1->rect());
		}
		else if (station == 2)
		{
			showScene[station]->addItem(imageProcess->mainScreenDrawFinal[2].DrawImage(0));
			ui.imageViewMainWindow_2->fitInView(QRectF(0, 0, imageProcess->drawImageMain[station].cols, imageProcess->drawImageMain[station].rows), Qt::KeepAspectRatio);
			//ui.imageViewMainWindow_2->setAlignment(Qt::AlignTop | Qt::AlignLeft);
			ui.imageViewMainWindow_2->setSceneRect(ui.imageViewMainWindow_2->rect());

			//ui.imageViewMainWindow_0->fitInView(showScene[station]->sceneRect(), Qt::KeepAspectRatio);
		}
		//showScene[station]->addItem(imageProcess->mainScreenDraw[0].DrawImage(0));

		

}

void MBsoftware::UpdateChart(int station, int param, int counter)
{
	//param = 2;

	chart[0]->setTitle(QString("POSTAJA %1, MERA: %2").arg(station).arg(types[station][currentType]->name[param]));

	if (types[station][currentType]->nominal[param] == 0)
	{
		float minValue = (types[station][currentType]->toleranceLow[param])/100 *2;
		if(minValue < 0)
			minValueChart[station] = minValue + types[station][currentType]->toleranceLow[param];
		else
			minValueChart[station] = minValue - types[station][currentType]->toleranceLow[param];
		float maxValue = (types[station][currentType]->toleranceHigh[param]) / 100 * 2;
		maxValueChart[station] = maxValue + types[station][currentType]->toleranceHigh[param];
	}
	else
	{
		float minValue = (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceLow[param]) / 100 * 2;
		if (minValue < 0)
			minValueChart[station] = (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceLow[param]) + minValue;
		else
		minValueChart[station] =  (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceLow[param])- minValue;

		float maxValue = (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceHigh[param]) / 100 *2;
		maxValueChart[station] = maxValue + (types[station][currentType]->nominal[param] + types[station][currentType]->toleranceHigh[param]);
	}


	axisY[0]->setRange(minValueChart[station], maxValueChart[station]);
	//chart->addAxis(axisY, Qt::AlignLeft);
	//series0[station]->attachAxis(axisY[station]);
	series0[0]->setBarWidth(1);

	QColor color("green");
	set0[0]->setColor(color);
	QColor color1("red");
	setBad[0]->setColor(color1);

	set0[0]->remove(0, set0[0]->count());

	setBad[0]->remove(0, setBad[0]->count());

	int stop = counter - 1;
	if (stop < 0)
		stop = CHART_DATA_SIZE - 1;
	int count = 0;
	count = counter;
	for (int i = 0; i < CHART_DATA_SIZE; i++)
	{
		if (dataArray[station][param][count] > 0)
		{
			if ((dataArray[station][param][count] < types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][count] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
			{
				set0[0]->append(dataArray[station][param][count]);
				setBad[0]->append(0);
			}
			else
			{
				setBad[0]->append(dataArray[station][param][count]);
				set0[0]->append(0);
			}
		}
		count++;
		if (count >= CHART_DATA_SIZE)
			count = 0;
	}

	chart[0]->axisX()->setRange(0, CHART_DATA_SIZE);
	chart[0]->axisY()->setRange(minValueChart[station], maxValueChart[station]);


	

}

void MBsoftware::UpdateChartData(int station, int param, int currCounter)
{
	int stop = currCounter - 1;
	if (stop == -1)
		stop = CHART_DATA_SIZE - 1;



	if (set0[0]->count() > CHART_DATA_SIZE)//shifta podatke 
	{
		set0[0]->remove(0, 1);
		setBad[0]->remove(0, 1);
		//for (int i = 0; i < CHART_DATA_SIZE; i++)
		//{
		if ((dataArray[station][param][stop] < types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][stop] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
		{
			set0[0]->append(dataArray[station][param][stop]);
			setBad[0]->append(0);
		}
		else
		{
			setBad[0]->append(dataArray[station][param][stop]);
			set0[0]->append(0);
		}
		//}
	}
	else
	{
		if ((dataArray[station][param][stop] < types[station][currentType]->toleranceHigh[param] + types[station][currentType]->nominal[param]) && (dataArray[station][param][stop] > types[station][currentType]->toleranceLow[param] + types[station][currentType]->nominal[param]))
		{
			set0[0]->append(dataArray[station][param][stop]);
			setBad[0]->append(0);
		}
		else
		{
			setBad[0]->append(dataArray[station][param][stop]);
			set0[0]->append(0);
		}
	}




	chart[0]->axisX()->setRange(0, CHART_DATA_SIZE);
	chart[0]->axisY()->setRange(minValueChart[station], maxValueChart[station]);




}

void MBsoftware::ResetChartData()
{

		for (int i = 0; i < 12; i++)  //xx   12 zamenja 11
		{
			for (int j = 0; j < CHART_DATA_SIZE; j++)
			{
				dataArray[chartStation][i][j] = 0;
			}
		}
		UpdateChart(chartStation,chartParam, charDataCounter[chartStation]);
	
}




void MBsoftware::ViewTimer()
{
	int bla = 10;

	QFont font;
	if (viewTimerTimer.timeCounter >= 10)
	{
		viewTimerTimer.SetStop();
		viewTimerTimer.CalcFrequency(viewTimerTimer.timeCounter);
		viewTimerTimer.timeCounter = 0;
		viewTimerTimer.SetStart();
	}

	viewTimerTimer.timeCounter++;
	viewCounter++;

	if (isActiveWindow())
	{
		grabKeyboard();
	}
	else
		releaseKeyboard();

	bool obnoviHistory = false;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		paramCounter[i] = types[i][currentType]->parameterCounter;

		if (paramCounter[i] != paramCounterOld[i])
		{
			obnoviHistory = true;

			paramCounterOld[i] = paramCounter[i];
		}
	}

	if (prevType != currentType)
	{
		pc2Initialized = 0;
		prevType = currentType;
		chartStation = 1;
		chartParam = 0;
		obnoviHistory = true;
		ResetChartData();
	}
	
	if (obnoviHistory == true)
	{
		obnoviHistory = false;
		SetHistory();
	}

	QString niz;
	if (viewCounter % 2 == 0)
	{
		if (login->currentRights > 0) //uporabnik prijavljen
		{

			if (slovensko) niz = QString("prijavljen");
			else  niz = QString("logged-in");

			ui.labelLogin->setText(QString("%1 %2!").arg(login->currentUser).arg(niz));
			ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}
		else
		{

			if (slovensko) niz = QString("Uporabnik odjavljen");
			else  niz = QString("User logged out");

			ui.labelLogin->setText(niz);
			ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
		}

		if ((processStatus == -5))
		{
			ui.labelStatus->setText(QString("EMG STOP!"));
			ui.labelStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}
		else if ((processStatus == -3))
		{
			ui.labelStatus->setText(QString("PRITISNITE GLAVNI START!"));
			ui.labelStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}

		else if (processStatus == 0)
		{
			ui.labelStatus->setText(QString("PRITISNITE START NAPRAVE!"));
			ui.labelStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if (processStatus == 1)
		{
			ui.labelStatus->setText(QString("NAPRAVA DELUJE!"));
			ui.labelStatus->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}
		else if (processStatus == 2)
		{
			ui.labelStatus->setText(QString("REFERENCIRANJE !"));
			ui.labelStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		//postaja status 

		//orientacija
		if ((cam[0]->imageReady[0] == 1))
		{
			ui.labelStatusP0->setText(QString("Slikam!"));
			ui.labelStatusP0->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[0]->imageReady[0] == 2))
		{
			ui.labelStatusP0->setText(QString("Obdelujem!"));
			ui.labelStatusP0->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if (cam[0]->imageReady[0] == 4)
		{
			ui.labelStatusP0->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
			ui.labelStatusP0->setText(QString("Obdelava koncana: %1 [ms] ").arg(imageProcess->processingTimer[0]->ElapsedTime()));
		}

		//postaja 2 iz strani
		if ((cam[2]->imageReady[0] == 1))
		{
			ui.labelStatusP2->setText(QString("Slikam!"));
			ui.labelStatusP2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[2]->imageReady[0] == 2))
		{
			ui.labelStatusP2->setText(QString("Obdelujem!"));
			ui.labelStatusP2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if (cam[2]->imageReady[0] == 4)
		{
			ui.labelStatusP2->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
			ui.labelStatusP2->setText(QString("Obdelava koncana: %1 [ms] ").arg(imageProcess->processingTimer[2]->ElapsedTime()));
		}

		if ((cam[1]->imageReady[0] == 1))
		{
			ui.labelStatusP1->setText(QString("Slikam 1!"));
			ui.labelStatusP1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[1]->imageReady[0] == 2))
		{
			ui.labelStatusP1->setText(QString("Obdelujem 1!"));
			ui.labelStatusP1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[1]->imageReady[1] == 1))
		{
			ui.labelStatusP1->setText(QString("Slikam 2!"));
			ui.labelStatusP1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[1]->imageReady[1] == 2))
		{
			ui.labelStatusP1->setText(QString("Obdelujem 2!"));
			ui.labelStatusP1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if ((cam[1]->imageReady[1] == 4) && (cam[1]->imageReady[0] == 4))
		{
			ui.labelStatusP1->setText(QString("Obdelava koncana: %1 [ms]!").arg(imageProcess->processingTimer[1]->ElapsedTime() + imageProcess->processingTimer[3]->ElapsedTime()));
			ui.labelStatusP1->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}



		//process status....
		ui.labelTip->setText(QString("TIP: %1 ").arg(types[0][currentType]->typeName));
		ui.labelTip->setStyleSheet(QStringLiteral("background-color:  rgb(236, 236, 236);"));

		//ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		if (uZed[0]->input[0][7].value == 1)
		{
			ui.labelVibrator->setText(QString("VRATA OK "));
			ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}
		else
		{
			ui.labelVibrator->setText(QString("VRATA ODPRTA "));
			if ((uZed[0]->input[0][6].value == 0))//koracno je o
			{
				if (viewCounter % 3 == 0)
				{
					if (vrataBlinking == 0)
					{
						ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
						vrataBlinking = 1;
					}
					else
					{
						ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
						vrataBlinking = 0;
					}
				}
			}
			else
				ui.labelVibrator->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}


		if (uZed[0]->input[0][7].value == 0)
		{
			ui.labelVrata->setText(QString("VRATA ODPRTA"));
			ui.labelVrata->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}
		else
		{
			ui.labelVrata->setText(QString("VRATA ZAPRTA"));
			ui.labelVrata->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}
		if (uZed[0]->input[0][6].value == 0)
		{
			ui.labelVrata->setText(QString("NORMALNO DELOVANJE"));
			ui.labelVrata->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
		}
		else
		{
			ui.labelVrata->setText(QString("KORACNI NACIN"));
			ui.labelVrata->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		//robot status


		if (epsonConnected == 0)
		{
			ui.labelEpson->setText(QString("ROBOT POVEZAVA NOK!"));
			ui.labelEpson->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		}
		else if (errorRobot > 0)
		{
			ui.labelEpson->setText(QString("ROBOT ERROR!"));
			ui.labelEpson->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}
		else if (epsonConnected == 1)
		{
			ui.labelEpson->setText(QString("ROBOT POVEZAVA OK!"));
			ui.labelEpson->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}

		//pc2 status
		if (pc2Connected == 1)
		{
			ui.labelPC2->setText(QString("PC2: TCP POVEZAVA OK!"));
			ui.labelPC2->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		}
		else
		{
			ui.labelPC2->setText(QString("PC2: TCP POVEZAVA NOK!"));
			ui.labelPC2->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		}
	}
	if (viewCounter % 3 == 0)
	{
		QString tmpString1;
		int cnt = 0;
		cnt = taktCounter - 1;
		if (cnt < 0)
			cnt = 0;
		tmpString1 = QString("%1").number(taktArray[cnt] / 1000, 'f', 2);
		//takt
		ui.labelTakt->setText(QString("TAKT Zadnji KOS: %1 [s]").arg(tmpString1));
		float taktAvr = 0;
		cnt = 0;
		for (int i = 0; i < 10; i++)
		{
			if (taktArray[i] > 0)
			{
				taktAvr += taktArray[i];
				cnt++;
			}
		}
		if (cnt == 0)
			taktAvr = 0;
		else
			taktAvr /= cnt;
		tmpString1 = QString("%1").number(taktAvr / 1000, 'f', 2);
		//takt
		ui.labelTakt_2->setText(QString("TAKT povp: %1 [s]").arg(tmpString1));


		int totalCounter = 0;

		float doberProcent = 0;
		float slabProcent = 0;

		QString tmpString2;
		QString tmpString3;
		//vsi stevci

		totalCounter = types[1][currentType]->goodMeasurementsCounter + types[1][currentType]->badMeasurementsCounter;

		if (totalCounter > 0)
		{
			doberProcent = (float)types[1][currentType]->goodMeasurementsCounter / totalCounter * 100;
			tmpString1 = QString("%1").number(doberProcent, 'f', 2);
			slabProcent = (float)types[1][currentType]->badMeasurementsCounter / totalCounter * 100;
			tmpString2 = QString("%1").number(slabProcent, 'f', 2);
		}
		else
		{
			doberProcent = 0;
			tmpString1 = "0";
			tmpString2 = "0";
			slabProcent = 0;
		}
		ui.labelGood_1->setText(QString("%1").arg(types[1][currentType]->goodMeasurementsCounter));
		ui.labelBAD_1->setText(QString("%1").arg(types[1][currentType]->badMeasurementsCounter));
		ui.labelGoodProcent_1->setText(QString("%1 %").arg(tmpString1));
		ui.labelBadProcent_1->setText(QString("%1 %").arg(tmpString2));


		totalCounter = types[2][currentType]->goodMeasurementsCounter + types[2][currentType]->badMeasurementsCounter;
		if (totalCounter > 0)
		{
			doberProcent = (float)types[2][currentType]->goodMeasurementsCounter / totalCounter * 100;
			tmpString1 = QString("%1").number(doberProcent, 'f', 2);
			slabProcent = (float)types[2][currentType]->badMeasurementsCounter / totalCounter * 100;
			tmpString2 = QString("%1").number(slabProcent, 'f', 2);
		}
		else
		{
			doberProcent = 0;
			slabProcent = 0;

			tmpString1 = "0";
			tmpString2 = "0";
		}
		ui.labelGood_2->setText(QString("%1").arg(types[2][currentType]->goodMeasurementsCounter));
		ui.labelBAD_2->setText(QString("%1").arg(types[2][currentType]->badMeasurementsCounter));
		ui.labelGoodProcent_2->setText(QString("%1 %").arg(tmpString1));
		ui.labelBadProcent_2->setText(QString("%1 %").arg(tmpString2));

		totalCounter = stolp1DobriCounter + stolp1SlabiCounter;
		if (totalCounter > 0)
		{
			doberProcent = (float)stolp1DobriCounter / totalCounter * 100;
			tmpString1 = QString("%1").number(doberProcent, 'f', 2);
			slabProcent = (float)stolp1SlabiCounter / totalCounter * 100;
			tmpString2 = QString("%1").number(slabProcent, 'f', 2);
		}
		else
		{
			doberProcent = 0;
			slabProcent = 0;
			tmpString1 = "0";
			tmpString2 = "0";
		}

		ui.labelGoodN0->setText(QString("%1").arg(stolp1DobriCounter));
		ui.labelBADN0->setText(QString("%1").arg(stolp1SlabiCounter));
		ui.labelGoodProcentN0->setText(QString("%1 %").arg(tmpString1));
		ui.labelBadProcentN0->setText(QString("%1 %").arg(tmpString2));

		//ui.labelTotalN0->setText(QString("%1").arg(totalCounter));

		totalCounter = stolp2DobriCounter + stolp2SlabiCounter;
		if (totalCounter > 0)
		{
			doberProcent = (float)stolp2DobriCounter / totalCounter * 100;
			slabProcent = (float)stolp2SlabiCounter / totalCounter * 100;
			tmpString1 = QString("%1").number(doberProcent, 'f', 2);
			tmpString2 = QString("%1").number(slabProcent, 'f', 2);
		}
		else
		{
			tmpString1 = "0";
			tmpString2 = "0";
			doberProcent = 0;
			slabProcent = 0;
		}

		ui.labelGoodN0_2->setText(QString("%1").arg(stolp2DobriCounter));
		ui.labelBADN0_2->setText(QString("%1").arg(stolp2SlabiCounter));
		ui.labelGoodProcentN0_2->setText(QString("%1 %").arg(tmpString1));
		ui.labelBadProcentN0_2->setText(QString("%1 %").arg(tmpString2));

		//ui.labelTotalN0_2->setText(QString("%1").arg(totalCounter));

		totalCounter = 0;
		//stevci VSEH KOSOV
		float slabiKamereProc;
		float slabiNavojProc;
		totalCounter = types[0][currentType]->totalGlobalMeasurementsCounter;
		if (totalCounter > 0)
		{
			doberProcent = (float)(stolp1DobriCounter + stolp2DobriCounter) / totalCounter * 100;
			slabiKamereProc = (float)(types[1][currentType]->totalGlobalMeasurementsCounter) / totalCounter * 100;
			slabiNavojProc = (float)(stolp1SlabiCounter + stolp2SlabiCounter) / totalCounter * 100;
			tmpString1 = QString("%1").number(doberProcent, 'f', 2);
			tmpString2 = QString("%1").number(slabiKamereProc, 'f', 2);
			tmpString3 = QString("%1").number(slabiNavojProc, 'f', 2);
		}
		else
		{
			doberProcent = 0;
			slabiKamereProc = 0;
			slabiNavojProc = 0;
			tmpString1 = "0";
			tmpString2 = "0";
			tmpString3 = "0";
		}

		ui.labelVsi->setText(QString("%1").arg(totalCounter));
		ui.labelDobri->setText(QString("%1").arg(stolp1DobriCounter + stolp2DobriCounter));
		ui.labelDobriProc->setText(QString("%1 %").arg(tmpString1));
		ui.labelSlabiKam->setText(QString("%1").arg(types[1][currentType]->totalGlobalMeasurementsCounter));
		ui.labelSlabiKamProc->setText(QString("%1 %").arg(tmpString2));
		ui.labelSlabiNavoj->setText(QString("%1").arg(stolp1SlabiCounter + stolp2SlabiCounter));
		ui.labelSlabiNavojProc->setText(QString("%1 %").arg(tmpString3));

		//status stolpov
		if ((stolp1StatusPrikaz == 0))
		{
			ui.labelStatusN0->setText(QString("%1akam KOS!").arg(QChar(0x010C)));
			ui.labelStatusN0->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}
		else if ((stolp1StatusPrikaz == 1))
		{
			ui.labelStatusN0->setText(QString("Obdelujem!"));
			ui.labelStatusN0->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}
		else if ((stolp1StatusPrikaz == 2))
		{
			tmpString1 = QString("%1").number(TestFun[1]->ElapsedTime() / 1000, 'f', 2);
			//ui.labelStatusN0->setText(QString("Obdelava koncana: %1 [s]!").arg(TestFun[1]->ElapsedTime()/1000));
			ui.labelStatusN0->setText(QString("Obdelava koncana: %1 [s]!").arg(tmpString1));
			ui.labelStatusN0->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}
		if ((stolp2StatusPrikaz == 0))
		{
			ui.labelStatusN1->setText(QString("Cakam KOS!"));
			ui.labelStatusN1->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}
		else if ((stolp2StatusPrikaz == 1))
		{
			ui.labelStatusN1->setText(QString("Obdelujem!"));
			ui.labelStatusN1->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}
		else if ((stolp2StatusPrikaz == 2))
		{
			tmpString1 = QString("%1").number(TestFun[2]->ElapsedTime() / 1000, 'f', 2);
			ui.labelStatusN1->setText(QString("Obdelava koncana: %1 [s]!").arg(tmpString1));
			ui.labelStatusN1->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
		}


		//skatele status:
		ui.labelGoodBox1->setText(QString("%1 / %2").arg(dobriBox1Counter).arg(types[0][currentType]->setting[1][0]));
		ui.labelGoodBox2->setText(QString("%1 / %2").arg(goodBox2Counter).arg(types[0][currentType]->setting[1][2]));
		ui.labelBadBox1->setText(QString("%1 / %2").arg(badBox1Counter).arg(types[0][currentType]->setting[1][1]));
		ui.labelBadBox2->setText(QString("%1 / %2").arg(badBox2Counter).arg(types[0][currentType]->setting[1][3]));
		ui.labelBadBox3->setText(QString("%1 / %2").arg(badBoxDimCounter).arg(types[0][currentType]->setting[1][4]));

		if (dobriBox1Counter >= types[0][currentType]->setting[1][0])
			ui.labelGoodBox1->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		else if (dobriBox1Counter >= types[0][currentType]->setting[1][0] * 0.8)
			ui.labelGoodBox1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		else
			ui.labelGoodBox1->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));

		if (goodBox2Counter >= types[0][currentType]->setting[1][2])
			ui.labelGoodBox2->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		else if (goodBox2Counter >= types[0][currentType]->setting[1][2] * 0.8)
			ui.labelGoodBox2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		else
			ui.labelGoodBox2->setStyleSheet(QStringLiteral("background-color: rgb(200, 200, 200);"));

		if (badBox1Counter >= types[0][currentType]->setting[1][1])
			ui.labelBadBox1->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		else if (badBox1Counter >= types[0][currentType]->setting[1][1] * 0.8)
			ui.labelBadBox1->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		else
			ui.labelBadBox1->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));

		if (badBox2Counter >= types[0][currentType]->setting[1][3])
			ui.labelBadBox2->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		else if (badBox2Counter >= types[0][currentType]->setting[1][3] * 0.8)
			ui.labelBadBox2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		else
			ui.labelBadBox2->setStyleSheet(QStringLiteral("background-color: rgb(200, 200, 200);"));

		if (badBoxDimCounter >= types[0][currentType]->setting[1][4])
			ui.labelBadBox3->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		else if (badBoxDimCounter >= types[0][currentType]->setting[1][4] * 0.8)
			ui.labelBadBox3->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
		else
			ui.labelBadBox3->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}

		if (viewCounter % 5 == 0)
		{
			GetError();
			ShowErrorMessage();
		}

	if (viewCounter % 2 == 0)
	{
		PopulateStatusBox();
		DrawMeasurements();
	}


	for (int i = 0; i < NR_STATIONS; i++)
	{
		if (updateImage[i] == 1)
		{
			updateImage[i] = 0;
			UpdateImageView(i);
			if (i == chartStation)
			{
				if (currChartUpdated == 0)
				{
					UpdateChartData(chartStation, chartParam, charDataCounter[i]);//dopolnim edni graf
					currChartUpdated = 1;
				}
			}
		}
	}

	//vpis stevcev na disk //takt
	if ((dobriBox1Counter != dobriBox1CounterOld) || (goodBox2Counter != goodBox2CounterOld) || (badBox1Counter != badBox1CounterOld) || (badBox2Counter != badBox2CounterOld) || (badBoxDimCounter != badBoxDimCounterOld))
	{
		WriteBoxCounter();
		if ((dobriBox1Counter != dobriBox1CounterOld) || (goodBox2Counter != goodBox2CounterOld))
		{
			TestFun[4]->SetStop();
			TestFun[4]->ElapsedTime();
			TestFun[4]->SetStart();
		}
		dobriBox1CounterOld = dobriBox1Counter;
		goodBox2CounterOld = goodBox2Counter;
		badBox1CounterOld = badBox1Counter;
		badBox2CounterOld = badBox2Counter;
		badBoxDimCounterOld = badBoxDimCounter;
	}

	//chart
	for (int i = 0; i < NR_STATIONS; i++)
	{
		if (types[i][currentType]->parametersChanged == true)
		{
			if(i == chartStation)
				UpdateChart(chartStation, chartParam, charDataCounter[chartStation]);
			types[i][currentType]->parametersChanged = false;
		}
	}
		if ((chartStation != chartStationOld) || (chartParam != chartParamOld))
		{
			UpdateChart(chartStation, chartParam, charDataCounter[chartStation]);
		}
		chartStationOld = chartStation;
		chartParamOld = chartParam;

}


void MBsoftware::OnClickedShowLptButton(int lptNum)
{
	lpt[lptNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowUZedButton(int cardNum)
{
	uZed[cardNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowCamButton(int camNum)
{
	cam[camNum]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowSmartCardButton(int cardNum)
{
	controlCardMB[cardNum]->ShowDialog();
}

void MBsoftware::OnClickedShowImageProcessing()
{
	QStringList list;
	QMessageBox::StandardButton reply;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		imageProcess->ShowDialog(login->currentRights);
	}

	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}

void MBsoftware::OnClickedSelectType(void)
{
	QStringList list;
	int stNum;
	int result;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{
			stNum = 0;

		DlgTypes dlg(this, types[stNum].size());

		for (int i = 0; i < types[stNum].size(); i++)
		{
			dlg.types[i] = types[stNum][i]->typeName;


		}
		result = dlg.ShowDialogSelectType(currentType);
		if (result >= 0)
		{
			prevType = currentType;
			currentType = result;
			SaveVariables();
			if (slovensko) AddLog("Tip spremenjen!", 3);
			else AddLog("Type changed!", 3);

			for (int i = 0; i < NR_STATIONS; i++)
			{

				types[i][currentType]->ResetCounters();
				types[i][currentType]->WriteCounters();
				sendRestPC2 = 1;
			}
			stolp1DobriCounter = 0;
			stolp2DobriCounter = 0;
			stolp1SlabiCounter = 0;
			stolp2SlabiCounter = 0;

			badBox1Counter = 0;
			badBox2Counter = 0;
			goodBox2Counter = 0;
			dobriBox1Counter = 0;
			badBoxDimCounter = 0;
			
		}
	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za izbiro tipa morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged in to select type. Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}  
void MBsoftware::OnClickedEditTypes(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		if (NR_STATIONS > 1)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text;

			if (slovensko)  text = QInputDialog::getItem(this, tr("Izberi postajo"), tr("Postaja:"), list, 0, false, &ok);
			else text = QInputDialog::getItem(this, tr("Select station"), tr("Station:"), list, 0, false, &ok);

			if (!ok) return;

			stNum = text.toInt();
		}
		else
			stNum = 0;
		types[stNum][currentType]->OnShowDialog(login->currentRights);
	}

	else
	{
		
		QString niz;

		if (slovensko) niz = QString("Za nastavljanje toleranc morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged in to edit type. Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedRemoveTypes(void)
{
	bool ok;
	QMessageBox box;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString destPath;
	QString sourcePath;
	QString sourcePropertyPath;
	QString sourcePath2,sourcePath3;
	QString destPath2;
	QString destPath3;
	QMessageBox::StandardButton reply;
	

	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko odstranjuje administrator ali operater, worker pa ne
	{

			if (types[0].size() > 1)
			{
				for (int i = 0; i < types[0].size(); i++)
					list.append(types[0][i]->typeName);

				dialog.setComboBoxItems(list);

				if (slovensko)
				{
					dialog.setWindowTitle("Izbris tipa");
					dialog.setLabelText("Izberi tip za izbris");
				}
				else
				{
					dialog.setWindowTitle("Delete type");
					dialog.setLabelText("Select type to remove");
				}

				if (dialog.exec() == IDOK)
				{
					for (int j = 0; j < NR_STATIONS; j++)
					{
						tip = dialog.textValue();
						destPath = QDir::currentPath() + QString("/%1/types/types_Station%2/DeletedTypes").arg(REF_FOLDER_NAME).arg(j);
						destPath2 = destPath;
						destPath3 = destPath;
						if (!QDir(destPath).exists())
							QDir().mkdir(destPath);

						destPath += QString("/%1_deleted.ini").arg(tip);
						destPath2 += QString("/counters_%1_deleted.txt").arg(tip);
						destPath3 += QString("/settings_%1_deleted.ini").arg(tip);

						sourcePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
						sourcePath2 = sourcePath + QString("/counters_%1.txt").arg(tip);
						sourcePath3 = sourcePath + QString("/Type_settings/settings_%1.ini").arg(tip);
						sourcePath += QString("/%1.ini").arg(tip);


						QFile::copy(sourcePath, destPath);
						QFile::copy(sourcePath2, destPath2);
						QFile::copy(sourcePath3, destPath3);

						QFile::remove(sourcePath);
						QFile::remove(sourcePath2);
						QFile::remove(sourcePath3);

						int cur = 0;
						for (int i = 0; i < types[j].size(); i++)
						{
							if (tip == types[j][i]->typeName)
							{
								types[j].erase(types[j].begin() + i);
							}
						}
						//izbrisemo tudi dynamicVariables za property tipov 
						sourcePropertyPath = QDir::currentPath() + QString("/%1/dynamicVariables/types_Station%2/parameters_%3").arg(REF_FOLDER_NAME).arg(j).arg(tip);

						QDir propDir(sourcePropertyPath);
						propDir.removeRecursively();


						prevType = currentType;
						currentType = 0;
						types[j][currentType]->parametersChanged = true;
					}
				}

			}
			else
			{
				box.setStandardButtons(QMessageBox::Ok);
				box.setIcon(QMessageBox::Warning);
				if (slovensko) box.setText("Ne morem izbrisati edinega tipa!");
				else box.setText("Unable to delete all types!");
				box.exec();
			}
		

	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za brisanje tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to delete types! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedAddTypes(void)//zaenkrat kopira celoten TIP 
{
	bool ok;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString filePath;
	QMessageBox::StandardButton reply;

	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko dodaja administrator ali operater, worker pa ne
	{

		if (slovensko)
		{
			dialog.setWindowTitle("Nov tip");
			dialog.setLabelText("Vnesi ime tipa:");
		}
		else
		{
			dialog.setWindowTitle("Add new type");
			dialog.setLabelText("Insert type name:");
		}

		if (dialog.exec() == IDOK)
		{
			tip = dialog.textValue();

			for (int j = 0; j < NR_STATIONS; j++)
			{
				filePath = QDir::currentPath() + QString("//%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
				types[j].push_back(new Types);

				types[j].back()->stationNumber = 0;
				types[j].back()->Init(tip, filePath);
				types[j].back()->InitSettingsWindow(filePath);

				for (int i = 0; i < types[j][currentType]->parameterCounter; i++)
				{
					types[j].back()->toleranceHigh.push_back(types[j][currentType]->toleranceHigh[i]);
					types[j].back()->toleranceLow.push_back(types[j][currentType]->toleranceLow[i]);
					types[j].back()->toleranceHighCond.push_back(types[j][currentType]->toleranceHighCond[i]);
					types[j].back()->toleranceLowCond.push_back(types[j][currentType]->toleranceLowCond[i]);
					types[j].back()->correctFactor.push_back(types[j][currentType]->correctFactor[i]);
					types[j].back()->offset.push_back(types[j][currentType]->offset[i]);
					types[j].back()->nominal.push_back(types[j][currentType]->nominal[i]);
					types[j].back()->isActive.push_back(types[j][currentType]->isActive[i]);
					types[j].back()->isConditional.push_back(types[j][currentType]->isConditional[i]);
					types[j].back()->name.push_back(types[j][currentType]->name[i]);
					types[j].back()->badCounterByParameter.push_back(types[j][currentType]->badCounterByParameter[i]);
					types[j].back()->dynamicParameters.resize(i + 1);
					for (int z = 0; z < types[j][currentType]->dynamicParameters[i].size(); z++)
					{
						types[j].back()->dynamicParameters[i].push_back((types[j][currentType]->dynamicParameters[i][z]));
					}

					types[j].back()->AddParameter(i);
					types[j].back()->measuredValue.push_back(0);
					types[j].back()->isGood.push_back(0);
					types[j].back()->conditional.push_back(0);
				}
				types[j].back()->parameterCounter = types[j][currentType]->parameterCounter;
				types[j].back()->WriteParameters();
				types[j].back()->WriteCounters();

				for (int i = 0; i < 4; i++)
				{
					for (int k = 0; k < 10; k++)
					{
						types[j].back()->setting[i][k] = types[j][currentType]->setting[i][k];
						types[j].back()->settingType[i][k] = types[j][currentType]->settingType[i][k];
						types[j].back()->settingText[i][k] = types[j][currentType]->settingText[i][k];
					}

				}
				if(j == 0)
				types[j].back()->WriteTypeSettings();

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					types[j].back()->prop[i] = types[j][currentType]->prop[i];

				}
				imageProcess->ConnectTypes(j,types[j]);

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					imageProcess->SaveFunctionParametersOnNewType(j,types[j].size() - 1, i);
				}
			}

		}
	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za dodajanje tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to add types! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnClickedTypeSettings(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	
	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko dodaja administrator ali operater, worker pa ne
	{
			stNum = 0;
		types[stNum][currentType]->OnShowDialogSettings(login->currentRights);
	}

	else
	{

		QString niz;

		if (slovensko) niz = QString("Za spreminjanje nastavitev tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to change type settings! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnClickedResetCounters(void)
{
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{
		//QMessageBox::StandardButton reply;
		QString niz;
		if (slovensko) niz = QString("Naj ponastavim stevce?");
		else niz = QString("Reset piece counters?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

		if (reply == QMessageBox::Yes)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{

				types[i][currentType]->ResetCounters();
				types[i][currentType]->WriteCounters();
				sendRestPC2 = 1;
			}
			stolp1DobriCounter = 0;
			stolp2DobriCounter = 0;
			stolp1SlabiCounter = 0;
			stolp2SlabiCounter = 0;
			
		}

	}
	else
	{
		
		QString niz;

		if (slovensko) niz = QString("Za ponastavitev stevcev morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged to reset counters! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

		if (reply == QMessageBox::Yes)
			login->ShowDialog();
	}

}
void MBsoftware::OnClickedLogin(void)
{
	
	login->ShowDialog();
}
void MBsoftware::OnClickedStatusBox(void)
{
	statusBox[0]->ShowDialog();

}
void MBsoftware::OnClickedSmcButton(void)
{
	if(smc.size() > 0)
	smc[0]->ShowDialog();
}
void MBsoftware::OnClickedShowHistory(void)
{
	QStringList list;
	int stNum;
	if (NR_STATIONS > 1)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			if (slovensko) list.push_back(QString("Postaja: %1").arg(i));
			else list.push_back(QString("Station: %1").arg(i));
		}
		bool ok;
		QString text;
		if (slovensko) text = QInputDialog::getItem(this, tr("Izberi postajo:"), tr(""), list, 0, false, &ok);
		else text = QInputDialog::getItem(this, tr("Select station:"), tr(""), list, 0, false, &ok);

		for (int i = 0; i < list.size(); i++)
		{
			if (text == list[i])
				stNum = i;
		}
	}
	else
		stNum = 0;

	history->OnShowDialog(stNum);
}
void MBsoftware::OnClickedGeneralSettings(void)
{
	QString niz;
	QMessageBox::StandardButton reply;
	if ((login->currentRights == 1) || (login->currentRights == 2))
		settings->OnShowDialog();
	else
	{
		if (slovensko) niz = QString("Za spreminjanje nastavitev tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to change type settings! Do you want to log in?");
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
	
}

void MBsoftware::OnClickedAboutUs()
{
	about->OnShowDialog();
}
void MBsoftware::onClientReadyRead()
{
	QString msg = client->readAll();

}
void MBsoftware::OnClickedTCPconnction(int index)
{
	if (tcp.size() >= index)
		tcp[index]->OnShowDialog();
}
void MBsoftware::SaveVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		stream << currentType;
		stream << endl;
		stream << chartStation;
		stream << endl;
		stream << chartParam;
		stream << endl;

		file.close();
	}
}
void MBsoftware::LoadVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QTextStream stream(&file);

			line = stream.readLine();
			currentType = line.toInt();
			line = stream.readLine();
			chartStation = line.toInt();
			line = stream.readLine();
			chartParam = line.toInt();
			file.close();
		}
		else
		{
			chartStation = 0;
			
			chartParam = 0;
		
		}

		if (currentType >= types[0].size())
			currentType = 0;
		//prevType = currentType;
}
void MBsoftware::mouseDoubleClickEvent(QMouseEvent * e)
{
	QRect containtRect;
	int windowPos = -1;
	int nrStation = -1;
	int chartSelected = -1;
	int station = 0;
	int param = 0;
	bool ok = false;
	int curr = 0;

	if (e->button() == Qt::LeftButton)
	{

		containtRect = ui.imageViewMainWindow_0->geometry();
		if (containtRect.contains(e->pos()) == true)
		{

				windowPos = 0;
				nrStation = 0;

		}
		containtRect = ui.frameChart->geometry();
		if (containtRect.contains(e->pos()) == true)
		{

				curr = QInputDialog::getInt(this, tr("Izberi Postajo"), tr("Postaja:"), 0, 0, NR_STATIONS-1, 1, &ok, NULL);
				if (ok)
				{
					station = curr;
					curr = QInputDialog::getInt(this, tr("Izberi mero"), tr("Mera:"), 0, 1, types[station][currentType]->parameterCounter, 1, &ok, NULL);
					if (ok)
					{
						chartStation = station;
						chartParam = curr -1;
					}
				}

		}
		
	}


}

void MBsoftware::keyPressEvent(QKeyEvent *event)
{	
 	qDebug() << event->key();
	QString fileP;
	CPointFloat points[3];
	int i = 0;
	QString name;
	int tmp;
	QString TT = "00ff";
	switch (event->key())
	{
	case  Qt::Key_1:
		measureStationStatus = 0;
		measureStationTrigger = true;
		rocnoProzi = 1;

		break;
	case  Qt::Key_2:
		orientationCameraTrigger = true;
		break;

	case  Qt::Key_3:
		types[1][currentType]->SetMeasuredValue(1500, 0);
		break;
	case  Qt::Key_4:
		processStatus = 0;
		break;
	case  Qt::Key_5:
		cam[1]->GrabImage(0);
		break;
	case  Qt::Key_6:
		
		break;
	case  Qt::Key_7:
	
		break;
	case  Qt::Key_8:
		
		break;

	case  Qt::Key_9:

		break;
	case  Qt::Key_F4:
		break;

	case  Qt::Key_F6:



		break;

	case  Qt::Key_A:
		cam[0]->DisableLive();
	
		break;
	case  Qt::Key_S:

		break;

	case  Qt::Key_R:



		break;

	case  Qt::Key_T:



		break;
	case  Qt::Key_E:


		break;
	case  Qt::Key_G:
		

		break;
	case  Qt::Key_I:
		
		tcp[0]->WriteD201Register(200);


		break;
	case  Qt::Key_J:
		tcp[0]->RequestPLCregister(200);

		break;
	case  Qt::Key_K:

		break;

	case Qt::Key_Space:
		if (startKey == 0)
			startKey = 1;
		else
			startKey = 0;
		break;

			break;
	case  Qt::Key_Escape:
	this->	repaint();
	for (int i = 0; i < cam.size(); i++)
	{
		if (cam[i]->isVisible())
			cam[i]->close();
	}

	for (int i = 0; i < smc.size(); i++)
	{
		if (smc[i]->isVisible())
			smc[i]->close();
	}
	for (int i = 0; i < controlCardMB.size(); i++)
	{
		if (controlCardMB[i]->isVisible())
			controlCardMB[i]->close();
	}
	for (int i = 0; i < statusBox.size(); i++)
	{
		if (statusBox[i]->isVisible())
			statusBox[i]->close();
	}
	for (int i = 0; i < lpt.size(); i++)
	{
		if (lpt[i]->isVisible())
			lpt[i]->close();
	}
	imageProcess->close();
		break;

	default:

		break;
	}
}

