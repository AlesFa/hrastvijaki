﻿#pragma once

#include "stdafx.h"
#include <QObject>

#define POMNILNIK_LAMEL 8
#define NR_CENTER  15
#define TMP_POMNILNIK 10




class Measurand : public QObject  //razred ki ga uporabljam za prenos meritev med razredi imageProcessing in MBSoftware32bit 
{
	Q_OBJECT

public:
	Measurand(QObject *parent);
	Measurand();
	~Measurand();

	void ResetMeasurand(int station, int index);


	int currentType;
	int prevType;
// za Hrast
	int pieceOnStation[NR_STATIONS];
	int orientation;
	int stNavojev;

	//spremenljivke za dinamicno dodajanje funkcij
	CLine simetrala, zgorajLine,spodajLine;
	CPointFloat topOfSimetralaPoint;
	vector<float> measuredValue[NR_STATIONS];



	CPointFloat zgorajTocka, spodajTocka;
	CPointFloat levRob[3], desniRob[3];



	//slika zgoraj dimenzije;
	//CCircle 
	CCircle innerCircle, outerCircle;
	CPointFloat centerKroga;
	float omittedAngles[8];
	float innerDev, outerDev;
	

	//slika zgoraj povrsina
	CCircle navojCircle;
	float navojCircleDev;
	CCircle krogPovrsinaZunanji;
	CCircle krogPovrsinaNotranji;
	float krogPovrsinaNotranjiDev;
	float krogPovrsinaZunanjiDev;
	int stDefects;
	float razdaljaZarez;
	float kotZarez;
	float omitedAngleNavoj[2];


	float thresholdKrogaBrezZarez;
	CCircle krogZaDeviacijobrezZarez;
	float zarezaAngle1[2];
	float zarezaAngle2[2];
	
	int thresholdZareze;
	CLine zareza1;
	CLine zareza2;


	//stranska obdelava
	float visina[3];
	float sirina[3];

};
