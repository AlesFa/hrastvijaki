#pragma once
#include"Signals.h"


class Pcie
{
public:
	Pcie();
	~Pcie();

public:
	Signals output[3][8];
	Signals input[3][8];
private:
	UCHAR data[3];
	int  offset;
	int cardNum;


public:
	int Init(int status);
	int SetDataByte(unsigned long port, UCHAR bits, int value);
	int SetDataByte(unsigned long port, UCHAR value);
	int SetDataByte16(unsigned long port, unsigned short int value);
	int GetDataByte(unsigned long port);
	int GetDataByte16(unsigned long port);
};

