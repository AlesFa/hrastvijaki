#pragma once

#include "qwidget.h"
#include "stdafx.h"
#include "ui_SMCdrive.h"
#include "wsc.h"

#define MAX_MOT		4 //maksimalno stevilo IDjev mkomunikacija omogoca MAX 32 

class SMCdrive : public QWidget
{
	Q_OBJECT

public:
	SMCdrive(QWidget *parent = Q_NULLPTR);
	void closeEvent(QCloseEvent * event);
	~SMCdrive();

	SMCdrive(QString port, int baudRate);
	bool isOpen;
	bool isValid;
	int crcLO;
	int crcHI;
	QByteArray getBuffer;
	int numMotors;
	int idDialog;
	QString motorName[MAX_MOT];
	int currentId;
	QStringList motorSettingsList[MAX_MOT];
	bool motorLive;

private:
	Ui::SMCdrive ui;
	QTimer  *viewTimer;
	int port;
	int code;




public:
	void ShowDialog();
	int Init(int Id);
	//void crc16(int* input, int inputLength);
	void Reset(int Id);
	QByteArray ReadStatus(int Id);
	void ReturnToOrigin(int Id);
	void CalcualteCRC(int*input, int lenght, QByteArray& sendData);
	void MotorOn(int id);
	void MotorOff(int Id);
	void GoToPositionInMM(int ID, float mm, int speed);
	float ReadPosition(int Id);
	void ReadInputSignals(int id);
	void GoToSavedPosition(int Id, int position);
	void Jog(int id, bool forward);
	void StopJog(int id);
	void WriteData(QByteArray data);
	void FlushData(); //samo za izpraznit buffer
	int  ReadData();

public slots:
	void ViewTimerFunction();
	void MotorOnFunction();
	void GoToOriginFunction();
	void ResetFunction();
	void IdChanged(int text);
	void JogPlusPressed();
	void JogMinusPressed();
	void JogPlusReleased();
	void JogMinusReleased();
	void MoveFunction();
};
