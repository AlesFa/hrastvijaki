#pragma once
#include <qwidget.h>
#include "ui_Serial.h"

#define RLS_Version			'v' //interface serial number in 8 Hex numbers
#define RLS_SerialNumber	's' //interface product serial number
#define RLS_ProductSerial	'R'
#define RLS_3Decimal		'?' //returns 3 decimal values (width not fixed) separated by colons and terminated with CR
#define RLS_4Decimal		'!' //returns 4 decimal values (width not fixed) separated by colons and terminated with CR
#define RLS_24cHEXstr		'>' //returns 24 character hexadecimal string + CR comprising 3 sets of 8 character hexadecimal strings
#define RLS_32cHEXstr		'<' //returns 32 character hexadecimal string + CR comprising 4 sets of 8 character hexadecimal strings
#define RLS_IndexMode		'I' //begin index mode On every reference/index E201-9Q returns position as 8 character hexadecimal string
#define RLS_IndexModeStop	'i'	//stop index mode (stops returning position on every reference/index)
#define RLS_ClearRefFlag	'c' //clears reference status flag
#define RLS_CoutToZero		'z' //sets current count value to zero (this also affects reference mark position)
#define RLS_ClearZeroOffs	'a' //clears zero offset value stored by �z� command
#define RLS_EncoderSuppStat	'e' //Read encoder supply status, voltage and current consumption (fixed width)
#define RLS_PowerSuppON		'n' //Turn on power supply to encoder (default at power-up)
#define RLS_PowerSuppOFF	'f' //Turn off power supply to encoder
#define RLS_InPinsStats		'p' //Status of hardware input pins on interface (0 or 1)
#define RLS_AutoTransON		'1'	//Begin auto transmission of encoder position in decimal form at 1 kHz rate
#define RLS_AutoTransOFF	'0' //Stop auto transmission


class Serial : public QWidget
{
	Q_OBJECT
public:
	Serial();

	Serial(QString port, int baudRate);


	virtual ~Serial();




private:
	Ui::Serial ui;
	int port;
	int code;
	int baudRate;
	char getBuffer[1024];
	QByteArray parseBuffer;
	int parseCounter;
	


protected:


public:
	bool isOpen;
	float encoder;



public:
	void ReadEncoder();
	void ReadData();
	void WriteData(QByteArray text);
};

