#include "stdafx.h"
#include "Server.h"


std::vector<QTcpSocket*>					Server::tcpConnections;
QTimer*										Server::connectionTimer;

Server::Server()
{
	numOfConnections = 0;
	waitTime = 1000;
	connectionTimer = new QTimer();
	connectionTimer->setSingleShot(true);
	connect(this, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
	connect(this, SIGNAL(readyRead()), this, SLOT(onServerReadyRead()));
	connect(connectionTimer, SIGNAL(timeout()), this, SLOT(onConnectionTimeout()));
}

void Server::StartListening()
{
	//Pretvorimo naslov v ustrezen format in �akamo na povezavo
	addr.setAddress(address);
	this->listen(addr, port);
	//Za�nemo od�tevati �as do povezave
	connectionTimer->start(waitTime);
}

void Server::onNewConnection()
{
	//Shranimo novo povezavo
	QTcpSocket *socket = this->nextPendingConnection();
	tcpConnections.push_back(socket);
	numOfConnections++;
	connect(tcpConnections.back(), SIGNAL(readyRead()), this, SLOT(onServerReadyRead()));

	//Nehamo poslu�ati in ustavimo �tevec
	this->close();
	connectionTimer->stop();

	//Potrdimo povezavo odjemalcu
	tcpConnections.back()->write("Connection established");
	tcpConnections.back()->flush();
}

void Server::onConnectionTimeout()
{
	//Ob izteku �asa za povezavo nehamo poslu�ati s stre�nikom in po�ljemo primeren signal
	this->close();
	emit(connectionTimeout());
}

void Server::onServerReadyRead()
{
	//Preberemo podatke od odjemalca
	recievedData = tcpConnections[0]->readAll();
	emit(dataReady());
}

Server::~Server()
{
	delete connectionTimer;
}
