#include "stdafx.h"
#include "TCP.h"


	QTcpSocket*									TCP::tcpConnections[MAX_CONN];
	QString										TCP::tcpConnectionsName[MAX_CONN];
	QTimer*										TCP::connectionTimer;
	QTimer*										TCP::sendRcvTImer;

TCP::~TCP()
{
	ui.TextEdit->clear();
}
TCP::TCP(QString ipAddress, int port, bool isServer)
{
	ui.setupUi(this);
	ui.TextEdit->setReadOnly(true);
	ui.TextEdit->setMaximumBlockCount(500);
	clientConnected = false;
	address = ipAddress;
	dataBufCounter = 0;
	this->port = port;
	numOfConnections = 0;
	waitTime = 1000; // po pribli�no 6ih sekundah  client neha po�iljati pro�njo za povezavo 
	connectionTimer = new QTimer();

	connectionTimer->setSingleShot(true);
	//sendRcvTImer = new QTimer();
	//sendRcvTImer->start(100);

	dataSend = false;
	readState = false;
	writeState = false,
	readWriteCounter = 0;

	D200 = 0;
	D201 = 0;

	timout = 5;


	connect(this, SIGNAL(appendText(QString)), this, SLOT(OnAppendText(QString)));
	
	connect(ui.buttonSend, SIGNAL(pressed()), this, SLOT(OnClickedSend()));


	if (isServer)
	{
		for (int i = 0; i < MAX_CONN; i++)
			tcpConnectionsName[i] = "";
		
		this->isServer = true;
		server = new QTcpServer(this);
		connect(server, SIGNAL(newConnection()), this, SLOT(OnNewConnection()));
	
	
		StartServer();
	}
	else
	{
		this->isServer = false;
		client = new QTcpSocket();
		//connect(client, SIGNAL(readyRead()), this, SLOT(OnClientReadyReadPLC()));
		//connect(client, SIGNAL(readyRead()), this, SLOT(OnClientReadyRead()));
		connect(client, SIGNAL(connected()), this, SLOT(OnClientConnected()));
		connect(client, SIGNAL(disconnected()), this, SLOT(OnClientDisconnected()));
		connect(connectionTimer, SIGNAL(timeout()), this, SLOT(OnConnectionTimeout()));


		//connect(sendRcvTImer, SIGNAL(timeout()), this, SLOT(SendRcvLoop()));

		StartClient();
	}

	slovensko = SLOVENSKO;

	if (slovensko) ui.buttonSend->setText("Poslji");
}


void TCP::OnShowDialog()
{
	setWindowState(Qt::WindowActive);
	activateWindow();

		show();
}

void TCP::StartServer()
{
	
	QString text(QString("SERVER STARTED: IP: %1 , Port: %2").arg(address).arg(port));


	emit appendText(text);

	//Pretvorimo naslov v ustrezen format in �akamo na povezavo
	addr.setAddress(address);
	server->listen(addr, port);

	//Za�nemo od�tevati �as do povezave
	//connectionTimer->start(waitTime);
}

void TCP::StartClient()
{
	QString text;

	if (slovensko) text = QString("POVEZUJEM SE NA SERVER: IP: %1 , Vrata: %2").arg(address).arg(port);
	else text = QString("CONNECTING TO SERVER: IP: %1 , Port: %2").arg(address).arg(port);

	//ui.TextEdit->appendPlainText(text);
	emit appendText(text);
	client->connectToHost(address, port);
	connectionTimer->start(waitTime);
}

void TCP::WriteData(QByteArray text)
{
	QByteArray sendData = text;
	QByteArray newText = text;
	newText.prepend("Sent:");
	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");
	

	if (carrageReturn.indexIn(sendData, 0) == -1)
	{
		sendData.append("\r\n");

	}
	else
	{
	newText.chop(2);
	}
	emit appendText(newText);
	//ui.TextEdit->appendPlainText(newText); //samo za prikaz v konzolo


	if	(clientConnected == true)
		{
		client->write(sendData);
		client->flush();
	

		}
	
}

void TCP::Disconnect()
{
	if (isServer)
	{

	}
	else
	{
		client->disconnectFromHost();
		client->close();
	}
}

void TCP::ReadDataRegister()
{
}

void TCP::RequestPLCregister(int regNum)/// trenutno prosi samo za register 200
{
	
	//byte tmpHex[3];
	//itoa(regNum, tmpHex, 16);
	
	/*char payload[21] = { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x01, 0x04, 0x00, 0x00, 0xC8, 0x00, 0x00, 0xA8, 0x01, 0x00 };


	if (clientConnected)
	{
		client->write(payload,21);
		client->flush();
	
		
	}*/

}



void TCP::WriteD201Register(int data)
{
	/*int low = data & 0xff;
	int high = (data >> 8) & 0xff;

	char payload[23] = { 0x50, 0x00, 0x00, 0xFF, 0xFF, 0x03, 0x00, 0x0E, 0x00, 0x10, 0x00, 0x01, 0x14, 0x00, 0x00, 0xC9, 0x00, 0x00, 0xA8, 0x01, 0x00, low, high };

	if (clientConnected)
	{
		client->write(payload, 23);
		client->flush();

	}*/
}

void TCP::WritePLC(QByteArray data)
{
	if (clientConnected == true)
	{
		client->write(data);

	}
}

int  TCP::WriteServerToClient(QString name, QByteArray data)
{
	QByteArray sendData = data;
	QByteArray newText = data;
	newText.prepend("Sent:");
	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");
	int currConnection = -1;

	for (int i = 0; i < MAX_CONN; i++)
	{
		if (name.compare(tcpConnectionsName[i]) == 0)
			currConnection = i;

	}


			if (currConnection > -1)
			{
				if (carrageReturn.indexIn(sendData, 0) == -1)
				{
					sendData.append("\r\n");

				}
				else
				{
					newText.chop(2);
				}
				emit appendText(newText);
				//ui.TextEdit->appendPlainText(newText); //samo za prikaz v konzolo

				tcpConnections[currConnection]->write(sendData);
				tcpConnections[currConnection]->flush();

				return 1;

			}
		
	





	return -1;
}

void TCP::OnNewConnection()
{
	//Shranimo novo povezavo
	QTcpSocket *socket = server->nextPendingConnection();
	int currConnection = -1;
	for (int i = 0; i < MAX_CONN; i++)
	{
		if (tcpConnectionsName[i] == "")//pomeni da je povezava prazna
		{
			currConnection = i;
			tcpConnections[i] = socket;
			break;
		}
	}
	//tcpConnectionsName.push_back("");
	//tcpConnections.push_back(socket);
	//int sendNum  = tcpConnections.size()- 1;
	//connect(tcpConnections.back(), SIGNAL(readyRead()), this, SLOT(OnServerReadyRead()));
	connect(tcpConnections[currConnection], &QAbstractSocket::readyRead, [=]() { OnServerReadyRead(currConnection); });
	connect(tcpConnections[currConnection], &QAbstractSocket::disconnected, [=]() { OnClientDisconnected(currConnection); });
	numOfConnections++;



	QHostAddress IP;
	QString ipString;
	IP = socket->peerAddress();
	ipString = IP.toString();





	QString text(QString("-----------------------CLIENT CONNECTED IP:%1-----------------------").arg(ipString));
	//ui.TextEdit->appendPlainText(text);
	emit appendText(text);

	if (ipString.compare("172.16.9.54") == 0)
	{
		tcpConnectionsName[currConnection] = "EPSON";

	}
	else if ((ipString.compare("172.16.9.13") == 0))
	{
		tcpConnectionsName[currConnection] = "PC2";
	}


	//Nehamo poslu�ati in ustavimo �tevec
	//server->close();
	//connectionTimer->stop();

	//Potrdimo povezavo odjemalcu
	//tcpConnections.back()->write("Connection established");
	//tcpConnections.back()->flush();
}


void TCP::OnConnectionTimeout()
{
	int state = -1;
	if (isServer)
	{

	}
	else
	{
		state = client->state();
		
		if (state == 2)//se se povezuje ponovno preverimo cez en timer
		{
			timout++;
			if (timout > 5)
			{
				client->waitForConnected(0);
				timout = 0;
			}
			connectionTimer->start(waitTime);
		}
		else if (state == 0)
		{
			client->disconnectFromHost();
			StartClient();
			connectionTimer->start(waitTime);
			timout = 0;
		}
		else if (state == 3)//connected to host
		{
			timout = 0;
		}

		
			
	}
}

void TCP::SendRcvLoop()
{
	//ukaz za register iz PLCja


	if (readWriteCounter % 2 == 0)
	{
		if (readState == false)
		{
			RequestPLCregister(200);
			readState = true;
		}
	}
	else
	{

		if (writeState == false)
		{
			WriteD201Register(D201);
			writeState = true;
		}
	}


	OnClientReadyReadPLC();




}

void TCP::OnServerReadyRead(int connection)
{
	//recievedData = tcpConnections[connection]->readAll();
	//emit appendText(recievedData);

	//QString msg = client->readAll();
	char message[REQUEST_SIZE];
	int statusCode = 0;
	bool split = false;
	QByteArray tmpArray;
	QByteArray tmpArray2;
	QByteArray tmpArray3;
	int parseCounter = 0;
	QByteArray msg;


	statusCode = tcpConnections[connection]->read(message, REQUEST_SIZE);
	msg = QByteArray(message, statusCode);
	//msg.chop(1);
	msg.prepend("Receive:");;
	emit appendText(msg);
	if (statusCode < 0)
	{
		int bla = 100;
	}
	else
	{
		{
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[i] = message[i];

			}
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[dataBufCounter] = message[i];

				if ((dataBuf[dataBufCounter -1] == 13)&& (dataBuf[dataBufCounter] == 10))//koda je napisana za projekt palcke LITVA
				{
					tmpArray.clear();
					tmpArray2.clear();

					for (int j = 0; j < dataBufCounter; j++)
					{
						tmpArray.append(dataBuf[j]);

						if (j > 0)
						{

							tmpArray2.append(tmpArray[j]);
						}

					}

					dataBufCounter = 0;
					split = false;
					if (parseCounter > REQUEST_SIZE)
						parseCounter = 0;
					//intBarvaArray[parseCounter] = integer;
					returnCharArray[parseCounter] = tmpArray[0];
					returnIntArray[parseCounter] = tmpArray2.toInt();
					//intBarvaSecundArray[parseCounter] = atoi(secInt);
					parseCounter++;
				}
				else
				{
					dataBufCounter++;
				}
			}
		}
		//emit dataReady(parseCounter);
		emit dataReady(tcpConnectionsName[connection], parseCounter);

	}

}


void TCP::OnClientReadyRead()
{
	//QString msg = client->readAll();
	char message[REQUEST_SIZE];
	int statusCode = 0;
	bool split = false;
	QByteArray tmpArray;
	QByteArray tmpArray2;
	QByteArray tmpArray3;
	int parseCounter = 0;
	QByteArray msg;
	

	if (clientConnected == true) 		// still connected ?
	{
		statusCode = client->read(message, REQUEST_SIZE);
		msg = QByteArray(message, statusCode);
		msg.chop(2);
		msg.prepend("Receive:");
		//ui.TextEdit->appendPlainText(msg);
		emit appendText(msg);
		if (statusCode < 0)
		{
			int bla = 100;
		}
		else
		{
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[i] = message[i];

			}
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[dataBufCounter] = message[i];

				if ((dataBuf[dataBufCounter - 1] == 13) && (dataBuf[dataBufCounter] == 10))//koda je napisana za projekt palcke LITVA
				{
					tmpArray.clear();
					tmpArray2.clear();
					tmpArray3.clear();
					split = false;
					for (int j = 0; j < dataBufCounter; j++)
					{
						tmpArray.append(dataBuf[j]);
						if (j > 0)
						{
							if (split == true)
							{
								tmpArray3.append(tmpArray[j]);
							}
							else
							{
								tmpArray2.append(tmpArray[j]);
							}
							if (tmpArray[j] == '-')
								split = true;
						}
					}

					dataBufCounter = 0;
					split = false;
					if (parseCounter > REQUEST_SIZE)
						parseCounter = 0;
					//intBarvaArray[parseCounter] = integer;
					returnCharArray[parseCounter] = tmpArray[0];
					returnIntArray[parseCounter] = tmpArray2.toInt();
					//intBarvaSecundArray[parseCounter] = atoi(secInt);
					parseCounter++;
				}
				else
				{
					dataBufCounter++;
				}
			}
		}

		//emit dataReady(parseCounter);
	}


}

void TCP::OnClientReadyReadPLC()

{
	//QString msg = client->readAll();
	char message[REQUEST_SIZE];
	int statusCode = 0;
	bool split = false;
	QByteArray tmpArray;
	QByteArray tmpArray2;
	QByteArray tmpArray3;
	int parseCounter = 0;
	QByteArray msg;


	if (clientConnected == true) 		// still connected ?
	{
		statusCode = client->read(message, REQUEST_SIZE);
		msg = QByteArray(message, statusCode);
		msg.chop(2);
		msg.prepend("Receive:");
		//ui.TextEdit->appendPlainText(msg);
		emit appendText(msg);
		if (statusCode < 0)
		{
			int bla = 100;
		}
		else
		{
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[i] = '/0';

			}
			for (int i = 0; i < statusCode; i++)
			{
				dataBuf[dataBufCounter] = message[i];

				if (dataBufCounter == statusCode -1)
				{

					if (readState)
					{
						char low = dataBuf[11];
						char high = dataBuf[12];
						//int result = high + (low >> (CHAR_BIT));
						UINT16 result = 0;
						result = high * 256 + (low & 0xff);

						result = result & 0xffff;
						D200 = result;
						readState = false;
						readWriteCounter++;
					}
					if (writeState)
					{
						writeState = false;
						readWriteCounter++;
					}





					dataBufCounter = 0;			
				}
				else
				{
					dataBufCounter++;
				}
			}
		}

	}


}

void TCP::OnClientConnected()
{
	if (isServer)
	{

	}
	else
	clientConnected = true;

	emit appendText("------------------------------CONNECTED TO SERVER------------------------------");
}

void TCP::OnClientDisconnected()
{
	if (isServer)
	{

	}
	else
	{

		connectionTimer->start();//poskus ponovne povezave 
	}
	emit appendText("------------------------------CONNECTION  LOST------------------------------");
}

void TCP::OnClientDisconnected(int get)
{
	if (isServer)
	{
		
		//tcpConnections.erase(tcpConnections.begin() + get);
		//tcpConnectionsName.erase(tcpConnectionsName.begin() + get);
		disconnect(tcpConnections[get], 0, 0, 0);
		tcpConnectionsName[get] = "";
		numOfConnections--;
	}
	else
	{


	}


	emit appendText("------------------------------CONNECTION  LOST------------------------------");
}

void TCP::OnClickedSend()
{
	QString sendText;
	QByteArray sendArray;
	
	 sendText = ui.lineSend->text();

	 sendArray = sendText.toLocal8Bit();

	 ui.lineSend->clear();
	 if (sendArray.size() > 0)
		 WriteData(sendArray);
}

void TCP::OnAppendText(QString text)
{
	ui.TextEdit->appendPlainText(text);
}







