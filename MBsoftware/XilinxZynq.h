#pragma once

#include <QObject>
#include "ui_XilinxZynq.h"
#include <QtNetwork/qudpsocket.h>
#include "Timer.h"
#include "Signals.h"
#define REQ_SIZE 100000
#define NR_REG	64
class XilinxZynq : public QWidget
{
	Q_OBJECT


public:
	XilinxZynq(QString ipAddress, int port);
	~XilinxZynq();
	void closeEvent(QCloseEvent * event);

private:
	Ui::XilinxForm ui;
	
public:
	void OnShowDialog();
	void StartClient();
	QReadWriteLock lock;
	QTimer*							connectionTimer;
	QTimer*							viewTimer;
	Timer							dataReadyFreq;
	QUdpSocket*		udpClient;
	QHostAddress hostIp;
	int port;
	int waitTime;
	char									dataBuf[REQ_SIZE];
	int										dataBufCounter;
	int										fps;

	void readPendingDatagrams();
	void CreateDisplay();
	void SetIONames();
	void WriteDatagram(QByteArray data);
	QByteArray CreateHexFormatForSend(int reg);
	QLabel* readRegLabel[NR_REG];
	QLabel* readRegValueLabel[NR_REG];
	QHBoxLayout* readHLayout[NR_REG];
	QLabel* writeRegLabel[NR_REG];
	QLabel* writeRegValueLabel[NR_REG];
	QHBoxLayout* writeHLayout[NR_REG];
	UINT16		readRegisters[NR_REG];
	UINT16		writeRegisters[NR_REG];
	int connectionOnCounter, connectionOnPrevCounter;
	bool isConnected, isConnectedOld;
	bool isInitialized;
	int isOutputP3;
	int isOutputP8;

	QCheckBox *checkBoxInput[4][16];
	QCheckBox *checkBoxOutput[3][16];
	QCheckBox *checkBoxIOP8[16];
	QCheckBox *checkBoxIOP3[16];
	QToolButton *buttonLight[8];
	QToolButton *buttonCamera[8];
	QLabel *triggerCounter[8];

	Signals luci[8];
	// output in input signali so po konektorjih
	//output index -> 0 ->output konektor P2
	//output index -> 1 -> I/O konektor P3
	//output index -> 2 -> I/O konektor P8

	//input index -> 0 ->output konektor P2
	//input index -> 1 -> I/O konektor P3
	//input index -> 2 -> I/O konektor P8
	//input index -> 2 -> input konektor P9

	Signals	output[3][16];
	Signals input[4][16];


	

private slots:
	void OnNewConnection();
	void OnConnectionTimeout();

	void OnViewTimer();
	void OnSend();
	void OnClickedOutput(int index,int bit);
	void OnClickedButtonLight(int index);
	void OnClickedButtonCam(int index);
	//void OnClickedControl(int index);
	

public:

	void SendHandShake();	//poslji 2643 kodo za zacetek komunikacije
	void SendSetTransmitSpeed(int speed);//nastavi hitrost posiljanja podatkov
	void SendWD();///poslji pulz da je komunikacija obstojeca

	int WriteRegister(int regNum);
	//input output control
	void InitAddresses();
	void SetInputSignals(int reg,int index);
	void SetOutputSignals(int reg, int index);
	void SetOutputValue(int nr, bool value);

	// output in input signali so po konektorjih
	//output index -> 0 ->output konektor P2
	//output index -> 1 -> I/O konektor P3
	//output index -> 2 -> I/O konektor P8
	void SetOutputValue(int index, int nr, bool value);
	void SetOutputReg(int reg ,int bits, bool value);
	//Nasavimo smer IO na konektorju 
	//direction: 1-> output ,0-> input
	//part 0-> vseh 16bit, 1-> spodnjih 8 bit, 2-> zgornjih 8 bit.
	void SetP3Direction(int part, int direction);
	//direction->1-> output, 0-> input
	void SetP8Direction(int direction);


	//kamera Control//
	void RocnoProzenjeKamere(int cam);
	void RocnoLuci(int luc,bool value);




	



};
