﻿#include "stdafx.h"
#include "imageProcessing.h"
#include "math.h"
#include "CLine.h"
#include "CPointFloat.h"
#include "CRectRotated.h"
#include "CCircle.h"
#include <conio.h>





std::vector<CCamera*>										imageProcessing::cam;
std::vector<CMemoryBuffer*>									imageProcessing::images;
CMemoryBuffer*												imageProcessing::currImage;
std::vector<Types*>											imageProcessing::types[NR_STATIONS];
Measurand*													imageProcessing::measureObject;
std::vector<Timer*>											imageProcessing::processingTimer;

Mat															imageProcessing::DrawImage;

QRect testRect;

int nrSavedImages[4];

int rubberResizeMode = 0;

imageProcessing::imageProcessing(QWidget *parent)
	: QWidget(parent)
{


	ui.setupUi(this);
	zoomFactor = 1;
	displayImage = 0;
	displayWindow = 0;
	prevDisplayImage = 0;
	prevDisplayWindow = 0;
	rowNum = 10;
	colNum = 4;
	currentStation = 0;
	isSceneAdded = false;
	selectedFunction = 0;
	drawRubberFunction = 0;

	ui.lineInsertImageIndex->setText("0");
	
	connect(ui.buttonImageUp, SIGNAL(pressed()), this, SLOT(OnPressedImageUp()));
	connect(ui.buttonImageDown, SIGNAL(pressed()), this, SLOT(OnPressedImageDown()));
	connect(ui.buttonCamDown, SIGNAL(pressed()), this, SLOT(OnPressedCamDown()));
	connect(ui.buttonCamUp, SIGNAL(pressed()), this, SLOT(OnPressedCamUp()));
	connect(ui.buttonZoomIn, SIGNAL(pressed()), this, SLOT(ZoomIn()));
	connect(ui.buttonReset, SIGNAL(pressed()), this, SLOT(ZoomReset()));
	connect(ui.buttonZoomOut, SIGNAL(pressed()), this, SLOT(ZoomOut()));
	connect(ui.buttonLoadImage, SIGNAL(pressed()), this, SLOT(OnLoadImage()));
	connect(ui.buttonLoadMultipleImages, SIGNAL(pressed()), this, SLOT(OnLoadMultipleImage()));
	connect(ui.buttonSaveImage, SIGNAL(pressed()), this, SLOT(OnSaveImage()));
	connect(ui.buttonProcessCam0, SIGNAL(pressed()), this, SLOT(OnProcessCam0()));
	connect(ui.buttonProcessCam1, SIGNAL(pressed()), this, SLOT(OnProcessCam1()));
	connect(ui.buttonProcessCam2, SIGNAL(pressed()), this, SLOT(OnProcessCam2()));
	connect(ui.buttonProcessCam3, SIGNAL(pressed()), this, SLOT(OnProcessCam3()));
	connect(ui.buttonProcessCam4, SIGNAL(pressed()), this, SLOT(OnProcessCam4()));
	connect(ui.buttonCreateRubber, SIGNAL(pressed()), this, SLOT(OnPressedCreateRubber()));
	connect(ui.buttonRubberResize, SIGNAL(pressed()), this, SLOT(OnPressedSelectRubber()));


	connect(ui.addParameter, SIGNAL(pressed()), this, SLOT(onPressedAddParameter()));
	connect(ui.removeParameter, SIGNAL(pressed()), this, SLOT(onPressedRemoveParameters()));
	connect(ui.update, SIGNAL(pressed()), this, SLOT(onPressedUpdateParameters()));
	connect(ui.buttonSurfaceInspection, SIGNAL(pressed()), this, SLOT(onPressedSurfaceInspection()));
	//connect(ui.imageView, SIGNAL(pressed()), this, SLOT(mousePressEvent()));
	//connect(ui.imageView, SIGNAL(released()), this, SLOT(mouseReleaseEvent()));
	connect(ui.lineInsertImageIndex, SIGNAL(editingFinished()), this, SLOT(OnDoneEditingLineInsertImageIndex()));

	//testFunctions
	connect(ui.buttonFunctionHeight, SIGNAL(pressed()), this, SLOT(AddnewHeightMeasurement()));
	connect(ui.buttonFunctionLineDeviation, SIGNAL(pressed()), this, SLOT(LineDeviationMeasurement()));
	connect(ui.buttonFunctionFunctionAddAsMeasuredParameter, SIGNAL(pressed()), this, SLOT(SaveSelectedDynamicFunctionAsMeasurment()));
	connect(ui.buttonParameterEdit, SIGNAL(pressed()), this, SLOT(EditSelectedDynamicFunctionParameter()));

	for (int i = 0; i < 7; i++)
	{
		processingTimer.push_back(new Timer());
	}
	rubberBand = new QRubberBand(QRubberBand::Rectangle, ui.imageView);//new rectangle band
	//gripTopLeft = new QSizeGrip(this);
	//gripBottomRight = new QSizeGrip(this);

	ui.imageView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	start = 0;

	variantManager = new QtVariantPropertyManager();
	
	slovensko = SLOVENSKO;

	QString niz;
	if (slovensko) niz = QString("Konstanta funkcije");
	else niz = QString("Function property");
	topItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
		niz);

	variantFactory = new QtVariantEditorFactory();

	variantEditor = new QtTreePropertyBrowser();
	variantEditor->setFactoryForManager(variantManager, variantFactory);
	variantEditor->addProperty(topItem);
	variantEditor->setPropertiesWithoutValueMarked(true);
	variantEditor->setResizeMode(QtTreePropertyBrowser::ResizeMode::ResizeToContents);
	variantEditor->setRootIsDecorated(true);
	ui.ParameterLayout->addWidget(variantEditor);


	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->addItem("");
	}


	NastaviJezikImgProc();




	QHBoxLayout* layout = new QHBoxLayout(rubberBand);
	//layout->setContentsMargins(0, 0, 0, 0);
	//setWindowFlags(Qt::SubWindow);

	gripTopLeft = new QSizeGrip(rubberBand);
	gripBottomRight = new QSizeGrip(rubberBand);

	layout->addWidget(gripTopLeft, 0, Qt::AlignLeft | Qt::AlignTop);
	layout->addWidget(gripBottomRight, 0, Qt::AlignRight | Qt::AlignBottom);

	//rubberBand->resize(50, 50);
	gripTopLeft->setVisible(false);
	gripBottomRight->setVisible(false);

	timer = new QTimer();
	
	connect(timer, SIGNAL(timeout()), this, SLOT(OnViewTimer()));

	for (int i = 0; i < 4; i++)
	{
		nrSavedImages[i] = 0;
	}

}

void imageProcessing::closeEvent(QCloseEvent * event)
{
	drawRubberFunction = 0;
	selectedFunction = 0;
	timer->stop();
	ClearDraw();
}

imageProcessing::~imageProcessing()
{
	delete variantManager;
	delete variantFactory;
	delete variantEditor;
	qDeleteAll(scene->items());
	item.erase(item.begin(), item.end());
	cam.clear();
	delete scene;

	//delete item;
	//delete topItem;

}

void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam, std::vector<CMemoryBuffer*> inputImages)
{

	cam = inputCam;
	images = inputImages;
	currImage = &cam[0]->image[0];
	displayWindow =0;
	displayImage = 0;

	ResizeDisplayRect();
	ShowImages();

}

void imageProcessing::ConnectMeasurand(Measurand * objects)
{

	measureObject = objects;
	
}

void imageProcessing::ConnectTypes(int station,std::vector<Types*> type)
{

		types[station] = type;

}


void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam)
{
	cam = inputCam;

}


void imageProcessing::ShowDialog(int rights)
{	
	timer->start(200);
		
	loginRights = rights;
	ReplaceCurrentBuffer();
	activateWindow();
	currentFunction = -1;
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}
	item.clear();
	//Če uporabnik nima administratorskih pravic se onemogoči nastavljanje parametrov slik
	ui.buttonProcessCam4->setEnabled(false);
	if ((rights == 1)|| (rights == 2))
	{
		if (rights == 1)
		{
			ui.addParameter->setEnabled(true);
			ui.removeParameter->setEnabled(true);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);
			ui.buttonRubberResize->setEnabled(true);
			ui.buttonCreateRubber->setEnabled(true);
			ui.buttonDrawIntensity->setEnabled(true);
			ui.buttonFunctionFunctionAddAsMeasuredParameter->setEnabled(true);
			ui.buttonFunctionHeight->setEnabled(true);
			ui.buttonFunctionLineDeviation->setEnabled(true);
			ui.buttonParameterEdit->setEnabled(true);

		}
		else
		{
			ui.addParameter->setEnabled(false);
			ui.removeParameter->setEnabled(false);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);
			ui.buttonRubberResize->setEnabled(false);
			ui.buttonCreateRubber->setEnabled(false);
			ui.buttonDrawIntensity->setEnabled(false);
			ui.buttonFunctionFunctionAddAsMeasuredParameter->setEnabled(false);
			ui.buttonFunctionHeight->setEnabled(false);
			ui.buttonFunctionLineDeviation->setEnabled(false);
			ui.buttonParameterEdit->setEnabled(false);

		}
		ui.update->setEnabled(true);
		ui.Cancel->setEnabled(true);

		
		ui.buttonSurfaceInspection->setEnabled(false);
	}

	else
	{
		ui.addParameter->setEnabled(false);
		ui.removeParameter->setEnabled(false);
		ui.update->setEnabled(false);
		ui.Cancel->setEnabled(false);
		ui.buttonProcessCam0->setEnabled(false);
		ui.buttonProcessCam1->setEnabled(false);
		ui.buttonProcessCam2->setEnabled(true);
		ui.buttonProcessCam3->setEnabled(false);
		ui.buttonSurfaceInspection->setEnabled(false);
	}
		setWindowState( Qt::WindowActive);
	show();
}

void imageProcessing::ClearDialog()
{
	int camNum = displayWindow;
	int imgNum = displayImage;;

}


void imageProcessing::NastaviJezikImgProc()
{
	if (slovensko)
	{
		this->setWindowTitle("Obdelava slik");
		ui.tabWidget->setTabText(0, "Obdelava slik");
		ui.tabWidget->setTabText(1, "Kalibracija");
		ui.tabWidget->setTabText(2, "Datoteke");

		ui.buttonSaveImage->setText("Shrani sliko");
		ui.buttonLoadImage->setText("Nalozi sliko");

		ui.labelInsertImageIndex->setText("Vnesi stevilko slike:");


		ui.buttonCamUp->setText("KAM+");
		ui.buttonCamDown->setText("KAM-");
		ui.buttonImageUp->setText("SLIKA+");
		ui.buttonImageDown->setText("SLIKA-");

		ui.addParameter->setText("Dodaj parameter");
		ui.removeParameter->setText("Odstrani parameter");

		ui.Cancel->setText("Preklici");
		ui.update->setText("Shrani");
	}
}

bool imageProcessing::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;
	//QGraphicsLineItem * item;

	QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
	QPointF position = mouseEvent->pos();
	QPointF scenePt = ui.imageView->mapToScene(mouseEvent->pos());
	
	//QGraphicsScene::mousePressEvent(event);
	int x, y;
	if (event->type() == QEvent::MouseMove)
	{

		if (creatingRubber == 1)
		{
			rubberBand->setGeometry(QRect(pos, mouseEvent->pos()).normalized());
		}

		if (changeRubberBand == 1)
		{

			QRect containtRect;
			QRect levoTopRect;
			QRect desnoSpodajRect;
			QPointF posTopLeft;
			QPointF posBottomRight;
			QPolygonF testPolygon;
			QRectF testR;

			testPolygon = ui.imageView->mapToScene(rubberBand->geometry());
			testR = testPolygon.boundingRect();
			containtRect = testR.toRect();
			posTopLeft = containtRect.topLeft();
			levoTopRect.setX(posTopLeft.x());
			levoTopRect.setY(posTopLeft.y());
			levoTopRect.setWidth(gripTopLeft->width());
			levoTopRect.setHeight(gripTopLeft->height());

			posBottomRight = containtRect.bottomRight();
			desnoSpodajRect.setX(posBottomRight.x());
			desnoSpodajRect.setY(posBottomRight.y());
			desnoSpodajRect.setWidth(gripBottomRight->width());
			desnoSpodajRect.setHeight(gripBottomRight->height());


			if (levoTopRect.contains(scenePt.toPoint()) == true) 
			{

				QApplication::setOverrideCursor(Qt::SizeFDiagCursor);

			}
			else if (desnoSpodajRect.contains(scenePt.toPoint()) == true)
			{
				QApplication::setOverrideCursor(Qt::SizeBDiagCursor);
			}
			else
				QApplication::restoreOverrideCursor();



		if (rubberGripperSelected > 0)
		{
			if (rubberGripperSelected == 1)
			{
				rubberBand->setGeometry(QRect(mouseEvent->pos(), pos2).normalized());
			}
			else if (rubberGripperSelected == 2)
			{
				rubberBand->setGeometry(QRect(pos, mouseEvent->pos()).normalized());
			}
		}

		}


		x = scenePt.x();
		y = scenePt.y();
		ui.labelX->setText(QString("X = %1").arg(x));
		ui.labelY->setText(QString("Y = %1").arg(y));

		if (currImage->buffer->empty())
			return false;

		if ((x < currImage->buffer[0].cols) && (y < currImage->buffer[0].rows) && (x > -1) && (y > -1))
		{
			if (currImage->buffer->channels() == 1)
			{
				b = g = r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x)];
			}
			else
			{
				b = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 0];
				g = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 1];
				r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 2];
			}


			ui.labelG->setText(QString("G = %1").arg(g));
			ui.labelB->setText(QString("B = %1").arg(b));
			ui.labelR->setText(QString("R = %1 ").arg(r));



			return false;
		}


		return true;
	}

	
		if (event->type() == QEvent::MouseButtonPress && QApplication::mouseButtons() == Qt::LeftButton)
		{
			origin = position.toPoint();

			if (!rubberBand)
				rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
			if (createRubber == 1)
			{
				creatingRubber = 1;
				rubberBand->setGeometry(QRect(origin, QSize()));
				pos = origin;
				rubberBand->show();
			}

			if (changeRubberBand == 1)
			{
				QRect containtRect;
				QRect levoTopRect;
				QRect desnoSpodajRect;
				QPointF posiii;
				pos = ui.imageView->mapFromScene(rubberRect[selectedRubberRect].topLeft());
				pos2 = ui.imageView->mapFromScene(rubberRect[selectedRubberRect].bottomRight());
				containtRect = rubberRect[selectedRubberRect];
				posiii = containtRect.topLeft();
				levoTopRect.setX(posiii.x());
				levoTopRect.setY(posiii.y());
				levoTopRect.setWidth(gripTopLeft->width());
				levoTopRect.setHeight(gripTopLeft->height());

				posiii = containtRect.bottomRight();
				desnoSpodajRect.setX(posiii.x());
				desnoSpodajRect.setY(posiii.y());
				desnoSpodajRect.setWidth(gripBottomRight->width());
				desnoSpodajRect.setHeight(gripBottomRight->height());

					if (levoTopRect.contains(scenePt.toPoint()) == true)
					{
						rubberGripperSelected = 1;

					}
					else if (desnoSpodajRect.contains(scenePt.toPoint()) == true)
					{
						rubberGripperSelected = 2;
					}
					else
						rubberGripperSelected = -1;

			}
			if (rubberResizeMode == 1)
			{
				QRect containtRect;
				selectedRubberRect = -1;
				for (int i = 0; i < rubberRect.size(); i++)
				{
					containtRect = rubberRect[i];

					if (containtRect.contains(scenePt.toPoint()) == true)
					{
						selectedRubberRect = i;
						rubberResizeMode = 0;
						break;
					}
				
				}
				if(selectedRubberRect >= 0)
				OnPressedResizeRubber();




			}


			

			return true;
		}
	if (event->type() == QEvent::MouseButtonRelease)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		int bla = 10;
		QPolygonF testPolygon;
		QRectF testR;
		QPoint point2;
		point2 = position.toPoint();

		if (creatingRubber == 1)
		{
			createRubber = 0;
			rubberBand->setGeometry(QRect(pos, point2).normalized());
			testPolygon = ui.imageView->mapToScene(rubberBand->geometry());
			testR = testPolygon.boundingRect();
			rubberRect.push_back(testR.toRect());

			pos2 = point2;
			creatingRubber = 0;
			rubberBand->hide();
			
			RectItem.push_back(new QGraphicsRectItem()); 
			RectItem.back() = scene->addRect(rubberRect.back(), QPen(Qt::yellow), QBrush(Qt::NoBrush));
			rubberRectFunction.push_back(-1);
		}

		if (changeRubberBand == 1)
		{
			if (rubberGripperSelected > 0)
			{
				//changeRubberBand = 0;
				if (rubberGripperSelected == 1)
				{
					rubberBand->setGeometry(QRect(point2, pos2).normalized());
					pos = point2;

				}
				else if (rubberGripperSelected == 2)
				{
					rubberBand->setGeometry(QRect(pos, point2).normalized());
					pos2 = point2;
				}
				rubberGripperSelected = -1;

				//rubberBand->hide();
			}
		}
		return true;
	}

	return false;
}

void imageProcessing::keyPressEvent(QKeyEvent * event)
{
	QPolygonF testPolygon;
	QRectF testR;
	switch (event->key())
	{
	case  Qt::Key_Escape:
		rubberBand->hide();
		rubberResizeMode = 0;
		selectedRubberRect = -1;
		changeRubberBand = -1;
		creatingRubber = -1;
		selectedFunction = -1;
		ClearDraw();

		break;
	case  Qt::Key_Return:
		if (changeRubberBand > 0)
		{
			rubberBand->hide();
			changeRubberBand = 0;
			testPolygon = ui.imageView->mapToScene(rubberBand->geometry().normalized());
			testR = testPolygon.boundingRect();
			rubberRect[selectedRubberRect] = testR.toRect();
			RectItem[selectedRubberRect] = scene->addRect(rubberRect[selectedRubberRect], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		}
		if (selectedFunction > 0)
		{
			if (drawRubberFunction == 1)
			{
				rubberRectFunction[0] = drawRubberFunction;
				OnPressedTestIntensityFunction();
			}
			else if (drawRubberFunction == 30)
			{
				rubberRectFunction[0] = drawRubberFunction;
				AddnewHeightMeasurement();// izrzse prki kvadrat 
			}
			else if (drawRubberFunction == 31)
			{
				rubberRectFunction[0] = drawRubberFunction;
				AddnewHeightMeasurement();//izrise drugi kvadrat
			}
			else if (drawRubberFunction == 32)
			{
				rubberRectFunction[0] = drawRubberFunction;
				AddnewHeightMeasurement();//testiramo funkcijo 

			}
			else if (drawRubberFunction == 33)
			{
				rubberRectFunction[0] = drawRubberFunction;
				LineDeviationMeasurement();//testiramo funkcijo 

			}
			else if (drawRubberFunction == 34)
			{
				rubberRectFunction[0] = drawRubberFunction;
				LineDeviationMeasurement();//testiramo funkcijo 

			}
			//int isChanged = 0;

			/*if (drawRubberFunction == 32)
			{
				ReadPropertyBrowserDynamicFunctions(selectedFunction);
				for (int i = 0; i < 20; i++)
				{
					if (tmpDynamicParameter[i] != tmpDynamicParameterold[i])
					{
						isChanged = 1;
					}
					tmpDynamicParameterold[i] = tmpDynamicParameter[i];
				}
			if (isChanged == 1)
			{
				AddnewHeightMeasurement();//testiramo funkcijo 
			}*/
		

		}


		break;

	
	default:
		break;
	}

}

void imageProcessing::propertyChanged(QtProperty * property)
{
	int bla = 100;
}


void imageProcessing::OnLoadImage()
{
	Mat img;
	QString fileName = QFileDialog::getOpenFileName(this, tr("Load Image"), "C://", "bmp image (*.bmp)");
	String name;

	int channel;
	if (!fileName.isEmpty())
	{
		QMessageBox::information(this, tr("file loaded"), fileName);
		name = fileName.toStdString();

		img = imread(name, -1);
		*currImage->buffer = img;
		ReplaceCurrentBuffer();
	}


	

}

void imageProcessing::OnLoadMultipleImage()
{
	QFileDialog dialog(this);
	Mat img;
	String name;
	dialog.setDirectory(QDir::homePath());
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setNameFilter(trUtf8("image file(*.bmp)"));
	dialog.setDirectory("C://");
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();

	if (!fileNames.isEmpty())
	{
		for (int i = 0; i < fileNames.size(); i++)
		{
			name = fileNames[i].toStdString();
			img = imread(name, -1);
			if (displayWindow < cam.size())
			{
				if (i < cam[displayWindow]->image.size())
				{
					cam[displayWindow]->image[i].buffer[0] = img;
				}
			}
			else
			{
				if (i < images.size())
					*images[i]->buffer = img;

			}
		}
		QMessageBox::information(this, QString("file loaded"),QString("%1 files loaded").arg(fileNames.size()));
		ReplaceCurrentBuffer();
	}

}

void imageProcessing::OnSaveImage()
{
	
	//image.fill(Qt::red); // A red rectangle.
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image "),
		QString(),
		tr("Images (*.bmp)"));

	String name;
	name = fileName.toStdString();
	imwrite(name, *currImage->buffer);

}




void imageProcessing::ZoomOut()
{
	zoomFactor = zoomFactor - 0.1;
	//if (zoomFactor > 0.09)
	//{
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.9, 0.9);
	//}
	//else
	//{
		//zoomFactor = 0.1;
		//ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	//}

}

void imageProcessing::ZoomIn()
{

	zoomFactor = zoomFactor + 0.1;

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	ui.imageView->scale(1.1, 1.1);

}

void imageProcessing::ZoomReset()
{

	zoomFactor = 1;
	ui.imageView->resetTransform();
	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	
}


void imageProcessing::ResizeDisplayRect()
{	
	ui.imageView->setGeometry(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
	ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	//ui.imageView->setSceneRect(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
}





int imageProcessing::FindThreadAndOrientation(int nrCam, int imageIndex, int draw)
{
	//detekcija obracanja
	{
	
		QRect detectInnerRect, detectMiddleCircleTop, detectMiddleCircleBottom, detectThreadRect;
		QRect threadRect[2];

		Mat test;



		int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
		int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
		int  whiteThreshold = 120;
		int blackThreshold = 150;
		int startOffset = 150;
		int orientacija = -1;
		CPointFloat startPoint, topPoint, bottomPoint, centerYpiece;
		vector <CPointFloat> threadPoints;

		centerYpiece.x = types[currentStation][currentType]->prop[0][0]->pointValue.x();
		centerYpiece.y = types[currentStation][currentType]->prop[0][0]->pointValue.y();
		int detectPointRectThreshold = types[currentStation][currentType]->prop[0][1]->intValue;
		int offsetInt = types[currentStation][currentType]->prop[0][2]->intValue;
		int maxFIlter = types[currentStation][currentType]->prop[0][3]->intValue;
		int minFilter = types[currentStation][currentType]->prop[0][4]->intValue;
		float faktorAvrDist = types[currentStation][currentType]->prop[0][5]->doubleValue;


		//MiddleCircleBottom.setCoords(startPoint.x - startOffset - 5, startPoint.y, startPoint.x - startOffset + 5, height - 10);


		detectThreadRect.setCoords(10, centerYpiece.y - 10, centerYpiece.x, centerYpiece.y + 10);

		cam[nrCam]->image[imageIndex].rect = detectThreadRect;
		cam[nrCam]->image[imageIndex].VerticalIntensity(2);


	
		int currInt = 0;
		int savedXStart = 0;
		int savedXStop = 0;
		CPointFloat startPiecePoint, stopPiecePoint;

		bool firstPoint = false;
		int filter = 0;
		for (int x = detectThreadRect.left(); x < detectThreadRect.right(); x++)
		{
			currInt = cam[nrCam]->image[imageIndex].intensity[x];
			if (currInt < detectPointRectThreshold)
			{
				if (filter > 5)
				{
					savedXStart = x - filter;
					startPiecePoint.SetPointFloat(QPoint(savedXStart, centerYpiece.y));
					break;
				}
				filter++;
			}
			else
			{
				filter = 0;
			}
		}
		filter = 0;
		for (int x = detectThreadRect.right(); x > detectThreadRect.left(); x--)
		{
			currInt = cam[nrCam]->image[imageIndex].intensity[x];
			if (currInt < detectPointRectThreshold)
			{
				if (filter > 5)
				{
					savedXStop = x + filter;
					stopPiecePoint.SetPointFloat(QPoint(savedXStop, centerYpiece.y));
					break;
				}
				filter++;
			}
			else
			{
				filter = 0;
			}
		}

		int numPoints = 0;
		float avrage = 0;
		for (int x = startPiecePoint.x; x < stopPiecePoint.x; x++)
		{
			currInt = cam[nrCam]->image[imageIndex].intensity[x];
			numPoints++;
			avrage += currInt;
		}
		avrage = avrage / numPoints;
		
		int threadTheshold = avrage + offsetInt;
		int l = 0;


		int stPix = 0;
		int prevStPix = 0;
		bool whiteToBlack = false;
		bool blackToWhite = false;
		int startThread;
		int tezisceX = 0;
		vector <CPointFloat> centerThread;
		for (int x = startPiecePoint.x + 6; x < stopPiecePoint.x - 6; x++)
		{
			currInt = cam[nrCam]->image[imageIndex].intensity[x];
			if (currInt > threadTheshold)
			{
				whiteToBlack = false;
				if (blackToWhite == false)
				{
					blackToWhite = true;
					startThread = x;
				}
				stPix++;
			}
			else
			{
				if (blackToWhite == true)
					whiteToBlack = true;
				prevStPix = stPix;
				stPix = 0;
			}
			if ((whiteToBlack) && (blackToWhite))
			{
				whiteToBlack = false;
				blackToWhite = false;
				tezisceX = 0;
				if ((prevStPix >= minFilter) && (prevStPix < maxFIlter))
				{
					for (int k = 0; k < prevStPix; k++)
					{
						tezisceX += startThread + k;
						if (draw)
						{
							CPointFloat draw;
							draw.SetPointFloat(startThread + k, centerYpiece.y);
							scene->addItem(draw.DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
						}
					}
					tezisceX /= prevStPix;
					centerThread.push_back(CPointFloat(tezisceX, centerYpiece.y));
					if (draw)
						scene->addItem(centerThread.back().DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 8));
				}
				startThread = 0;
			}

		}

		/////////procesiranje navojev stetje navojev
		int avrDistance = 0;
		int newAvrDistance = 0;
		int umes = 0;
	
		vector <CPointFloat> realThreadPoints;
		if ((centerThread.size() > 1))
		{
			for (int i = 0; i < centerThread.size(); i++)
			{
				umes = 0;

				 if (i <= centerThread.size() - 2)
				{
					umes = (centerThread[i + 1].x - centerThread[i].x);

				}

				avrDistance += umes;
			}
			avrDistance /= centerThread.size()-1;

			newAvrDistance = avrDistance * faktorAvrDist;


			for (int i = 0; i < centerThread.size(); i++)
			{
				umes = 0;

				 if (i == 0)
				{
				umes = (centerThread[i + 1].x - centerThread[i].x);
				}
				else if (i < centerThread.size())
				{
					umes = (centerThread[i].x -centerThread[i -1].x );
				}


				//{
				//	umes = (centerThread[i].x - centerThread[i - 1].x);

			//	}
				if ((umes > 0)&&(umes < newAvrDistance))
					realThreadPoints.push_back(centerThread[i]);
			}

			int distance = (stopPiecePoint.x - startPiecePoint.x) /4;

			int zadnjaCetrt = stopPiecePoint.x - distance;

			int count = 0;

			if (realThreadPoints.size() >= 2)
			{

				measureObject->stNavojev = realThreadPoints.size();
				for (int i = 0; i < realThreadPoints.size(); i++)
				{
					if ((realThreadPoints[i].x > zadnjaCetrt) && (realThreadPoints[i].x < stopPiecePoint.x))
					{
						count++;
					}
				}
				if (count > 1)
				{
					measureObject->orientation = 0;
				}
				else 
					measureObject->orientation = 1;
		

			}
			else
			{
				measureObject->stNavojev = 0;
				measureObject->orientation = -1;
			}

		}
		else
		{
			measureObject->stNavojev = 0;
			measureObject->orientation = -1;
		}

		//

		if (draw)
		{
			ui.listFunctionsReturn->item(0)->setText(QString("povprecje = %1").arg(avrage));
			ui.listFunctionsReturn->item(1)->setText(QString("Povprecje za iskanje navoja = %1").arg(threadTheshold));
			ui.listFunctionsReturn->item(2)->setText(QString("povprecna razdalja med navoji = %1").arg(newAvrDistance));
			ui.listFunctionsReturn->item(3)->setText(QString("orientacija = %1").arg(measureObject->orientation));
			ui.listFunctionsReturn->item(4)->setText(QString("st. navojev = %1").arg(measureObject->stNavojev));
			for (int i = 0; i < realThreadPoints.size(); i++)
			{
				scene->addItem(realThreadPoints[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 8));
			}
		}


		QRect QrectDraw;
		QrectDraw.setRight(centerYpiece.x);
		QrectDraw.setX(0);
		QrectDraw.setY(centerYpiece.y-160);
		QrectDraw.setHeight(320);
		

		Rect drawRect;

		drawRect.x = QrectDraw.x();

		drawRect.y = QrectDraw.y();
		if (drawRect.y < 0)
			drawRect.y = 0;
		drawRect.height = QrectDraw.height();
		drawRect.width = QrectDraw.width();
	
		int offsetDrawX, offsetDrawY;
		offsetDrawX = 0;
		offsetDrawY = 0 - drawRect.y;

		QFont font("MS Shell Dlg 2");
		font.setPointSize(48);

		drawImageMain[0] = cam[nrCam]->image[imageIndex].buffer[0](drawRect).clone();

		//mainScreenDrawTekoci[0].AddPoint(CPointFloat(startPiecePoint.x + offsetDrawX, startPiecePoint.y + offsetDrawY), 20, QPen(Qt::red), QBrush(Qt::red), "bla", CPointFloat(startPiecePoint.x + 50 +offsetDrawX, startPiecePoint.y + 50+ offsetDrawY), false,font);
		//mainScreenDrawTekoci[0].AddPoint(CPointFloat(stopPiecePoint.x + offsetDrawX, stopPiecePoint.y + offsetDrawY), 20, QPen(Qt::red), QBrush(Qt::red), "bla", CPointFloat(stopPiecePoint.x + 50 +offsetDrawX, stopPiecePoint.y + 50+ offsetDrawY), false,font);
		for (int i = 0; i < realThreadPoints.size(); i++)
		{
			mainScreenDrawTekoci[0].AddPoint(CPointFloat(realThreadPoints[i].x + offsetDrawX, realThreadPoints[i].y + offsetDrawY), 15, QPen(Qt::green), QBrush(Qt::green), QString("%1").arg(i+1), CPointFloat(realThreadPoints[i].x+offsetDrawX, realThreadPoints[i].y + offsetDrawY), true, font);
		}
		//Mat nov = cam[nrCam]->image[imageIndex].buffer[0].clone();
		mainScreenDrawTekoci[0].AddImage(drawImageMain[0]);

		
		


		if (draw)
		{
			scene->addRect(detectInnerRect, QPen(Qt::blue), QBrush(Qt::NoBrush));
			scene->addRect(detectMiddleCircleTop, QPen(Qt::red), QBrush(Qt::NoBrush));
			scene->addItem(topPoint.DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addRect(detectMiddleCircleBottom, QPen(Qt::green), QBrush(Qt::NoBrush));
			scene->addItem(bottomPoint.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
			scene->addItem(startPoint.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
			//scene->addRect(detectThreadRect, QPen(Qt::magenta), QBrush(Qt::NoBrush));
			scene->addItem(centerYpiece.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 10));
			scene->addItem(cam[nrCam]->image[imageIndex].DrawIntensity(QPen(Qt::yellow)));
			scene->addItem(stopPiecePoint.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 10));
			scene->addItem(startPiecePoint.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 10));
			/*for (int i = 0; i < threadPoints.size(); i++)
			{
				scene->addItem(threadPoints[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
			}*/


		}




		return 0;
	}
}

int imageProcessing::FindInnerAndOuterRadij(int nrCam, int imageIndex, int draw)
{
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;


	int threshold = 120;
	


	CPointFloat startPointCenter;
	startPointCenter = CPointFloat(width / 2, height / 2);


	int rMinInner = 50;
	int rMaxInner = 2000;
	QRect imageRect;
	imageRect.setCoords(20, 20, width - 20, height - 20);
	vector<float> angles;
	vector<CPointFloat> edgePointsInner;
	float edgesShare;
	
	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 40, false, threshold, startPointCenter, rMinInner, rMaxInner, false, false, angles, edgePointsInner, &edgesShare);
	//ni kosa 
	CCircle innerCircle, outerCircle;;

	innerCircle.SetCircleNpoints(edgePointsInner, 50, 1000);
	CPointFloat pieceCenter = innerCircle.GetCenter();
	if (draw)
	{
		scene->addItem(startPointCenter.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		for (int i = 0; i < edgePointsInner.size(); i++)
		{
			scene->addItem(edgePointsInner[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
		}
		scene->addItem(innerCircle.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 50));
	}

	CLine vertikalCenterLine;
	CPointFloat topPiece, bottomPiece;

	vertikalCenterLine.SetLine(pieceCenter, CPointFloat(pieceCenter.x, 20));
	topPiece = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(vertikalCenterLine, false, 2, false, threshold,100,true);
	vertikalCenterLine.SetLine(pieceCenter, CPointFloat(pieceCenter.x, height-20));
	bottomPiece = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(vertikalCenterLine, false, 3, false, threshold, 100, true);

	if (draw)
	{
		scene->addItem(topPiece.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		scene->addItem(bottomPiece.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
	}

	CLine horizontalLineTop, horizontalLineBottom;
	CPointFloat oporaLevoTop[2], oporaDesnoTop[2], oporaLevoBottom[2], oporaDesnoBottom[2];
	
	int offset = 50;

	horizontalLineTop.SetLine(CPointFloat(topPiece.x, topPiece.y- offset), CPointFloat(offset, topPiece.y - offset));
	oporaLevoTop[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineTop, true, 3, false, threshold, 30, true);
	oporaLevoTop[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineTop, true, 3, true, threshold, 30, true);
	if (draw)
		scene->addItem(horizontalLineTop.DrawSegment(QPen(Qt::green), 1));
	//scene->addItem(horizontalLineTop.DrawSegment(QPen(Qt::green), 1));

	horizontalLineTop.SetLine(CPointFloat(topPiece.x, topPiece.y- offset), CPointFloat(width- offset, topPiece.y - offset));
	
	oporaDesnoTop[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineTop, true, 3, false, threshold, 30, true);
	oporaDesnoTop[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineTop, true, 3, true, threshold, 30, true);
	//scene->addItem(horizontalLineTop.DrawSegment(QPen(Qt::green), 2));
	if( draw)
		scene->addItem(horizontalLineTop.DrawSegment(QPen(Qt::red), 1));

	horizontalLineBottom.SetLine(CPointFloat(bottomPiece.x, bottomPiece.y + offset), CPointFloat(offset, bottomPiece.y + offset));
	if (draw)
	scene->addItem(horizontalLineBottom.DrawSegment(QPen(Qt::green), 1));
	oporaLevoBottom[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineBottom, true, 3, false, threshold, 30, true);
	oporaLevoBottom[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineBottom, true, 3, true, threshold, 30, true);
	//scene->addItem(horizontalLineTop.DrawSegment(QPen(Qt::green), 1));

	horizontalLineBottom.SetLine(CPointFloat(bottomPiece.x, bottomPiece.y + offset), CPointFloat(width+ offset, bottomPiece.y + offset));
	if (draw)
	scene->addItem(horizontalLineBottom.DrawSegment(QPen(Qt::red), 1));
	oporaDesnoBottom[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineBottom, true, 3, false, threshold, 30, true);
	oporaDesnoBottom[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(horizontalLineBottom, true, 3, true, threshold, 30, true);

	 

	CLine oporaLevoLine[2], oporaDesnoLine[2];
	CLine parallelOporaLevo[2], parallelOporaDesno[2];
	CLine topLine, bottomLine;

	for (int i = 0; i < 2; i++)
	{
		oporaLevoLine[i].SetLine(oporaLevoTop[i], oporaLevoBottom[i]);
		
		oporaDesnoLine[i].SetLine(oporaDesnoTop[i], oporaDesnoBottom[i]);
	}
	topLine.SetLine(oporaLevoTop[0], oporaDesnoTop[0]);
	bottomLine.SetLine(oporaLevoBottom[0], oporaDesnoBottom[0]);
	
	parallelOporaLevo[0] = oporaLevoLine[0].GetParallel(20);
	parallelOporaLevo[1] = oporaLevoLine[1].GetParallel(-20);
	parallelOporaDesno[0] = oporaDesnoLine[0].GetParallel(-20);
	parallelOporaDesno[1] = oporaDesnoLine[1].GetParallel(20);

	for (int i = 0; i < 2; i++)
	{
		parallelOporaLevo[i].p1 = parallelOporaLevo[i].GetIntersectionPoint(topLine);
		parallelOporaDesno[i].p1 = parallelOporaDesno[i].GetIntersectionPoint(topLine);
		parallelOporaLevo[i].p2 = parallelOporaLevo[i].GetIntersectionPoint(bottomLine);
		parallelOporaDesno[i].p2 = parallelOporaDesno[i].GetIntersectionPoint(bottomLine);
	}

	CPointFloat omejitevPointLevoZg[2], omejitevPointDesnoZg[2];
	CPointFloat omejitevPointLevoSp[2], omejitevPointDesnoSp[2];

	for (int i = 0; i < 2; i++)
	{
		omejitevPointLevoZg[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(parallelOporaLevo[i], true, 3, false, threshold, 30, true);
		omejitevPointLevoSp[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(parallelOporaLevo[i], false, 3, false, threshold, 30, true);

		

		omejitevPointDesnoZg[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(parallelOporaDesno[i], true, 3, false, threshold, 30, true);
		omejitevPointDesnoSp[i] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(parallelOporaDesno[i], false, 3, false, threshold, 30, true);
	}
	if (draw)
	{
		for (int i = 0; i < 2; i++)
		{
			scene->addItem(oporaLevoTop[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
			scene->addItem(oporaDesnoTop[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
			scene->addItem(oporaLevoBottom[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
			scene->addItem(oporaDesnoBottom[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
			scene->addItem(oporaLevoLine[i].DrawSegment(QPen(Qt::green), 1));
			scene->addItem(oporaDesnoLine[i].DrawSegment(QPen(Qt::green), 1));
			scene->addItem(parallelOporaLevo[i].DrawSegment(QPen(Qt::blue), 1));
			scene->addItem(parallelOporaDesno[i].DrawSegment(QPen(Qt::blue), 1));
			scene->addItem(omejitevPointLevoZg[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addItem(omejitevPointLevoSp[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addItem(omejitevPointDesnoZg[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
			scene->addItem(omejitevPointDesnoSp[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
		}
	}

	float omejitve[8];
	CLine omejitveLine[8];

	float fiDeg;

	float dy[8];
	float dx[8];
	dy[0] = omejitevPointDesnoZg[1].y - pieceCenter.y;
	dx[0] = omejitevPointDesnoZg[1].x - pieceCenter.x;
	dy[1] = omejitevPointDesnoZg[0].y - pieceCenter.y;
	dx[1] = omejitevPointDesnoZg[0].x - pieceCenter.x;

	dy[2] = omejitevPointLevoZg[0].y - pieceCenter.y;
	dx[2] = omejitevPointLevoZg[0].x - pieceCenter.x;
	dy[3] = omejitevPointLevoZg[1].y - pieceCenter.y;
	dx[3] = omejitevPointLevoZg[1].x - pieceCenter.x;

	dy[4] = omejitevPointLevoSp[1].y - pieceCenter.y;
	dx[4] = omejitevPointLevoSp[1].x - pieceCenter.x;
	dy[5] = omejitevPointLevoSp[0].y - pieceCenter.y;
	dx[5] = omejitevPointLevoSp[0].x - pieceCenter.x;

	dy[6] = omejitevPointDesnoSp[0].y - pieceCenter.y;
	dx[6] = omejitevPointDesnoSp[0].x - pieceCenter.x;
	dy[7] = omejitevPointDesnoSp[1].y - pieceCenter.y;
	dx[7] = omejitevPointDesnoSp[1].x - pieceCenter.x;

	float fiRad;
	for (int i = 0; i < 8; i++)
	{
		fiDeg = atan2(dy[i], dx[i]) * 180.0 / M_PI;	//atan2 vrne kot med -Pi in Pi

		if (fiDeg < 0)
			fiDeg += 360;  //Spravimo kot na interval 0 do 360 stopinj
		fiRad = fiDeg * M_PI / 180;
		omejitve[i] = fiRad;
		measureObject->omittedAngles[i] = fiRad;
	}
		omejitveLine[0].SetLine(pieceCenter, omejitevPointDesnoZg[0]);
		omejitveLine[1].SetLine(pieceCenter, omejitevPointDesnoZg[1]);

		omejitveLine[2].SetLine(pieceCenter, omejitevPointLevoZg[0]);
		omejitveLine[3].SetLine(pieceCenter, omejitevPointLevoZg[1]);


		omejitveLine[4].SetLine(pieceCenter, omejitevPointLevoSp[0]);
		omejitveLine[5].SetLine(pieceCenter, omejitevPointLevoSp[1]);


		omejitveLine[6].SetLine(pieceCenter, omejitevPointDesnoSp[0]);
		omejitveLine[7].SetLine(pieceCenter, omejitevPointDesnoSp[1]);




		if (draw)
		{
			for (int i = 0; i < 8; i++)
			{
			scene->addItem(omejitveLine[i].DrawSegment(QPen(Qt::red), 1));

			}
		}

		rMinInner = innerCircle.GetRadius() +100;

		vector<float> anglesOuter;
		vector<CPointFloat> edgePointsOuter;

		cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 30, true, threshold, pieceCenter, rMinInner, rMaxInner, true, OmitAngle, anglesOuter, edgePointsOuter, &edgesShare);

		outerCircle.SetCircleNpoints(edgePointsOuter, rMinInner, rMaxInner);

		if (draw)
		{
			for (int i = 0; i < edgePointsOuter.size(); i++)
			{
				scene->addItem(edgePointsOuter[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			}
			scene->addItem(outerCircle.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 50));
		}

		float Rextreme[2];  //indeks izbere min/max
		float fiRextreme[2]; //indeks izbere min/max
		float maxStDevLocal;
		float fiMaxStDevLocal;
		float maxStDevLocalToGlobal;
		float fiMaxStDevLocalToGlobal;
		float stDevGlobal;
		int minSectorPoints;
		int maxSectorPoints;
		if (anglesOuter.size() > 0)
		{
			outerCircle.CircleStDev(anglesOuter, edgePointsOuter, 30*M_PI/180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
			measureObject->outerDev = maxStDevLocalToGlobal;
		}
		else
			measureObject->outerDev = 100;

		if (angles.size() > 0)
		{
			innerCircle.CircleStDev(angles, edgePointsInner, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
			measureObject->innerDev = maxStDevLocalToGlobal;
		}
		else
			measureObject->innerDev = 100;


		//racunanje standardne deviacije znotraj in zunaj 
		vector<float> odstopanjeInner;
		vector<float> odstopanjeOuter;
		CPointFloat tmpCenterInner = innerCircle.GetCenter();
		CPointFloat tmpCenterOut = outerCircle.GetCenter();
		float avrOdstopanjeInner = 0;
		float avrOdstopanjeOut = 0;

	

		float tmpDistance = 0;
		for(int i = 0; i < edgePointsInner.size();i++)
		{
			tmpDistance = edgePointsInner[i].GetDistance(tmpCenterInner);
			odstopanjeInner.push_back(tmpDistance - innerCircle.radius);
			avrOdstopanjeInner += odstopanjeInner[i];
		}
		for (int i = 0; i < edgePointsOuter.size(); i++)
		{
			tmpDistance = edgePointsOuter[i].GetDistance(tmpCenterOut);
			odstopanjeOuter.push_back(tmpDistance - outerCircle.radius);
			avrOdstopanjeOut = odstopanjeOuter[i];
		}
		if(odstopanjeInner.size()> 0)
		avrOdstopanjeInner /= odstopanjeInner.size();
	
		if (odstopanjeOuter.size() > 0)
		avrOdstopanjeOut /= odstopanjeOuter.size();
		//zaenkrat racunam stadstopanjeInner.size();ndardno deviacijo po celem krogu.
		float innerDev =0;
		float outerDev = 0;
		for (int i = 0; i < odstopanjeInner.size(); i++)
		{
			innerDev +=pow(2, (odstopanjeInner[i] - avrOdstopanjeInner));
		}
		if( odstopanjeInner.size() > 0)
		innerDev /= odstopanjeInner.size(); //za 20 stopinj 

		for (int i = 0; i < odstopanjeOuter.size(); i++)
		{
			outerDev += pow(2, (odstopanjeOuter[i] - avrOdstopanjeOut));
		}
		if (edgePointsOuter.size() > 0)
			outerDev /= edgePointsOuter.size(); //za 20 stopinj 


	


		measureObject->innerCircle = innerCircle;
		measureObject->outerCircle = outerCircle;

		

	return 0;
}

bool imageProcessing::IsPiecePresent(int nrCam, int imageIndex, QRect area,int thresHoldIntensity,int nrPoints, int draw)
{
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	int avrInt = 0;
	int detectedPoints;
	if (draw)
	{
		QPen pen;
		pen.setWidth(2);
		scene->addRect(area, pen, QBrush(Qt::NoBrush));
	}

	cam[nrCam]->image[imageIndex].rect = area;
	if ((area.left() > 0) && (area.top() > 0) && (area.width() < width) && (area.height() < height) && (area.right() < width) && (area.bottom() < height))
	{
		avrInt = cam[nrCam]->image[imageIndex].GetAverageIntensity(area);
		
		cam[nrCam]->image[imageIndex].CreateIntensity();
		if (width > height)
			cam[nrCam]->image[imageIndex].HorizontalIntensity(2);
		else
			cam[nrCam]->image[imageIndex].VerticalIntensity(2);
		cam[nrCam]->image[imageIndex].DetectTransitions(40);

		detectedPoints = cam[nrCam]->image[imageIndex].detectedPoints.size();

		if ((detectedPoints >= nrPoints) && (avrInt < thresHoldIntensity))
		{

			return true;

		}
		else
			return false;

	}

	return false;
}

int imageProcessing::FindTopAndBottom(int nrCam, int imageIndex, int draw)
{
	int threshold = 120;
	int levoDesnoOffset = 80;
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	QRect imageRect;
	QRect zgorajRect, spodajRect;
	imageRect.setCoords(20, 20, width - 20, height - 20);
	CLine vertikalLine;
	CLine findOporeLevoLine;
	CLine findOporeDesnoLine;
	CLine findRightEndLine;
	CLine findLeftEndLine;
	CLine findRightEndBottomLine;
	CLine findLeftEndBottomLine;
	CLine findLeftEndMiddleLine, findRightEndMiddleLine;
	CLine oporaLine[4];
	CPointFloat oporaLevo[2];
	CPointFloat oporaDesno[2];
	CPointFloat levoMax, desnoMax, levoMaxSpodaj, desnoMaxSpodaj, levoMaxMiddle, desnoMaxMiddle;


	
	vertikalLine.SetLine(CPointFloat(width / 2, height - 10), CPointFloat(width / 2, 20));
	CPointFloat spodajStart, zgorajStart;
	spodajStart = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(vertikalLine, true, 2, false, threshold, 50, true);
	zgorajStart = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(vertikalLine, true, 2, true, threshold, 50, true);
	zgorajStart = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(vertikalLine, true, 2, true, threshold, 50, true);



	findOporeLevoLine.SetLine(CPointFloat(10, spodajStart.y + 30), CPointFloat(width/2, spodajStart.y + 30));
	findOporeDesnoLine.SetLine(CPointFloat(width/2, spodajStart.y + 30), CPointFloat(width-10, spodajStart.y + 30));

	findLeftEndLine.SetLine(CPointFloat(10, zgorajStart.y + 100), CPointFloat(width/2, zgorajStart.y + 100));
	findRightEndLine.SetLine(CPointFloat(width/2, zgorajStart.y + 100), CPointFloat(width-10, zgorajStart.y + 100));

	findLeftEndBottomLine.SetLine(CPointFloat(10, spodajStart.y - 100), CPointFloat(width / 2, spodajStart.y - 100));
	findRightEndBottomLine.SetLine(CPointFloat(width/2, spodajStart.y - 100), CPointFloat(width-10, spodajStart.y - 100));

	int sred = ((spodajStart.y - zgorajStart.y) / 2) + zgorajStart.y;
	findLeftEndMiddleLine.SetLine(CPointFloat(10, sred), CPointFloat(width / 2, sred));
	findRightEndMiddleLine.SetLine(CPointFloat(width / 2, sred), CPointFloat(width - 10, sred));

	oporaLevo[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findOporeLevoLine, true, 2, false, threshold, 20, true);
	oporaLevo[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findOporeLevoLine, false, 2, false, threshold, 20, true);

	oporaDesno[0] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findOporeDesnoLine, true, 2, false, threshold, 20, true);
	oporaDesno[1] = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findOporeDesnoLine, false, 2, false, threshold, 20, true);

	levoMax = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findLeftEndLine, false, 2, true, threshold, 2, true);
	desnoMax = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findRightEndLine, true, 2, true, threshold, 2, true);

	levoMaxSpodaj = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findLeftEndBottomLine, false, 2, true, threshold, 2, true);
	desnoMaxSpodaj = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findRightEndBottomLine, true, 2, true, threshold, 2, true);

	levoMaxMiddle = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findLeftEndMiddleLine, false, 2, true, threshold, 2, true);
	desnoMaxMiddle = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(findRightEndMiddleLine, true, 2, true, threshold, 2, true);

	

	
	float middleZgoraj = levoMax.GetDistance(desnoMax)/2;
	float middleSpodaj = levoMaxSpodaj.GetDistance(desnoMaxSpodaj)/2;

	CLine simetrala;
	simetrala.SetLine(CPointFloat(levoMaxSpodaj.x + middleSpodaj, levoMaxSpodaj.y), CPointFloat(levoMax.x + middleZgoraj, levoMax.y));

	CPointFloat topOfSimetralaPoint;
	CLine topLine;
	topLine.SetLine(CPointFloat(10, 10), CPointFloat(2000, 10));
	CPointFloat intersecttmp;


	intersecttmp = simetrala.GetIntersectionPoint(topLine);
	simetrala.p2 = intersecttmp;

	topOfSimetralaPoint = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(simetrala, true, 1, true, 120, 10, false);


	measureObject->topOfSimetralaPoint = topOfSimetralaPoint;
	measureObject->simetrala = simetrala;

		oporaLine[0].SetLine(oporaLevo[0], 90);
		oporaLine[1].SetLine(oporaLevo[1], 90);
		oporaLine[2].SetLine(oporaDesno[0], 90);
		oporaLine[3].SetLine(oporaDesno[1], 90);
	

		

		/*zgorajRect.setCoords(levoMax.x + levoDesnoOffset, zgorajStart.y - 50, desnoMax.x - levoDesnoOffset, zgorajStart.y + 50);
		spodajRect.setCoords(oporaLevo[1].x + 20, spodajStart.y - 50, oporaDesno[0].x - 20, spodajStart.y + 50);

		vector <CPointFloat> zgorajTocke;
		vector <CPointFloat> spodajTocke;
		CRectRotated tmpRect;
		tmpRect = zgorajRect;

		cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 1, zgorajRect.width()/3, 1, true, threshold, zgorajTocke, 1, false);
		tmpRect = spodajRect;
		cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 0, spodajRect.width() / 3, 1, true, threshold, spodajTocke, 1, false);
		CLine zgorajLine, spodajLine;

		zgorajLine.SetLine(zgorajTocke);
		spodajLine.SetLine(spodajTocke);
		CPointFloat visinaPoint = zgorajLine.GetIntersectionPoint(simetrala);
		CPointFloat spodajVisinaPoint = spodajLine.GetIntersectionPoint(simetrala);

		CLine drawLine;
		drawLine.SetLine(visinaPoint, spodajVisinaPoint);

		float pieceHeight = visinaPoint.GetDistance(spodajVisinaPoint);
		QPen pen(Qt::red);
		pen.setWidth(15);
		QFont font("MS Shell Dlg 2");
		font.setPointSize(48);

		measureObject->visina[0] = pieceHeight;

		CPointFloat midd = simetrala.GetMiddlePoint();
		mainScreenDrawTekoci[2].AddArrow(drawLine, pen, 50, 2, types[2][currentType]->name[0], CPointFloat(midd.x+50, midd.y+50), true, font);
		mainScreenDrawTekoci[2].AddPoint(CPointFloat(200 , 200), 15, QPen(Qt::green), QBrush(Qt::green), QString("%1").arg(12), CPointFloat(300 , 300), true, font);
		//mainScreenDrawTekoci[0].AddPoint(CPointFloat(realThreadPoints[i].x + offsetDrawX, realThreadPoints[i].y + offsetDrawY), 15, QPen(Qt::green), QBrush(Qt::green), QString("%1").arg(i + 1), CPointFloat(realThreadPoints[i].x + offsetDrawX, realThreadPoints[i].y + offsetDrawY), true, font);
		measureObject->levRob[0] = levoMax;
		measureObject->levRob[1] = levoMaxMiddle;
		measureObject->levRob[2] = levoMaxSpodaj;
		measureObject->desniRob[0] = desnoMax;
		measureObject->desniRob[1] = desnoMaxMiddle;
		measureObject->desniRob[2] = desnoMaxSpodaj;

		measureObject->spodajTocka = spodajStart;
		measureObject->zgorajTocka = zgorajStart;
		measureObject->zgorajLine = zgorajLine;
		measureObject->spodajLine = spodajLine;
		*/

	if (draw)
	{
		//scene->addItem(zgorajStart.DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 20));
		//scene->addItem(spodajStart.DrawDot(QPen(Qt::red), QBrush(Qt::red), 20));
		//scene->addItem(spodajStart.DrawDot(QPen(Qt::red), QBrush(Qt::red), 20));
		scene->addItem(topOfSimetralaPoint.DrawDot(QPen(Qt::green), QBrush(Qt::green), 20));

		for (int i = 0; i < 3; i++)
		{
			scene->addItem(measureObject->levRob[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 20));
			scene->addItem(measureObject->desniRob[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 20));
		}

		/*for (int i = 0; i < 2; i++)
		{
			scene->addItem(oporaLevo[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 5));
			scene->addItem(oporaDesno[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));
		}
		for (int i = 0; i < 4; i++)
		{
			scene->addItem(oporaLine[i].DrawLine(imageRect,QPen(Qt::green)));
		}
		scene->addRect(zgorajRect, QPen(Qt::red), QBrush(Qt::NoBrush));
		scene->addRect(spodajRect, QPen(Qt::red), QBrush(Qt::NoBrush));

		for (int i = 0; i < zgorajTocke.size(); i++)
		{
			scene->addItem(zgorajTocke[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
		}
		for (int i = 0; i < spodajTocke.size(); i++)
		{
			scene->addItem(spodajTocke[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
		}
		scene->addItem(zgorajLine.DrawLine(imageRect, QPen(Qt::green)));
		scene->addItem(spodajLine.DrawLine(imageRect, QPen(Qt::green)));*/
		scene->addItem(simetrala.DrawLine(imageRect, QPen(Qt::blue)));
	}

	
	//float distance = zgorajLine.GetDistance(spodajLine);

	return 0;
}

int imageProcessing::FindLeftAndRight(int nrCam, int imageIndex, int draw)
{
	int threshold = 120;
	int levoDesnoOffset = 80;
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	CPointFloat topPiece = measureObject->zgorajTocka;
	CPointFloat bottomPiece = measureObject->spodajTocka;
	CLine simetrala = measureObject->simetrala;
	CPointFloat leftPiece = measureObject->levRob[1];
	CPointFloat rightPiece = measureObject->desniRob[1];
	QRect imageRect;
	imageRect.setCoords(20, 20, width - 20, height - 20);

	QRect levo1;
	QRect desno1;

	levo1.setCoords(leftPiece.x - 50, topPiece.y + 50, leftPiece.x + 50, topPiece.y + 450);
	desno1.setCoords(rightPiece.x - 50, topPiece.y + 50, rightPiece.x + 50, topPiece.y + 450);

	if (draw)
	{
		scene->addRect(levo1, QPen(Qt::green), QBrush(Qt::NoBrush));
		scene->addRect(desno1, QPen(Qt::green), QBrush(Qt::NoBrush));
	}

	vector <CPointFloat> levoTocke1;
	vector <CPointFloat> desnoTocke1;
	CRectRotated tmpRect;
	CLine levo1Line, desno1Line;
	
	tmpRect = levo1;
	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 3, tmpRect.width() / 3, 1, true, threshold, levoTocke1, 1, false);
	tmpRect = desno1;
	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 2, tmpRect.width() / 3, 1, true, threshold, desnoTocke1, 1, false);


	levo1Line.SetLine(levoTocke1);
	desno1Line.SetLine(desnoTocke1);

	float sirina1;
	float sirinaAvr = 0;
	float minSirina1 = 4000;
	float maxSirina1 = 0;
	CPointFloat tmp;
	CLine tmpLine;
	CLine drawLine;


	QPen pen(Qt::yellow);
	pen.setWidth(15);
	QFont font("MS Shell Dlg 2");
	font.setPointSize(48);



	for (int i = 0; i < levoTocke1.size(); i++)
	{
		tmpLine = levo1Line.GetPerpendicular(levoTocke1[i]);
		tmp = desno1Line.GetIntersectionPoint(tmpLine);
			sirina1 = levoTocke1[i].GetDistance(tmp);
			if (minSirina1 < sirina1)
				minSirina1 = sirina1;
			if (maxSirina1 > sirina1)
				maxSirina1 = sirina1;

			sirinaAvr += sirina1;

			if (i == (int)levoTocke1.size() / 2)
			{

				CPointFloat tmpsimPoint = tmpLine.GetIntersectionPoint(simetrala);
				drawLine.SetLine(levoTocke1[i], tmp);
				mainScreenDrawTekoci[2].AddArrow(drawLine, pen, 50, 2, types[2][currentType]->name[1], CPointFloat(tmpsimPoint.x + 100, tmpsimPoint.y + 5), true, font);

			}

	}

	sirinaAvr /= levoTocke1.size();

	measureObject->sirina[0] = sirinaAvr;

	if (draw)
	{
		for (int i = 0; i < levoTocke1.size(); i++)
		{
			scene->addItem(levoTocke1[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		}
		for (int i = 0; i < desnoTocke1.size(); i++)
		{
			scene->addItem(desnoTocke1[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		}

		scene->addItem(levo1Line.DrawLine(imageRect, QPen(Qt::green)));
		scene->addItem(desno1Line.DrawLine(imageRect, QPen(Qt::green)));
	}





	QRect levo2;
	QRect desno2;


	levo2.setCoords(leftPiece.x - 60, bottomPiece.y - 50, leftPiece.x + 350, bottomPiece.y -200);
	desno2.setCoords(rightPiece.x - 350, bottomPiece.y - 60, rightPiece.x + 50, bottomPiece.y - 200);

	if (draw)
	{
		scene->addRect(levo2, QPen(Qt::green), QBrush(Qt::NoBrush));
		scene->addRect(desno2, QPen(Qt::green), QBrush(Qt::NoBrush));

	}

	vector <CPointFloat> levoTocke2;
	vector <CPointFloat> desnoTocke2;

	CLine levo2Line, desno2Line;

	tmpRect = levo2;
	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 3, tmpRect.width() / 3, 1, true, threshold, levoTocke2, 1, false);
	tmpRect = desno2;
	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect, 2, tmpRect.width() / 3, 1, true, threshold, desnoTocke2, 1, false);


	levo2Line.SetLine(levoTocke2);
	desno2Line.SetLine(desnoTocke2);


	if (draw)
	{
		for (int i = 0; i < levoTocke2.size(); i++)
		{
			scene->addItem(levoTocke2[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		}
		for (int i = 0; i < desnoTocke2.size(); i++)
		{
			scene->addItem(desnoTocke2[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));
		}

		scene->addItem(levo2Line.DrawLine(imageRect, QPen(Qt::blue)));
		scene->addItem(desno2Line.DrawLine(imageRect, QPen(Qt::blue)));
	}

	float sirina2;
	float sirina2Avr = 0;
	float minSirina2 = 4000;
	float maxSirina2 = 0;
	QPen pen2(Qt::green);
	pen2.setWidth(15);
	for (int i = 0; i < levoTocke2.size(); i++)
	{
		tmpLine = levo2Line.GetPerpendicular(levoTocke2[i]);
		tmp = desno2Line.GetIntersectionPoint(tmpLine);
		sirina2 = levoTocke2[i].GetDistance(tmp);
		if (minSirina2 < sirina2)
			minSirina2 = sirina2;
		if (maxSirina2 > sirina2)
			maxSirina2 = sirina2;

		sirina2Avr += sirina2;



		if (i == (int)levoTocke2.size() / 2)
		{

			CPointFloat tmpsimPoint = tmpLine.GetIntersectionPoint(simetrala);
			drawLine.SetLine(levoTocke2[i], tmp);
			mainScreenDrawTekoci[2].AddArrow(drawLine, pen2, 50, 2, types[2][currentType]->name[2], CPointFloat(tmpsimPoint.x + 100, tmpsimPoint.y + 2), true, font);

		}


	}
	sirina2Avr /= levoTocke2.size();
	
		measureObject->sirina[1] = sirina2Avr;
	return 0;
}

int imageProcessing::FindTopSurfaceAndDim(int nrCam, int imageIndex, int draw)
{
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	QRect imageRect;
	imageRect.setCoords(10, 10, width - 10, height - 10);
	CCircle innerCircle = measureObject->innerCircle;
	CCircle outerCircle = measureObject->outerCircle;
	CPointFloat center1 = innerCircle.GetCenter();
	CPointFloat center2 = outerCircle.GetCenter();
	CPointFloat centerKroga = CPointFloat((center1.x + center2.x) / 2, (center1.y + center2.y) / 2);
	measureObject->centerKroga = centerKroga;
	measureObject->centerKroga = innerCircle.GetCenter();
	CCircle notCircle1;
	CCircle notCircle2;
	CCircle notCircle3;
	CCircle notCircle4;
	int min1 = types[currentStation][currentType]->prop[3][0]->intValue;
	int max1 = types[currentStation][currentType]->prop[3][1]->intValue;
	int min2 = types[currentStation][currentType]->prop[3][2]->intValue;
	int max2 = types[currentStation][currentType]->prop[3][3]->intValue;
	int offset1 = types[currentStation][currentType]->prop[3][4]->intValue;
	int offsetThresholdInnerOuter = types[currentStation][currentType]->prop[3][5]->intValue;
	notCircle1.SetCenter(centerKroga);//
	notCircle2.SetCenter(centerKroga);
	notCircle3.SetCenter(centerKroga);
	notCircle4.SetCenter(centerKroga);
	notCircle1.SetRadius(innerCircle.radius + min1);
	notCircle2.SetRadius(innerCircle.radius + max1);
	notCircle3.SetRadius(innerCircle.radius + min2);
	notCircle4.SetRadius(innerCircle.radius + max2);


	//notCircle1.setR

	cam[nrCam]->image[imageIndex].GetAverageIntensity(notCircle1, notCircle1.radius, notCircle2.radius, 1);

	float avrIntKrog1 = 0;
	for (int i = 0; i < notCircle1.averageIntensity.size(); i++)
	{
		avrIntKrog1 += notCircle1.averageIntensity[i];
	}
	if (notCircle1.averageIntensity.size() > 0)
		avrIntKrog1 /= notCircle1.averageIntensity.size();
	else
		avrIntKrog1 = 0;




	cam[nrCam]->image[imageIndex].GetAverageIntensity(notCircle3, notCircle3.radius, notCircle4.radius, 1);

	float avrIntKrog2 = 0;
	for (int i = 0; i < notCircle3.averageIntensity.size(); i++)
	{
		avrIntKrog2 += notCircle3.averageIntensity[i];
	}
	if (notCircle3.averageIntensity.size() > 0)
		avrIntKrog2/= notCircle3.averageIntensity.size();
	else
		avrIntKrog2 = 0;

	int thresholdNavoja = (avrIntKrog1 + avrIntKrog2) / 2 + offset1;
	int thresholdNotranjiZunanjiRob = (avrIntKrog1 + avrIntKrog2) / 2 + offsetThresholdInnerOuter;
	int razlika = abs(avrIntKrog1 - avrIntKrog2);
	measureObject->thresholdZareze = thresholdNavoja;
	vector <CPointFloat> tockeNavoja;
	vector <CPointFloat> tockeNavoja2;
	vector <CPointFloat> tockeKrogPovrsinaNotranja;
	vector <CPointFloat> tockeKrogPovrsinaZunanja;
	vector <float> kot;
	vector <float> kot2;
	vector <float> kotKrogpovrsinaNotranja;
	vector <float> kotKrogpovrsinaZunanja;
	float delezKroga;
	CCircle krogNavoj;
	CCircle krogNavoj1;
	CCircle krogNavoj2;
	CCircle krogPovrsinaZunanji;
	CCircle krogPovrsinaNotranji;
	
	//poskusimo novo metodo OTSU za iskanje pragu.

	float celKrog = 2 * M_PI;
	float odT = 0;
	float doT = 0;
	const int  stOsekov = 180;
	odT = 0;
	doT = celKrog / stOsekov;
	int mojX = 0;
	int mojY = 0;
	int nj = 2 * M_PI * notCircle4.radius / stOsekov;
	int ni = notCircle4.radius - notCircle1.radius;
	float r = 0;
	float fi;


	//vector<CPointFloat> noveTockeNavoja[stOsekov];
	CMemoryBuffer novSlika[stOsekov];

	//novSlika.Create(ni, nj, 1);


	vector<CPointFloat> noveTockeKrogMartin;
	vector<CPointFloat> noveTockeKrogMiro;
	vector<float> kotMartin;
	vector<float> kotMiro;
	int tmpInt;
	int novX = 0;
	int novY = 0;
	CPointFloat toc1, toc2;
	CPointFloat resPoint;
	CLine testLine;


	for (int odsek = 0; odsek < stOsekov; odsek++)
	{
		odT = celKrog / stOsekov * odsek;
		doT = celKrog / stOsekov * (odsek + 1);
		novX = 0;
		novY = 0;
		novSlika[odsek].Create(ni, nj, 1);
		for (int i = 0; i < ni; i++)
		{
			r = notCircle1.radius + (notCircle4.radius - notCircle1.radius) * ((float)i) / ni;
			novX++;
			novY = 0;
			for (int j = 0; j < nj; j++)
			{

				fi = odT + (doT - odT)*((float)j) / nj;

					mojX = centerKroga.x + r * cos(fi);
					mojY = centerKroga.y + r * sin(fi);

				
				//if (odsek == 2)
				//scene->addItem((CPointFloat(mojX,mojY).DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 1)));
			if (mojX > 0 && mojY > 0)
			{
				tmpInt = cam[nrCam]->image[imageIndex].GetAverageIntensity(mojX, mojY);

				novSlika[odsek].SetAverageIntensity(i, j, tmpInt);
				int test = novSlika[odsek].GetAverageIntensity(i, j);

				novY++;
			}

			}
		}
		QRect slikaRct;
		slikaRct.setRect(0, 0, ni, nj);

		int pragOdsek = novSlika[odsek].GetThresholdOtsu(slikaRct);
		int bla = 100;
		bla = 100;
		measureObject->omitedAngleNavoj[0] = odT;
		measureObject->omitedAngleNavoj[1] = doT;

		mojX = centerKroga.x + notCircle2.radius * cos(fi);
		mojY = centerKroga.y + notCircle2.radius * sin(fi);
		toc1.SetPointFloat(mojX, mojY);

		mojX = centerKroga.x + notCircle3.radius * cos(fi);
		mojY = centerKroga.y + notCircle3.radius * sin(fi);
		toc2.SetPointFloat(mojX, mojY);

		testLine.SetLine(toc1, toc2);
		resPoint.SetPointFloat(0, 0);
		resPoint = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(testLine, true, 1, false, pragOdsek);
		if (resPoint.x > 0)
		{
		
		noveTockeKrogMartin.push_back(resPoint);
		kotMartin.push_back(fi);
		}

		//cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 1, 30, false, pragOdsek, centerKroga, notCircle2.radius, notCircle3.radius, true, OmitAngle2, kot2, noveTockeNavoja[odsek], &delezKroga);
		if (draw)
		{
			//for (int m = 0; m < noveTockeNavoja[odsek].size(); m++)
			//	scene->addItem((noveTockeNavoja[odsek][m].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 4)));
				scene->addItem((resPoint.DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3)));
		}
	}

	//mirotova metoda. 
	//najprej poiscemo povp intenzitete in nato filter.

	odT = 0;
	doT = celKrog;
	novX = 0;
	novY = 0;
	 nj = 2 * M_PI * notCircle4.radius;
	 ni = notCircle2.radius - notCircle1.radius;
int 	 ni2 = notCircle4.radius - notCircle3.radius;
	 int innerCircleAvrageInt[180];
	 int outerCircleAvrageInt[180];
	 int cur = 0;
	 int avrInt = 0, avrInt2 = 0;
	 int stevec = 0;



	 for (int j = 0; j < 360; j += 2)
	 {
		 fi = j * M_PI / 180;
		avrInt = 0;
		stevec = 0;

		for (int i = 0; i < ni; i++)
		{
			r = notCircle1.radius + (notCircle2.radius - notCircle1.radius) * ((float)i) / ni;

			mojX = centerKroga.x + r * cos(fi);
			mojY = centerKroga.y + r * sin(fi);

			if (mojX > 0 && mojY > 0)
			{
				tmpInt = cam[nrCam]->image[imageIndex].GetAverageIntensity(mojX, mojY);
				avrInt += tmpInt;
				stevec++;
			}
		}
		if (stevec > 0)
		{
			avrInt /= stevec;
		}
		else
			avrInt = 0;
		stevec = 0;
		for (int i = 0; i < ni2; i++)
		{
			r = notCircle3.radius + (notCircle4.radius - notCircle3.radius) * ((float)i) / ni;

			mojX = centerKroga.x + r * cos(fi);
			mojY = centerKroga.y + r * sin(fi);
			//if (odsek == 2)

			if (mojX > 0 && mojY > 0)
			{
				tmpInt = cam[nrCam]->image[imageIndex].GetAverageIntensity(mojX, mojY);
				avrInt2 += tmpInt;
				stevec++;
			}
		}
		if (stevec > 0)
		{
			avrInt2 /= stevec;
		}
		else
			avrInt2 = 0;
		innerCircleAvrageInt[cur] = avrInt;
		outerCircleAvrageInt[cur] = avrInt2;
		cur++;

	}
	 const int filter = 3;

	 int low = 0, high = 0;
	 avrInt = 0;
	 avrInt2 = 0;
	 stevec = 0;
	 int currThres;


	 for (int i = 0; i < 180; i++)
	 {

		 low = i - filter;
		 if (low < 0)
			 low = 180 + low;
		 high = i + filter;
		 if (high >= 180)
			 high = high - 180;

		 avrInt = 0;
		 avrInt2 = 0;
		 stevec = 0;
		 for (int j = low; j < high; j++)
		 {
			 avrInt += innerCircleAvrageInt[j];
			 avrInt2 += outerCircleAvrageInt[j];
			 stevec++;

			 currThres = ((avrInt / stevec) + (avrInt2 / stevec)) / 2;
			 int bla = 100;			 
		 }
		 fi = i * 2*M_PI/180;//da dobimo kot od o -360 stopinj
		 mojX = centerKroga.x + notCircle2.radius * cos(fi);
		 mojY = centerKroga.y + notCircle2.radius * sin(fi);
		 toc1.SetPointFloat(mojX, mojY);

		 mojX = centerKroga.x + notCircle3.radius * cos(fi);
		 mojY = centerKroga.y + notCircle3.radius * sin(fi);
		 toc2.SetPointFloat(mojX, mojY);
		 resPoint.SetPointFloat(0, 0);
		 testLine.SetLine(toc1, toc2);
		 resPoint = cam[nrCam]->image[imageIndex].GetEdgePointOnSegment2D(testLine, true, 1, false, currThres+ offset1);
		 if (resPoint.x > 0)
		 {
			 noveTockeKrogMiro.push_back(resPoint);
			 kotMiro.push_back(fi);
		 }
		 if (draw)
		 {
			 //scene->addItem((toc1.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 1)));
			 //scene->addItem((toc2.DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 1)));
			 scene->addItem((resPoint.DrawDot(QPen(Qt::green), QBrush(Qt::green), 3)));


		 }
	 }

	 float Rextreme[2];  //indeks izbere min/max
	 float fiRextreme[2]; //indeks izbere min/max
	 float maxStDevLocal;
	 float fiMaxStDevLocal;
	 float maxStDevLocalToGlobal;
	 float fiMaxStDevLocalToGlobal;
	 float stDevGlobal;
	 int minSectorPoints;
	 int maxSectorPoints;


	if (razlika > 10)//preverim ce je res navoj na mestu
	{


		//cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, false, 2, 30, true, thresholdNavoja, centerKroga, notCircle2.radius, notCircle3.radius, false, false, kot, tockeNavoja, &delezKroga);
		//cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 30, false, thresholdNavoja, centerKroga, notCircle1.radius, notCircle3.radius, false, false, kot2, tockeNavoja2, &delezKroga);
		//cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 30, true, threshold, pieceCenter, rMinInner, rMaxInner, true, OmitAngle, anglesOuter, edgePointsOuter, &edgesShare);



		//krogNavoj1.SetCircleNpoints(tockeNavoja, notCircle2.radius, notCircle3.radius);

		//krogNavoj2.SetCircleNpoints(tockeNavoja2, notCircle2.radius, notCircle3.radius);
		krogNavoj1.SetCircleNpoints(noveTockeKrogMartin, notCircle2.radius, notCircle3.radius);
		krogNavoj2.SetCircleNpoints(noveTockeKrogMiro, notCircle2.radius, notCircle3.radius);
		float miroDev = 1000, martinDev = 1000;

		if (noveTockeKrogMartin.size() > 120)
		{
			krogNavoj1.CircleStDev(kotMartin, noveTockeKrogMartin, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
			martinDev = maxStDevLocalToGlobal;
		}
		if (noveTockeKrogMiro.size() > 120)
		{
			krogNavoj2.CircleStDev(kotMiro, noveTockeKrogMiro, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
			miroDev = maxStDevLocalToGlobal;
		}
		
		int selectedDev = -1;
		if (miroDev > martinDev)
		{
			selectedDev = 1;
		}
		else if ((miroDev < martinDev))
		{
			selectedDev = 2;
		}
		else
			selectedDev = -1;

		if (selectedDev == 2 && noveTockeKrogMiro.size() > 120)//mirotova metoda
		{
			krogNavoj = krogNavoj2;
			measureObject->navojCircleDev = miroDev;
		}
		else 	if (selectedDev == 1 && noveTockeKrogMartin.size() > 120)//martinova metoda
		{
			krogNavoj = krogNavoj1;
			measureObject->navojCircleDev = martinDev;
		}
		else
		{
			measureObject->navojCircleDev = 10000;
			krogNavoj.SetCircle(CPointFloat(0, 0), CPointFloat(0, 0), CPointFloat(0, 0));
		}
		

		//krogNavoj = krogNavoj2;

		cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, false, 2, 30, true, thresholdNotranjiZunanjiRob, centerKroga, notCircle4.radius, measureObject->outerCircle.radius, false, false, kotKrogpovrsinaZunanja, tockeKrogPovrsinaZunanja, &delezKroga);
		cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 30, true, thresholdNotranjiZunanjiRob, centerKroga, notCircle4.radius, measureObject->outerCircle.radius, false, false, kotKrogpovrsinaNotranja, tockeKrogPovrsinaNotranja, &delezKroga);
		//cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 30, true, threshold, pieceCenter, rMinInner, rMaxInner, true, OmitAngle, anglesOuter, edgePointsOuter, &edgesShare);
		measureObject->thresholdKrogaBrezZarez = thresholdNotranjiZunanjiRob;
		measureObject->krogZaDeviacijobrezZarez = notCircle4;
	
		krogPovrsinaNotranji.SetCircleNpoints(tockeKrogPovrsinaNotranja, notCircle3.radius, measureObject->outerCircle.radius);
		krogPovrsinaZunanji.SetCircleNpoints(tockeKrogPovrsinaZunanja, notCircle3.radius, measureObject->outerCircle.radius);

	}
	else
	{
		krogNavoj.SetCircle(CPointFloat(0, 0), CPointFloat(0, 0), CPointFloat(0, 0));

	}






	measureObject->navojCircle = krogNavoj;
	measureObject->krogPovrsinaZunanji = krogPovrsinaZunanji;
	measureObject->krogPovrsinaNotranji = krogPovrsinaNotranji;


	QFont font("MS Shell Dlg 2");
	font.setPointSize(72);
	QPen penDraw;
	penDraw.setWidth(10);
	penDraw.setColor(Qt::yellow);

	penDraw.setColor(Qt::yellow);
	mainScreenDrawTekoci[1].AddCircle(krogNavoj, penDraw, QBrush(Qt::NoBrush), 0, "");
	penDraw.setColor(Qt::blue);
	mainScreenDrawTekoci[1].AddCircle(krogPovrsinaZunanji, penDraw, QBrush(Qt::NoBrush), 0, "");
	penDraw.setColor(Qt::red);
	mainScreenDrawTekoci[1].AddCircle(krogPovrsinaNotranji, penDraw, QBrush(Qt::NoBrush), 0, "");

	//mainScreenDrawTekoci[1].AddPoint(CPointFloat(200, 200), 15, QPen(Qt::green), QBrush(Qt::green), "bal", CPointFloat(300, 300), true, font);




	/*if (kot2.size() > 0)
	{
		krogNavoj.CircleStDev(kot2, tockeNavoja2, 30 * M_PI/180 , 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
		measureObject->navojCircleDev = maxStDevLocalToGlobal;
	}
	else
		measureObject->navojCircleDev = 100;

		*/
	if (kotKrogpovrsinaZunanja.size() > 0)
	{
		krogPovrsinaZunanji.CircleStDev(kotKrogpovrsinaZunanja, tockeKrogPovrsinaZunanja, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
		measureObject->krogPovrsinaZunanjiDev = maxStDevLocalToGlobal;
	}
	else
		measureObject->krogPovrsinaZunanjiDev = 100;

		if (kotKrogpovrsinaNotranja.size() > 0)
		{
			krogPovrsinaNotranji.CircleStDev(kotKrogpovrsinaNotranja, tockeKrogPovrsinaNotranja, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
			measureObject->krogPovrsinaNotranjiDev = maxStDevLocalToGlobal;
		}
		else
			measureObject->krogPovrsinaNotranjiDev = 100;

	if (draw)
	{
		scene->addItem(notCircle3.DrawCircle(QPen(Qt::green), QBrush(Qt::NoBrush), 50));
		scene->addItem(notCircle4.DrawCircle(QPen(Qt::green), QBrush(Qt::NoBrush), 50));
		scene->addItem(krogNavoj1.DrawCircle(QPen(Qt::yellow), QBrush(Qt::NoBrush), 50));
		scene->addItem(krogNavoj2.DrawCircle(QPen(Qt::green), QBrush(Qt::NoBrush), 50));
		scene->addItem(krogPovrsinaNotranji.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 50));
		scene->addItem(krogPovrsinaZunanji.DrawCircle(QPen(Qt::blue), QBrush(Qt::NoBrush), 50));
		scene->addItem(notCircle1.DrawCircle(QPen(Qt::blue), QBrush(Qt::NoBrush), 50));
		scene->addItem(notCircle2.DrawCircle(QPen(Qt::blue), QBrush(Qt::NoBrush), 50));
		ui.listFunctionsReturn->item(0)->setText(QString("krog1 avrInt: %1").arg(avrIntKrog1));
		ui.listFunctionsReturn->item(1)->setText(QString("krog1 avrInt: %1").arg(avrIntKrog2));
		ui.listFunctionsReturn->item(2)->setText(QString("Prag intenzitete za iskanje navoja: %1").arg(thresholdNavoja));
		ui.listFunctionsReturn->item(3)->setText(QString("PragIntenzitete za iskanje notranjega in zunanjega RIBA: %1").arg(thresholdNotranjiZunanjiRob));
		scene->addItem(innerCircle.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 150));
		scene->addItem(outerCircle.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 50));
		for (int i = 0; i < tockeNavoja.size(); i++)
		{
			scene->addItem(tockeNavoja[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3));
		}
		for (int i = 0; i < tockeNavoja2.size(); i++)
		{
			scene->addItem(tockeNavoja2[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 3));
		}
		for (int i = 0; i < tockeKrogPovrsinaNotranja.size(); i++)
		{
			scene->addItem(tockeKrogPovrsinaNotranja[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
		}
		for (int i = 0; i < tockeKrogPovrsinaZunanja.size(); i++)
		{
			scene->addItem(tockeKrogPovrsinaZunanja[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
		}
	}
	return 0;
}
int imageProcessing::findZareze(int nrCam, int imageIndex, int draw)
{
	int offsetRadij = 20;
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	QRect imageRect;
	CPointFloat center = measureObject->centerKroga;
	float zunanjiRadij = measureObject->krogPovrsinaZunanji.radius - offsetRadij;
	float notranjiRadij = measureObject->krogPovrsinaNotranji.radius + 50;
	float threshold = types[currentStation][currentType]->prop[3][9]->doubleValue;
	imageRect.setCoords(10, 10, width - 10, height - 10);
	CLine zgoraj, spodaj, levo, desno;
	zgoraj.SetLine(CPointFloat(10, 10), CPointFloat(2420, 10));
	spodaj.SetLine(CPointFloat(10, 2010), CPointFloat(2440, 2010));
	levo.SetLine(CPointFloat(10, 10), CPointFloat(10, 2010));
	desno.SetLine(CPointFloat(2420, 10), CPointFloat(2420, 2010));

	
	float delezKroga;
	vector <CPointFloat> tockeZarez;
	vector <float >		kotTock;

	vector <float >		kotZareza1;
	vector <float >		kotZareza2;
	vector <CPointFloat> tockeZareza1;
	vector <CPointFloat> tockeZareza2;
	float kot = 2; //to je priblizno 45 stopinj kolikor mora biti dosti da pokrijem zarezo

	CLine zareza1Line;
	CLine zareza2Line;



	CCircle notKolobar;
	CCircle zunKolobar;

	notKolobar.SetRadius(600);
	notKolobar.SetCenter(measureObject->krogPovrsinaNotranji.GetCenter());
	zunKolobar.SetRadius(720);
	zunKolobar.SetCenter(measureObject->krogPovrsinaNotranji.GetCenter());

	if (draw)
	{
		scene->addItem(notKolobar.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 0));
		scene->addItem(zunKolobar.DrawCircle(QPen(Qt::red), QBrush(Qt::NoBrush), 0));
	}

	cam[nrCam]->image[imageIndex].RadialIntensity(measureObject->krogPovrsinaNotranji.GetCenter(), notKolobar.radius, zunKolobar.radius, 360, 1);
	float sizeI = 0;
	float calValue = 0;
	vector <float> tmpFloat;
	for (int i = 0; i < 360; i++)
	{
		sizeI += cam[nrCam]->image[imageIndex].intensityRadial[i];
		tmpFloat.push_back(cam[nrCam]->image[imageIndex].intensityRadial[i]);
	}
	if (sizeI > 0)
		calValue = sizeI / 360;
	else
		calValue = 50;

	int thres = 0;
	if (threshold > 0)
		thres = (int)calValue / threshold;
	else
		  thres = 100;
	

	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, true, 2, 20, false, thres, center, notranjiRadij, zunanjiRadij, false, false, kotTock, tockeZarez, &delezKroga);
	
	if (draw)
	{
		for (int i = 0; i < tockeZarez.size(); i++)
		{
			scene->addItem(tockeZarez[i].DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 3));
		}
		ui.listFunctionsReturn->item(4)->setText(QString("PovpVrednost Kolobar: %1 Prag Za Iskanje ZAREZ: %2").arg(calValue).arg(thres));
	}

	float minRad;
	float maxRad;
	float min;
	float max;
	const int size = 12;
	vector <CPointFloat> tockeOdseki[size];
	vector <CPointFloat> tockeOdseki2[size];
	vector <float > kotiOdseki[size];
	vector <float > kotiOdseki2[size];
 		if (kotTock.size() > 0)
		{

			int maxTock[size];
			int maxTock2[size];
			for (int i = 0; i < size; i++)
			{
				maxTock[i] = 0;
				maxTock2[i] = 0;
			}
			float od = 0;
			float doT = 0;
			float step = 6.28/size;

		for (int j = 0; j < size; j++)
			{
				if (j == 0)
				{
						od = 0;
						doT = step;
				}
				else
				{
					od = doT;
					doT = doT + step;
				}
				for (int i = 0; i < kotTock.size(); i++)
				{
					if ((kotTock[i] > od) && (kotTock[i] < doT))
					{
						maxTock[j]++;
						tockeOdseki[j].push_back(tockeZarez[i]);
						kotiOdseki[j].push_back(kotTock[i]);
					}
				}
		}
		for (int j = 0; j < size; j++)
		{
			if (j == 0)
			{
				od = step/2;
				doT = 6.28 - step/2;
				min = od;
				max = doT;
			}
			else
			{
				od = od + step;
				doT = doT + step;
				if(doT >= 6.28)
					doT = 0 + (doT - 6.28);


				if (od > doT)
				{
					min = doT;
					max = od;
				}
				else
				{
					max = doT;
					min = od;
				}
			}

			for (int i = 0; i < kotTock.size(); i++)
			{
				if (j == 0)
				{
					if ((kotTock[i] > max) || (kotTock[i] < min))
					{
						maxTock2[j]++;
						tockeOdseki2[j].push_back(tockeZarez[i]);
						kotiOdseki2[j].push_back(kotTock[i]);
					}
				}
				else
				{
					if ((kotTock[i] > min) && (kotTock[i] < max))
					{
						maxTock2[j]++;
						tockeOdseki2[j].push_back(tockeZarez[i]);
						kotiOdseki2[j].push_back(kotTock[i]);
					}
				}
			}
		}

		int max1 = 0;
		int index1 = 0;
		int max2 = 0;
		int index2 = 0;

		for (int i = 0; i < size; i++)
		{
			if (tockeOdseki[i].size() > max1)
			{
				max1 = tockeOdseki[i].size();
				index1 = i;
			}
			if (tockeOdseki2[i].size() > max2)
			{
				max2 = tockeOdseki2[i].size();
				index2 = i;
			}
		}



		int max3 = 0;
		int indexMax3 = 0;
		bool upostevaj[size];
		for (int i = 0; i < size; i++)
			upostevaj[i] = true;


		int j = 0;
		int sel = 0;
		if (max1 > max2)
		{
			sel = index1;
		}
		else
			sel = index2;

		int neviIndex = sel - 2;
		if (neviIndex < 0)
			neviIndex = size + neviIndex;

		 j = neviIndex;
		for (int i = 0; i <5; i++)
			{
				j = j % 12;

			upostevaj[j] = false;
			j++;
			}

		for (int i = 0; i < size; i++)
		{
			if (max1 > max2)
			{
				if (upostevaj[i] == true)
				{
					if (tockeOdseki[i].size() > max3)
					{

						max3 = tockeOdseki[i].size();
						indexMax3 = i;
					}
				}
			}
			else
			{
				if (upostevaj[i] == true)
				{
					if (tockeOdseki2[i].size() > max3)
					{
						max3 = tockeOdseki2[i].size();
						indexMax3 = i;
					}
				}
			}
		}
		int pravaIzbira = -1;
		int zareza1Odsek = 0;
		if (max1 > max2)
		{
			tockeZareza1 = tockeOdseki[index1];
			tockeZareza2 = tockeOdseki[indexMax3];
			zareza1Odsek = index1;
			pravaIzbira = 0;
		}
		else
		{
			tockeZareza1 = tockeOdseki2[index2];
			zareza1Odsek = index2;
			tockeZareza2 = tockeOdseki2[indexMax3];
			pravaIzbira = 1;
		}
			if (draw)
			{
				for (int i = 0; i < tockeZareza1.size(); i++)
				{
					scene->addItem(tockeZareza1[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
				}
				for (int i = 0; i < tockeZareza2.size(); i++)
				{
					scene->addItem(tockeZareza2[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
				}
			
			}

			zareza1Line.SetLine(tockeZareza1);
			zareza2Line.SetLine(tockeZareza2);

			vector <CPointFloat> dodatneTockeZareza1;
			vector <CPointFloat> dodatneTockeZareza2;
			//dodano se vec tock pri premici
			//zareza 1
			int selected = 0;
			float izmera1;
			if (pravaIzbira == 0)
			{
				selected = index1 - 1;
				if (selected < 0)
					selected = size - 1;
				for (int i = 0; i < tockeOdseki[selected].size(); i++)
				{
					izmera1 = zareza1Line.GetDistance(tockeOdseki[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza1.push_back(tockeOdseki[selected][i]);
					}
				}
				selected = index1 + 1;
				if (selected > size -1)
					selected =0;

				for (int i = 0; i < tockeOdseki[selected].size(); i++)
				{
					izmera1 = zareza1Line.GetDistance(tockeOdseki[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza1.push_back(tockeOdseki[selected][i]);
					}
				}
			}
			else
			{
				selected = index2 - 1;
				if (selected < 0)
					selected = size - 1;
				for (int i = 0; i < tockeOdseki2[selected].size(); i++)
				{
					izmera1 = zareza1Line.GetDistance(tockeOdseki2[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza1.push_back(tockeOdseki2[selected][i]);
					}
				}
				selected = index2 + 1;
				if (selected > size - 1)
					selected = 0;

				for (int i = 0; i < tockeOdseki2[selected].size(); i++)
				{
					izmera1 = zareza1Line.GetDistance(tockeOdseki2[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza1.push_back(tockeOdseki2[selected][i]);
					}
				}
			}

			if (draw)
			{
				for (int i = 0; i < dodatneTockeZareza1.size(); i++)
				{
					scene->addItem(dodatneTockeZareza1[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
				}

			}
			for (int i = 0; i < dodatneTockeZareza1.size(); i++)
			{
				tockeZareza1.push_back(dodatneTockeZareza1[i]);
			}
			//zareza2
			if (pravaIzbira == 0)
			{
				selected = indexMax3 - 1;
				if (selected < 0)
					selected = size - 1;
				for (int i = 0; i < tockeOdseki[selected].size(); i++)
				{
					izmera1 = zareza2Line.GetDistance(tockeOdseki[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza2.push_back(tockeOdseki[selected][i]);
					}
				}
				selected = indexMax3 + 1;
				if (selected > size - 1)
					selected = 0;

				for (int i = 0; i < tockeOdseki[selected].size(); i++)
				{
					izmera1 = zareza2Line.GetDistance(tockeOdseki[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza2.push_back(tockeOdseki[selected][i]);
					}
				}
			}
			else 
			{
				selected = indexMax3 - 1;
				if (selected < 0)
					selected = size - 1;
				for (int i = 0; i < tockeOdseki2[selected].size(); i++)
				{
					izmera1 = zareza2Line.GetDistance(tockeOdseki2[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza2.push_back(tockeOdseki2[selected][i]);
					}
				}
				selected = indexMax3 + 1;
				if (selected > size - 1)
					selected = 0;

				for (int i = 0; i < tockeOdseki2[selected].size(); i++)
				{
					izmera1 = zareza2Line.GetDistance(tockeOdseki2[selected][i]);
					if (izmera1 < 100)
					{
						dodatneTockeZareza2.push_back(tockeOdseki2[selected][i]);
					}
				}
			}

			if (draw)
			{
				for (int i = 0; i < dodatneTockeZareza2.size(); i++)
				{
					scene->addItem(dodatneTockeZareza2[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
				}

			}
			for (int i = 0; i < dodatneTockeZareza2.size(); i++)
			{
				tockeZareza2.push_back(dodatneTockeZareza2[i]);
			}
			zareza1Line.SetLine(tockeZareza1);
			zareza2Line.SetLine(tockeZareza2);

			vector <CPointFloat> noveTockeZareza1;
			vector <CPointFloat> noveTockeZareza2;
			for (int i = 0; i < tockeZareza1.size(); i++)
			{
				izmera1 = zareza1Line.GetDistance(tockeZareza1[i]);
				if (izmera1 < 10)
				{
					noveTockeZareza1.push_back(tockeZareza1[i]);
					if(draw)
					{
						scene->addItem(noveTockeZareza1.back().DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 10));
					}
				}
			}
			for (int i = 0; i < tockeZareza2.size(); i++)
			{
				izmera1 = zareza2Line.GetDistance(tockeZareza2[i]);
				if (izmera1 < 10)
				{
					noveTockeZareza2.push_back(tockeZareza2[i]);
					if (draw)
					{
						scene->addItem(noveTockeZareza2.back().DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 10));
					}
				}
			}

			zareza1Line.SetLine(noveTockeZareza1);
			zareza2Line.SetLine(noveTockeZareza2);
			if (draw)
			{
				scene->addItem(zareza1Line.DrawLine(imageRect, QPen(Qt::magenta)));
				scene->addItem(zareza2Line.DrawLine(imageRect, QPen(Qt::magenta)));
			}
			QFont font("MS Shell Dlg 2");
			font.setPointSize(72);
			QPen penDraw;
			penDraw.setWidth(10);

			penDraw.setColor(Qt::magenta);
			mainScreenDrawTekoci[1].AddLine(imageRect, zareza1Line, penDraw);
			mainScreenDrawTekoci[1].AddLine(imageRect, zareza2Line, penDraw);

			CPointFloat tockeNaPremici[4], tockeNaPremici2[4];

			tockeNaPremici[0] = zareza1Line.GetIntersectionPoint(zgoraj);
			tockeNaPremici[1] = zareza1Line.GetIntersectionPoint(spodaj);
			tockeNaPremici[2] = zareza1Line.GetIntersectionPoint(levo);
			tockeNaPremici[3] = zareza1Line.GetIntersectionPoint(desno);
			tockeNaPremici2[0] = zareza2Line.GetIntersectionPoint(zgoraj);
			tockeNaPremici2[1] = zareza2Line.GetIntersectionPoint(spodaj);
			tockeNaPremici2[2] = zareza2Line.GetIntersectionPoint(levo);
			tockeNaPremici2[3] = zareza2Line.GetIntersectionPoint(desno);
			bool pOk = false;
			for (int i = 0; i < 4; i++)
			{
				if ((tockeNaPremici[i].x > 0 && tockeNaPremici[i].x < width) && (tockeNaPremici[i].y > 0 && tockeNaPremici[i].y < height))
				{
					if (pOk == false)
					{
						zareza1Line.p1 = tockeNaPremici[i];
						pOk = true;
					}
					else
					{
						zareza1Line.p2 = tockeNaPremici[i];
						break;
					}
				}
			}
			pOk = false;
			for (int i = 0; i < 4; i++)
			{
				if ((tockeNaPremici2[i].x > 0 && tockeNaPremici2[i].x < width) && (tockeNaPremici2[i].y > 0 && tockeNaPremici2[i].y < height))
				{
					if (pOk == false)
					{
						zareza2Line.p1 = tockeNaPremici2[i];
						pOk = true;
					}
					else
					{
						zareza2Line.p2 = tockeNaPremici2[i];
						break;
					}
				}
			}

			float angle, angle2;
			float distance1, distance2;
			float distance; 
			distance1 = zareza1Line.GetDistance(center);
			distance2 = zareza2Line.GetDistance(center);
			//distance = zareza1Line.GetDistance(zareza2Line, center);

			distance = distance1 + distance2;
			angle = zareza1Line.GetLineAngleInDegrees();
			angle2 = zareza2Line.GetLineAngleInDegrees();
			angle = angle - angle2;
			measureObject->razdaljaZarez = distance;
			measureObject->kotZarez = angle;
			measureObject->zareza1 = zareza1Line;
			measureObject->zareza2 = zareza2Line;


		





			//tukaj se enkrat izracunam deviacijo zunanjega roba, kjer so izuzete zareze

			vector <float > koti;
			vector < CPointFloat > tocke;
			float delez;
			CCircle krogZunanji;
			CCircle izracunanKrog = measureObject->krogPovrsinaZunanji;
			CPointFloat zareza1Edge[2];
			CPointFloat zareza2Edge[2];
			float zareza1Kot[2];
			float zareza2Kot[2];

			float kotZareza1LowToHigh[2];
			float kotZareza2LowToHigh[2];
			int minKot = 100;
			measureObject->outerCircle.CircleLineIntersection(zareza1Line, zareza1Edge, zareza1Kot);
			measureObject->outerCircle.CircleLineIntersection(zareza2Line, zareza2Edge, zareza2Kot);

			if (zareza1Kot[0] < zareza1Kot[1])
			{
				kotZareza1LowToHigh[0] = zareza1Kot[0];
				kotZareza1LowToHigh[1] = zareza1Kot[1];
			}
			else
			{
				kotZareza1LowToHigh[0] = zareza1Kot[1];
				kotZareza1LowToHigh[1] = zareza1Kot[0];
			}

				measureObject->zarezaAngle1[0] = kotZareza1LowToHigh[0]; //od 
				measureObject->zarezaAngle1[1] = kotZareza1LowToHigh[1]; //do


			if (zareza2Kot[0] < zareza2Kot[1])
			{
				kotZareza2LowToHigh[0] = zareza2Kot[0];
				kotZareza2LowToHigh[1] = zareza2Kot[1];
			}
			else
			{
				kotZareza2LowToHigh[0] = zareza2Kot[1];
				kotZareza2LowToHigh[1] = zareza2Kot[0];
			}


				measureObject->zarezaAngle2[0] = kotZareza2LowToHigh[0]; //od 
				measureObject->zarezaAngle2[1] = kotZareza2LowToHigh[1]; //do


			




			if (draw)
			{
				//for (int i = 0; i < tockeZareza1.size(); i++)
				//{
			//		scene->addItem(tockeZareza1[i].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 10));
				//}


				for (int i = 0; i < 2; i++)
				{
					scene->addItem(zareza1Edge[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 15));
					scene->addItem(zareza2Edge[i].DrawDot(QPen(Qt::darkBlue), QBrush(Qt::darkBlue), 15));
				}
			}



			cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRadial(imageRect, false, 2, 30, true, measureObject->thresholdKrogaBrezZarez, measureObject->krogZaDeviacijobrezZarez.GetCenter(), measureObject->krogZaDeviacijobrezZarez.radius, measureObject->outerCircle.radius, true, OmitAngleZareza, koti, tocke, &delez);

			krogZunanji.SetCircleNpoints(tocke, measureObject->krogZaDeviacijobrezZarez.radius, measureObject->outerCircle.radius);


			float Rextreme[2];  //indeks izbere min/max
			float fiRextreme[2]; //indeks izbere min/max
			float maxStDevLocal;
			float fiMaxStDevLocal;
			float maxStDevLocalToGlobal;
			float fiMaxStDevLocalToGlobal;
			float stDevGlobal;
			int minSectorPoints;
			int maxSectorPoints;



			if (draw)
			{
				for (int i = 0; i < tocke.size(); i++)
				{
					scene->addItem(tocke[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 10));
				}

			}
			if (koti.size() > 0)
			{
				krogZunanji.CircleStDev(koti, tocke, 30 * M_PI / 180, 0.5, Rextreme, fiRextreme, &maxStDevLocal, &fiMaxStDevLocal, &maxStDevLocalToGlobal, &fiMaxStDevLocalToGlobal, &stDevGlobal, &minSectorPoints, &maxSectorPoints);
				measureObject->krogPovrsinaZunanjiDev = maxStDevLocalToGlobal;
			}
			else
				measureObject->krogPovrsinaZunanjiDev = 100;

		
		}
		
	
	return 0;
}
int imageProcessing::InspectSurface(int nrCam, int imageIndex, int draw)
{
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	int outerOffset = types[currentStation][currentType]->prop[3][6]->intValue;
	int thres = types[currentStation][currentType]->prop[3][7]->intValue;
	int minSize = types[currentStation][currentType]->prop[3][8]->intValue;
	Mat imageCopy = cam[nrCam]->image[imageIndex].buffer->clone();
	Mat imageCopy2;
	Mat imageCopy3;
	Mat imageCopy4;
	Mat imageCopy5;
	CPointFloat centerKroga = measureObject->centerKroga;


	//blur(imageCopy, imageCopy, Size(5, 5));

	Mat mask(cv::Size(width, height), CV_8UC3, Scalar(0));



		
	blur(imageCopy, imageCopy2, Size(200, 200));
	//blur(imageCopy, imageCopy5, Size(20, 20));
	

	imageCopy3 = imageCopy2 - imageCopy;
	//
	//imageCopy3.copyTo(*cam[nrCam]->image[3].buffer);
	imageCopy2.copyTo(*cam[nrCam]->image[2].buffer);
//	cam[nrCam]->image[2].buffer = &imageCopy2.clone();
//	cam[nrCam]->image[3].buffer = &imageCopy3.clone();
	Point center;
	center.x = measureObject->krogPovrsinaZunanji.GetCenter().x;
	center.y = measureObject->krogPovrsinaZunanji.GetCenter().y;
	circle(mask, center, measureObject->krogPovrsinaZunanji.radius - outerOffset, Scalar(255, 255, 255), -1);
	center.x = measureObject->krogPovrsinaNotranji.GetCenter().x;
	center.y = measureObject->krogPovrsinaNotranji.GetCenter().y;
	circle(mask, center, measureObject->krogPovrsinaNotranji.radius + outerOffset, Scalar(0, 0, 0), -1);
	mask.copyTo(*cam[nrCam]->image[3].buffer);
	bitwise_and(imageCopy3, mask, imageCopy3);

	Point point1, point2;

	if (types[1][currentType]->typeName.compare("11.22.424.771.009") == 0)
	{
		point1.x = measureObject->zareza1.p1.x;
		point1.y = measureObject->zareza1.p1.y;
		point2.x = measureObject->zareza1.p2.x;
		point2.y = measureObject->zareza1.p2.y;
		line(imageCopy3, point1, point2, (255, 255, 255), 40, 8, 0);

		point1.x = measureObject->zareza2.p1.x;
		point1.y = measureObject->zareza2.p1.y;
		point2.x = measureObject->zareza2.p2.x;
		point2.y = measureObject->zareza2.p2.y;
		line(imageCopy3, point1, point2, (255, 255, 255), 40, 8, 0);

	}
	imageCopy3.copyTo(*cam[nrCam]->image[3].buffer);


	/*center.x = measureObject->outerCircle.GetCenter().x;
	center.y = measureObject->outerCircle.GetCenter().y;
	circle(mask2, center, measureObject->outerCircle.radius + outerOffset, Scalar(255, 255, 255), 1,8,5);
	bitwise_and(imageCopy5, mask, imageCopy5);
	mask2.copyTo(*cam[nrCam]->image[4].buffer);*/

	//imshow("sf", imageCopy4);
	//circle(mask, (i[0], i[1]), i[2], (255, 255, 255), thickness = -1)

	vector<vector<Point> > contours;
	vector<vector<Point> > contoursAfterFilter;
	vector<vector<Point> > contoursAfterSecundFilter;
	Mat gray;
	//binarize image
	cvtColor(imageCopy3, gray, COLOR_BGR2GRAY);
	threshold(gray, gray, thres, 255, 0);
	cvtColor(gray, imageCopy4, COLOR_GRAY2RGB);
	imageCopy4.copyTo(*cam[nrCam]->image[4].buffer);

	try
	{
		findContours(gray, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
	}
	catch (cv::Exception& e)
	{
		const char* err_msg = e.what();
		//qDebug << "exception caught: " << err_msg << std::endl;
	}
	Moments m;

	if (draw)
	{
		ui.listFunctionsReturn->item(5)->setText(QString("STEVILO POVRSINSKIH NAPAK: %1").arg(contours.size()-1));
	}
	int stej = 0;
	if (contours.size() > 0)
	{
		for (int i = 0; i < contours.size() - 1; i++)
		{
			m = moments(contours[i]);

			if ((int)m.m00 > minSize)
			{
				contoursAfterFilter.push_back(contours[i]);
				if (draw)
				{
					ui.listFunctionsReturn->item(stej+6)->setText(QString("POVRSINSKA NAPAKA: %1 VELIKOST : %2 [pix]").arg(i).arg(m.m00));
				}
				stej++;
			}
		}

	}

	QPen pen;
	pen.setWidth(8);
	pen.setBrush(Qt::red);


	//filtriram se zareze
	float tmpRadij;
	float minRadij = 10000;
	float maxRadij = 0;
	float dis = 0;
	for (int i = 0; i < contoursAfterFilter.size();i++)
	{
		minRadij = 10000;
		maxRadij = 0;
		tmpRadij = 0;
		dis = 0;
		for (int j = 0; j < contoursAfterFilter[i].size(); j++)
		{
			tmpRadij = centerKroga.GetDistance(CPointFloat(contoursAfterFilter[i][j].x, contoursAfterFilter[i][j].y));

			if (tmpRadij < minRadij)
				minRadij = tmpRadij;

			if (tmpRadij > maxRadij)
				maxRadij = tmpRadij;
		}
		
		dis =abs( maxRadij - minRadij);

		if (dis > 30)
		{
			contoursAfterSecundFilter.push_back(contoursAfterFilter[i]);
		}

	}


	QImage image(imageCopy.data, imageCopy.cols, imageCopy.rows, imageCopy.step, QImage::Format_RGB888);
	QImage destImage(width, height, QImage::Format_RGB888);

	//QImage image(cam[nrCam]->image[imageIndex].buffer[0].data,720, 540, QImage::Format_RGB888);
	QPainter *painter;
	painter = new QPainter(&image);
	painter->setPen(pen);
	int r, g, b;
	int index = 0;

	for (int i = 0; i < contoursAfterSecundFilter.size(); i++)
	{
		for (int j = 0; j < contoursAfterSecundFilter[i].size(); j++)
		{
			painter->drawPoint(contoursAfterSecundFilter[i][j].x, contoursAfterSecundFilter[i][j].y);
		}
	}
	//drawContours(cam[nrCam]->image[imageIndex].buffer[0](regRect), contours1, i, (255, 255, 255), 1);
	painter->end();

	Mat tmp(height, width, CV_8UC3, (uchar*)image.bits(), image.bytesPerLine());
	cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
	//cvtColor(tmp, result, CV_BGR2RGB);
	tmp.copyTo(*cam[nrCam]->image[4].buffer);


	measureObject->stDefects = contoursAfterSecundFilter.size();


	QPen penDraw;
	penDraw.setWidth(5);
	penDraw.setColor(Qt::red);

	mainScreenDrawTekoci[1].AddCircle(measureObject->innerCircle, penDraw, QBrush(Qt::NoBrush), 20, "");
	mainScreenDrawTekoci[1].AddCircle(measureObject->outerCircle, penDraw, QBrush(Qt::NoBrush), 20, "");

	return 0;
}


bool imageProcessing::OmitAngle(float alphaRad)
{
	if ((alphaRad > measureObject->omittedAngles[7]) && (alphaRad < measureObject->omittedAngles[6]))
		return true;
	if ((alphaRad > measureObject->omittedAngles[5]) && (alphaRad < measureObject->omittedAngles[4]))
		return true;
	if ((alphaRad > measureObject->omittedAngles[3]) && (alphaRad < measureObject->omittedAngles[2]))
		return true;
	if ((alphaRad > measureObject->omittedAngles[1]) && (alphaRad < measureObject->omittedAngles[0]))
		return true;
	
		
	return false;
}

bool imageProcessing::OmitAngle2(float alphaRad)
{
	if ((alphaRad > measureObject->omitedAngleNavoj[1]) || (alphaRad < measureObject->omitedAngleNavoj[0]))
		return true;



	
	return false;
}
bool imageProcessing::OmitAngleZareza(float alphaRad)
{
	if (measureObject->zarezaAngle1[0] - measureObject->zarezaAngle1[1] > -2)
	{
		if ((alphaRad > measureObject->zarezaAngle1[0]) && (alphaRad < measureObject->zarezaAngle1[1]))
			return true;
	}
	else
	{
		if ((alphaRad > measureObject->zarezaAngle1[1]) || (alphaRad < measureObject->zarezaAngle1[0]))
			return true;
	}
	if (measureObject->zarezaAngle2[0] - measureObject->zarezaAngle2[1] > -2)
	{
		if ((alphaRad > measureObject->zarezaAngle2[0]) && (alphaRad < measureObject->zarezaAngle2[1]))
			return true;
	}
	else
	{
		if ((alphaRad > measureObject->zarezaAngle2[1]) || (alphaRad < measureObject->zarezaAngle2[0]))
			return true;
	}



	return false;
}

void imageProcessing::ClearDraw()
{
	//dodamo na draw sliko 
	qDeleteAll(scene->items());
	//qDeleteAll(sceneForMainWindow->items());
	QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
	pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
}
void imageProcessing::ClearFunctionList()
{
	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->item(i)->setText("");
	}
}
void imageProcessing::SetCurrentBuffer(int dispWindow, int dispImage)
{
	displayWindow = dispWindow;
	displayImage = dispImage;
	//ReplaceCurrentBuffer();
}

void imageProcessing::ReplaceCurrentBuffer()
{
	//if ((displayImage != prevDisplayImage) || (displayWindow != prevDisplayWindow))
	//{
		if (displayWindow == cam.size())
		{
			currImage = images[displayImage];
		}
		else
		{
			currImage = &cam[displayWindow]->image[displayImage];
		}

	if (displayWindow == cam.size())
	{
		QString niz;
		if (slovensko) niz = QString("SHRANJENE SLIKE");
		else niz = QString("SAVED IMAGES");

		ui.labelLocation->setText(QString("%1: %2").arg(niz).arg(displayImage));
	}
	else
	{
		if (slovensko)  ui.labelLocation->setText(QString("KAM: %1, SLIKA: %2").arg(displayWindow).arg(displayImage));
		else ui.labelLocation->setText(QString("CAM: %1, IMAGE: %2").arg(displayWindow).arg(displayImage));
	}
		

	ui.lineInsertImageIndex->setText(QString("%1").arg(displayImage));

	//if((displayImage != prevDisplayImage) || ( prevDisplayWindow != displayWindow)) // zaradi cudnega pojava na dialogu
	//ResizeDisplayRect();
	
	
	ClearDraw();
	ShowImages();


	prevDisplayImage = displayImage;
	prevDisplayWindow = displayWindow;

	if(displayWindow < cam.size())
	currentStation = 0;



	ui.labelCurrStation->setText(QString("station: %1").arg(currentStation));
}



void imageProcessing::OnPressedImageUp()
{
	ClearDialog();
	displayImage++;
	if (displayWindow == cam.size())
	{
		if (displayImage > images.size()-1)
			displayImage = 0;
	}
	else
	{
		if (displayImage > cam[displayWindow]->num_images - 1)
			displayImage = 0;

	}
	ReplaceCurrentBuffer();


}

void imageProcessing::OnPressedImageDown()
{
	ClearDialog();
	displayImage--;
	if (displayWindow == cam.size())
	{
		if ((displayImage > images.size()) ||(displayImage < 0))
			displayImage = images.size()-1;
	}
	else
	{
		if((displayImage >= cam[displayWindow]->num_images - 1) || (displayImage < 0))
			displayImage = cam[displayWindow]->num_images - 1;

	}

	ReplaceCurrentBuffer();

}

void imageProcessing::OnPressedCamUp()
{	

	ClearDialog();
	displayImage = 0;
	displayWindow++;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = 0;

	ReplaceCurrentBuffer();

}

void imageProcessing::OnPressedCamDown()
{
	ClearDialog();
	displayImage = 0;
	displayWindow--;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = cam.size() ;

	ReplaceCurrentBuffer();

}

int imageProcessing::ConvertImageForDisplay(int imageNumber)
{
	Mat test;

	if (currImage->buffer->empty())
	{
		return 0;
	}

	if(currImage->buffer->channels() == 1)
		cvtColor(*currImage->buffer, test, COLOR_GRAY2RGB);
	else
		cvtColor(*currImage->buffer, test, COLOR_BGR2RGB);


	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	
	//QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	
	//showScene->setPixmap(QPixmap::fromImage(qimgOriginal));

		return 1;
}

void imageProcessing::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
		QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			scene = new QGraphicsScene(this);
			sceneForMainWindow = new QGraphicsScene(this);
			ui.imageView->setScene(scene);
			setMouseTracking(true);
			ui.imageView->viewport()->installEventFilter(this);
			//scene->installEventFilter(this);
			
			pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
			pixmapVideo->setZValue(-100);
			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
		zoomFactor = 1;
		ui.imageView->resetTransform();
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.5, 0.5);
	}
}
void imageProcessing::OnProcessCam0()
{
	currentFunction = 0;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();
	int imageNum = 0;
	bool ok;

	

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}
	else
	{
		if (images.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}


}
void imageProcessing::OnProcessCam1()//uporablejna za iz vrha obdelavo kosa 
{
	int imageNum = 0;
	bool ok;
	ClearDraw();
	currentFunction = 1;
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera1(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera1(displayWindow, imageNum, 1);
		}
	}
	else
	{
		ProcessingCamera1(displayWindow, imageNum, 1);

	}

}

void imageProcessing::OnProcessCam2()
{
	currentFunction = 2;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);

	int imageNum = 0;
	bool ok;

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera2(displayWindow, imageNum, 1);
		}
	}
	else
	{
		ProcessingCamera2(displayWindow, imageNum, 1);

	}
	

}

void imageProcessing::OnProcessCam3()
{
	currentFunction = 3;
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();
	ClearDraw();
		//ProcessingCamera3(0, 0, 1);


	int imageNum = 0;
	bool ok;

	if (displayWindow <= cam.size()-1)
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera3(displayWindow, imageNum, 1);
		}
		else
			ProcessingCamera3(displayWindow, imageNum, 1);
	}
	else
	{
		imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size()- 1, 1, &ok, NULL);

		if (ok)
			ProcessingCamera3(displayWindow, imageNum, 1);
	}


}


void imageProcessing::OnProcessCam4()
{
	currentFunction = 4;
	PopulatePropertyBrowser(currentFunction);

	ProcessingCamera4(displayWindow, displayImage, 1);
}

void imageProcessing::OnDoneEditingLineInsertImageIndex()
{
	int bla = 100;
	QString index = ui.lineInsertImageIndex->text();
	int indexnum = index.toInt();
	
	int size = images.size();
	if (displayWindow == cam.size())
	{
		if (indexnum > size - 1)
			displayImage = size - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}

	else
	{
		if (indexnum > cam[displayWindow]->num_images - 1)
			displayImage = cam[displayWindow]->num_images - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}


	
	ReplaceCurrentBuffer();

	
}





void imageProcessing::onPressedAddParameter()
{
	bool ok;
	int insertNum = 0;
	
	if (currentFunction > -1)
	{
		insertNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("insert num:"), item.size(), 0, item.size(), 1, &ok);
		if (ok)
		{

			QStringList selectableTypes;
			selectableTypes << tr("bool parameter") << tr("Int Parameter") << tr("Double Parameter") << tr("rectangle") <<tr("PointFloat");
			int propType;

			QString parameterType = QInputDialog::getItem(this, tr("parameter Type select"),
				tr("Select:"), selectableTypes, 0, false, &ok);

			if (ok)
			{
				QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
					tr("Parameter Name:"), QLineEdit::Normal, tr("name"), &ok);
				if (ok)
				{
					for (int i = 0; i < 5; i++)
					{
						if (parameterType.compare(selectableTypes[i]) == 0)
						{
							if (i == 0)//bool value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction][insertNum]->boolValue);
								item[insertNum]->setEnabled(true);
							}
							else if (i == 1)//int value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Int, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->intValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->intMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->intMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);



							}
							else if (i == 2)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 3)//rectangle
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 4)//Point
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Point, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}


						}
					}
					//item.insert(item.begin() + 2, item.back());
					//topItem->addSubProperty(item[insertNum]);
					PopulatePropertyBrowser(currentFunction);
				}
			}
		}
	}
	else
	{
		 QMessageBox::information(this, tr("information"), "No function is selected");
	}
		

}

void imageProcessing::onPressedRemoveParameters()
{
	int deleteNum = 0;
	bool ok;
	if (currentFunction > -1)
	{
		deleteNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("delete param:"), item.size()-1, 0, item.size(), 1, &ok);
		if (ok)
		{
			types[currentStation][currentType]->prop[currentFunction].erase(types[currentStation][currentType]->prop[currentFunction].begin() + deleteNum);
			//item.erase(item.begin() + deleteNum);

		}


		PopulatePropertyBrowser(currentFunction);

	}
	else
	{
		QMessageBox::information(this, tr("information"), "No function is selected");
	}

}

void imageProcessing::onPressedUpdateParameters()
{

	 if (selectedFunction > 0)
	{
		if (selectedFunction == 10)
		{
			//moramo se zracunati offest od tocke na simetrali do kvadrata, da bo kvadrat postavljen na pravo mesto
			int offsetX1, offsetY1, offsetX2, offsetY2;

			offsetX1 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[1];
			offsetY1 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[2];
			offsetX2 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[5];
			offsetY2 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[6];

			//types[currentStation][currentType]->OnAddParameter();
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][0] = selectedFunction;
			for (int i = 1; i < 30; i++)
			{
				types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][i] = tmpDynamicParameter[i];
			}
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][20] = offsetX1;
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][21] = offsetY1;
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][22] = offsetX2;
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][23] = offsetY2;

			types[currentStation][currentType]->WriteParameters();

			selectedFunction = 0;
			drawRubberFunction = 0;
			currentStation = 0;
			selectedDynamicParameter = -1;
		}
		if (selectedFunction == 11)
		{
			//moramo se zracunati offest od tocke na simetrali do kvadrata, da bo kvadrat postavljen na pravo mesto
			int offsetX1, offsetY1;

			offsetX1 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[1];
			offsetY1 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[2];

			//types[currentStation][currentType]->OnAddParameter();
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][0] = selectedFunction;
			for (int i = 1; i < 30; i++)
			{
				types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][i] = tmpDynamicParameter[i];
			}
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][11] = offsetX1;
			types[currentStation][currentType]->dynamicParameters[selectedDynamicParameter][12] = offsetY1;

			types[currentStation][currentType]->WriteParameters();

			selectedFunction = 0;
			drawRubberFunction = 0;
			currentStation = 0;
			selectedDynamicParameter = -1;
		}
	}
	 else if (currentFunction > -1)
		 SaveFunctionParameters(currentType, currentFunction);
}

void imageProcessing::onPressedSurfaceInspection()
{
	currentFunction = 8;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	

	if(displayWindow == cam.size())
	CheckDefects(displayImage, 1, 1);

}

void imageProcessing::OnPressedCreateRubber()
{
	createRubber = 1;

}

void imageProcessing::OnPressedResizeRubber()
{

	gripTopLeft->setVisible(true);
	//gripTopLeft->repaint();
	gripBottomRight->setVisible(true);
	//gripBottomRight->repaint();
	changeRubberBand = 1;
	if (RectItem[selectedRubberRect]->scene() != nullptr)
	{
		scene->removeItem(RectItem[selectedRubberRect]);
	}

	int x = rubberRect[selectedRubberRect].x();
	int y = rubberRect[selectedRubberRect].y();
	int x2 = x + rubberRect[selectedRubberRect].width();
	int y2 = y + rubberRect[selectedRubberRect].height();
	QPointF topLeft = ui.imageView->mapFromScene(QPointF(x, y));
	QPointF bottomR = ui.imageView->mapFromScene(QPointF(x2, y2));

	QPoint left = topLeft.toPoint();
	QPoint right = bottomR.toPoint();
	rubberBand->setGeometry(QRect(left, right));
	rubberBand->show();

}

void imageProcessing::OnPressedSelectRubber()
{
	rubberResizeMode = 1;
}

void imageProcessing::OnPressedTestIntensityFunction()
{
	selectedFunction = 1;
	if (drawRubberFunction == 0)
	{
		RectItem.clear();
		ClearDraw();
		rubberRect.clear();
		rubberRectFunction.clear();
		drawRubberFunction = 1;
		OnPressedCreateRubber();
	}
	else if (drawRubberFunction == 1)
	{
		ClearDraw();
		scene->addRect(rubberRect[0], QPen(Qt::red), QBrush(Qt::NoBrush));
		RectItem[0] = scene->addRect(rubberRect[0], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		currImage->rect = rubberRect[0];
		currImage->CreateIntensity();
		currImage->VerticalIntensity(2);
		scene->addItem(currImage->DrawIntensity(QPen(Qt::yellow)));
		drawRubberFunction = 0;
		//selectedFunction = -1;
	}


}



void imageProcessing::PopulatePropertyBrowser(int function)
{

	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();

	
	for (int i = 0; i < types[currentStation][currentType]->prop[function].size(); i++)
	{
		if (types[currentStation][currentType]->prop[function][i]->propType == 0)	//bool
		{
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->boolValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 1)	//int
		{
			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 2)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 3)	//rect
		{
			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 4)	//CPointFloat 
		{
			item.push_back(variantManager->addProperty(QVariant::Point, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->pointValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else
		{

		}
		if ((loginRights == 1)||(loginRights == 2))
			item.back()->setEnabled(true);
		else
			item.back()->setEnabled(false);
	}

}

void imageProcessing::PopulatePropertyBrowserDynamicFunctions(int function)
{
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();
	for (int i = 0; i < 30; i++)
	{
		tmpDynamicParameter[i] = 0;
	}
	if (function == 10)//height or width function in two rect
	{
		item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 1")));
		item.back()->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item.back()->setEnabled(true);
		topItem->addSubProperty(item.back());
		item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 2")));
		item.back()->setValue(QRect(tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8]));
		item.back()->setEnabled(true);
		topItem->addSubProperty(item.back());

		QStringList list;
		list.append("HORIZONTAL");
		list.append("VERTICAL");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("distance Direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("TOP TO BOTTOM");
		list.append("BOTTOM TO TOP");
		list.append("LEFT TO RIGHT");
		list.append("RIGHT TO LEFT");

		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 1 direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());


		list.clear();
		list.append("WHITE TO BLACK");
		list.append("BLACK TO WHITE");

		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 1 CONDITION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());
		

		list.clear();
		list.append("TOP TO BOTTOM");
		list.append("BOTTOM TO TOP");
		list.append("LEFT TO RIGHT");
		list.append("RIGHT TO LEFT");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 2 direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());


		list.clear();
		list.append("WHITE TO BLACK");
		list.append("BLACK TO WHITE");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 2 CONDITION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("AVRAGE");
		list.append("MINIMUM");
		list.append("MAXIMUM");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("TYPE")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();

		item.push_back(variantManager->addProperty(QVariant::Int, QString("FILTER SIZE")));
		item.back()->setAttribute(QLatin1String("minimum"), 1);
		item.back()->setEnabled(true);
		item.back()->setValue(1);
		topItem->addSubProperty(item.back());

		item.push_back(variantManager->addProperty(QVariant::Int, QString("THRESHOLD")));
		item.back()->setAttribute(QLatin1String("minimum"), 1);
		item.back()->setAttribute(QLatin1String("maximum"), 255);
		item.back()->setEnabled(true);
		item.back()->setValue(120);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("ARROW");
		list.append("LINE");
		list.append("NO DRAW");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("DRAW")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("FIRST RECT POINT");
		list.append("SECOND RECT POINT");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("DRAW LOCATION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		item.push_back(variantManager->addProperty(QVariant::Color, QString("Draw Color")));
		

		int value = 65535;
		//QString hexValue = QString::number(value, 16);
		QString text = QString("%1").number(value, 16);
		text.prepend("#00");
		QColor color(text);
		item.back()->setEnabled(true);
		item.back()->setValue(color);
		topItem->addSubProperty(item.back());

	}
	else if (function == 11)
	{
	item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 1")));
	item.back()->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
	item.back()->setEnabled(true);
	topItem->addSubProperty(item.back());

	QStringList list;
	list.clear();
	list.append("TOP TO BOTTOM");
	list.append("BOTTOM TO TOP");
	list.append("LEFT TO RIGHT");
	list.append("RIGHT TO LEFT");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area direction")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());


	list.clear();
	list.append("WHITE TO BLACK");
	list.append("BLACK TO WHITE");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area CONDITION")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());


	item.push_back(variantManager->addProperty(QVariant::Int, QString("THRESHOLD")));
	item.back()->setAttribute(QLatin1String("minimum"), 1);
	item.back()->setAttribute(QLatin1String("maximum"), 255);
	item.back()->setEnabled(true);
	item.back()->setValue(120);
	topItem->addSubProperty(item.back());


	list.clear();
	list.append("AVRAGE");
	list.append("MAXIMUM");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("TYPE")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());

	list.clear();

	item.push_back(variantManager->addProperty(QVariant::Int, QString("FILTER SIZE")));
	item.back()->setAttribute(QLatin1String("minimum"), 1);
	item.back()->setEnabled(true);
	item.back()->setValue(5);
	topItem->addSubProperty(item.back());

	item.push_back(variantManager->addProperty(QVariant::Color, QString("Draw Color")));


	int value = 60000;
	//QString hexValue = QString::number(value, 16);
	QString text = QString("%1").number(value, 16);
	text.prepend("#00");
	QColor color(text);
	item.back()->setEnabled(true);
	item.back()->setValue(color);
	topItem->addSubProperty(item.back());


	}

		/*QtVariantProperty *reportType = variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), "Report Type");
		QStringList types;
		types << "Bug" << "Suggestion" << "To Do";
		reportType->setAttribute("enumNames", types);
		reportType->setValue(1); // "Suggestion"*/

	/*
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("HEIGHT")));
			item.back()->setValue(true);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());*/

}

void imageProcessing::UpdatePropertyBrwserDynamicFunctions(int function)
{
	if (function == 10)
	{
		item[0]->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item[1]->setValue(QRect(tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8]));
		item[2]->setValue(tmpDynamicParameter[9]);
		item[3]->setValue(tmpDynamicParameter[10]);
		item[4]->setValue(tmpDynamicParameter[11]);
		item[5]->setValue(tmpDynamicParameter[12]);
		item[6]->setValue(tmpDynamicParameter[13]);
		item[7]->setValue(tmpDynamicParameter[14]);
		item[8]->setValue(tmpDynamicParameter[15]);
		item[9]->setValue(tmpDynamicParameter[16]);
		item[10]->setValue(tmpDynamicParameter[17]);
		item[11]->setValue(tmpDynamicParameter[18]);
		item[12]->setValue(tmpDynamicParameter[19]);


		for (int i = 0; i < 30; i++)
		{
			tmpDynamicParameterold[i] = tmpDynamicParameter[i];
		}
	}
	else if (function == 11)
	{
		item[0]->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item[1]->setValue(tmpDynamicParameter[5]);
		item[2]->setValue(tmpDynamicParameter[6]);
		item[3]->setValue(tmpDynamicParameter[7]);
		item[4]->setValue(tmpDynamicParameter[8]);
		item[4]->setValue(tmpDynamicParameter[9]);
		item[4]->setValue(tmpDynamicParameter[10]);//color

		for (int i = 0; i < 30; i++)
		{
			tmpDynamicParameterold[i] = tmpDynamicParameter[i];
		}
	}


}

void imageProcessing::ReadPropertyBrowserDynamicFunctions(int function)
{
	QRect tmpRect;

		if (function == 10)
		{
			tmpRect = item[0]->value().toRect();
			if(rubberRect.size() > 0)
			rubberRect[0] = tmpRect;
			tmpDynamicParameter[1] = tmpRect.x();
			tmpDynamicParameter[2] = tmpRect.y();
			tmpDynamicParameter[3] = tmpRect.width();
			tmpDynamicParameter[4] = tmpRect.height();
			tmpRect = item[1]->value().toRect();
			if (rubberRect.size() > 1)
			rubberRect[1] = tmpRect;
			tmpDynamicParameter[5] = tmpRect.x();
			tmpDynamicParameter[6] = tmpRect.y();
			tmpDynamicParameter[7] = tmpRect.width();
			tmpDynamicParameter[8] = tmpRect.height();
			tmpDynamicParameter[9] = item[2]->value().toInt(); // horizotal or verikal
			tmpDynamicParameter[10] = item[3]->value().toInt(); // rect1 direction
			tmpDynamicParameter[11] = item[4]->value().toInt(); //rect1 black to white
			tmpDynamicParameter[12] = item[5]->value().toInt(); //rect2 direction
			tmpDynamicParameter[13] = item[6]->value().toInt(); //rect2 black to white
			tmpDynamicParameter[14] = item[7]->value().toInt(); ////TYPE
			tmpDynamicParameter[15] = item[8]->value().toInt(); //FILTER SIZE
			tmpDynamicParameter[16] = item[9]->value().toInt(); //THRESHOLD
			tmpDynamicParameter[17] = item[10]->value().toInt(); //TYPE OF DRAW
			tmpDynamicParameter[18] = item[11]->value().toInt(); //LOCATION OF DRAW
			QString colorString = item[12]->value().toString(); //DRAW COLOR 
			bool ok;
			colorString = colorString.remove('#');
			int ColorInt = colorString.toInt(&ok, 16);
			tmpDynamicParameter[19] = ColorInt;

			int bla = 100;
		}
		else if (function == 11)
		{
			tmpRect = item[0]->value().toRect();
			if (rubberRect.size() > 0)
				rubberRect[0] = tmpRect;
			tmpDynamicParameter[1] = tmpRect.x();
			tmpDynamicParameter[2] = tmpRect.y();
			tmpDynamicParameter[3] = tmpRect.width();
			tmpDynamicParameter[4] = tmpRect.height();
			tmpDynamicParameter[5] = item[1]->value().toInt(); // rect direction
			tmpDynamicParameter[6] = item[2]->value().toInt(); // black to white
			tmpDynamicParameter[7] = item[3]->value().toInt(); //prag
			tmpDynamicParameter[8] = item[4]->value().toInt(); //tip
			tmpDynamicParameter[9] = item[5]->value().toInt(); //filter size 

			QString colorString = item[6]->value().toString(); //DRAW COLOR 
			bool ok;
			colorString = colorString.remove('#');
			int ColorInt = colorString.toInt(&ok, 16);
			tmpDynamicParameter[10] = ColorInt;


		}
	
}

void imageProcessing::SaveFunctionParameters(int typeNr, int function)
{
	QStringList list;

	int type = -1;
	int intSize;
	if (function > -1)
	{
		for (int i = 0; i < item.size(); i++)
		{
			if (i < 10)
				intSize = 2;
			else if (i < 100)
				intSize = 3;
			type = item[i]->valueType();
			switch (type)
			{
			case QVariant::Bool: //bool
				types[currentStation][typeNr]->prop[function][i]->propType = 0;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->boolValue = item[i]->value().toBool();
				break;
			case QVariant::Int: //int
				types[currentStation][typeNr]->prop[function][i]->propType = 1;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->intValue = item[i]->value().toInt();

				break;
			case QVariant::Double: //double
				types[currentStation][typeNr]->prop[function][i]->propType = 2;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->doubleValue = item[i]->value().toDouble();
				break;
			case QVariant::Rect: //rectangle
				types[currentStation][typeNr]->prop[function][i]->propType = 3;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toRect();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			case QVariant::Point: //point
				types[currentStation][typeNr]->prop[function][i]->propType = 4;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->pointValue = item[i]->value().toPoint();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			}
		}

		//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(currentStation);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[0][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QFile file(filePath);
			file.remove();

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[currentStation][typeNr]->prop[function].size(); i++)
			{
				switch (types[currentStation][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.height());
					break;
				case 4://PointF
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.x())
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.y());

					break;


				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}

			QMessageBox::information(this, tr("information"), "Parameters saved");
		
	}
	else
		QMessageBox::information(this, tr("information"), "No function is selected");
	

}

void imageProcessing::SaveFunctionParametersOnNewType(int station, int typeNr, int function)
{

	QStringList list;
			//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(station);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[station][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[station][typeNr]->prop[function].size(); i++)
			{
				switch (types[station][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.height());
					break;
				case 4://point
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.x())
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.y());


				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}


}

void imageProcessing::DeleteTypeProperty(QString  typeName)
{


}

void imageProcessing::LoadTypesProperty()
{
	QStringList values;
	
	QString filePath;
	QString dirPath;
	QDir dir;
	QString functionName;
	int functionNr;
	int n = 0;


	for (int i = 0; i < NR_STATIONS; i++)
	{

		//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
		dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(dirPath, QSettings::IniFormat);

		if (!QDir(dirPath).exists())
			QDir().mkdir(dirPath);


		dir.setPath(dirPath);
		dir.setNameFilters(QStringList() << "parameters*");
		int count = dir.count();
		QStringList dirList = dir.entryList();
		for (int j = 0; j < count; j++)
		{

			filePath = dirPath + QString("/%1").arg(dirList.at(j));


			dir.setPath(filePath);
			dir.setNameFilters(QStringList() << "*.ini");
			int count2 = dir.count();
			QStringList list = dir.entryList();
			//inputs
			for (int k = 0; k < count2; k++)
			{
				n = 0;
				functionName = list[k];
				functionName.chop(4);
				functionName.remove(0, 9);
				functionNr = functionName.toInt();

				QString path = filePath + QString("/%1").arg(list.at(k));
				QSettings settings(path, QSettings::IniFormat);

				do {
					values.clear();
					values = settings.value(QString("parameter%1").arg(n)).toStringList();

					if (values.size() > 1)
					{
						types[i][j]->prop[functionNr].push_back(new CProperty());

						switch (values[0].toInt())
						{
						case 0://bool
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->boolValue = values[2].toInt();
							break;
						case 1://integer
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->intValue = values[2].toInt();
							types[i][j]->prop[functionNr].back()->intMinValue = values[3].toInt();
							types[i][j]->prop[functionNr].back()->intMaxValue = values[4].toInt();
							break;
						case 2://double
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->doubleValue = values[2].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMinValue = values[3].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMaxValue = values[4].toDouble();
							break;
						case 3://rectangle
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->rectValue.setX(values[2].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setY(values[3].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setWidth(values[4].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setHeight(values[5].toInt());

							break;
						case 4://rectangle
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->pointValue.setX(values[2].toInt());
							types[i][j]->prop[functionNr].back()->pointValue.setY(values[3].toInt());


							break;
						}
					}
					n++;
				} while (values.size() > 0);
			}
		}
	}
}

void imageProcessing::OnViewTimer()
{
	int isChanged = 0;

	if (selectedFunction > 0)
	{
		if (drawRubberFunction == 32)
		{
			ReadPropertyBrowserDynamicFunctions(selectedFunction);
			for (int i = 0; i < 30; i++)
			{
				if (tmpDynamicParameter[i] != tmpDynamicParameterold[i])
				{
					isChanged = 1;
				}
				tmpDynamicParameterold[i] = tmpDynamicParameter[i];
			}
			for (int i = 0; i < rubberRect.size(); i++)
			{
				if (rubberRect[i] != rubberRectOld[i])
				{
					tmpDynamicParameter[5] = rubberRect[1].x();
					tmpDynamicParameter[6] = rubberRect[1].y();
					tmpDynamicParameter[7] = rubberRect[1].width();
					tmpDynamicParameter[8] = rubberRect[1].height();
					tmpDynamicParameter[1] = rubberRect[0].x();
					tmpDynamicParameter[2] = rubberRect[0].y();
					tmpDynamicParameter[3] = rubberRect[0].width();
					tmpDynamicParameter[4] = rubberRect[0].height();
					rubberRectOld[i] = rubberRect[i];
					isChanged = 1;
				}
			}
			if (isChanged == 1)
			{
				AddnewHeightMeasurement();//testiramo funkcijo 
				isChanged = 0;
			}
		}
		else if (drawRubberFunction == 34)
		{
			ReadPropertyBrowserDynamicFunctions(selectedFunction);
			for (int i = 0; i < 30; i++)
			{
				if (tmpDynamicParameter[i] != tmpDynamicParameterold[i])
				{
					isChanged = 1;
				}
				tmpDynamicParameterold[i] = tmpDynamicParameter[i];
			}
			for (int i = 0; i < rubberRect.size(); i++)
			{
				if (rubberRect[i] != rubberRectOld[i])
				{
					tmpDynamicParameter[1] = rubberRect[0].x();
					tmpDynamicParameter[2] = rubberRect[0].y();
					tmpDynamicParameter[3] = rubberRect[0].width();
					tmpDynamicParameter[4] = rubberRect[0].height();
					rubberRectOld[i] = rubberRect[i];
					isChanged = 1;
				}
			}
			if (isChanged == 1)
			{
				LineDeviationMeasurement();//testiramo funkcijo 
				isChanged = 0;
			}
		}
	}
}

void imageProcessing::AddnewHeightMeasurement(void)
{
	selectedFunction = 10;


	if (drawRubberFunction == 0)
	{
		RectItem.clear();
		ClearDraw();
		rubberRect.clear();
		rubberRectFunction.clear();
		drawRubberFunction = 30;
		OnPressedCreateRubber();
		PopulatePropertyBrowserDynamicFunctions(selectedFunction);
		
	}
	else if (drawRubberFunction == 30)//prvi kvadrat
	{
		scene->addRect(rubberRect[0], QPen(Qt::red), QBrush(Qt::NoBrush));
		RectItem[0] = scene->addRect(rubberRect[0], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		currImage->rect = rubberRect[0];
		tmpDynamicParameter[1] = rubberRect[0].x();
		tmpDynamicParameter[2] = rubberRect[0].y();
		tmpDynamicParameter[3] = rubberRect[0].width();
		tmpDynamicParameter[4] = rubberRect[0].height();
		OnPressedCreateRubber();
		drawRubberFunction = 31;
	}
	else if (drawRubberFunction == 31)//drugi kvadrat
	{
		tmpDynamicParameter[5] = rubberRect[1].x();
		tmpDynamicParameter[6] = rubberRect[1].y();
		tmpDynamicParameter[7] = rubberRect[1].width();
		tmpDynamicParameter[8] = rubberRect[1].height();
		scene->addRect(rubberRect[1], QPen(Qt::red), QBrush(Qt::NoBrush));
		RectItem[1] = scene->addRect(rubberRect[1], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		drawRubberFunction = 32;
	}
	else if (drawRubberFunction == 32)//test funkcije in izris 
	{
		ClearDraw();

		

		FindTopAndBottom(2, 0, 1);

		dynamicFunctionRect1.setRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]);
		dynamicFunctionRect2.setRect(tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8]);
		

		scene->addRect(dynamicFunctionRect1, QPen(Qt::red), QBrush(Qt::NoBrush));
		scene->addRect(dynamicFunctionRect2, QPen(Qt::red), QBrush(Qt::NoBrush));

		DynamicFunctionMeasureDistance(displayWindow, displayImage, 1, dynamicFunctionRect1, dynamicFunctionRect2, tmpDynamicParameter[9], tmpDynamicParameter[10], tmpDynamicParameter[11], tmpDynamicParameter[12], tmpDynamicParameter[13], tmpDynamicParameter[14], tmpDynamicParameter[15], tmpDynamicParameter[16], tmpDynamicParameter[17], tmpDynamicParameter[18], tmpDynamicParameter[19], 0, 0,0,0,1,-1);
		//drawRubberFunction = 0;
	}
	if (drawRubberFunction > 0)
		UpdatePropertyBrwserDynamicFunctions(selectedFunction);
}

void imageProcessing::LineDeviationMeasurement(void)
{
	selectedFunction = 11;
	if (drawRubberFunction == 0)
	{
		RectItem.clear();
		ClearDraw();
		rubberRect.clear();
		rubberRectFunction.clear();
		drawRubberFunction = 33;
		OnPressedCreateRubber();
		PopulatePropertyBrowserDynamicFunctions(selectedFunction);
	}
	else if (drawRubberFunction == 33)//prvi kvadrat
	{
		scene->addRect(rubberRect[0], QPen(Qt::red), QBrush(Qt::NoBrush));
		RectItem[0] = scene->addRect(rubberRect[0], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		currImage->rect = rubberRect[0];
		tmpDynamicParameter[1] = rubberRect[0].x();
		tmpDynamicParameter[2] = rubberRect[0].y();
		tmpDynamicParameter[3] = rubberRect[0].width();
		tmpDynamicParameter[4] = rubberRect[0].height();
		OnPressedCreateRubber();
		drawRubberFunction = 34;
	}
	else if (drawRubberFunction == 34)//test funkcije in izris 
	{
		ClearDraw();

		dynamicFunctionRect1.setRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]);

		FindTopAndBottom(2, 0, 1);
		DynamicFunctionLineDeviation(displayWindow, displayImage, 1, dynamicFunctionRect1,tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8], tmpDynamicParameter[9], tmpDynamicParameter[10], 0, 0,1, -1);
	}

	if (drawRubberFunction > 0)
		UpdatePropertyBrwserDynamicFunctions(selectedFunction);

}

int imageProcessing::SaveSelectedDynamicFunctionAsMeasurment()
{

	QStringList list;
	int stNum;

	if (NR_STATIONS > 1)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			list.push_back(QString("%1").arg(i));
		}
		bool ok;
		QString text;

		if (slovensko)  text = QInputDialog::getItem(this, tr("Izberi postajo"), tr("Postaja:"), list, 0, false, &ok);
		else text = QInputDialog::getItem(this, tr("Select station"), tr("Station:"), list, 0, false, &ok);

		if (!ok) return 0;

		stNum = text.toInt();
	}
	else
		stNum = 0;
	if (selectedFunction == 10)
	{

		//moramo se zracunati offest od tocke na simetrali do kvadrata, da bo kvadrat postavljen na pravo mesto
		int offsetX1, offsetY1, offsetX2,offsetY2;

		offsetX1 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[1];
		offsetY1 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[2];
		offsetX2 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[5];
		offsetY2 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[6];


		types[stNum][currentType]->OnAddParameter();
		types[stNum][currentType]->name[types[stNum][currentType]->parameterCounter - 1] = "Dynamic10";
		types[stNum][currentType]->isActive[types[stNum][currentType]->parameterCounter - 1] = 1;
		types[stNum][currentType]->correctFactor[types[stNum][currentType]->parameterCounter - 1] = 1;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][0] = selectedFunction;
		for (int i = 1; i < 30; i++)
		{
			types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][i] = tmpDynamicParameter[i];
		}
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][20] = offsetX1;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][21] = offsetY1;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][22] = offsetX2;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][23] = offsetY2;
	
		types[stNum][currentType]->WriteParameters();

		selectedFunction = 0;
		drawRubberFunction = 0;
	}	
	else if (selectedFunction == 11)
	{

		//moramo se zracunati offest od tocke na simetrali do kvadrata, da bo kvadrat postavljen na pravo mesto
		int offsetX1, offsetY1;

		offsetX1 = measureObject->topOfSimetralaPoint.x - tmpDynamicParameter[1];
		offsetY1 = measureObject->topOfSimetralaPoint.y - tmpDynamicParameter[2];


		types[stNum][currentType]->OnAddParameter();
		types[stNum][currentType]->name[types[stNum][currentType]->parameterCounter - 1] = "Dynamic11";
		types[stNum][currentType]->isActive[types[stNum][currentType]->parameterCounter - 1] = 1;
		types[stNum][currentType]->correctFactor[types[stNum][currentType]->parameterCounter - 1] = 1;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][0] = selectedFunction;
		for (int i = 1; i < 30; i++)
		{
			types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][i] = tmpDynamicParameter[i];
		}
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][11] = offsetX1;
		types[stNum][currentType]->dynamicParameters[types[stNum][currentType]->parameterCounter - 1][12] = offsetY1;

		types[stNum][currentType]->WriteParameters();

		selectedFunction = 0;
		drawRubberFunction = 0;
	}





	return 0;
}

int imageProcessing::EditSelectedDynamicFunctionParameter()
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	bool ok;
	QString text;
	if (NR_STATIONS > 1)
		{

			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text;

			if (slovensko)  text = QInputDialog::getItem(this, tr("Izberi postajo"), tr("Postaja:"), list, 0, false, &ok);
			else text = QInputDialog::getItem(this, tr("Select station"), tr("Station:"), list, 0, false, &ok);

			if (!ok) return 0;

			stNum = text.toInt();
		}
		else
			stNum = 0;

	list.clear();
	for (int i = 0; i < types[stNum][currentType]->parameterCounter; i++)
	{

		if(types[stNum][currentType]->dynamicParameters[i][0] > 0)
		list.push_back(QString("%1").arg(i));
	}


	if (slovensko)  text = QInputDialog::getItem(this, tr("Izberi paramter"), tr("Paramter:"), list, 0, false, &ok);
	else text = QInputDialog::getItem(this, tr("Select station"), tr("Station:"), list, 0, false, &ok);

	if (!ok) return 0;

	stNum = text.toInt();
	QRect rect1, rect2;
	float result;
	int currFunction = types[2][currentType]->dynamicParameters[stNum][0];
	FindTopAndBottom(2, 0, 1);//tukaj po novem poiscem samo simetralo ostale funkcije so dinamicne
	if (currFunction == 10)//funkcija za meritev sirin in visin;
	{
	
		rect1.setRect(types[2][currentType]->dynamicParameters[stNum][1], types[2][currentType]->dynamicParameters[stNum][2], types[2][currentType]->dynamicParameters[stNum][3], types[2][currentType]->dynamicParameters[stNum][4]);
		rect2.setRect(types[2][currentType]->dynamicParameters[stNum][5], types[2][currentType]->dynamicParameters[stNum][6], types[2][currentType]->dynamicParameters[stNum][7], types[2][currentType]->dynamicParameters[stNum][8]);
		result = DynamicFunctionMeasureDistance(2, 0, 1, rect1, rect2, types[2][currentType]->dynamicParameters[stNum][9], types[2][currentType]->dynamicParameters[stNum][10], types[2][currentType]->dynamicParameters[stNum][11],
			types[2][currentType]->dynamicParameters[stNum][12], types[2][currentType]->dynamicParameters[stNum][13], types[2][currentType]->dynamicParameters[stNum][14], types[2][currentType]->dynamicParameters[stNum][15],
			types[2][currentType]->dynamicParameters[stNum][16], types[2][currentType]->dynamicParameters[stNum][17], types[2][currentType]->dynamicParameters[stNum][18], types[2][currentType]->dynamicParameters[stNum][19], types[2][currentType]->dynamicParameters[stNum][20],
			types[2][currentType]->dynamicParameters[stNum][21], types[2][currentType]->dynamicParameters[stNum][22], types[2][currentType]->dynamicParameters[stNum][23], 1, stNum);
		//measureObject->measuredValue[2].push_back(result);

		PopulatePropertyBrowserDynamicFunctions(currFunction);

		tmpDynamicParameter[1] = types[2][currentType]->dynamicParameters[stNum][1];
		tmpDynamicParameter[2] = types[2][currentType]->dynamicParameters[stNum][2];
		tmpDynamicParameter[3] = types[2][currentType]->dynamicParameters[stNum][3];
		tmpDynamicParameter[4] = types[2][currentType]->dynamicParameters[stNum][4];
		tmpDynamicParameter[5] = types[2][currentType]->dynamicParameters[stNum][5];
		tmpDynamicParameter[6] = types[2][currentType]->dynamicParameters[stNum][6];
		tmpDynamicParameter[7] = types[2][currentType]->dynamicParameters[stNum][7];
		tmpDynamicParameter[8] = types[2][currentType]->dynamicParameters[stNum][8];
		for (int i = 9; i < 24; i++)
		{
			tmpDynamicParameter[i] = types[2][currentType]->dynamicParameters[stNum][i];
		}
		selectedDynamicParameter = stNum;
		currentStation = 2;
		UpdatePropertyBrwserDynamicFunctions(currFunction);
		drawRubberFunction = 32;
		selectedFunction = 10;

		

	}
	else if (currFunction == 11)
	{


		rect1.setRect(types[2][currentType]->dynamicParameters[stNum][1], types[2][currentType]->dynamicParameters[stNum][2], types[2][currentType]->dynamicParameters[stNum][3], types[2][currentType]->dynamicParameters[stNum][4]);
		
		result = DynamicFunctionLineDeviation(2, 0, 1, rect1, types[2][currentType]->dynamicParameters[stNum][5], types[2][currentType]->dynamicParameters[stNum][6], types[2][currentType]->dynamicParameters[stNum][7],
			types[2][currentType]->dynamicParameters[stNum][8], types[2][currentType]->dynamicParameters[stNum][9], types[2][currentType]->dynamicParameters[stNum][10], types[2][currentType]->dynamicParameters[stNum][11] , types[2][currentType]->dynamicParameters[stNum][12], 1, stNum);
			
		//measureObject->measuredValue[2].push_back(result);

		PopulatePropertyBrowserDynamicFunctions(currFunction);

		tmpDynamicParameter[1] = types[2][currentType]->dynamicParameters[stNum][1];
		tmpDynamicParameter[2] = types[2][currentType]->dynamicParameters[stNum][2];
		tmpDynamicParameter[3] = types[2][currentType]->dynamicParameters[stNum][3];
		tmpDynamicParameter[4] = types[2][currentType]->dynamicParameters[stNum][4];
		tmpDynamicParameter[5] = types[2][currentType]->dynamicParameters[stNum][5];
		tmpDynamicParameter[6] = types[2][currentType]->dynamicParameters[stNum][6];
		tmpDynamicParameter[7] = types[2][currentType]->dynamicParameters[stNum][7];
		tmpDynamicParameter[8] = types[2][currentType]->dynamicParameters[stNum][8];
		for (int i = 9; i < 30; i++)
		{
			tmpDynamicParameter[i] = types[2][currentType]->dynamicParameters[stNum][i];
		}
		selectedDynamicParameter = stNum;
		currentStation = 2;
		UpdatePropertyBrwserDynamicFunctions(currFunction);
		drawRubberFunction = 34;
		selectedFunction = 11;


	}



	return 0;
}

float  imageProcessing::DynamicFunctionMeasureDistance(int nrCam, int imageindex, int draw, QRect rect1, QRect rect2, int horOrVer, int rec1Dir, int rect1BlackToWhite, int rec2Dir, int rect2BlackToWhite, int type, int filter, int threshold, int drawType,int drawLocation, int color, int xOffset1, int yOffset1,int xoffset2, int yoffset2,int testMode,int currParam)
{

		CRectRotated firstRect = rect1;
		CRectRotated secundRect = rect2;
		vector<CPointFloat> tmpPoints;
		vector<CPointFloat> tmpPoints2;
		CPointFloat point1, point2;
		float x, y;
		int nrPoints;

		//offset;
		CPointFloat topSimetrala = measureObject->topOfSimetralaPoint;
		CRectRotated tmpRect1;
		CRectRotated tmpRect2;
		if (testMode == 0)
		{
			//tmpRect1 = rect1;
			tmpRect1.setX(topSimetrala.x - xOffset1);
			tmpRect1.setY(topSimetrala.y - yOffset1);
			tmpRect1.setWidth(rect1.width());
			tmpRect1.setHeight(rect1.height());

			//tmpRect2 = rect2;
			tmpRect2.setX(topSimetrala.x - xoffset2);
			tmpRect2.setY(topSimetrala.y - yoffset2);
			tmpRect2.setWidth(rect2.width());
			tmpRect2.setHeight(rect2.height());
		}
		else
		{
			tmpRect1 = rect1;
			tmpRect2 = rect2;
		}
		

		if (rec1Dir < 2)
			nrPoints = rect1.width()/2;
		else
			nrPoints = rect1.height()/2;
		cam[nrCam]->image[imageindex].GetFirstEdgePointsVectorRotated(tmpRect1, rec1Dir, nrPoints, 1, rect1BlackToWhite, threshold, tmpPoints, 1, false);

		if (rec2Dir < 2)
			nrPoints = rect2.width()/2;
		else
			nrPoints = rect2.height()/2;

		cam[nrCam]->image[imageindex].GetFirstEdgePointsVectorRotated(tmpRect2, rec2Dir, nrPoints, 1, rect2BlackToWhite, threshold, tmpPoints2, 1, false);

		if (type == 0)//avrage //min/max -pri min in max je potrebno preverjat smer v katero isce
		{
			x = 0;
			y = 0;
			for (int i = 0; i < tmpPoints.size(); i++)
			{
				x += tmpPoints[i].x;
				y += tmpPoints[i].y;
			}
			x /= tmpPoints.size();
			y /= tmpPoints.size();
			point1.SetPointFloat(x, y);
			x = 0;
			y = 0;
			for (int i = 0; i < tmpPoints2.size(); i++)
			{
				x += tmpPoints2[i].x;
				y += tmpPoints2[i].y;
			}
			x /= tmpPoints2.size();
			y /= tmpPoints2.size();
			point2.SetPointFloat(x, y);
		}
		else if (type == 1) //izracuna minimalno razdaljo 
		{

		}

		CLine pravokotnica;
		CLine paralla;
		CPointFloat interPoint;
		float distance;
		CLine drawLine;
		if (horOrVer == 0)//horizontalno tocke poiscejo pravokotnico na simetralo
		{
			if (drawLocation == 0)
			{
				paralla = measureObject->simetrala.GetParallel(point2);
				pravokotnica = measureObject->simetrala.GetPerpendicular(point1);
				interPoint = pravokotnica.GetIntersectionPoint(paralla);
				distance = interPoint.GetDistance(point1);
				drawLine.SetLine(point1, interPoint);
			}
			else
			{
				paralla = measureObject->simetrala.GetParallel(point1);
				pravokotnica = measureObject->simetrala.GetPerpendicular(point2);
				interPoint = pravokotnica.GetIntersectionPoint(paralla);
				distance = interPoint.GetDistance(point2);
				drawLine.SetLine(point2, interPoint);

			}
		}
		else
		{
			if (drawLocation == 0)
			{
				pravokotnica = measureObject->simetrala.GetPerpendicular(point2);
				paralla = measureObject->simetrala.GetParallel(point1);
				interPoint = pravokotnica.GetIntersectionPoint(paralla);
				distance = interPoint.GetDistance(point1);
				drawLine.SetLine(point1, interPoint);
			}
			else
			{
				pravokotnica = measureObject->simetrala.GetPerpendicular(point1);
				paralla = measureObject->simetrala.GetParallel(point2);
				interPoint = pravokotnica.GetIntersectionPoint(paralla);
				distance = interPoint.GetDistance(point2);
				drawLine.SetLine(point2, interPoint);
			}

		}

		
		//QString hexValue = QString::number(value, 16);
		QString text = QString("%1").number(color, 16);

			if (text.size() == 0)
				text.prepend("#000000");
			if (text.size() == 1)
				text.prepend("#00000");
			if (text.size() == 2)
				text.prepend("#0000");
			if (text.size() == 3)
				text.prepend("#000");
			if (text.size() == 4)
				text.prepend("#00");
			if (text.size() == 5)
				text.prepend("#0");
			if (text.size() == 6)
				text.prepend("#");

		QColor colorR(text);
		QPen drawPen;
		drawPen.setWidth(3);
		drawPen.setColor(colorR);
		if (draw)
		{
			
			scene->addRect(tmpRect2, drawPen, QBrush(Qt::NoBrush));
			scene->addRect(tmpRect1, drawPen, QBrush(Qt::NoBrush));
			ui.listFunctionsReturn->item(0)->setText(QString("distance = %1").arg(distance));
		
			
			if (drawType == 0)
				scene->addItem(drawLine.DrawArrow(drawPen, 2, 30, 30));
			else if (drawType == 1)
				scene->addItem(drawLine.DrawSegment(drawPen, 3));

			for (int i = 0; i < tmpPoints.size(); i++)
			{
				scene->addItem(tmpPoints[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));

			}
			for (int i = 0; i < tmpPoints2.size(); i++)
			{
				scene->addItem(tmpPoints2[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));

			}
			scene->addItem(point1.DrawDot(drawPen, QBrush(Qt::green), 10));
			scene->addItem(point2.DrawDot(drawPen, QBrush(Qt::green), 10));

		}
		QPen pen(colorR);
		pen.setWidth(15);
		QFont font("MS Shell Dlg 2");
		font.setPointSize(48);

		//testMODE 
		if (testMode == 0)//doda izrise za glavni ekran. Pri trenutnem projektu  hrast za postajo 2
		{
			QString tmp;
			if (currParam >= 0)
				tmp = types[2][currentType]->name[currParam];
			else
				tmp = "";
			CPointFloat midd = drawLine.GetMiddlePoint();
			if (drawType == 0)
			mainScreenDrawTekoci[2].AddArrow(drawLine, pen, 50, 2, tmp, CPointFloat(midd.x + 50, midd.y + 50), true, font);
			else if (drawType == 1)
			mainScreenDrawTekoci[2].AddArrow(drawLine, pen, 50, 3, tmp, CPointFloat(midd.x + 50, midd.y + 50), true, font);
		}



		return distance;
}

float imageProcessing::DynamicFunctionLineDeviation(int nrCam, int imageIndex, int draw, QRect rect, int recDirection, int rectBlackToWhite, int threshold, int type, int filter, int color, int xOffset, int yOffset, int testMode, int currParam)
{
	CRectRotated firstRect = rect;
	vector<CPointFloat> tmpPoints;
	CPointFloat point1;
	float x, y;
	int nrPoints;

	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	//mainScreenDrawFinal[3].Reset();
	//mainScreenDrawFinal[3] = mainScreenDrawTekoci[3];
	QRect area;
	area.setCoords((10), 10, width -10, height - 10);

	//offset;
	CPointFloat topSimetrala = measureObject->topOfSimetralaPoint;
	CRectRotated tmpRect1;
	if (testMode == 0)
	{
		tmpRect1.setX(topSimetrala.x - xOffset);
		tmpRect1.setY(topSimetrala.y - yOffset);
		tmpRect1.setWidth(rect.width());
		tmpRect1.setHeight(rect.height());

	}
	else
	{
		tmpRect1 = rect;
	}


	if (recDirection < 2)//
		nrPoints = rect.width() / 2;
	else
		nrPoints = rect.height() / 2;


	cam[nrCam]->image[imageIndex].GetFirstEdgePointsVectorRotated(tmpRect1, recDirection, nrPoints, 1, rectBlackToWhite, threshold, tmpPoints, 1, false);


	//potegne premico cez tocke.

	CLine line;
	line.SetLine(tmpPoints);
	float dist = 0;
	vector < float > odstopanja;
	vector < int  > index;
	int currNum = 0;

	vector< float > odstopanjaFilter;

	for (int i = 0; i < tmpPoints.size(); i++)
	{
		odstopanja.push_back(line.GetDistance(tmpPoints[i]));
	}
	//filter 

	for (int i = 0; i < odstopanja.size(); i++)
	{
		dist += odstopanja[i];
		currNum++;
		if ((currNum == filter) || (i == odstopanja.size() - 1))
		{
			if (currNum > 0)
			{
				odstopanjaFilter.push_back(dist / currNum);
				index.push_back(i);
			}
			currNum = 0;
			dist = 0;
		}

	}
	dist = 0;
	float result;
	float max = 0;
	int selectedIndex;
	if (type == 0)//avrage 
	{
		for (int i = 0; i < odstopanjaFilter.size(); i++)
		{
			dist += odstopanjaFilter[i];
		}
		if (odstopanjaFilter.size() > 0)
			result = dist / odstopanjaFilter.size();
		else
			result = -1;
	}
	else if (type == 1) //POISCE MAX ODSTOPANJE GLEDE NA FILTER
	{
		for (int i = 0; i < odstopanjaFilter.size(); i++)
		{
			if (odstopanjaFilter[i] > max)
			{
				max = odstopanjaFilter[i];
				selectedIndex = index[i];
			}
		}
		result = max;
	}





	QString text = QString("%1").number(color, 16);

	if (text.size() == 0)
		text.prepend("#000000");
	if (text.size() == 1)
		text.prepend("#00000");
	if (text.size() == 2)
		text.prepend("#0000");
	if (text.size() == 3)
		text.prepend("#000");
	if (text.size() == 4)
		text.prepend("#00");
	if (text.size() == 5)
		text.prepend("#0");
	if (text.size() == 6)
		text.prepend("#");

	QColor colorR(text);
	QPen drawPen;
	drawPen.setWidth(3);
	drawPen.setColor(colorR);

	int maxX = 0; int maxY = 0;
	int counter = 0;
	for (int i = selectedIndex; i < filter+ selectedIndex; i++)
	{
		if (i < odstopanja.size())
		{
			maxX += tmpPoints[i].x;
			maxY += tmpPoints[i].y;
			counter++;
		}
	}
	CPointFloat centerOdst;
	if (counter > 0)
	{
		centerOdst.SetPointFloat(maxX / counter, maxY / counter);
	}

	if (draw)
	{

		scene->addRect(tmpRect1, drawPen, QBrush(Qt::NoBrush));


			scene->addItem(line.DrawLine(area,drawPen));

		for (int i = 0; i < tmpPoints.size(); i++)
		{
			scene->addItem(tmpPoints[i].DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));

		}
		
		scene->addItem(point1.DrawDot(drawPen, QBrush(Qt::green), 10));
	
		if (type == 1)
		{
			for (int i = selectedIndex; i < filter+selectedIndex; i++)
			{
				if (i < odstopanja.size())
				{
					scene->addItem(tmpPoints[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 8));
				}
			}
		}

	}
	QPen pen(colorR);
	pen.setWidth(15);
	QFont font("MS Shell Dlg 2");
	font.setPointSize(48);

	//testMODE 
	if (testMode == 0)//doda izrise za glavni ekran. Pri trenutnem projektu  hrast za postajo 2
	{
		QString tmp;
		if (currParam >= 0)
			tmp = types[2][currentType]->name[currParam];
		else
			tmp = "";
			mainScreenDrawTekoci[2].AddLine(area,line,pen);
		if(type == 1)
			mainScreenDrawTekoci[2].AddPoint(centerOdst, 30, pen, QBrush(colorR), "", CPointFloat(0, 0), false, font);
	}



	return result;
}

int imageProcessing::ProcessingCamera0(int nrCam, int imageIndex, int draw)
{
	processingTimer[0]->SetStart();
	mainScreenDrawTekoci[0].Reset();

	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\PredObdelavo\\Orientacija\\"), QString("%1.bmp").arg(nrSavedImages[0]));
	nrSavedImages[0]++;
	if (nrSavedImages[0] > 1)
		nrSavedImages[0] = 0;

	FindThreadAndOrientation(nrCam, imageIndex, draw);
	mainScreenDrawFinal[0].Reset();
	mainScreenDrawFinal[0] = mainScreenDrawTekoci[0];
	//drawImageMain[0] = cam[nrCam]->image[imageIndex].buffer[0].clone();


	cam[nrCam]->imageReady[imageIndex] = 3;
	emit measurePieceSignal(nrCam, imageIndex);
	

	processingTimer[0]->SetStop();
	processingTimer[0]->ElapsedTime();
	return 0;
}
int imageProcessing::ProcessingCamera1(int nrCam, int imageIndex, int draw)
{
	processingTimer[1]->SetStart();
	//mainScreenDrawTekoci[1].Reset();
	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\PredObdelavo\\ZgorajDim\\"), QString("%1.bmp").arg(nrSavedImages[1]));
	nrSavedImages[1]++;
	if (nrSavedImages[1] > 15)
		nrSavedImages[1] = 0;
		int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
		int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;

		//drawImageMain[1] = cam[nrCam]->image[imageIndex].buffer[0].clone();
		//mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);
		//drawImageMain[1] = cam[nrCam]->image[imageIndex].buffer[0].clone();

		//mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);
	QRect area;
	area.setCoords((width / 2 - 5), 50, width / 2 + 5, height - 50);




	if (IsPiecePresent(nrCam, imageIndex, area, 200, 2, draw) == true)
	{
		measureObject->pieceOnStation[nrCam] = 1;
		FindInnerAndOuterRadij(nrCam, imageIndex, draw);

	}
	else
	{
		measureObject->ResetMeasurand(1, 0);
	}


	//mainScreenDrawFinal[1].Reset(); 
	//mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];

	cam[nrCam]->imageReady[imageIndex] = 3;
	emit measurePieceSignal(nrCam, imageIndex);

	processingTimer[1]->SetStop();
	processingTimer[1]->ElapsedTime();

	return 0;
}

int imageProcessing::ProcessingCamera2(int nrCam, int imageIndex, int draw)
{
	processingTimer[2]->SetStart();
	mainScreenDrawTekoci[2].Reset();
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;

	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\PredObdelavo\\StranDim\\"), QString("%1.bmp").arg(nrSavedImages[2]));
	nrSavedImages[2]++;
	if (nrSavedImages[2] > 15)
		nrSavedImages[2] = 0;
	//drawImageMain[1] = cam[nrCam]->image[imageIndex].buffer[0].clone();
	//mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);



	drawImageMain[2] = cam[nrCam]->image[imageIndex].buffer[0].clone();
	mainScreenDrawTekoci[2].AddImage(drawImageMain[2]);

	QRect area;
	area.setCoords((width / 2 - 5), 50, width / 2 + 5, height - 50);


if (IsPiecePresent(nrCam, imageIndex, area, 200, 2, draw) == true)
	{
		measureObject->pieceOnStation[nrCam] = 1;
		FindTopAndBottom(nrCam, imageIndex, draw);//tukaj po novem poiscem samo simetralo ostale funkcije so dinamicne
		//FindLeftAndRight(nrCam, imageIndex, draw);
		int curFun = 0;
		QRect rect1, rect2;
		float result;
		for (int i = 0; i < types[2][currentType]->parameterCounter; i++)
		{
			curFun = types[2][currentType]->dynamicParameters[i][0];

			if (curFun == 10)
			{
				rect1.setRect(types[2][currentType]->dynamicParameters[i][1], types[2][currentType]->dynamicParameters[i][2], types[2][currentType]->dynamicParameters[i][3], types[2][currentType]->dynamicParameters[i][4]);
				rect2.setRect(types[2][currentType]->dynamicParameters[i][5], types[2][currentType]->dynamicParameters[i][6], types[2][currentType]->dynamicParameters[i][7], types[2][currentType]->dynamicParameters[i][8]);
				result = DynamicFunctionMeasureDistance(nrCam, imageIndex, draw, rect1, rect2, types[2][currentType]->dynamicParameters[i][9], types[2][currentType]->dynamicParameters[i][10], types[2][currentType]->dynamicParameters[i][11],
					types[2][currentType]->dynamicParameters[i][12], types[2][currentType]->dynamicParameters[i][13], types[2][currentType]->dynamicParameters[i][14], types[2][currentType]->dynamicParameters[i][15],
					types[2][currentType]->dynamicParameters[i][16], types[2][currentType]->dynamicParameters[i][17], types[2][currentType]->dynamicParameters[i][18], types[2][currentType]->dynamicParameters[i][19], types[2][currentType]->dynamicParameters[i][20],
					types[2][currentType]->dynamicParameters[i][21], types[2][currentType]->dynamicParameters[i][22], types[2][currentType]->dynamicParameters[i][23],0,i);
				measureObject->measuredValue[2].push_back(result);
			}
			else if (curFun == 11)
			{
				rect1.setRect(types[2][currentType]->dynamicParameters[i][1], types[2][currentType]->dynamicParameters[i][2], types[2][currentType]->dynamicParameters[i][3], types[2][currentType]->dynamicParameters[i][4]);

				result = DynamicFunctionLineDeviation(nrCam, imageIndex, draw, rect1, types[2][currentType]->dynamicParameters[i][5], types[2][currentType]->dynamicParameters[i][6], types[2][currentType]->dynamicParameters[i][7],
					types[2][currentType]->dynamicParameters[i][8], types[2][currentType]->dynamicParameters[i][9], types[2][currentType]->dynamicParameters[i][10], types[2][currentType]->dynamicParameters[i][11],
					types[2][currentType]->dynamicParameters[i][12], 0, i);
				measureObject->measuredValue[2].push_back(result);
			}



		}

	}
	else
	{
		measureObject->pieceOnStation[nrCam] = 0;
		measureObject->ResetMeasurand(2, 0);
	}

	mainScreenDrawFinal[2].Reset();
	mainScreenDrawFinal[2] = mainScreenDrawTekoci[2];

	cam[nrCam]->imageReady[imageIndex] = 3;
	emit measurePieceSignal(nrCam, imageIndex);

	processingTimer[2]->SetStop();
	processingTimer[2]->ElapsedTime();

	return 0;
}




int imageProcessing::ProcessingCamera3(int nrCam, int imageIndex, int draw)//obdelava iz vrha povrsina.
{
	processingTimer[3]->SetStart();
	//mainScreenDrawTekoci[3].Reset();
	mainScreenDrawTekoci[1].Reset();



	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\PredObdelavo\\ZgorajPov\\"), QString("%1.bmp").arg(nrSavedImages[3]));
	nrSavedImages[3]++;
	if (nrSavedImages[3] > 15)
		nrSavedImages[3] = 0;
	//FindInnerAndOuterRadij(nrCam, imageIndex, draw);
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	//mainScreenDrawFinal[3].Reset();
	//mainScreenDrawFinal[3] = mainScreenDrawTekoci[3];
	QRect area;
	area.setCoords((width / 2 - 5), 50, width / 2 + 5, height - 50);
	if (IsPiecePresent(nrCam, imageIndex, area, 200, 2, draw) == true)
	{


		cam[nrCam]->imageReady[imageIndex] = 3;
		FindTopSurfaceAndDim(nrCam, imageIndex, draw);
		if ((measureObject->krogPovrsinaNotranji.radius > 0) && (measureObject->krogPovrsinaZunanji.radius > 0))
		{
			if (types[1][currentType]->typeName.compare("11.22.424.771.009") == 0)
				findZareze(nrCam, imageIndex, draw); // iskanju zarez se enkrat izmerim deviacijo zunanjega roba, ker izpustim kote, pri katerih je porezkano od zarez
			InspectSurface(nrCam, imageIndex, draw);
			drawImageMain[1] = cam[1]->image[4].buffer[0].clone();
			mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);


		}
		else
		{
			drawImageMain[1] = cam[1]->image[1].buffer[0].clone();
			mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);
		}

	}
	else
	{
		measureObject->ResetMeasurand(1, 1);
		drawImageMain[1] = cam[nrCam]->image[0].buffer[0].clone();
		mainScreenDrawTekoci[1].AddImage(drawImageMain[1]);
	}
	mainScreenDrawFinal[1].Reset();
	mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];
	emit measurePieceSignal(nrCam, imageIndex);
	processingTimer[3]->SetStop();
	processingTimer[3]->ElapsedTime();

	return 0;
}

int imageProcessing::ProcessingCamera4(int id, int imageIndex, int draw) //nova funkcija za poteg karakteristike  modula X os 
{
	

	return 0;
}

int imageProcessing::CheckDefects(int nrCam, int piece,int draw)
{
	return 0;
}









