﻿#pragma once

#include <QWidget>
#include "ui_imageProcessing.h"
#include "Camera.h"
#include "CCustomGraphicsItem.h"
#include "Timer.h"
#include "Measurand.h"
#include "qtpropertymanager.h"
#include "qtvariantproperty.h"
#include "qttreepropertybrowser.h"
#include "Types.h"
#include "DelayedDraw.h"

using namespace std;
using namespace cv;


class imageProcessing : public QWidget
{
	Q_OBJECT

public:
	imageProcessing(QWidget *parent = Q_NULLPTR);
	void closeEvent(QCloseEvent * event);
	//imageProcessing();
	~imageProcessing();


	static std::vector<CCamera*>									cam;
	static std::vector<CMemoryBuffer*>								images;
	static std::vector<Timer*>										processingTimer;
	static Measurand*												measureObject;
	static std::vector<Types*>										types[NR_STATIONS];
	static CMemoryBuffer*											currImage;
	static Mat														DrawImage;
	

	float zoomFactor;
	float zoomMultiplier;
	int displayWindow;
	int displayImage;
	int prevDisplayWindow;
	int prevDisplayImage;
	int rowNum;
	int colNum;
	int currentStation;
	bool isSceneAdded;
	int loginRights;

	QRubberBand*  rubberBand;
	QPoint origin;
	QPoint pos;
	QPoint pos2;
	QSizeGrip* gripTopLeft;
	QSizeGrip* gripBottomRight;
	int start;
	int createRubber;
	int creatingRubber;
	int changeRubberBand;
	int rubberGripperSelected;
	vector <QRect> rubberRect;
	vector< QGraphicsRectItem*> RectItem;
	QRect rubberRectOld[2];
	vector <int> rubberRectFunction;
	int selectedRubberRect;
	int selectedFunction;
	int drawRubberFunction;
	int tmpDynamicParameter[30];
	int tmpDynamicParameterold[30];
	int selectedDynamicParameter;
	QTimer* timer; //zacasno za prepoznavo , ce se je propertyBrowser dinamicnih funkcij  spremenil
	QRect dynamicFunctionRect1;
	QRect dynamicFunctionRect2;


	QGraphicsPixmapItem*	pixmapVideo;

	void ConnectImages(std::vector<CCamera*> inputCam, std::vector <CMemoryBuffer*> inputImages);
	void ConnectMeasurand(Measurand* objects);
	void ConnectTypes(int station,std::vector<Types*> type);
	void ConnectImages(std::vector<CCamera*> inputCam);
	void ShowDialog(int rights);
	void ClearDialog();
	void ReplaceCurrentBuffer();

	QGraphicsScene*	scene;
	QGraphicsScene*	sceneForMainWindow;

	//qpropertyBrowser za lastnosti tipov in funkcij
	QtVariantPropertyManager *variantManager; //dodajamo QtProperty in QtVariantProperty
	QtVariantEditorFactory *variantFactory; //dodat v editor
	QtTreePropertyBrowser *variantEditor; //widget za prikaz GPropertyBrowser
	QtProperty *topItem;				//top item
	vector<QtVariantProperty*> item;
	int currentFunction;
	int currentType;
	void PopulatePropertyBrowser(int function);			//prepise tabele v qpropertyBrowser
	void PopulatePropertyBrowserDynamicFunctions(int function);			//prepise tabele s funkcijami za univerzalne obdelave
	void UpdatePropertyBrwserDynamicFunctions(int funcion);
	void ReadPropertyBrowserDynamicFunctions(int function);
	void SaveFunctionParameters(int type, int currentFunction);//shrani v datoteke parametre funkcije kadar se spreminja oz dodaja parametri
	void SaveFunctionParametersOnNewType(int station, int type, int currentFunction);//shrani v datoteke parametre funkcije kadar se spreminja oz dodaja parametri
	void DeleteTypeProperty(QString type);
	void LoadTypesProperty();

	CDelayedDraw mainScreenDrawTekoci[4];
	CDelayedDraw mainScreenDrawFinal[4];

	void NastaviJezikImgProc();

protected:
	bool eventFilter(QObject *obj, QEvent *event);
	void keyPressEvent(QKeyEvent * event);
	//void mousePressEvent(QMouseEvent *event);
	//void mouseReleaseEvent(QMouseEvent *event);
	void propertyChanged(QtProperty *property);


private:
	Ui::imageProcessing ui;
	
	/*QGraphicsEllipseItem* ellipses;
	QGraphicsRectItem* rectangles;
	QGraphicsLineItem* lines;
	QGraphicsPolygonItem* polygons;
	QGraphicsPixmapItem* pixels;
	QGraphicsPathItem* paths;
	CCustomGraphicsItem* myItem;

	vector<QGridLayout*> gridLayout;
	vector<QLabel*> paramLabels;
	vector<QLineEdit*> paramLineEdit;
	vector<QToolButton*> removeButton;
	QSignalMapper* signalMapper;*/

	bool slovensko;


public slots:
	void OnPressedImageUp();
	void OnPressedImageDown();
	void OnPressedCamUp();
	void OnPressedCamDown();
	int ConvertImageForDisplay(int imageNumber);
	void ShowImages();
	void ResizeDisplayRect();
	void OnProcessCam0(); //za klicanje obdelave iz gumba
	void OnProcessCam1();
	void OnProcessCam2();
	void OnProcessCam3();
	void OnProcessCam4();
	void OnDoneEditingLineInsertImageIndex();
	void onPressedAddParameter();
	void onPressedRemoveParameters();
	void onPressedUpdateParameters();
	void onPressedSurfaceInspection();
	void OnPressedCreateRubber();
	void OnPressedResizeRubber();
	void OnPressedSelectRubber();

	//testFunctions
	void OnPressedTestIntensityFunction();


	//funkcije za obdelave
	void OnViewTimer();
	void AddnewHeightMeasurement(void);
	void LineDeviationMeasurement(void);
	int SaveSelectedDynamicFunctionAsMeasurment();
	int EditSelectedDynamicFunctionParameter();

	//horOrVer -> isce v horizontalni ali v vertikalni smeri 0-> horizontalno
	 //rec1Dir , rec2Dir direction: 0: [0]->[1], 1: [1]->[0], 2: [0]->[3], 3: [3]->[0]   (številke v oglatih oklepajih so indeksi rectRot.polygonPoints)
	//Ni potrebno predhodno pognati FindEdges, ker išèemo robove z GetEdgePointOnSegment2D, ki uporablja samo intenzitete pikslov
	//nLines je število linij, na katerih iščemo robove
	float DynamicFunctionMeasureDistance(int cam, int imageindex, int draw, QRect rect1, QRect rect2, int horOrVer, int rec1Dir,int rect1BlackToWhite, int rec2Dir, int rect2BlackToWhite, int type, int filter, int threshold,int drawType,int drawLocation,int color, int xOffset1, int yOffset1, int xoffset2, int yoffset2,int testMode, int currParam);
	float DynamicFunctionLineDeviation(int cam, int imageIndex, int draw, QRect rect, int recDirection, int rectBlackToWhite, int threshold, int type, int filter, int color, int xOffset, int yOffset, int testMode, int currParam);

	int ProcessingCamera0(int id, int imageIndex,int draw); //obdelava orientacije
	int ProcessingCamera1(int id, int imageIndex, int draw);// obdelava kamera 1
	int ProcessingCamera2(int id, int imageIndex, int draw);// obdelava kamera 2
	int ProcessingCamera3(int id, int imageIndex, int draw);// obdelava kamera karakteristika
	int ProcessingCamera4(int id, int imageIndex, int draw);// obdelava kamera karakteristika
	int CheckDefects(int indexImage, int nrPiece, int draw);

	//Obdelava površine
	//Funkcija za izračun orientacije objekta/oblike na binarni sliki. Vrne kot v radianih!
		int InspectSurface(int imageIndex, int nrPiece, int draw);

		int FindThreadAndOrientation(int id, int imageIndex, int draw); //uporabi za iskanje navoja
		int FindInnerAndOuterRadij(int id, int imageIndex, int draw); //uporabi za iskanje tezisc na laserski crti
		bool IsPiecePresent(int id, int imageIndex, QRect area, int thresHoldIntensity,int nrPoints, int draw); // preveri ce je kos na sliki poisce prehode in povprecno intenziteno 
		int  FindTopAndBottom(int id, int imageIndex, int draw); // za vse tipe enaka funkcija, kjer najdem opore in zgornji del kosa ter izracunam visino
		int FindLeftAndRight(int id, int imageIndex, int draw);
		int FindTopSurfaceAndDim(int id, int imageIndex, int draw);
		int findZareze(int id, int imageIndex, int draw);//samo za poseben tip, kjer je potrebno poiskat zarezev v matici

		static bool OmitAngle(float alphaRad);
		static bool OmitAngle2(float alphaRad);
		static bool OmitAngleZareza(float alphaRad);
	void ClearDraw();
	void ClearFunctionList();
	void SetCurrentBuffer(int dispWindow, int dispImage);
	void ZoomOut();
	void ZoomIn();
	void ZoomReset();
	void OnLoadImage();
	void OnLoadMultipleImage();
	void OnSaveImage();

signals:
	void imagesReady();
	void measurePieceSignal(int nrCam, int imageIndex);//signal za v razred MBVISION OnFrameReady


//testno risanje 
	public:
	int drawOffsetX[NR_STATIONS];
	int drawOffsetY[NR_STATIONS];
	vector<QRect>				drawRect[NR_STATIONS];
	vector<Qt::GlobalColor>		drawRectColor[NR_STATIONS];
	vector<CPointFloat>			drawPoint[NR_STATIONS];
	vector<Qt::GlobalColor>		drawPointColor[NR_STATIONS];
	vector<Qt::GlobalColor>		drawPointSize[NR_STATIONS];
	Mat							drawImageMain[NR_STATIONS];



};
