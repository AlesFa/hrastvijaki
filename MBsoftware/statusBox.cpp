#include "stdafx.h"
#include "statusBox.h"

StatusBox::StatusBox(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	for (int i = 0; i < 100; i++)
	{
		ui.statusList->addItem("");
	}
}

StatusBox::~StatusBox()
{
}

void StatusBox::ShowDialog()
{

	Qt::WindowFlags flags = windowFlags();
	this->setWindowFlags((flags | Qt::WindowStaysOnTopHint));
	show();
	activateWindow();
	
}

void StatusBox::AddItem(int index, QString text)
{
	ui.statusList->item(index)->setText(text);
}
